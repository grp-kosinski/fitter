/*
 * Stamp.h
 *
 *  Created on: Mar 3, 2021
 *      Author: kkarius
 */

#ifndef STAMP_H_
#define STAMP_H_
#include<Density.h>
#include<cuda_util_include/cutil_math.h>
#include<util.h>
#include<map>

static const float GAUSSIAN_KERNEL_RESOLUTION_CONVERSION_FACTOR = 0.42466090014400953;
struct Stamp_Register {
	TDensity * h_stamp = nullptr;
	TDensity * d_stamp = nullptr;
	int * h_stamp_offsets = nullptr;
	int * d_stamp_offsets = nullptr;
	uint * h_stamp_pixel_dim = nullptr;
	uint * d_stamp_pixel_dim = nullptr;
	size_t * h_stamp_vol = nullptr;
	float * h_pixel_size = nullptr;
};

class Stamp {
public:
	Stamp(){};
	virtual ~Stamp(){};
	void createInstance(const float & resolution, const float & pixel_size, const int &gpu_index);
	void freeInstance(const int& gpu_index);
	void freeAllInstances(void);
	void populateOffsets(Density *const& density, int gpu_index);
private:
	friend class DensityGenerator;
	std::map<int,Stamp_Register> _instance_registry;
};

#endif /* STAMP_H_ */
