/*
 * AlignmentProtocol.cu
 *
 *  Created on: Jan 14, 2021
 *      Author: kkarius
 */

#include<AlignmentProtocol.h>
#include<dirent.h>
#include<stdio.h>
#include<string.h>
#include<util.h>
#include<chrono>
#include<iostream>
#include<map>
#include<fstream>
#include<assert.h>

#ifndef CLUSTER
#include<IMP/rmf/atom_io.h>
#include<IMP/rmf.h>
#include<IMP/atom/Representation.h>
#include<RMF/FileHandle.h>
#include<RMF/FileConstHandle.h>
#include<RMF/infrastructure_macros.h>
#include<RMF/utility.h>
#include<nvml.h>
#endif

#include<cuda.h>
#include<cuda_runtime.h>
#include<cuda_runtime_api.h>
#include<cublas_v2.h>
#include<cusolverDn.h>

#include <cmath>

#pragma pack(1)
struct M_alpha {
	double m00;
	double m01;
	double m02;
	double m03;
	double m10;
	double m11;
	double m12;
	double m13;
	double m20;
	double m21;
	double m22;
	double m23;
	double m30;
	double m31;
	double m32;
	double m33;
	__device__
	M_alpha& operator+=(const M_alpha& rhs) {
		//matrix is supposed to be symmetric, other additions unneccessary
		m00 += rhs.m00;
		m01 += rhs.m01;
		m02 += rhs.m02;
		m03 += rhs.m03;
		m11 += rhs.m11;
		m12 += rhs.m12;
		m13 += rhs.m13;
		m22 += rhs.m22;
		m23 += rhs.m23;
		m33 += rhs.m33;
		return *this;}
	__device__
	volatile M_alpha& operator+=(volatile M_alpha& rhs) volatile {
		//matrix is supposed to be symmetric, other additions unneccessary
		m00 += rhs.m00;
		m01 += rhs.m01;
		m02 += rhs.m02;
		m03 += rhs.m03;
		m11 += rhs.m11;
		m12 += rhs.m12;
		m13 += rhs.m13;
		m22 += rhs.m22;
		m23 += rhs.m23;
		m33 += rhs.m33;
		return *this;}
};

unsigned long long getFreeGlobal(int gpu_index){
	unsigned long long v_100_mem = 34359738368ull;
#ifdef CLUSTER
	return v_100_mem;
#else
	nvmlInit_v2();
	nvmlDevice_t device_handle;
	nvmlMemory_t device_memory_info;
	nvmlDeviceGetHandleByIndex_v2(0,&device_handle);
	nvmlDeviceGetMemoryInfo(device_handle, &device_memory_info);
	return device_memory_info.free;
#endif
}

__host__ __device__
void rotate(float4 *  position_0, float4 * position_r, float4 * rotation){
	// rot_quat = [a,b,c,d]
	// pos_quat = [0,x,y,z] - in memory as [x,y,z,0]
	//  (a^2*x + 2*a*c*z - 2*a*d*y + b^2*x + 2*b*c*y + 2*b*d*z - c^2*x - d^2*x)*i
	//+ (a^2*y - 2*a*b*z + 2*a*d*x - b^2*y + 2*b*c*x + 2*c*d*z + c^2*y - d^2*y)*j
	//+ (a^2*z + 2*a*b*y - 2*a*c*x - b^2*z + 2*b*d*x - c^2*z + 2*c*d*y + d^2*z)*k

	//x (a^2*x + 2*a*c*z - 2*a*d*y + b^2*x + 2*b*c*y + 2*b*d*z - c^2*x - d^2*x)
	position_r->x = rotation->x*rotation->x*position_0->x
	    + 2*rotation->x*rotation->z*position_0->z
	    - 2*rotation->x*rotation->w*position_0->y
	    + rotation->y*rotation->y*position_0->x
            + 2*rotation->y*rotation->z*position_0->y
  	    + 2*rotation->y*rotation->w*position_0->z
	    - rotation->z*rotation->z*position_0->x
	    - rotation->w*rotation->w*position_0->x;
	//y (a^2*y - 2*a*b*z + 2*a*d*x - b^2*y + 2*b*c*x + 2*c*d*z + c^2*y - d^2*y)
	position_r->y = rotation->x*rotation->x*position_0->y
	    - 2*rotation->x*rotation->y*position_0->z
	    + 2*rotation->x*rotation->w*position_0->x
	    - rotation->y*rotation->y*position_0->y
	    + 2*rotation->y*rotation->z*position_0->x
	    + 2*rotation->z*rotation->w*position_0->z
            + rotation->z*rotation->z*position_0->y
	    - rotation->w*rotation->w*position_0->y;
	//z (a^2*z + 2*a*b*y - 2*a*c*x - b^2*z + 2*b*d*x + 2*c*d*y - c^2*z + d^2*z)
	position_r->z = rotation->x*rotation->x*position_0->z
	    + 2*rotation->x*rotation->y*position_0->y
	    - 2*rotation->x*rotation->z*position_0->x
	    -   rotation->y*rotation->y*position_0->z
	    + 2*rotation->y*rotation->w*position_0->x
	    + 2*rotation->z*rotation->w*position_0->y
	    -   rotation->z*rotation->z*position_0->z
	    +   rotation->w*rotation->w*position_0->z;
}

template <unsigned int blockSize>
__global__
void calc_rmsd_single(float4 * d_coords_4, int num_particles, int num_frames, float * d_rmsds) {
	extern __shared__ float buffer3[];
	int f = blockIdx.x+1;
	int p;
	float4 * d_frame;
	float4 * d_p1;
	float4 * d_p2;
	int tid = threadIdx.x;

	while (f < num_frames){
		d_frame = d_coords_4 + f*num_particles;
		buffer3[threadIdx.x] = 0;
		p=threadIdx.x;
		while (p < num_particles){
				d_p1 = d_coords_4 + p;
				d_p2 = d_frame + p;
				buffer3[threadIdx.x] += (d_p1->x - d_p2->x)*(d_p1->x - d_p2->x)+(d_p1->y - d_p2->y)*(d_p1->y - d_p2->y)+(d_p1->z - d_p2->z)*(d_p1->z - d_p2->z);
				p+=blockDim.x;
			}
			__syncthreads();
			if (blockSize >= 512){if (tid < 256) { buffer3[tid] += buffer3[tid + 256];}__syncthreads();}
			if (blockSize >= 256){if (tid < 128) { buffer3[tid] += buffer3[tid + 128]; } __syncthreads();}
			if (blockSize >= 128){if (tid <  64) { buffer3[tid] += buffer3[tid + 64]; } __syncthreads();}
			warp_reduce<float,blockSize>(buffer3,tid);
			__syncthreads();

			if (threadIdx.x ==0)
				d_rmsds[f-1] = sqrtf(buffer3[0]);
		f += gridDim.x;
	}
}

template <unsigned int blockSize>
__global__
void calc_rmsd(float4 * d_coords_4, int num_particles, int num_frames, float * d_rmsds) {
	extern __shared__ float buffer3[];
	int f1 = blockIdx.x;
	int tid = threadIdx.x;
	int f2,p;
	float4 * d_frame1;
	float4 * d_frame2;
	float4 * d_p1;
	float4 * d_p2;

	while (f1 < num_frames){
		f2 = f1+1;
		d_frame1 = d_coords_4 + f1*num_particles;
		while (f2 < num_frames){
			d_frame2 = d_coords_4 + f2*num_particles;
			if (blockSize >= 512){if (tid < 256) {buffer3[tid + 256] = 0;}__syncthreads();}
			if (blockSize >= 256){if (tid < 128) {buffer3[tid + 128] = 0;} __syncthreads();}
			if (blockSize >= 128){if (tid <  64) {buffer3[tid + 64] = 0;} __syncthreads();}
			if (blockSize >= 64){if (tid <  32) {buffer3[tid + 32] = 0;} __syncthreads();}
			if (blockSize >= 32){if (tid <  16) {buffer3[tid + 16] = 0;} __syncthreads();}
			buffer3[threadIdx.x] = 0;
			p=threadIdx.x;
			while (p < num_particles){
				d_p1 = d_frame1 + p;
				d_p2 = d_frame2 + p;
				buffer3[threadIdx.x] += (d_p1->x - d_p2->x)*(d_p1->x - d_p2->x)+(d_p1->y - d_p2->y)*(d_p1->y - d_p2->y)+(d_p1->z - d_p2->z)*(d_p1->z - d_p2->z);
				p+=blockDim.x;
			}
			__syncthreads();
			if (blockSize >= 512){ if (tid < 256) { buffer3[tid] += buffer3[tid + 256];}__syncthreads();}
			if (blockSize >= 256){	if (tid < 128) { buffer3[tid] += buffer3[tid + 128]; } __syncthreads();}
			if (blockSize >= 128){if (tid <  64) { buffer3[tid] += buffer3[tid + 64]; } __syncthreads();}
			warp_reduce<float,blockSize>(buffer3,tid);
			__syncthreads();

			if (threadIdx.x ==0)
				d_rmsds[f1*num_frames-(f1*(f1+1))/2+f2-1] = sqrtf(buffer3[0]);
			f2 +=1;
		}
		f1 += gridDim.x;
	}
}

template <bool OFFDEVICE,unsigned int blockSize>
__global__
void center_and_m_alpha(float * d_particles, M_alpha * d_m_alphas, float3 * d_coms, int num_frames, int num_particles){
	//holds intermediates for center of mass and M_alpha calculations --> size = thread_number*(|M_alpha| + 3*float)
	extern __shared__ float3 buffer[];
	float3 * coms = &buffer[0];
	M_alpha * malphas = (M_alpha *) &buffer[0];
	int tid = threadIdx.x;
	//frames -- first frame is zeroframe
	int f = blockIdx.x+1;
	//particles
	int p;
	//pointer for current frame
	float * d_frame;
	float * d_frame0 = &d_particles[0];
	//pointer for current particles, plus d for efficiency
	float4 * p0;
	float4 * p1;
	float d;
	//looping over current frames
	while (f < num_frames){
		d_frame = &d_particles[4*f*num_particles];
		//init coms and malphas to zero
		coms[threadIdx.x] = {0};
		p = threadIdx.x;
		//looping over particles, calculating center of mass
		while (p < num_particles){
			coms[threadIdx.x].x += d_frame[4*p];
			coms[threadIdx.x].y += d_frame[4*p + 1];
			coms[threadIdx.x].z += d_frame[4*p + 2];
			p += blockDim.x;
		}
		__syncthreads();
		if (blockSize >= 512){ if (tid < 256) { coms[tid] += coms[tid + 256];}__syncthreads();}
		if (blockSize >= 256){	if (tid < 128) { coms[tid] += coms[tid + 128]; } __syncthreads();}
		if (blockSize >= 128){if (tid <  64) { coms[tid] += coms[tid + 64]; } __syncthreads();}
		if (threadIdx.x < 32)warp_reduce<float3,blockSize>(coms,tid);
		__syncthreads();
		coms[0] /= __int2float_rn(num_particles);
		if (OFFDEVICE)
			d_coms[f-1] = coms[0];
		//transform coordinates of frame f to coms
		p = threadIdx.x;
		//looping over particles, calculating center of mass
		while (p < num_particles){
			d_frame[4*p] -= coms[0].x;
			d_frame[4*p + 1] -= coms[0].y;
			d_frame[4*p + 2] -= coms[0].z;
			p += blockDim.x;
		}
		//init __shared__ for M_alpha calculation
		malphas[threadIdx.x] = {0};
		p = threadIdx.x;
		while (p < num_particles){
			p0 = (float4 *) &d_frame0[4*p];
			p1 = (float4 *) &d_frame[4*p];
			d = p1->x * p1->x + p1->y * p1->y + p1->z * p1->z + p0->x * p0->x + p0->y * p0->y
					+ p0->z * p0->z;
			malphas[threadIdx.x].m00 += d - 2 * p1->x * p0->x - 2 * p1->y * p0->y - 2 * p1->z * p0->z;
			malphas[threadIdx.x].m01 += 2 * (p1->y * p0->z - p1->z * p0->y);
			malphas[threadIdx.x].m02 += 2 * (-p1->x * p0->z + p1->z * p0->x);
			malphas[threadIdx.x].m03 += 2 * (p1->x * p0->y - p1->y * p0->x);
			malphas[threadIdx.x].m11 += d - 2 * p1->x * p0->x + 2 * p1->y * p0->y + 2 * p1->z * p0->z;
			malphas[threadIdx.x].m12 += -2 * (p1->x * p0->y + p1->y * p0->x);
			malphas[threadIdx.x].m13 += -2 * (p1->x * p0->z + p1->z * p0->x);
			malphas[threadIdx.x].m22 += d + 2 * p1->x * p0->x - 2 * p1->y * p0->y + 2 * p1->z * p0->z;
			malphas[threadIdx.x].m23 += -2 * (p1->y * p0->z + p1->z * p0->y);
			malphas[threadIdx.x].m33 += d + 2 * p1->x * p0->x + 2 * p1->y * p0->y - 2 * p1->z * p0->z;
			p += blockDim.x;
		}
		__syncthreads();
		//reduce malphas
		if (blockSize >= 512){if (tid < 256) { malphas[tid] += malphas[tid + 256];}__syncthreads();}
		if (blockSize >= 256){if (tid < 128) { malphas[tid] += malphas[tid + 128]; } __syncthreads();}
		if (blockSize >= 128){if (tid <  64) { malphas[tid] += malphas[tid + 64]; } __syncthreads();}
		if (threadIdx.x < 32)warp_reduce<M_alpha,blockSize>(malphas,tid);
		__syncthreads();
		//symmetrize, spread out the work a little
		if (threadIdx.x ==0){
			malphas[0].m10 =malphas[0].m01;
			malphas[0].m20 =malphas[0].m02;
			malphas[0].m30 =malphas[0].m03;
		} else if (threadIdx.x ==  1){
			malphas[0].m21 =malphas[0].m12;
			malphas[0].m31 =malphas[0].m13;
			malphas[0].m32 =malphas[0].m23;
		}
		__syncthreads();
		if (threadIdx.x == 0)
			d_m_alphas[f-1] = malphas[0];
		f += gridDim.x;
	}
}

template<bool OFFDEVICE>
void _center_and_m_alpha(float * d_particles, M_alpha * d_m_alphas, float3 * d_coms, int num_frames, int num_particles,
	unsigned int gridDim,unsigned int blockDim){
	uint sharedMem = 2*blockDim*sizeof(M_alpha);
	switch (blockDim)
	{
//	technically impossible
//	case 512:
//		center_and_m_alpha<OFFDEVICE,512><<<gridDim,blockDim,sharedMem>>>(d_particles,d_m_alphas,d_coms,num_frames, num_particles);
//		break;
	case 256:
		CudaSafeCall(cudaFuncSetAttribute((void *)&(center_and_m_alpha<OFFDEVICE,256>),cudaFuncAttributeMaxDynamicSharedMemorySize, 98304));
		center_and_m_alpha<OFFDEVICE,256><<<gridDim,blockDim,sharedMem>>>(d_particles,d_m_alphas,d_coms,num_frames, num_particles);
		CudaCheckError();
		break;
	case 128:
		center_and_m_alpha<OFFDEVICE,128><<<gridDim,blockDim,sharedMem>>>(d_particles,d_m_alphas,d_coms,num_frames, num_particles);
		CudaCheckError();
		break;
	case 64:
		center_and_m_alpha<OFFDEVICE,64><<<gridDim,blockDim,sharedMem>>>(d_particles,d_m_alphas,d_coms,num_frames, num_particles);
		CudaCheckError();
		break;
	case 32:
		center_and_m_alpha<OFFDEVICE,32><<<gridDim,blockDim,sharedMem>>>(d_particles,d_m_alphas,d_coms,num_frames, num_particles);
		CudaCheckError();
		break;
	}
}

__global__
void rotate_frames_into_place(float4 * d_coords, float4 * d_quats,int num_particles, int num_frames){
	// rot_quat = [a,b,c,d] [1,0,0,0] is identity
	// pos_quat = [0,x,y,z] - in memory as [x,y,z,0]
	//	(a^2*x + 2*a*c*z - 2*a*d*y + b^2*x + 2*b*c*y + 2*b*d*z - c^2*x - d^2*x)*i
	//+ (a^2*y - 2*a*b*z + 2*a*d*x - b^2*y + 2*b*c*x + 2*c*d*z + c^2*y - d^2*y)*j
	//+ (a^2*z + 2*a*b*y - 2*a*c*x - b^2*z + 2*b*d*x + 2*c*d*y - c^2*z + d^2*z)*k
	extern __shared__ float4 buffer2[];
	float4 * rot_quat = &buffer2[0];
	float4 orig_coords;// = &buffer2[1];
	//frame-- first frame is zeroframe, second frame is 'optimal' frame
	int f = blockIdx.x + 2;
	//particle
	int p;
	while (f < num_frames){
		//get appropriate rot_quat
		if (threadIdx.x == 0)
			rot_quat[0] = d_quats[f];
		__syncthreads();
		p= threadIdx.x;
		while (p<num_particles){
			orig_coords = d_coords[num_particles + p];
			rotate(&orig_coords,&d_coords[f*num_particles + p],rot_quat);
			p += blockDim.x;
		}
		f += gridDim.x;
	}
}

__global__
void rotate_frames_in_place_align(float4 * d_coords,double * d_U,int num_particles, int num_frames){
	// rot_quat = [a,b,c,d] [1,0,0,0] is identity
	// pos_quat = [0,x,y,z] - in memory as [x,y,z,0]
	//	(a^2*x + 2*a*c*z - 2*a*d*y + b^2*x + 2*b*c*y + 2*b*d*z - c^2*x - d^2*x)*i
	//+ (a^2*y - 2*a*b*z + 2*a*d*x - b^2*y + 2*b*c*x + 2*c*d*z + c^2*y - d^2*y)*j
	//+ (a^2*z + 2*a*b*y - 2*a*c*x - b^2*z + 2*b*d*x + 2*c*d*y - c^2*z + d^2*z)*k
	extern __shared__ float4 buffer2[];
	float4 * rot_quat =  &buffer2[0];
	float4 orig_coords;// = &buffer2[1];
	//frame-- first frame is zeroframe
	int f = blockIdx.x +1;
	//particle
	int p;
	while (f < num_frames){
		//get appropriate rot_quat
		if (threadIdx.x == 0){
			rot_quat->x = (float) d_U[(f-1)* 16 + 12];
			rot_quat->y = -(float) d_U[(f-1)* 16 + 13];
			rot_quat->z = -(float) d_U[(f-1)* 16 + 14];
			rot_quat->w = -(float) d_U[(f-1)* 16 + 15];
//			printf("Frame:%i Worker:%i -- %f %f %f %f\n",f,blockIdx.x,rot_quat->x,rot_quat->y,rot_quat->z,rot_quat->w);
		}
		__syncthreads();
		p= threadIdx.x;
		while (p<num_particles){
			orig_coords = d_coords[f*num_particles + p];
			rotate(&orig_coords,&d_coords[f*num_particles + p],rot_quat);
			p += blockDim.x;
		}
		f += gridDim.x;
	}
}

inline float scaled_rand(int _rand_max, float bound) {
	return (float) (rand() % _rand_max) / (float) _rand_max * bound;
}

inline float4 random_quat_close_to_unity(){
	float4 ret;
	float u = scaled_rand(10000,1.0)/100;
	float v = 0.25 + (scaled_rand(10000,1.0)-0.5)/100;
	float w = scaled_rand(10000,1.0);
	ret.x = sqrtf(1-u)*(sinf(2*M_PI*v));
	ret.y = sqrtf(1-u)*(cosf(2*M_PI*v));
	ret.z = sqrtf(u)*(sinf(2*M_PI*w));
	ret.w = sqrtf(u)*(cosf(2*M_PI*w));
	return ret;
}

inline float4 random_quat(){
	float4 ret;
	float u = scaled_rand(10000,1.0);
	float v = scaled_rand(10000,1.0);
	float w = scaled_rand(10000,1.0);
	ret.x = sqrtf(1-u)*(sinf(2*M_PI*v));
	ret.y = sqrtf(1-u)*(cosf(2*M_PI*v));
	ret.z = sqrtf(u)*(sinf(2*M_PI*w));
	ret.w = sqrtf(u)*(cosf(2*M_PI*w));
	return ret;
}

inline float3 random_trans(){
	return {scaled_rand(10000,50.0)-25,scaled_rand(10000,50.0)-25,scaled_rand(10000,50.0)-25};
}

AlignmentProtocol::AlignmentProtocol(){
	getMemoryInfo(0,global_mem,shared_mem);
#ifdef CLUSTER
	shared_mem = 98304ull;
	global_mem = 34359738368ull;
#endif
};
bool AlignmentProtocol::align(float4 * h_particles_4,float4 * d_particles_4, int frames,int particle_num,const bool OFFDEVICE,float3 *& h_coms, double *& h_U,int gridDim,int blockDim){
	//Expects h_particles_4 and d_particles_4 allocated and the former initiated with the data!
	//alignment steps
	//0. center zero frame and copy data to GPU
	//1. calculate m_alphas
	//2. find EVs
	//3. rotate coordinates

	//CHECK IF SHARED MEM IF DEVICE IS SUFFICIENT FOR CHOSEN BLOCKDIM
	uint sharedMem = 2*blockDim*sizeof(M_alpha);
#ifdef CLUSTER
	if (blockDim > 256)
		return false;
#else
	if (blockDim > 256 || sharedMem > shared_mem)
		return false;
#endif
	//transform zero frame to center of mass
	float4 coms = {0};
	for (int i=0;i<particle_num;i++)
		coms += h_particles_4[i];
	coms /= particle_num;
	for (int i=0;i<particle_num;i++)
		h_particles_4[i] -= coms;
	if (OFFDEVICE){
		h_coms[0].x = coms.x;
		h_coms[0].y = coms.y;
		h_coms[0].z = coms.z;
	}
	CudaSafeCall(cudaMemcpy(d_particles_4,h_particles_4,particle_num*sizeof(*d_particles_4),cudaMemcpyHostToDevice));
	CudaCheckError();
	float3 * d_coms;
	cudaMalloc((void **)&d_coms,(frames-1)*sizeof(*d_coms));
	//particle num
	int P = particle_num;
	M_alpha * d_m_alphas;
	cudaMalloc((void **) &d_m_alphas,frames*sizeof(*d_m_alphas));
	//recast for cusolver use
	double * d_m_alpha_matrices = reinterpret_cast<double *>(d_m_alphas);
	//recast for kernel use
	float * d_particles = reinterpret_cast<float *>(d_particles_4);

	//#######################################################################
	//						setup		-		 cusolver library
	//#######################################################################
	//cusolver resources and parameters
	double *d_U; /* ldu-by-m-by-batchSize */
	double *d_V; /* ldv-by-n-by-batchSize */
	double *d_S; /* -by-batchSizee */
	CudaCheckError();
	//initate resources
	//cusolver "-1" since the first frame stays fixed and the data elements signify rotations
	cudaMalloc((void**) &d_U, sizeof(double) * 4 * 4 * (frames-1));
	cudaMalloc((void**) &d_V, sizeof(double) * 4 * 4 * (frames-1));
	cudaMalloc((void**) &d_S, sizeof(double) * 4 * (frames-1));

	cusolverDnHandle_t cusolverH = NULL;
	cudaStream_t stream = NULL;
	gesvdjInfo_t gesvdj_params = NULL;

	cusolverStatus_t status = CUSOLVER_STATUS_SUCCESS;
	cudaError_t cudaStat1 = cudaSuccess;
	cudaError_t cudaStat2 = cudaSuccess;
	cudaError_t cudaStat3 = cudaSuccess;
	cudaError_t cudaStat4 = cudaSuccess;
	cudaError_t cudaStat5 = cudaSuccess;

	const int batchSize = (frames - 1);
	int info[batchSize]; /* info = [info0 ; info1] */
//
//		double *d_A = d_m_alpha_matrices; /* lda-by-n-by-batchSize */
	CudaCheckError();
	int* d_info = NULL; /* batchSize */
	int lwork = 0; /* size of workspace */
	double *d_work = NULL; /* d_coords workspace for gesvdjBatched */
//
	//configure
	/* step 1: create cusolver handle, bind a stream  */
	status = cusolverDnCreate(&cusolverH);
	assert(CUSOLVER_STATUS_SUCCESS == status);
	cudaStat1 = cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking);
	assert(cudaSuccess == cudaStat1);
	status = cusolverDnSetStream(cusolverH, stream);
	assert(CUSOLVER_STATUS_SUCCESS == status);
	/* step 2: configuration of gesvdj */
	status = cusolverDnCreateGesvdjInfo(&gesvdj_params);
	assert(CUSOLVER_STATUS_SUCCESS == status);
	/* default value of tolerance is machine zero */
	status = cusolverDnXgesvdjSetTolerance(gesvdj_params, 1.e-7);
	assert(CUSOLVER_STATUS_SUCCESS == status);
	/* default value of max. sweeps is 100 */
	status = cusolverDnXgesvdjSetMaxSweeps(gesvdj_params, 15);
	assert(CUSOLVER_STATUS_SUCCESS == status);
	/* disable sorting */
	status = cusolverDnXgesvdjSetSortEig(gesvdj_params, 1);
	assert(CUSOLVER_STATUS_SUCCESS == status);
	/* step 3: copy A to d_coords */
	cudaStat5 = cudaMalloc((void**) &d_info, sizeof(int) * batchSize);
	assert(cudaSuccess == cudaStat1);
	assert(cudaSuccess == cudaStat2);
	assert(cudaSuccess == cudaStat3);
	assert(cudaSuccess == cudaStat4);
	assert(cudaSuccess == cudaStat5);
	CudaCheckError();
	/* step 4: query working space of gesvdjBatched */
	status = cusolverDnDgesvdjBatched_bufferSize(cusolverH, CUSOLVER_EIG_MODE_VECTOR, 4, 4, d_m_alpha_matrices,
			4, d_S, d_U, 4, d_V, 4, &lwork, gesvdj_params, batchSize);
	assert(CUSOLVER_STATUS_SUCCESS == status);
	cudaStat1 = cudaMalloc((void**) &d_work, sizeof(double) * lwork);
	assert(cudaSuccess == cudaStat1);

	cublasHandle_t handle;
	cublasStatus_t cublas_status;
	cublas_status = cublasCreate(&handle);

	//#######################################################################
	//						end setup    -		 cusolver library
	//#######################################################################
	//step 1 - center and calculate M_alphas
	if (OFFDEVICE){
		_center_and_m_alpha<true>(d_particles,d_m_alphas,d_coms,frames,P,gridDim,blockDim);
	}
	else {
		_center_and_m_alpha<false>(d_particles,d_m_alphas,d_coms,frames,P,gridDim,blockDim);
	}
	cudaDeviceSynchronize();
	CudaCheckError();
	//step 2 - determine rotation quat by EVs
	cusolverDnDgesvdjBatched(cusolverH, CUSOLVER_EIG_MODE_VECTOR, 4, 4, d_m_alpha_matrices, 4, d_S, d_U,
				4, d_V, 4, d_work, lwork, d_info, gesvdj_params, batchSize);
	cudaDeviceSynchronize();
	CudaCheckError();
	if (!OFFDEVICE){
		//step 3 - rotate in device memory
		//shared mem - one for rot_quat
		rotate_frames_in_place_align<<<gridDim,blockDim,sizeof(float4)>>>(d_particles_4,d_U,P,frames);
		cudaDeviceSynchronize();
		cudaMemcpy(h_particles_4,d_particles_4,frames*particle_num*sizeof(*d_particles_4),cudaMemcpyDeviceToHost);
		CudaCheckError();
	} else {
		cudaMemcpy(h_U,d_U,sizeof(double) * 4 * 4 * (frames-1),cudaMemcpyDeviceToHost);
		cudaMemcpy(h_coms+1,d_coms,sizeof(*h_coms)*(frames-1),cudaMemcpyDeviceToHost);
		CudaCheckError();
	}
	return true;
}

void _rmsd_single(float4 *& d_particle_set,float *& d_rmsds, int & num_particles, int & num_frames,unsigned int gridDim,
		unsigned int blockDim){
	unsigned int sharedMem =  2*blockDim*sizeof(*d_rmsds);;
	switch (blockDim)
	{
	case 1024:
		calc_rmsd_single<1024><<<gridDim,1024,sharedMem>>>(d_particle_set,num_particles,num_frames,d_rmsds);
		break;
	case 512:
		calc_rmsd_single<512><<<gridDim,512,sharedMem>>>(d_particle_set,num_particles,num_frames,d_rmsds);
		break;
	case 256:
		calc_rmsd_single<256><<<gridDim,256,sharedMem>>>(d_particle_set,num_particles,num_frames,d_rmsds);
		break;
	case 128:
		calc_rmsd_single<128><<<gridDim,128,sharedMem>>>(d_particle_set,num_particles,num_frames,d_rmsds);
		break;
	case 64:
		calc_rmsd_single<64><<<gridDim,64,sharedMem>>>(d_particle_set,num_particles,num_frames,d_rmsds);
		break;
	case 32:
		calc_rmsd_single<32><<<gridDim,32,sharedMem>>>(d_particle_set,num_particles,num_frames,d_rmsds);
		break;
	}
}

void _rmsd(float4 *& d_particle_set,float *& h_rmsds,float *& d_rmsds, int & num_particles, int & num_frames,
		unsigned int gridDim, unsigned int blockDim){
	unsigned int sharedMem = 2*blockDim*sizeof(*d_rmsds);
	switch (blockDim)
	{
	case 1024:
		calc_rmsd<1024><<<gridDim,1024,sharedMem>>>(d_particle_set,num_particles,num_frames,d_rmsds);
		break;
	case 512:
		calc_rmsd<512><<<gridDim,512,sharedMem>>>(d_particle_set,num_particles,num_frames,d_rmsds);
		break;
	case 256:
		calc_rmsd<256><<<gridDim,256,sharedMem>>>(d_particle_set,num_particles,num_frames,d_rmsds);
		break;
	case 128:
		calc_rmsd<128><<<gridDim,128,sharedMem>>>(d_particle_set,num_particles,num_frames,d_rmsds);
		break;
	case 64:
		calc_rmsd<64><<<gridDim,64,sharedMem>>>(d_particle_set,num_particles,num_frames,d_rmsds);
		break;
	case 32:
		calc_rmsd<32><<<gridDim,32,sharedMem>>>(d_particle_set,num_particles,num_frames,d_rmsds);
		break;
	}
}

bool AlignmentProtocol::rmsd(float4 *& d_particle_set,float *& h_rmsds, int & num_particles, int & num_frames,
		unsigned int gridDim, unsigned int blockDim){
#ifndef CLUSTER
	unsigned long long free_global = getFreeGlobal(0);
#else
	unsigned long long free_global = 34359738368ull;
#endif
	float * d_rmsds;
	if (num_frames*num_frames*sizeof(*d_rmsds) > free_global || 2*blockDim*sizeof(float) > shared_mem)
		return false;
	CudaSafeCall(cudaMalloc((void **)&d_rmsds,num_frames*(num_frames-1)/2*sizeof(*d_rmsds)));
	_rmsd(d_particle_set,h_rmsds,d_rmsds,num_particles,num_frames,gridDim,blockDim);
	CudaCheckError();
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(h_rmsds,d_rmsds,num_frames*(num_frames-1)/2*sizeof(*d_rmsds),cudaMemcpyDeviceToHost));
	cudaFree(d_rmsds);
	return true;
}

void AlignmentProtocol::benchmark(std::string output_file){
	int particle_nums[3] = {100,1000,10000};
	int frame_nums[4] = {100,1000,10000,20000};
	int worker_nums[5] = {8,16,32,64,128};
	int thread_nums[6] = {32,64,128,256,512,1024};
	int max_frames_num = 20000;
	int max_particle_vol = 10000*max_frames_num;
	float bounding_box[3] = {1000,1000,1000};

	float3 * h_coms;
	double * h_U;
	float4 * h_test_coords = (float4 *) malloc(max_particle_vol*sizeof(*h_test_coords));
	float4 * d_test_coords;
	float * h_rmsds = (float *) malloc(max_frames_num*(max_frames_num-1)/2*sizeof(*h_rmsds));
	cudaMalloc((void **) &d_test_coords,max_particle_vol*sizeof(*d_test_coords));

	srand(time(NULL));
	//	printf("Generating %i particles in zero frame.\n", num_particles);
	printf("Preparing particles for benchmark ...\n");
	for (int p = 0; p < max_particle_vol; p++) {
		h_test_coords[p].x = scaled_rand(10000, bounding_box[0]);
		h_test_coords[p].y = scaled_rand(10000, bounding_box[1]);
		h_test_coords[p].z = scaled_rand(10000, bounding_box[2]);
	}
	cudaMemcpy(d_test_coords,h_test_coords,max_particle_vol*sizeof(*d_test_coords),cudaMemcpyHostToDevice);

//	std::ofstream bench;
//	bench.open(output_file.c_str());
//	bench << "operation,particles,frames,workers,threads,time" << std::endl;
//	std::chrono::_V2::system_clock::time_point start;
//	std::chrono::_V2::system_clock::time_point finish;
//	std::chrono::duration<double> elapsed;
//	bool ran;
//	for (int w=0;w<5;w++){
//		for (int t=5;t<6;t++){
//			for (int p=0;p<3;p++){
//				for (int f=0;f<4;f++){
//					//alignment test
//					printf("Benchmarking align with %i particles in %i frames with %i workers a %i threads ...",particle_nums[p],frame_nums[f],worker_nums[w],thread_nums[t]);
//					bench << "align," << particle_nums[p] << "," << frame_nums[f]  << "," << worker_nums[w] <<"," <<thread_nums[t] << ",";
//					start = std::chrono::high_resolution_clock::now();
//					ran = align(h_test_coords,d_test_coords,frame_nums[f],particle_nums[p],false,h_coms,h_U,worker_nums[w],thread_nums[t]);
//					finish = std::chrono::high_resolution_clock::now();
//					if (ran){
//						elapsed = finish - start;
//						bench << elapsed.count();
//						std::cout << "Time: " << elapsed.count() << std::endl;
//					} else {
//						std::cout << "insufficient gpu resources" << std::endl;
//					}
//					bench << std::endl;
//				}
//			}
//		}
//	}
//	for (int w=0;w<5;w++){
//			for (int t=5;t<6;t++){
//				for (int p=0;p<3;p++){
//					for (int f=0;f<4;f++){
//						//alignment test
//						printf("Benchmarking rmsd with %i particles in %i frames with %i workers a %i threads ...",particle_nums[p],frame_nums[f],worker_nums[w],thread_nums[t]);
//						bench << "rmsd," << particle_nums[p] << "," << frame_nums[f]  << "," << worker_nums[w] <<"," <<thread_nums[t] << ",";
//						start = std::chrono::high_resolution_clock::now();
//						ran = rmsd(d_test_coords,h_rmsds, particle_nums[p], frame_nums[f],worker_nums[w],thread_nums[t]);
//						finish = std::chrono::high_resolution_clock::now();
//						if (ran){
//							elapsed = finish - start;
//							bench << elapsed.count();
//							std::cout << "Time: " << elapsed.count() << std::endl;
//						} else {
//							std::cout << "insufficient gpu resources" << std::endl;
//						}
//						bench << std::endl;
//					}
//				}
//			}
//	}
//	bench.close();


	std::ofstream bench_1;
	bench_1.open("bench_particles_align_opt.csv");

	std::ofstream bench_2;
	bench_2.open("bench_frames_align_opt.csv");
	//bench_1 << "particles,time" <<std::endl;
	bench_2 << "frames,time" << std::endl;
	std::chrono::_V2::system_clock::time_point start;
	std::chrono::_V2::system_clock::time_point finish;
	std::chrono::duration<double> elapsed;
	bool ran;
	for (int p=500;p<=10000;p+=500){
		printf("Benchmarking align with %i particles in 20k frames with 128 workers a 128 threads ...",p);
		start = std::chrono::high_resolution_clock::now();
		ran = align(h_test_coords,d_test_coords,20000,p,false,h_coms,h_U,128,128);

		finish = std::chrono::high_resolution_clock::now();
		if (ran){
			elapsed = finish - start;
			bench_1 << p << "," << elapsed.count();
			std::cout << "Time: " << elapsed.count() << std::endl;
		} else {
			std::cout << "insufficient gpu resources" << std::endl;
		}
		bench_1 << std::endl;
	}
	int max_particle_num = 10000;
	for (int f=0;f<=20000;f+=1000){
		printf("Benchmarking align with 10k particles in %i frames with 128 workers a 512 threads ...",f);
		start = std::chrono::high_resolution_clock::now();
		ran = align(h_test_coords,d_test_coords,f,max_particle_num,false,h_coms,h_U,128,128);
		finish = std::chrono::high_resolution_clock::now();
		if (ran){
			elapsed = finish - start;
			bench_2 << f << "," << elapsed.count();
			std::cout << "Time: " << elapsed.count() << std::endl;
		} else {
			std::cout << "insufficient gpu resources" << std::endl;
		}
		bench_2 << std::endl;
	}

}

void AlignmentProtocol::pureTest(uint gridDim,uint blockDim){
	//1. create particles and frames, reorient them
	//2. align them
	//3. measure rmsd matrix
	printf("Starting pureTest ... \n");
	float4 * h_particle_set;
	float4 * d_particle_set;
	int num_particles = 1000;
	int num_frames = 1000;
	float bounding_box[3] = {100,100,100};
	//this holds the host memory for the resulting calculations
	float * h_rmsds = (float *) malloc(num_frames*(num_frames-1)/2*sizeof(*h_rmsds));
	//not used in this example
	float3 * h_coms;
	double * h_m_alphas;
	//creates one zero frame of num_particles particles and orients it randomly to produce num_frames-1 other frames
	create_random_test_frames(h_particle_set,num_particles,num_frames,bounding_box);

	//allocate and copy to device
	cudaMalloc((void **)&d_particle_set,num_frames*num_particles*sizeof(*d_particle_set));
	cudaMemcpy(d_particle_set,h_particle_set,num_frames*num_particles*sizeof(*d_particle_set),cudaMemcpyHostToDevice);
	cudaDeviceSynchronize();
	align(h_particle_set,d_particle_set,num_frames,num_particles,false,h_coms,h_m_alphas,gridDim,blockDim);
	cudaDeviceSynchronize();
	rmsd(d_particle_set,h_rmsds, num_particles, num_frames,gridDim,blockDim);
	cudaDeviceSynchronize();

	float sum = 0;
	for (int r=0;r<num_frames*(num_frames-1)/2;r++){
		if (h_rmsds[r]>0.1)
			printf("%i\n",r);
		sum += h_rmsds[r];
	}
	sum /= (num_frames*num_frames);
	printf("This number should be close to zero: %f\n",sum);
}

void AlignmentProtocol::testCube(void){
	printf("Starting cubeTest ... \n");
	int num_particles = 8;
	//rotation set hardcoded in create_test_cube
	int num_frames = 24;
	float4 * h_particle_set;
	float4 * d_particle_set;
	//not needed for calculation, but for api
	float3 * h_coms;
	double * h_U;
	h_particle_set = (float4 *) malloc(num_particles*num_frames*sizeof(*h_particle_set));
	cudaMalloc((void **) &d_particle_set,num_particles*num_frames*sizeof(*h_particle_set));

	create_test_cube(h_particle_set,d_particle_set);
	cudaDeviceSynchronize();
	float * h_rmsds = (float *) malloc(num_frames*(num_frames-1)/2*sizeof(float));
	align(h_particle_set,d_particle_set,num_frames,num_particles,false,h_coms,h_U,16,64);
	rmsd(d_particle_set,h_rmsds,num_particles,num_frames,16,64);
	cudaFree(d_particle_set);
	float sum = 0;
	for (int r=0;r<num_frames*(num_frames-1)/2;r++){
		sum += h_rmsds[r];
	}
	sum /= num_frames*(num_frames-1)/2;
	printf("This number should be close to zero: %f\n",sum);
}

void AlignmentProtocol::getKarneySet(const char * rot_file_name,float4 *& quats, int & rot_num){
	std::ifstream filestream;
	std::vector<std::string> strs;
	std::string current_line;
	std::map<float,std::string> alpha_to_path;
	std::map<std::string,size_t> path_to_rot_sample_volume;
	filestream.open(rot_file_name, std::fstream::in);
	//skip first three lines
	std::getline(filestream, current_line);
	std::getline(filestream, current_line);
	std::getline(filestream, current_line);
	//get rot number
	std::getline(filestream, current_line);
	rot_num = std::stoi(current_line.substr(0, current_line.find(" ")));
	quats = (float4 *) malloc(rot_num*sizeof(*quats));
	float * quats_f = reinterpret_cast<float *>(quats);

	int n = 0;
	while (std::getline(filestream, current_line)){
		current_line = current_line.substr(1,current_line.size());
		quats_f[4*n] = std::stof(current_line.substr(0,11));
		quats_f[4*n+1] = std::stof(current_line.substr(12,12));
		quats_f[4*n+2] = std::stof(current_line.substr(25,12));
		quats_f[4*n+3] = std::stof(current_line.substr(38,12));
		n++;
	}
	filestream.close();
}

void AlignmentProtocol::create_test_cube(float4 *& h_particle_set,float4 *& d_particle_set){
	int num_particles = 8;
	int num_frames = 24;

	h_particle_set[0] = {1,1,1,1};
	h_particle_set[1] = {-1,1,1,1};
	h_particle_set[2] = {-1,-1,1,1};
	h_particle_set[3] = {1,-1,1,1};
	h_particle_set[4] = {1,-1,-1,1};
	h_particle_set[5] = {1,1,-1,1};
	h_particle_set[6] = {-1,1,-1,1};
	h_particle_set[7] = {-1,-1,-1,1};

	float4 * h_rot_quats = (float4 *) malloc(num_frames*sizeof(*h_rot_quats));
	h_rot_quats[0]= {1.000000,0.000000,0.000000,0.000000};
	h_rot_quats[1]= {0.000000,1.000000,0.000000,0.000000};
	h_rot_quats[2]= {0.000000,0.000000,1.000000,0.000000};
	h_rot_quats[3]= {0.000000,0.000000,0.000000,1.000000};
	h_rot_quats[4]= {0.500000,0.500000,0.500000,0.500000};
	h_rot_quats[5]= {0.500000,0.500000,0.500000,-0.500000};
	h_rot_quats[6]= {0.500000,0.500000,-0.500000,0.500000};
	h_rot_quats[7]= {0.500000,0.500000,-0.500000,-0.500000};
	h_rot_quats[8]= {0.500000,-0.500000,0.500000,0.500000};
	h_rot_quats[9]= {0.500000,-0.500000,0.500000,-0.500000};
	h_rot_quats[10]= {0.500000,-0.500000,-0.500000,0.500000};
	h_rot_quats[11]= {0.500000,-0.500000,-0.500000,-0.500000};
	h_rot_quats[12]= {0.707107,0.707107,0.000000,0.000000};
	h_rot_quats[13]= {0.707107,-0.707107,0.000000,0.000000};
	h_rot_quats[14]= {0.707107,0.000000,0.707107,0.000000};
	h_rot_quats[15]= {0.707107,0.000000,-0.707107,0.000000};
	h_rot_quats[16]= {0.707107,0.000000,0.000000,0.707107};
	h_rot_quats[17]= {0.707107,0.000000,0.000000,-0.707107};
	h_rot_quats[18]= {0.000000,0.707107,0.707107,0.000000};
	h_rot_quats[19]= {0.000000,0.707107,-0.707107,0.000000};
	h_rot_quats[20]= {0.000000,0.707107,0.000000,0.707107};
	h_rot_quats[21]= {0.000000,0.707107,0.000000,-0.707107};
	h_rot_quats[22]= {0.000000,0.000000,0.707107,0.707107};
	h_rot_quats[23]= {0.000000,0.000000,0.707107,-0.707107};

	float4 * h_trans_vec = (float4 *) malloc(num_frames*sizeof(*h_trans_vec));
	h_trans_vec[0] = {10,0,0,0};
	h_trans_vec[1] = {0,10,0,0};
	h_trans_vec[2] = {0,0,10,0};
	h_trans_vec[3] = {10,10,0,0};
	h_trans_vec[4] = {0,10,10,0};
	h_trans_vec[5] = {10,0,10,0};
	h_trans_vec[6] = {10,10,10,0};
	h_trans_vec[7] = {-10,0,0,0};
	h_trans_vec[8] = {0,-10,0,0};
	h_trans_vec[9] = {0,0,-10,0};
	h_trans_vec[10] = {-10,-10,0,0};
	h_trans_vec[11] = {0,-10,-10,0};
	h_trans_vec[12] = {-10,0,-10,0};
	h_trans_vec[13] = {-10,-10,-10,0};
	h_trans_vec[14] = {10,-10,0,0};
	h_trans_vec[15] = {10,0,-10,0};
	h_trans_vec[16] = {10,-10,-10,0};
	h_trans_vec[17] = {-10,10,0,0};
	h_trans_vec[18] = {-10,0,10,0};
	h_trans_vec[19] = {-10,10,10,0};
	h_trans_vec[20] = {-10,10,-10,0};
	h_trans_vec[21] = {-10,-10,10,0};
	h_trans_vec[22] = {10,-10,10,0};
	h_trans_vec[23] = {10,-10,-50,0};

	for (int f = 1; f < num_frames; f++) {
		for (int p = 0; p < num_particles; p++) {
			rotate(&h_particle_set[p], &h_particle_set[f*num_particles + p], &h_rot_quats[f]);
			h_particle_set[f*num_particles + p].w = 1;
			h_particle_set[f*num_particles + p] += h_trans_vec[f];
		}
	}
	cudaMemcpy(d_particle_set,h_particle_set,num_frames*num_particles*sizeof(*d_particle_set),cudaMemcpyHostToDevice);
}

void AlignmentProtocol::create_random_test_frames(float4 *& h_particle_set,const int num_particles, const int num_frames, const float * bounding_box) {
	srand(time(NULL));
	h_particle_set = (float4 *)malloc(sizeof(float4) * num_particles * num_frames);
	float bound_x = bounding_box[0];
	float bound_y = bounding_box[1];
	float bound_z = bounding_box[2];
	int _rand_max = 10000;
	for (int p = 0; p < num_particles; p++) {
		h_particle_set[p].x = scaled_rand(_rand_max, bound_x);
		h_particle_set[p].y = scaled_rand(_rand_max, bound_y);
		h_particle_set[p].z = scaled_rand(_rand_max, bound_z);
	}
	float4 rotation_quat;
	for (int f = 1; f < num_frames; f++) {
		rotation_quat = random_quat();
		for (int p = 0; p < num_particles; p++)
			rotate(&h_particle_set[p], &h_particle_set[f*num_particles + p], &rotation_quat);
	}
}

void AlignmentProtocol::_read_dir_entries(void){
    if (dir_exists(_search_dir)){
		DIR *di;
		char *ptr1,*ptr2;
		int retn;
		struct dirent *dir;
		di = opendir(_search_dir.c_str()); //specify the directory name
		{
			while ((dir = readdir(di)) != NULL)
			{
				ptr1=strtok(dir->d_name,".");
				ptr2=strtok(NULL,".");
				if(ptr2!=NULL)
				{
					retn = strcmp(ptr2,"rmf");
					retn = retn && strcmp(ptr2,"rmf3");
					if(retn==0)
					{
//						printf("Found rmf file: %s/%s.%s\n",_search_dir.c_str(),ptr1,ptr2);
						_rmf_file_paths.push_back(std::string(_search_dir + "/" + std::string(ptr1) + "." + std::string(ptr2)));
					}
				}
			}
			closedir(di);
			}
		}
    for (std::string _filepath: _rmf_file_paths){
    	printf("Found rmf file: %s\n",_filepath.c_str());
    }
}

void AlignmentProtocol::prepare(){
	_read_dir_entries();
}

#ifndef CLUSTER
IMP::algebra::Vector3D coms(IMP::atom::Hierarchy& hierarchy, IMP::Model * model){
	IMP::algebra::Vector3D ret = {0,0,0};
	int c = 0;
	for (auto leave : IMP::core::get_leaves(hierarchy)) {
		if (IMP::core::RigidMember(leave).get_is_setup(model,
						leave.get_particle_index())){
			ret += IMP::core::XYZR(leave).get_coordinates();
			c++;
		}
	}
	ret /= c;
	return ret;
}

void load_RMF_into_RAM(RMF::FileConstHandle fh, IMP::Model* model, IMP::atom::Hierarchy hierarchy,
		float4 *& h_coords_4, size_t start_frame, size_t amount,size_t offset, size_t num_particles) {
	IMP::core::GenericHierarchies leaves;
	IMP::algebra::Vector3D vec;
	double radius;
	int f = offset;
	for (int file_f = start_frame; file_f < start_frame + amount; file_f++) {
		IMP::rmf::load_frame(fh, RMF::FrameID(file_f));
		leaves = IMP::core::get_leaves(hierarchy);
		IMP::atom::Hierarchy leave_h;
		int c = 0;
		printf("Loading frame %i ...\n", f);
		for (auto leave : leaves) {
			if (IMP::core::RigidMember(leave).get_is_setup(model,
							leave.get_particle_index())){
				vec = IMP::core::XYZR(leave).get_coordinates();
				radius = IMP::core::XYZR(leave).get_radius();
				h_coords_4[f * num_particles + c].x = (float) vec[0];
				h_coords_4[f * num_particles + c].y = (float) vec[1];
				h_coords_4[f * num_particles + c].z = (float) vec[2];
				h_coords_4[f * num_particles + c].w = (float) radius;
				c ++;
			}
		}
		f++;
	}
}

std::list<IMP::core::RigidBody> get_unique_rigid_bodies(RMF::FileConstHandle in_fh,
		IMP::core::Hierarchy hierarchy,IMP::Model* model,int frame_id){
	std::list<IMP::core::RigidBody> unique_rigid_bodies;
	IMP::rmf::load_frame(in_fh, RMF::FrameID(frame_id));
	IMP::core::RigidBody rb;
	IMP::core::GenericHierarchies leaves = IMP::core::get_leaves(hierarchy);
	for (auto leave : leaves) {
		if (IMP::core::RigidMember(leave).get_is_setup(model,
				leave.get_particle_index())) {
			rb = IMP::core::RigidMember(leave).get_rigid_body();
			if (std::find(unique_rigid_bodies.begin(), unique_rigid_bodies.end(), rb)
					== unique_rigid_bodies.end()) {
				unique_rigid_bodies.push_back(rb);
			}
		}
	}
	return unique_rigid_bodies;
}

std::list<IMP::core::Hierarchy> get_non_rigid_members(RMF::FileConstHandle in_fh,
		IMP::core::Hierarchy hierarchy,IMP::Model* model,int frame_id){
	std::list<IMP::core::Hierarchy> non_rigid_members;
	IMP::rmf::load_frame(in_fh, RMF::FrameID(frame_id));
	IMP::core::Hierarchy h;
	IMP::core::GenericHierarchies leaves = IMP::core::get_leaves(hierarchy);
	for (auto leave : leaves) {
		if (!IMP::core::RigidMember(leave).get_is_setup(model,
				leave.get_particle_index())) {
			if (std::find(non_rigid_members.begin(), non_rigid_members.end(), leave)
					== non_rigid_members.end()) {
				non_rigid_members.push_back(leave);
			}
		}
	}
	return non_rigid_members;
}

bool double_equals(double a, double b, double epsilon = 0.000001)
{
    return std::abs(a - b) < epsilon;
}

void get_dimensions(RMF::FileConstHandle fh, IMP::Model * model, IMP::atom::Hierarchy hierarchy,
		int & num_frames, int & num_particles, int & num_rigid_members, std::list<size_t> & rigid_particle_indices) {
	num_frames = fh.get_number_of_frames();
	IMP::core::GenericHierarchies leaves = IMP::core::get_leaves(hierarchy);
	num_particles = 0;
	for (auto leave : leaves) {
		//remember if something is member of a rigid body
		if (IMP::core::RigidMember(leave).get_is_setup(model,
				leave.get_particle_index())){
			rigid_particle_indices.push_back(num_particles);
			num_rigid_members++;
		}
		num_particles++;
	}
}
void get_dimensions_ext(RMF::FileConstHandle fh, IMP::Model * model, IMP::atom::Hierarchy hierarchy,
		size_t & num_frames, size_t & num_particles,size_t & num_kernels,double *& kernel_params,
		std::map<std::string,std::list<size_t>> & subunit_map,std::list<size_t> & rigid_particle_indices) {
	num_frames = fh.get_number_of_frames();
	IMP::core::GenericHierarchies leaves = IMP::core::get_leaves(hierarchy);
	num_particles = 0;
	double radius;
	bool not_add;
	for (auto leave : leaves) {
		radius = IMP::core::XYZR(leave).get_radius();
		not_add = false;
		for (int i =0;i<num_kernels;i++){
			not_add |= double_equals(radius,kernel_params[i]);
		}
		if (!not_add){
			kernel_params[num_kernels] = radius;
			num_kernels++;
		}
		//remember the subunit for every particle index
		if (subunit_map.find(leave.get_parent().get_parent().get_parent()->get_name())
				== subunit_map.end()){
			//subunit not yet in map
			std::list<size_t> new_list;
			new_list.push_back(num_particles);
			subunit_map[leave.get_parent().get_parent().get_parent()->get_name()] = new_list;
		} else {
			subunit_map[leave.get_parent().get_parent().get_parent()->get_name()].push_back(num_particles);
		}
		//remember if something is member of a rigid body
		if (IMP::core::RigidMember(leave).get_is_setup(model,
				leave.get_particle_index())){
			rigid_particle_indices.push_back(num_particles);
		} else {
		}
		num_particles++;
	}
	printf("RMF contains %i particles in each one of %i frames, %u kernels are implied ...\n",
			num_particles, num_frames,num_kernels);
	printf("The following kernels are implied: ...\n");
	for (int k =0;k<num_kernels;k++){
		printf("Kernel #%i has parameter %f ...\n",k,kernel_params[k]);
	}
}

void AlignmentProtocol::rmfTest(void){
	//the analogue of pureTest, yet with rmf inputs and outputs
	//1. create particles and frames, reorient them
	//2. align them
	//3. measure rmsd matrix
	printf("Starting rmfTest ... \n");
	int num_frames = 100;
	float * h_rmsds;
	//create a test file from
	testFileRMF("./test/concat_small.rmf3","./test_file.rmf",num_frames);
	alignRMF("./test_file.rmf","./test_file_aligned.rmf");
	rmsdRMF("./test_file_aligned.rmf",h_rmsds,num_frames);
	float sum = 0;
	for (int r=0;r<num_frames*(num_frames-1)/2;r++){
		sum += h_rmsds[r];
	}
	sum /= (num_frames*num_frames);
	printf("This number should be close to zero: %f\n",sum);
}

void AlignmentProtocol::rmsdRMF(std::string infile_name,float *& h_rmsds,int & num_frames){
	RMF::FileConstHandle fh = RMF::open_rmf_file_read_only(infile_name);
	IMP_NEW(IMP::Model, model, ());
	IMP::atom::Hierarchy hierarchy = IMP::rmf::create_hierarchies(fh, model)[0];
	IMP::Restraints restraints = IMP::rmf::create_restraints(fh,model);
	IMP::display::Geometries geometries = IMP::rmf::create_geometries(fh);
	std::list<IMP::core::RigidBody> unique_rigid_bodies = get_unique_rigid_bodies(fh,hierarchy,model,0);
	std::list<IMP::core::Hierarchy> non_rigid_members = get_non_rigid_members(fh,hierarchy,model,0);
	num_frames = 0;
	int num_particles = 0;
	int num_rigid_members = 0;
	std::list<size_t> rigid_particle_indices;
	//loads rigid members only!
	get_dimensions(fh,model, hierarchy, num_frames, num_particles,num_rigid_members,rigid_particle_indices);
	float4 * h_particles_4 = (float4 *) malloc(num_frames*num_rigid_members*sizeof(*h_particles_4));
	float4 * d_particles_4;
	cudaMalloc((void **)&d_particles_4,num_frames*num_particles*sizeof(*d_particles_4));
	load_RMF_into_RAM(fh, model, hierarchy,h_particles_4, 0,num_frames,0,num_rigid_members);
	cudaMemcpy(d_particles_4,h_particles_4,num_frames*num_particles*sizeof(*d_particles_4),cudaMemcpyHostToDevice);
	cudaDeviceSynchronize();
	h_rmsds = (float *) malloc(num_frames*(num_frames-1)/2*sizeof(*h_rmsds));
	rmsd(d_particles_4,h_rmsds,num_rigid_members, num_frames,16,64);
}

void AlignmentProtocol::rmfTestAlignment(std::string infile_name,std::string aligned_file_name){
	printf("Starting rmfAlignmentTest ... \n");
	alignRMF(infile_name,aligned_file_name);
	RMF::FileConstHandle fh = RMF::open_rmf_file_read_only(aligned_file_name);
	IMP_NEW(IMP::Model, model, ());
	IMP::atom::Hierarchy hierarchy = IMP::rmf::create_hierarchies(fh, model)[0];
	IMP::Restraints restraints = IMP::rmf::create_restraints(fh,model);
	IMP::display::Geometries geometries = IMP::rmf::create_geometries(fh);
	std::list<IMP::core::RigidBody> unique_rigid_bodies = get_unique_rigid_bodies(fh,hierarchy,model,0);
	std::list<IMP::core::Hierarchy> non_rigid_members = get_non_rigid_members(fh,hierarchy,model,0);
	int num_frames = 0;
	int num_particles = 0;
	int num_rigid_members = 0;
	std::list<size_t> rigid_particle_indices;
	//loads rigid members only!
	get_dimensions(fh,model, hierarchy, num_frames, num_particles,num_rigid_members,rigid_particle_indices);
	float4 * h_particles_4 = (float4 *) malloc(num_frames*num_rigid_members*sizeof(*h_particles_4));
	float4 * d_test_4;

	int num_rots;
	float4 * h_rot_quats;
	float4 * d_rot_quats;
	getKarneySet("./cffk-orientation-dc8bb42/data/c48u157.quat",h_rot_quats,num_rots);
	cudaMalloc((void **) &d_rot_quats,num_rots*sizeof(*d_rot_quats));
	cudaMemcpy(d_rot_quats,h_rot_quats,num_rots*sizeof(*d_rot_quats),cudaMemcpyHostToDevice);
	CudaCheckError();
	//zero frame, aligned frame,perturbed frames
	cudaMalloc((void **)&d_test_4,(2+num_rots)*num_particles*sizeof(*d_test_4));
	load_RMF_into_RAM(fh, model, hierarchy,h_particles_4, 0,num_frames,0,num_rigid_members);
	//copy the zero frame to device, it's supposed to be the reference
	cudaMemcpy(d_test_4,h_particles_4,num_rigid_members*sizeof(*d_test_4),cudaMemcpyHostToDevice);
	CudaCheckError();

	float4 sum;
	float tol = 1e-2;
	bool com_origin = true;
	for (int f = 0;f < num_frames;f++){
		sum = {0,0,0,0};
		for (int p = 0;p < num_rigid_members;p++)
			sum += h_particles_4[f*num_rigid_members + p];
		sum /= num_rigid_members;
		com_origin = com_origin && sum.x<tol && sum.y<tol && sum.z <tol;
	}
	printf("The center of mass of all frames is shifted to the origin: %s\n",com_origin?"true":"false");
//
//	int num_workers = 16;
//	const int block_dim = 32;
	float * h_rmsds = (float *) malloc((num_rots+1)*sizeof(*h_rmsds));
	float * d_rmsds;
	cudaMalloc((void **) &d_rmsds,(num_rots+1)*sizeof(*h_rmsds));
	CudaCheckError();
	bool test = true;
	//test global alignments
	for (int f = 1; f < num_frames; f++){
		//frame0 is at 0, the chosen frame f is at 1
		cudaMemcpy(d_test_4+num_rigid_members, h_particles_4 + f*num_rigid_members,num_rigid_members*sizeof(*d_test_4),cudaMemcpyHostToDevice);
		CudaCheckError();
		//the rest is full of rotated versions of frame f
		rotate_frames_into_place<<<16,32,sizeof(float4)>>>(d_test_4,d_rot_quats,num_rigid_members,num_rots);
		CudaCheckError();
//		void calc_rmsd_single(float4 * d_coords_4, int num_particles, int num_frames, float * d_rmsds)
//		calc_rmsd_single<<<num_workers,block_dim,shared_rmsd_single>>>(d_test_4,num_rigid_members,num_rots,d_rmsds);
//		void _rmsd_single(float4 *& d_particle_set,float *& d_rmsds, int & num_particles, int & num_frames,
//			unsigned int blockDim,unsigned int gridDim)
		_rmsd_single(d_test_4,d_rmsds, num_rigid_members, num_rots,16,64);
		cudaDeviceSynchronize();
		cudaMemcpy(h_rmsds,d_rmsds,(num_rots+1)*sizeof(*h_rmsds),cudaMemcpyDeviceToHost);
//		for (int i = 1; i<num_rots+1;i++)
//			test = test & bool(h_rmsds[0] <= h_rmsds[i]);
		test = true;
		for (int i = 1; i < num_rots-2;i++){
			test = test && (h_rmsds[0] <= h_rmsds[i]);
			if (h_rmsds[0] > h_rmsds[i])
				printf("%i %i %f %f\n",f,i,h_rmsds[0],h_rmsds[i]);
		}
		printf("Frame %i aligned globally: %s\n",f,test?"true":"false");
	}

	//random perturbations close to unity
	int num_rand_rots=500;
	float4 * h_rand_rot_quats = (float4 *)malloc(num_rand_rots*sizeof(*h_rand_rot_quats));
	float4 * d_rand_rot_quats;
	float * h_rmsds_rand = (float *) malloc((num_rand_rots+1)*sizeof(*h_rmsds_rand));
	float * d_rmsds_rand;
	cudaMalloc((void **) &d_rmsds_rand,(num_rand_rots+1)*sizeof(*d_rmsds_rand));
	cudaMalloc((void **) &d_rand_rot_quats,num_rand_rots*sizeof(*d_rand_rot_quats));
	CudaCheckError();

	//test global alignments
	for (int f = 1; f<num_frames; f++){
		for (int i=0;i<num_rand_rots;i++){
			h_rand_rot_quats[i] = random_quat_close_to_unity();
		}
		cudaMemcpy(d_rand_rot_quats,h_rand_rot_quats,num_rand_rots*sizeof(*d_rand_rot_quats),cudaMemcpyHostToDevice);
		CudaCheckError();
		//frame0 is at 0, the chosen frame f is at 1
		cudaMemcpy(d_test_4+num_rigid_members, h_particles_4 + f*num_rigid_members,num_rigid_members*sizeof(*d_test_4),cudaMemcpyHostToDevice);
		CudaCheckError();
		//the rest is full of rotated versions of frame f
		rotate_frames_into_place<<<16,32,sizeof(float4)>>>(d_test_4,d_rand_rot_quats,num_rigid_members,num_rand_rots);
		CudaCheckError();
//		void calc_rmsd_single(float4 * d_coords_4, int num_particles, int num_frames, float * d_rmsds)
		_rmsd_single(d_test_4,d_rmsds, num_rigid_members, num_rots,16,64);
		CudaCheckError();
		cudaDeviceSynchronize();
		cudaMemcpy(h_rmsds_rand,d_rmsds_rand,(num_rand_rots+1)*sizeof(*h_rmsds_rand),cudaMemcpyDeviceToHost);
//		cudaMemcpy(h_rmsds_rand,d_rmsds_rand,40,cudaMemcpyDeviceToHost);
		CudaCheckError();
//		for (int i = 1; i<num_rots+1;i++)
//			test = test & bool(h_rmsds[0] <= h_rmsds[i]);
		test = true;
		for (int i = 1; i < num_rand_rots-1;i++){
			test = test && (abs(h_rmsds_rand[0] - h_rmsds_rand[i]) <= 0.1);
			if (h_rmsds_rand[0] > h_rmsds_rand[i])
				printf("%i %i %f %f\n",f,i,h_rmsds_rand[0],h_rmsds_rand[i]);
		}
		printf("Frame %i aligned locally: %s\n",f,test?"true":"false");
	}
}

void AlignmentProtocol::testFileRMF(std::string infile_name,std::string testfile_name,int test_frame_num){
	//load a file with at least one frame
	//randomize this one frame and write the random new frames to a new file
	//load that file
	//align it
	//measure rmsd
	{
		RMF::FileConstHandle fh = RMF::open_rmf_file_read_only(infile_name);
		IMP_NEW(IMP::Model, model, ());
		IMP::atom::Hierarchy hierarchy = IMP::rmf::create_hierarchies(fh, model)[0];
		IMP::Restraints restraints = IMP::rmf::create_restraints(fh,model);
		IMP::display::Geometries geometries = IMP::rmf::create_geometries(fh);
		std::list<IMP::core::RigidBody> unique_rigid_bodies = get_unique_rigid_bodies(fh,hierarchy,model,0);
		std::list<IMP::core::Hierarchy> non_rigid_members = get_non_rigid_members(fh,hierarchy,model,0);
		int num_frames = 0;
		int num_particles = 0;
		int num_rigid_members = 0;
		std::list<size_t> rigid_particle_indices;
		get_dimensions(fh,model, hierarchy, num_frames, num_particles,num_rigid_members,rigid_particle_indices);
		float4 rand_quat;
		float3 rand_trans;
		RMF::FileHandle orh = RMF::create_rmf_file(testfile_name);
		orh.set_producer("Le Kai");
		IMP::rmf::add_hierarchy(orh,hierarchy);
		IMP::rmf::add_restraints(orh,restraints);
		IMP::rmf::add_geometries(orh,geometries);
		IMP::core::GenericHierarchies leaves;
		IMP::algebra::Vector3D translation;
		IMP::algebra::Vector4D rotation_vec;
		IMP::rmf::load_frame(fh, RMF::FrameID(0));
		IMP::algebra::Vector3D zero_frame_coms = coms(hierarchy,model);
		IMP::algebra::Transformation3D coms_translation(-zero_frame_coms);
		for (IMP::core::RigidBody rb: unique_rigid_bodies)
			IMP::core::transform(rb,coms_translation);
		IMP::rmf::save_frame(orh,"0");
		IMP::algebra::Transformation3D transformation;
		IMP::algebra::Rotation3D rotation;
		for (int f = 1; f<test_frame_num; f++){
			rand_quat = random_quat();
			rand_trans = random_trans();
			translation = {rand_trans.x,rand_trans.y,rand_trans.z};
			rotation = IMP::algebra::Rotation3D(rotation_vec);
			transformation = IMP::algebra::Transformation3D(rotation,translation);
			for (IMP::core::RigidBody rb: unique_rigid_bodies)
				IMP::core::transform(rb,transformation);
			model->update();
			IMP::rmf::save_frame(orh,std::to_string(f));
			for (IMP::core::RigidBody rb: unique_rigid_bodies)
				IMP::core::transform(rb,transformation.get_inverse());
			model->update();
		}
	}
}

void AlignmentProtocol::alignRMF(std::string infile_name, std::string outfile_name){
	RMF::FileConstHandle fh = RMF::open_rmf_file_read_only(infile_name);
	IMP_NEW(IMP::Model, model, ());
	IMP::atom::Hierarchy hierarchy = IMP::rmf::create_hierarchies(fh, model)[0];
	IMP::Restraints restraints = IMP::rmf::create_restraints(fh,model);
	IMP::display::Geometries geometries = IMP::rmf::create_geometries(fh);
	std::list<IMP::core::RigidBody> unique_rigid_bodies = get_unique_rigid_bodies(fh,hierarchy,model,0);
	std::list<IMP::core::Hierarchy> non_rigid_members = get_non_rigid_members(fh,hierarchy,model,0);
	//	RMF::FileConstHandle fh, IMP::Model * model, IMP::atom::Hierarchy hierarchy,
	//			size_t & num_frames, size_t & num_particles,size_t & num_kernels,double *& kernel_params,
	//			std::map<std::string,std::list<size_t>> subunit_map,std::list<size_t> rigid_particle_indices
	int num_frames = 0;
	int num_particles = 0;
	int num_rigid_members = 0;
	std::list<size_t> rigid_particle_indices;
	get_dimensions(fh,model, hierarchy, num_frames, num_particles,num_rigid_members,rigid_particle_indices);

	//loads rigid members only!
	float4 * h_particles_4 = (float4 *) malloc(num_frames*num_rigid_members*sizeof(*h_particles_4));
	float4 * d_particles_4;
	cudaMalloc((void **)&d_particles_4,num_frames*num_rigid_members*sizeof(*d_particles_4));
	load_RMF_into_RAM(fh, model, hierarchy,h_particles_4, 0,num_frames,0,num_rigid_members);
	cudaMemcpy(d_particles_4,h_particles_4,num_frames*num_rigid_members*sizeof(*d_particles_4),cudaMemcpyHostToDevice);
	cudaDeviceSynchronize();
	float3 * h_coms = (float3 *) malloc(num_frames*sizeof(float3));
	double * h_U = (double *) malloc(16*(num_frames-1)*sizeof(double));

	align(h_particles_4,d_particles_4,num_frames,num_rigid_members,true,h_coms,h_U,16,64);
	CudaCheckError();

	RMF::FileHandle orh = RMF::create_rmf_file(outfile_name);
	orh.set_producer("Le Kai");
	IMP::rmf::add_hierarchy(orh,hierarchy);
	IMP::rmf::add_restraints(orh,restraints);
	IMP::rmf::add_geometries(orh,geometries);
	IMP::core::GenericHierarchies leaves;
	IMP::algebra::Vector3D translation;
	IMP::algebra::Vector4D rotation_vec;
	IMP::algebra::Rotation3D rotation;
	IMP::algebra::Transformation3D transformation;
	IMP::algebra::Transformation3D transformation_lat;
	IMP::algebra::Transformation3D transformation_rot;

	RMF_FOREACH(RMF::FrameID ni, fh.get_frames()){
		fh.set_current_frame(ni);
		IMP::rmf::load_frame(fh, ni);
		translation[0] = - h_coms[ni.get_index()].x;
		translation[1] = - h_coms[ni.get_index()].y;
		translation[2] = - h_coms[ni.get_index()].z;
		transformation_lat = IMP::algebra::Transformation3D(translation);
		if (ni.get_index() == 0){
			transformation = IMP::algebra::Transformation3D(translation);
			for (IMP::core::RigidBody rb: unique_rigid_bodies)
				IMP::core::transform(rb,transformation);
			model->update();
			IMP::rmf::save_frame(orh,std::to_string(ni.get_index()));
			continue;
		}
		if (ni.get_index() > 0){
			rotation_vec[0] = h_U[(ni.get_index()-1)*16+12];
			rotation_vec[1] = -h_U[(ni.get_index()-1)*16+13];
			rotation_vec[2] = -h_U[(ni.get_index()-1)*16+14];
			rotation_vec[3] = -h_U[(ni.get_index()-1)*16+15];
		}
		rotation = IMP::algebra::Rotation3D(rotation_vec);
		transformation = IMP::algebra::Transformation3D(rotation,translation);
		transformation_rot = IMP::algebra::Transformation3D(rotation);
//		std::cout << ni.get_index() << ":" << transformation_rot << std::endl;
		for (IMP::core::RigidBody rb: unique_rigid_bodies){
				IMP::core::transform(rb,transformation_lat);
				IMP::core::transform(rb,transformation_rot);
		}
		model->update();
		IMP::rmf::save_frame(orh,std::to_string(ni.get_index()));
	}
	cudaDeviceSynchronize();
	cudaFree(d_particles_4);
}
#endif

#ifndef TEST
#include <stdio.h>
#include <getopt.h>

int main(int argc, char ** argv)
{
    static const struct option long_options[] =
    {
//        { "AAA", no_argument,       0, 'a' },
//        { "BBB", no_argument,       0, 'b' },
//        { "CCC", required_argument, 0, 'c' },
//        { "DDD", no_argument,       0, 'd' },
//        { "EEE", no_argument,       0, 0   },
//        { "FFF", required_argument, 0, 0   },
//        0
    };
    while (1)
    {
        int index = -1;
//        struct option * opt = 0;
        int result = getopt_long(argc, argv,
            "abc:d",
            long_options, &index);
        if (result == -1) break; /* end of list */
        switch (result)
        {
//            case 'a': /* same as index==0 */
//                printf("'a'/'AAA' was specified.\n");
//                break;
//            case 'b': /* same as index==1 */
//                printf("'b'/'BBB' was specified.\n");
//                break;
//            case 'c': /* same as index==2 */
//                printf("'c'/'CCC' was specified. Arg: <%s>\n",
//                    optarg);
//                break;
//            case 'd': /* same as index==3 */
//                printf("'d'/'DDD' was specified.\n");
//                break;
//            case 0: /* all parameter that do not */
//                    /* appear in the optstring */
//                opt = (struct option *)&(long_options[index]);
//                printf("'%s' was specified.",
//                    opt->name);
//                if (opt->has_arg == required_argument)
//                    printf("Arg: <%s>", optarg);
//                printf("\n");
//                break;
            default: /* unknown */
                break;
        }
    }
    /* print all other parameters */
    AlignmentProtocol align("");
#ifndef CLUSTER
    if (argc == 3)
    {
    	std::string in_file(argv[1]);
    	std::string out_file(argv[2]);
    	if (file_exists(in_file)){
    		align.alignRMF(in_file,out_file);
    	}
    }
    if (argc == 2){
    	std::string in_file(argv[1]);
    	if (file_exists(in_file)){
    		float * h_rmsds;
    		int num_frames = 0;
    	    align.rmsdRMF(in_file,h_rmsds,num_frames);
    	    for (int i=0;i<num_frames*(num_frames-1)/2-1;i++)
    	    	std::cout << h_rmsds[i] << ",";
    	    std::cout << h_rmsds[num_frames*num_frames-1] << std::endl;
    	 }
    }
#endif
    if (argc == 1){
#ifdef CLUSTER
    	align.benchmark("benchmark.csv"); std::exit(0);
#else
    	std::cout << "Do you want to benchmark? y/n" << std::endl;
    	while (true)
    	{
    		std::string s;
    	    std::cin >> std::ws;   // skip any leading whitespace
    	    std::getline( std::cin, s );
    	    if (s.empty()) continue;
    	    switch (toupper( s[ 0 ] ))
    	    {
    	    	case 'Y': align.benchmark("benchmark.csv"); std::exit(0);
    	    	case 'N': std::exit(0);
    	    }
    	    std::cout << "Invalid choice." << std::endl;
    	}
#endif
    }
    else {
		std::cout << "Call pattern not recognised." << std::endl;
		std::cout << "Option 1: align input.rmf output.rmf  --  for alignment" << std::endl;
		std::cout << "Option 2: align input.rmf --  for rmsd matrix" << std::endl;
		std::cout << "Option 3: align -- benchmark " << std::endl;
	}
    return 0;
}
#endif
