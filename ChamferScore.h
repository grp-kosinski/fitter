/*
 * ChamferScore.h
 *
 *  Created on: Aug 14, 2021
 *      Author: kkarius
 */

#ifndef CHAMFERSCORE_H_
#define CHAMFERSCORE_H_

#include<map>
#include<util.h>
#include<cuda_util_include/cutil_math.h>
#include<derived_atomic_functions.h>

#include<ChebychevNodes.h>
#include<McEwenNodes.h>
#include<Density.h>

struct ChamferScore_Register {
	ChebychevNodes_Register * trans_node = nullptr;
	McEwenNodes_Register * rot_node = nullptr;
	float * d_diffmap = nullptr;
	float * h_diffmap = nullptr;
	float * d_working_mem = nullptr;
	float * d_score = nullptr;
	float * d_dist = nullptr;
	float * d_entropies = nullptr;
	float * h_rot_nodes = nullptr;
	float * d_rot_nodes = nullptr;
	size_t * h_rot_vol = nullptr;
	size_t * d_rot_vol = nullptr;
	size_t * h_translation_vol = nullptr;
	size_t * d_translation_vol = nullptr;
	size_t * h_translation_stride = nullptr;
	uint * h_target_padding = nullptr;
	uint3 * h_padding_pixel_dim = nullptr;
	float3 * h_padding_coord_dim = nullptr;
	uint * h_padding_off_surface_indexes = nullptr;
};

class ChamferScore {
public:
	ChamferScore(){};
	void score(Density *const& target, Density *const& query, const int& translation_offset,
			const int& translation_volume,const int& gpu_index);
	void createInstance(Density *const& target,Density *const& query,ChebychevNodes *const& trans_nodes, McEwenNodes *const& rot_nodes, const size_t & translation_vol, const int & gpu_index);
	void defineSurfaces(Density *const& target, const float& threshold0_t, const float& threshold1_t,
			Density *const& query, const float& threshold0_q, const float& threshold1_q,const int& gpu_index);
	void getPaddingVolume(uint & padding_vol, const int & gpu_index);
	virtual ~ChamferScore(){};

	std::map<int,ChamferScore_Register> _instance_registry;
};

#endif /* CHAMFERSCORE_H_ */
