/*
 * McEwenNodes.cu
 *
 *  Created on: Mar 26, 2021
 *      Author: kkarius
 */

#include <McEwenNodes.h>
#include <complex>


__host__ __device__
void euler_to_quat(float3 *const& euler, float4 *const& q){
	q->x = +cos(0.5f*euler->y)*cos(0.5f*(euler->x + euler->z));
	q->y = -sin(0.5f*euler->y)*sin(0.5f*(euler->x - euler->z));
	q->z = +sin(0.5f*euler->y)*cos(0.5f*(euler->x - euler->z));
	q->w = +cos(0.5f*euler->y)*sin(0.5f*(euler->x + euler->z));
}

const std::complex<double> i(0, 1);
__host__
std::complex<double> w(int mp){
	if (mp%2 == 0)
		return std::complex<double>(2/(1 - powf((double)mp,2.0)),0.0);
	if (abs(mp) == 1)
		return std::complex<double>(0.0,((double) mp)*M_PI/2);
	return std::complex<double>(0.0,0.0);
}

__host__
std::complex<double> v(const double &beta,const uint & L){
	double f = 1.0/(2.0*(double)L-1);
	std::complex<double> sum(0.0,0.0);
	int m;
	for (int j = 0; j < 2*L-1;++j){
		m = j - ((int)L-1);
		sum += w(m)*std::exp(i*beta*(double)m);
	}
	return f*sum;
}


void McEwenNodes::getRotations(float4 * rot_qs, const uint& offset, const uint& vol, const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	McEwenNodes_Register * instance = &_instance_registry[gpu_index];
	float3 eulers;
	float4 q;
	size_t max_nodes = instance->h_max_nodes[0];
	for (uint r = 0; r < vol; r++){
		if (r < max_nodes){
			getRotationEulers(eulers, offset + r, gpu_index);
			euler_to_quat(&eulers, &q);
			rot_qs[r] = q;
		} else {
			//just assign identity
			rot_qs[r] = {1.0f,0.0f,0.0f,0.0f};
		}
	} 
}
std::vector<uint> McEwenNodes::refinement_from_level(const int& level){
	std::vector<uint> ret = {2,1,2};
	for (int i=0; i<level; i++){
		ret[0] *= 2;
		ret[2] *= 2;
		ret[1] = ((2*ret[1]-1)*3)/2 +1;
	}
	return ret;
}

void McEwenNodes::buildWeights(double * const & h_weights, uint *const& h_refinements, size_t *const& h_num_refinements){
	uint M,L,N;
	const double f0 = 4*M_PI*M_PI;
	double f1;
	double beta;
	size_t offset = 0;
	for (int r=0;r < h_num_refinements[0];r++){
		M = h_refinements[3*r];
		L = h_refinements[3*r + 1];
		N = h_refinements[3*r + 2];
		f1 = 1/((double)M * (double) N)*f0;
		for (int b = 0; b < L; b++){
			beta = (2*((double)b)+1)*M_PI/(2*(double)L - 1);
			if (b == L-1){
				h_weights[offset + b] = std::real(f1*v(beta,L));
			} else {
				h_weights[offset + b] = std::real(f1*(v(beta,L) + v(2*M_PI-beta,L)));
			}
		}
		offset += L;
	}
}

int McEwenNodes::getMaximumNodes(int gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	McEwenNodes_Register * instance = &_instance_registry[gpu_index];
	int ret;
	int num_refs = instance->h_num_refinements[0]-1;
	ret = instance->h_refinements[3*num_refs]*instance->h_refinements[3*num_refs + 1]*instance->h_refinements[3*num_refs + 2];
	return ret;
}

void McEwenNodes::getRotationIndex(uint3 & abg,const uint& r,const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	McEwenNodes_Register * instance = &_instance_registry[gpu_index];
	uint L = instance->h_refinements[1];
    uint N = instance->h_refinements[2];
    abg.x = r/(L*N);
    abg.z = (r - abg.x*L*N)/L;
    abg.y =  r - abg.x*L*N - abg.z*L;	
}

void McEwenNodes::quadrature(float * h_values,float & result,const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	McEwenNodes_Register * instance = &_instance_registry[gpu_index];
	size_t max_nodes = instance->h_max_nodes[0];
	result = 0.0f;
	uint L = instance->h_refinements[1];
	uint N = instance->h_refinements[2];
	uint3 abg;
	for (int r=0; r < max_nodes; ++r){
    	abg.x = r/(L*N);
    	abg.z = (r - abg.x*L*N)/L;
    	abg.y =  r - abg.x*L*N - abg.z*L;	
		result += instance->h_weights[abg.y]*h_values[r];
	} 
}	

void McEwenNodes::getRotationEulers(float3 & eulers,const uint& r,const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	McEwenNodes_Register * instance = &_instance_registry[gpu_index];
	uint3 abg;
 	getRotationIndex(abg, r, gpu_index);
        uint M = instance->h_refinements[0];
        uint L = instance->h_refinements[1];
        uint N = instance->h_refinements[2];
        //range from 0..M-1,0..L-1,0..N-1
        eulers = {2.0f*((float) M_PI)*(((float) abg.x)/((float) M)),
                       ((float) M_PI)*(2.0f*((float) abg.y) + 1.0f)/(2.0f*((float) L)-1.0f),
                  2.0f*((float) M_PI)*(((float) abg.z)/((float) N))};	
}

void McEwenNodes::createInstance(const uint3 & MLN, int gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		std::vector<uint> refinements(3);
		McEwenNodes_Register instance;
		instance.h_max_nodes = (size_t *) malloc(sizeof(*instance.h_max_nodes));
		instance.h_num_refinements = (size_t *) malloc(sizeof(*instance.h_num_refinements));
		CudaSafeCall(cudaMalloc((void **)&instance.d_num_refinements,sizeof(*instance.d_num_refinements)));
		CudaCheckError();
		instance.h_refinements = (uint *) malloc(3*refinements.size()*sizeof(*instance.h_refinements));
		CudaSafeCall(cudaMalloc((void **) &instance.d_refinements,3*refinements.size()*sizeof(*instance.d_refinements)));
		size_t num_weights = MLN.y;
		instance.h_weights = (double *) malloc(num_weights*sizeof(*instance.h_weights));
		CudaSafeCall(cudaMalloc((void **)&instance.d_weights,num_weights*sizeof(*instance.d_weights)));

		instance.h_num_refinements[0] = 1;
		CudaSafeCall(cudaMemcpy(instance.d_num_refinements,instance.h_num_refinements,sizeof(instance.d_refinements),cudaMemcpyHostToDevice));
		instance.h_refinements[0] = MLN.x;
		instance.h_refinements[1] = MLN.y;
		instance.h_refinements[2] = MLN.z;
		CudaSafeCall(cudaMemcpy(instance.d_refinements,instance.h_refinements,3*instance.h_num_refinements[0]*sizeof(*instance.d_refinements),cudaMemcpyHostToDevice));
		buildWeights(instance.h_weights,instance.h_refinements,instance.h_num_refinements);
		CudaSafeCall(cudaMemcpy(instance.d_weights,instance.h_weights,num_weights*sizeof(*instance.d_weights),cudaMemcpyHostToDevice));

		instance.h_max_nodes[0] = vol(MLN);
		printf("Created McEwenNodes with refinements: %u %u %u, %u\n", MLN.x, MLN.y, MLN.z,instance.h_max_nodes[0]);
		_instance_registry[gpu_index] = instance;
	}
}

void McEwenNodes::createInstance(const int & refinement_level,int gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		std::vector<uint> refinements = McEwenNodes::refinement_from_level(refinement_level);
		McEwenNodes_Register instance;
		instance.h_max_nodes = (size_t *) malloc(sizeof(*instance.h_max_nodes));
		instance.h_num_refinements = (size_t *) malloc(sizeof(*instance.h_num_refinements));
		CudaSafeCall(cudaMalloc((void **)&instance.d_num_refinements,sizeof(*instance.d_num_refinements)));
		instance.h_refinements = (uint *) malloc(3*refinements.size()*sizeof(*instance.h_refinements));
		CudaSafeCall(cudaMalloc((void **) &instance.d_refinements,3*refinements.size()*sizeof(*instance.d_refinements)));
		size_t num_weights = 0;
		for (int r = 0; r < refinements.size()/3; r++){
			num_weights += refinements[3*r+1];
		}
		instance.h_weights = (double *) malloc(num_weights*sizeof(*instance.h_weights));
		CudaSafeCall(cudaMalloc((void **)&instance.d_weights,num_weights*sizeof(*instance.d_weights)));

		instance.h_num_refinements[0] = refinements.size()/3;
		if (refinement_level == 0) instance.h_num_refinements[0] = 1;
		CudaSafeCall(cudaMemcpy(instance.d_num_refinements,instance.h_num_refinements,sizeof(instance.d_refinements),cudaMemcpyHostToDevice));
		if (refinement_level > 0){
			for (int i = 0; i<refinements.size(); i++) instance.h_refinements[i] = refinements[i];
		} else {
			for (int i = 0; i<3; i++) instance.h_refinements[i] = 1;
		}
		CudaSafeCall(cudaMemcpy(instance.d_refinements,instance.h_refinements,3*instance.h_num_refinements[0]*sizeof(*instance.d_refinements),cudaMemcpyHostToDevice));
		buildWeights(instance.h_weights,instance.h_refinements,instance.h_num_refinements);
		CudaSafeCall(cudaMemcpy(instance.d_weights,instance.h_weights,num_weights*sizeof(*instance.d_weights),cudaMemcpyHostToDevice));

		size_t num_refs_m = instance.h_num_refinements[0]-1;
		instance.h_max_nodes[0] = instance.h_refinements[3*num_refs_m]*instance.h_refinements[3*num_refs_m + 1]*instance.h_refinements[3*num_refs_m + 2];
		printf("Created McEwenNodes with max refinement: %u %u %u, %u\n", instance.h_refinements[3*num_refs_m],instance.h_refinements[3*num_refs_m + 1],instance.h_refinements[3*num_refs_m + 2],instance.h_max_nodes[0]);
		_instance_registry[gpu_index] = instance;
	}
}
