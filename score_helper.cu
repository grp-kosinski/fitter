

/*
 * score.h
 *
 *  Created on: Aug 18, 2021
 *      Author: kkarius
 */


#include<score_helper.h>
#include<Density.h>
#include<McEwenNodes.h>
#include<ChebychevNodes.h>

__device__
float squaref(float & f){
	return f*f;
}

__device__
float idf(float & f){
	return f;
}

__device__
float entf(float & f){
	if (f <= 0.0f) return 0.0f;
	return -f*logf(f);
}

__host__ __device__
void eulers_to_memory(float3 * const& eulers,float * const& coeff){
	float3 cosa = {cos(eulers->x),cos(eulers->y),cos(eulers->z)};
	float3 sina = {sin(eulers->x),sin(eulers->y),sin(eulers->z)};
	//euler Z1Y2Z3 --> matrix (https://en.wikipedia.org/wiki/Euler_angles)
	// 0 1 2
	// 3 4 5 --> 0 1 2 3 4 5 6 7 8
	// 6 7 8
	coeff[0] = cosa.x*cosa.y*cosa.z - sina.x*sina.z;
	coeff[1] = -cosa.z*sina.x - cosa.x*cosa.y*sina.z;
	coeff[2] = cosa.x*sina.y;
	coeff[3] = cosa.x*sina.z + cosa.y*cosa.z*sina.x;
	coeff[4] = cosa.x*cosa.z - cosa.y*sina.x*sina.z;
	coeff[5] = sina.x*sina.y;
	coeff[6] = -cosa.z*sina.y;
	coeff[7] = sina.y*sina.z;
	coeff[8] = cosa.y;
}

__host__ __device__
void eulers_to_memory_inverse(float3 * const& eulers,float * const& coeff){
	float3 cosa = {cos(eulers->x),cos(eulers->y),cos(eulers->z)};
	float3 sina = {sin(eulers->x),sin(eulers->y),sin(eulers->z)};
	//euler Z1Y2Z3 --> matrix (https://en.wikipedia.org/wiki/Euler_angles)
	//additionally: transpose to inverse (orthogonal matrix)
	// 0 1 2
	// 3 4 5 --> 0 1 2 3 4 5 6 7 8
	// 6 7 8
	coeff[0] = cosa.x*cosa.y*cosa.z - sina.x*sina.z;
	coeff[3] = -cosa.z*sina.x - cosa.x*cosa.y*sina.z;
	coeff[6] = cosa.x*sina.y;
	coeff[1] = cosa.x*sina.z + cosa.y*cosa.z*sina.x;
	coeff[4] = cosa.x*cosa.z - cosa.y*sina.x*sina.z;
	coeff[7] = sina.x*sina.y;
	coeff[2] = -cosa.z*sina.y;
	coeff[5] = sina.y*sina.z;
	coeff[8] = cosa.y;
}

__device__
void rotation_to_memory(float * const& coeff, const uint3& abg,uint *const& refinements,const int& s){
	//after "A novel sampling theorem on the rotation group" - McEwen, Wiaux
	//all conventions taken from their papers/corresponding codebase
	uint M = refinements[3*s];
	uint L = refinements[3*s+1];
	uint N = refinements[3*s+2];
	//range from 0..M-1,0..L-1,0..N-1
	// coeffs must be 9 floats of memory
	float3 eulers = {2.0f*((float) M_PI)*(__uint2float_rn(abg.x)/__uint2float_rn(M)),
						  ((float) M_PI)*(2.0f*__uint2float_rn(abg.y) + 1.0f)/(2.0f*__uint2float_rn(L)-1.0f),
					 2.0f*((float) M_PI)*(__uint2float_rn(abg.z)/__uint2float_rn(N))};
	eulers_to_memory(&eulers,coeff);
}

__device__
void rotation_to_memory_inverse(float * const& coeff, const uint3& abg,uint *const& refinements){
	//after "A novel sampling theorem on the rotation group" - McEwen, Wiaux
	//all conventions taken from their papers/corresponding codebase
	uint M = refinements[0];
	uint L = refinements[1];
	uint N = refinements[2];
	//range from 0..M-1,0..L-1,0..N-1
	// coeffs must be 9 floats of memory
	float3 eulers = {2.0f*((float) M_PI)*(__uint2float_rn(abg.x)/__uint2float_rn(M)),
			 1.0f*((float) M_PI)*(2.0f*__uint2float_rn(abg.y) + 1.0f)/(2.0f*__uint2float_rn(L)-1.0f),
			 2.0f*((float) M_PI)*(__uint2float_rn(abg.z)/__uint2float_rn(N))};
	//eulers = {0.0f,0.0f,0.0f};
	eulers_to_memory_inverse(&eulers,coeff);
}


__device__
void rotation_to_memory(float * const& coeff, const uint3& abg,uint *const& refinements){
	//after "A novel sampling theorem on the rotation group" - McEwen, Wiaux
	//all conventions taken from their papers/corresponding codebase
	uint M = refinements[0];
	uint L = refinements[1];
	uint N = refinements[2];
	//range from 0..M-1,0..L-1,0..N-1
	// coeffs must be 9 floats of memory
	float3 eulers = {2.0f*((float) M_PI)*(__uint2float_rn(abg.x)/__uint2float_rn(M)),
						  ((float) M_PI)*(2.0f*__uint2float_rn(abg.y) + 1.0f)/(2.0f*__uint2float_rn(L)-1.0f),
					 2.0f*((float) M_PI)*(__uint2float_rn(abg.z)/__uint2float_rn(N))};
	eulers_to_memory(&eulers,coeff);
}

__device__
void translation_to_memory(float *const& transformation, float *const& d_trans_nodes,
		uint3 *const& t_node_dim, int trans_id){
	uint3 trans_index;

	trans_index.z = trans_id/(t_node_dim->x*t_node_dim->y);
	trans_index.y = (trans_id - trans_index.z*t_node_dim->x*t_node_dim->y)/t_node_dim->x;
	trans_index.x = trans_id - trans_index.y*t_node_dim->x - trans_index.z*t_node_dim->x*t_node_dim->y;
	transformation[0] = d_trans_nodes[trans_index.x];
	transformation[1] = d_trans_nodes[trans_index.y + t_node_dim->x];
	transformation[2] = d_trans_nodes[trans_index.z + t_node_dim->x + t_node_dim->y];
}

__device__
void translation_to_memory_inverse(float *const& transformation, float *const& d_trans_nodes,
		uint3 *const& t_node_dim, int trans_id){
	uint3 trans_index;

	trans_index.z = trans_id/(t_node_dim->x*t_node_dim->y);
	trans_index.y = (trans_id - trans_index.z*t_node_dim->x*t_node_dim->y)/t_node_dim->x;
	trans_index.x = trans_id - trans_index.y*t_node_dim->x - trans_index.z*t_node_dim->x*t_node_dim->y;
	transformation[0] = - d_trans_nodes[trans_index.x];
	transformation[1] = - d_trans_nodes[trans_index.y + t_node_dim->x];
	transformation[2] = - d_trans_nodes[trans_index.z + t_node_dim->x + t_node_dim->y];
}
__device__
void translate(float3 & node_rt,const float3 & node_r,float3 *const& scale, float *const& translation){
	node_rt.x = scale->x*node_r.x + translation[0];
	node_rt.y = scale->y*node_r.y + translation[1];
	node_rt.z = scale->z*node_r.z + translation[2];
}

__device__
void translate(float3 & node_rt,const float3 & node_r, float *const& translation){
	node_rt.x = node_r.x + translation[0];
	node_rt.y = node_r.y + translation[1];
	node_rt.z = node_r.z + translation[2];
}

__device__
void rotate(float3 & node_r,const float3 & node, float *const& rotation){
//	node_r = node; //use this to not rotate at all
	node_r.x = rotation[0]*node.x + rotation[1]*node.y + rotation[2]*node.z;
	node_r.y = rotation[3]*node.x + rotation[4]*node.y + rotation[5]*node.z;
	node_r.z = rotation[6]*node.x + rotation[7]*node.y + rotation[8]*node.z;
}

__host__ __device__
void rot_index(uint *const& refinements, const size_t& num_refinements, uint3 & abg, uint *const& rot_linear_index,
		const int& r, const int &s){
	uint L_c = refinements[3*s+1];
	uint N_c = refinements[3*s+2];
	uint L_m = refinements[3*(num_refinements-1)+1];
	uint N_m = refinements[3*(num_refinements-1)+2];
	int n = num_refinements - s - 1;
        abg.x = r/(L_c*N_c);
	abg.z = (r - abg.x*L_c*N_c)/L_c;
	abg.y =  r - abg.x*L_c*N_c - abg.z*L_c;
	rot_linear_index[0] =abg.x*ipow(2,n)*L_m*N_m + abg.z*ipow(2,n)*L_m + abg.y*ipow(3,n) - (1-ipow(3,n))/2;
}

__host__ __device__
void rot_index(uint *const& refinements, uint3 & abg, uint *const& rot_linear_index, const int& r){
	uint L = refinements[1];
	uint N = refinements[2];
        abg.x = r/(L*N);
	abg.z = (r - abg.x*L*N)/L;
	abg.y =  r - abg.x*L*N - abg.z*L;
	rot_linear_index[0] =abg.x*L*N + abg.z*L + abg.y;
}

__host__ __device__
void rot_index(uint *const& refinements, uint3 & abg, const uint& r){
	uint L = refinements[1];
	uint N = refinements[2];
    	abg.x = r/(L*N);
	abg.z = (r - abg.x*L*N)/L;
	abg.y =  r - abg.x*L*N - abg.z*L;
}

//template <typename T>
//__global__
//void initValue(T * d_target, size_t vol, T value){
//	size_t grid_size = blockDim.x*gridDim.x;
//	int tid = blockDim.x*blockIdx.x + threadIdx.x;
//	while (tid < vol ){
//		d_target[tid] = value;
//		tid += grid_size;
//	}
//}

void index_to_transformation(Density *const& target, ChebychevNodes *const& trans_nodes, McEwenNodes *const& rot_nodes,
		int s, const size_t& index, float3 * const& translation, float3 * const& eulers, const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * density_instance_target = &target->_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = &trans_nodes->_instance_registry[gpu_index];
	McEwenNodes_Register * rot_nodes_instance = &rot_nodes->_instance_registry[gpu_index];
	uint rotation_vol = rot_nodes_instance->h_max_nodes[0];
	uint3 * t_node_dim = trans_nodes_instance->h_node_dim;
	uint translation_index = index/rotation_vol;
	int rotation_index = index - translation_index*rotation_vol;
	uint rot_linear_index;
	uint3 abg;
	uint3 trans_index;
	trans_index.z = translation_index/(t_node_dim->x*t_node_dim->y);
	trans_index.y = (translation_index - trans_index.z*t_node_dim->x*t_node_dim->y)/t_node_dim->x;
	trans_index.x = translation_index - trans_index.y*t_node_dim->x - trans_index.z*t_node_dim->x*t_node_dim->y;
	float3 tshift;
	tshift = {trans_nodes_instance->h_node_list[trans_index.x]*density_instance_target->h_coord_dim[0].x,
			  trans_nodes_instance->h_node_list[trans_index.y]*density_instance_target->h_coord_dim[0].y,
			  trans_nodes_instance->h_node_list[trans_index.z]*density_instance_target->h_coord_dim[0].z};
//	printf("OUT: %f %f %f --> %f %f %f\n",trans_nodes_instance->h_node_list[trans_index.x],trans_nodes_instance->h_node_list[trans_index.y],trans_nodes_instance->h_node_list[trans_index.z],
//			tshift.x,tshift.y,tshift.z);
	float3 cropoffset = {density_instance_target->h_crop_pixel_offset->x*density_instance_target->h_pixel_size[0],
						 density_instance_target->h_crop_pixel_offset->y*density_instance_target->h_pixel_size[0],
						 density_instance_target->h_crop_pixel_offset->z*density_instance_target->h_pixel_size[0]};
	translation[0] += tshift + cropoffset;
	rot_index(rot_nodes_instance->h_refinements, rot_nodes_instance->h_num_refinements[0], abg, &rot_linear_index,rotation_index, s);
	uint M = rot_nodes_instance->h_refinements[3*s];
	uint L = rot_nodes_instance->h_refinements[3*s+1];
	uint N = rot_nodes_instance->h_refinements[3*s+2];
	eulers[0] = {2.0f*((float) M_PI)*(((float) abg.x)/((float) M)),
			     ((float) M_PI)*(2.0f*((float) abg.y) + 1.0f)/(2.0f*((float)L)-1.0f),
				 2.0f*((float) M_PI)*(((float) abg.z)/((float)N))};
}
