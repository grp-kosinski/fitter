 /* Engine.h
 *
 *  Created on: Dec 15, 2020
 *      Author: kkarius
 */

#ifndef ENGINE_H_
#define ENGINE_H_

#include<string>
#include<functional>
#include<vector>
#include<queue>
#include<map>
#include<tuple>

#include<thread>
#include<mutex>
#include<condition_variable>

#include<chrono>
#include<iostream>
#include<cstdint>
#include<cstdio>

#include<util.h>

class Engine {
	typedef std::function<void(void)> fp_t;
	typedef std::function<void(int)> fp_t_gpu;
	typedef std::function<void(std::ofstream *,int)> fp_t_gpu_bench;
	typedef std::function<void(std::ofstream *,int)> fp_t_gpu_out;
public:
	Engine(std::string name, bool per_gpu);
	~Engine();
	void dispatch(const  fp_t_gpu& op);
	void dispatch(fp_t_gpu&& op);
	void dispatch(const fp_t_gpu_bench& op);
	void dispatch(fp_t_gpu_bench&& op);
	Engine(const Engine& rhs) = delete;
	Engine& operator=(const Engine& rhs) = delete;
	Engine(Engine&& rhs) = delete;
	Engine& operator=(Engine&& rhs) = delete;
	void benchmark_molmap(void);
	void estimateParameters(const int& num_gpus);
	void cam_benchmark(void);
	void print_flags(void);
	void prescreen_molmap_benchmark(void);
	void cam_search_test(char * test_mrc, const char * out_p, const int& t_ref_level, const int& r_ref_level, const int& s_ref_level, const int& num_tests, const size_t& translation_offset, const size_t& translation_end);
	void cam_entropy_parameter_search_calc_entropy(char * target_mrc, char * query_pdb, 
	const char * out_p, const float & target_threshold, const float& r_range,const float & r0, const float& delta_r,
	 const int& t_ref_level, const int& r_ref_level, const int& s_ref_level);
	void cam(const char * target_mrc, const char * query_pdb, const char * out_p, const float & target_threshold,
			const int& solution_num, const float & resolution,const int& t_ref_level, const int& r_ref_level, const int& s_ref_level);
	void camDescend(const char * target_mrc, const char * query_pdb, const char * out_p, const float & target_threshold,
			const float & resolution, const int& s_ref_level, const float& d, const float& epsilon, const int& M, const int & trans_S, const int & rot_S);
	void camFFTScan(const char * target_mrc, const char * query_pdb, const char * working_dir, float3 t_max, float4 q_max, uint3 rot_grid, float query_threshold, float resolution);
	void ovFFTScan(const char * target_mrc, const char * query_pdb, const char * working_dir, float3 t_max, float4 q_max, uint3 rot_grid, float query_threshold);
	void camEnt(uint3 rot_grid, const char * working_dir);
	void camImproved(const char * target_mrc, const char * query, const char * out_p, const float & target_threshold,
				const int& solution_num, const float & resolution, const uint& translation_offset, const uint& translation_end, const int& t_ref_level, const int& r_ref_level, const int& s_ref_level);
	void chamfer(const char * target_mrc, const char * query_pdb, const char * out_p, const float & resolution, const float & target_t0,
			const float & target_t1,const float & query_t0,const float & query_t1,const int& solution_num,const int& t_ref_level,
			const int& r_ref_level);
	void cam_entropy_parameter_search_molmap(char * query_pdb, const float& r_range,const float & r0, const float& delta_r, const float& pixel_size);
	std::vector<std::ofstream *> benchmarks;
	std::map<std::thread::id,bool> waitflags;
	bool check_flags(void);
	bool check_per_GPU_done(void);
	const char * test_dir = "molmap_benchmark";
	size_t get_workload(void){return q_.size();}
	void get_benchmark_molecular_map_pdbs();
	int _thread_count;
private:
	bool _main_task_finished = false;
	bool _per_gpu;
	std::map<std::thread::id,int> thread_to_gpu_;
	std::string name_;
	std::mutex lock_;
	std::vector<std::thread> threads_;
	std::map<int,std::queue<fp_t>> _per_gpu_q;
	std::map<std::thread::id,bool> _per_gpu_complete_flags;
	std::queue< fp_t_gpu_bench> q_;
	std::condition_variable cv_;
	bool quit_ = false;
	void dispatch_thread_handler(void);
	void dispatch_thread_handler_per_GPU(void);
};

#endif /* ENGINE_H_ */
