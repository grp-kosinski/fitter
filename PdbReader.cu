/*
 * CPdbReader.cpp
 *
 *  Created on: Feb 25, 2019
 *      Author: kkarius
 */

#include <PdbReader.h>
#include <util.h>

std::map<AtomType,float> atomic_number = {{AtomType::H,1.0},{AtomType::C,6.0},{AtomType::N,7.0},{AtomType::O,8.0},{AtomType::S,8.0},{AtomType::Se,34.0}};
std::map<std::string,AtomType> atom_symbol_to_type = {{"H",AtomType::H},{"C",AtomType::C},{"N",AtomType::N},{"O",AtomType::O},{"S",AtomType::S},{"Se",AtomType::Se}};

__host__ __device__
void eulers_to_memory(const float3 & eulers,float * const& coeff){
	float3 cosa = {cos(eulers.x),cos(eulers.y),cos(eulers.z)};
	float3 sina = {sin(eulers.x),sin(eulers.y),sin(eulers.z)};
	//euler Z1Y2Z3 --> matrix (https://en.wikipedia.org/wiki/Euler_angles)
	// 0 1 2
	// 3 4 5 --> 0 1 2 3 4 5 6 7 8
	// 6 7 8
	coeff[0] = cosa.x*cosa.y*cosa.z - sina.x*sina.z;
	coeff[1] = -cosa.z*sina.x - cosa.x*cosa.y*sina.z;
	coeff[2] = cosa.x*sina.y;
	coeff[3] = cosa.x*sina.z + cosa.y*cosa.z*sina.x;
	coeff[4] = cosa.x*cosa.z - cosa.y*sina.x*sina.z;
	coeff[5] = sina.x*sina.y;
	coeff[6] = -cosa.z*sina.y;
	coeff[7] = sina.y*sina.z;
	coeff[8] = cosa.y;
}

PdbReader::PdbReader(const char * path){
	if (file_exists(path)){
		std::ifstream ifs;
		ifs.open(path, std::ifstream::in);
		_path = path;
		count_atoms(ifs);
		ifs.close();
		_h_coordinates = (float4 *) malloc(atom_count * sizeof(*_h_coordinates));
		_h_coordinate_line_indexes = (int *) malloc(atom_count * sizeof(*_h_coordinate_line_indexes));
	}
}

PdbReader::~PdbReader() {}

void PdbReader::count_atoms(std::ifstream & ifs){
	std::string line;
	RecordType type;
	while (std::getline(ifs,line)){
		type = get_type(line);
		if (type == ATOM || type == HETATM) atom_count++;
	}
}

AtomType PdbReader::get_atom_type(std::string & line){
	std::regex re("\\s+");
	return atom_symbol_to_type[std::regex_replace(line.substr(76,78),re,"")];
}

void PdbReader::metrics(const char * fname,size_t & atom_count, float * const & bounding_box){
	std::ifstream ifs;
	ifs.open(fname, std::ifstream::in);
	std::string line;
	RecordType type;
	float4 current_atom;
	for (int i=0;i<3;i++){
		bounding_box[i] = FLT_MAX;
		bounding_box[i+3] = 0;
	}
	while (std::getline(ifs,line)){
		type = PdbReader::get_type(line);
		if (type == ATOM || type == HETATM) {
				atom_count++;
				current_atom = PdbReader::get_atom(line);
				for (int i=0;i<3;i++){
					bounding_box[i] = std::min(bounding_box[i],reinterpret_cast<float *>(&current_atom)[i]);
					bounding_box[3+i] = std::max(bounding_box[3+i],reinterpret_cast<float *>(&current_atom)[i]);
				}
		}
	}
}

float4 PdbReader::get_atom(std::string & line){
	std::regex re("\\s+");
	float4 atom_coords;
	std::string coord_str;
	atom_coords.x = std::stof(std::regex_replace(line.substr(31,38),re,""));
	atom_coords.y = std::stof(std::regex_replace(line.substr(39,46),re,""));
	atom_coords.z = std::stof(std::regex_replace(line.substr(47,54),re,""));
	atom_coords.w = atomic_number[get_atom_type(line)];
	return atom_coords;
}

void PdbReader::readCoords(void){
	RecordType type;
	std::string line;
	std::ifstream ifs;
	ifs.open(_path, std::ifstream::in);
	int c = 0;
	float4 current_atom;
	while (std::getline(ifs,line)){
		type = get_type(line);
		std::vector<std::string> nline;
		if (type == ATOM || type == HETATM){
			current_atom = get_atom(line);
			_h_coordinates[c] = current_atom;
			c++;
			nline.push_back(line.substr(0,30));
			nline.push_back(line.substr(54,line.size()-1));
		} else {
			nline.push_back(line);
		}
		lines.push_back(nline);
	}
	atom_count = c;

	ifs.close();
}

void PdbReader::boundingBox(float3 & lower_left_front,float3 & upper_right_back){
	lower_left_front = {FLT_MAX,FLT_MAX,FLT_MAX};
	upper_right_back = {-FLT_MAX,-FLT_MAX,-FLT_MAX};
	float4 * coord;
	for (int c = 0; c < atom_count; ++c){
		coord = &_h_coordinates[c];
		lower_left_front.x = std::min(lower_left_front.x,coord->x);
		lower_left_front.y = std::min(lower_left_front.y,coord->y);
		lower_left_front.z = std::min(lower_left_front.z,coord->z);
		upper_right_back.x = std::max(upper_right_back.x,coord->x);
		upper_right_back.y = std::max(upper_right_back.y,coord->y);
		upper_right_back.z = std::max(upper_right_back.z,coord->z);
	}
}

void PdbReader::writeTransformed(const char * fname, const float3& rotation, const float3& translation){
	std::ofstream out_file;
	out_file.open(fname,std::ofstream::out);
	float3 lower_left_front;
	float3 upper_right_back;
	boundingBox(lower_left_front,upper_right_back);
//	printf("BBOX: %f %f %f -- %f %f %f\n",lower_left_front.x,lower_left_front.y,lower_left_front.z,upper_right_back.x,upper_right_back.y,upper_right_back.z);
	float3 center = 0.5*(upper_right_back + lower_left_front);
	int atom_line = 0;
	float * rot_coeffs = (float *) malloc(9*sizeof(*rot_coeffs));
	eulers_to_memory(rotation,rot_coeffs);
	float3 coord;
	float3 coord_r;
	for (std::vector<std::string> line: lines){
		if (line.size() == 1){
			out_file << line[0];
//			printf("%s\n",line[0].c_str());
		}
		else {
			std::string before = line[0];
			std::string after = line[1];
			coord = {_h_coordinates[atom_line].x,_h_coordinates[atom_line].y,_h_coordinates[atom_line].z};
			coord -= center;
			coord_r.x = rot_coeffs[0]*coord.x + rot_coeffs[1]*coord.y + rot_coeffs[2]*coord.z;
			coord_r.y = rot_coeffs[3]*coord.x + rot_coeffs[4]*coord.y + rot_coeffs[5]*coord.z;
			coord_r.z = rot_coeffs[6]*coord.x + rot_coeffs[7]*coord.y + rot_coeffs[8]*coord.z;
			coord_r += translation;
//			coord_r += center;
			std::string coords = string_format("%8.3f%8.3f%8.3f",coord_r.x,coord_r.y,coord_r.z);
			std::string sum = before+coords+after;
			out_file << sum;
//			printf("%s\n",sum.c_str());
			atom_line ++;
		}
		out_file << std::endl;
	}
	free(rot_coeffs);
	out_file.flush();
	out_file.close();
}

//STOLEN from Chimera
RecordType PdbReader::get_type(std::string line) {
	char rt[7];		// PDB record type
	int i;
	for (i = 0; line[i] != '\0' && line[i] != '\n' && i < 6; i += 1) {
		if (islower(line[i]))
			rt[i] = _toupper(line[i]);
		else
			rt[i] = line[i];
	}
	if (i < 6)
		for (; i < 6; i += 1)
			rt[i] = ' ';
	rt[6] = '\0';

	switch (rt[0]) {

	case 'A':
		switch (rt[1]) {
		case 'N':
			if (strcmp(rt + 2, "ISOU") == 0)
				return ANISOU;
			break;
		case 'T':
			if (strcmp(rt + 2, "OM  ") == 0)
				return ATOM;
			if (rt[4] == ' ' && rt[5] >= '1' && rt[5] <= '9')
				return (RecordType) (ATOM + (rt[5] - '0'));
			break;
		case 'U':
			if (strcmp(rt + 2, "THOR") == 0)
				return AUTHOR;
			break;
		}
		break;

	case 'C':;
		switch (rt[1]) {
		case 'A':
			if (strcmp(rt + 2, "VEAT") == 0)
				return CAVEAT;
			break;
		case 'I':
			if (strcmp(rt + 2, "SPEP") == 0)
				return CISPEP;
			break;
		case 'O':
			if (strcmp(rt + 2, "MPND") == 0)
				return COMPND;
			if (strcmp(rt + 2, "NECT") == 0)
				return CONECT;
			break;
		case 'R':
			if (strcmp(rt + 2, "YST1") == 0)
				return CRYST1;
			break;
		}
		break;

	case 'D':
		if (strcmp(rt + 1, "BREF ") == 0)
			return DBREF;
		if (strcmp(rt + 1, "BREF1") == 0)
			return DBREF1;
		if (strcmp(rt + 1, "BREF2") == 0)
			return DBREF2;
		break;

	case 'E':
		switch (rt[1]) {
		case 'N':
			if (strcmp(rt + 2, "D   ") == 0)
				return END;
			if (strcmp(rt + 2, "DMDL") == 0)
				return ENDMDL;
			break;
		case 'X':
			if (strcmp(rt + 2, "PDTA") == 0)
				return EXPDTA;
			break;
		}
		break;

	case 'F':
		switch (rt[1]) {
		case 'T':
			if (strcmp(rt + 2, "NOTE") == 0)
				return FTNOTE;
			break;
		case 'O':
			if (strcmp(rt + 2, "RMUL") == 0)
				return FORMUL;
			break;
		}
		break;

	case 'H':
		switch (rt[1]) {
		case 'E':
			if (strcmp(rt + 2, "TATM") == 0)
				return HETATM;
			if (strcmp(rt + 2, "ADER") == 0)
				return HEADER;
			if (strcmp(rt + 2, "T   ") == 0)
				return HET;
			if (strcmp(rt + 2, "TNAM") == 0)
				return HETNAM;
			if (strcmp(rt + 2, "TSYN") == 0)
				return HETSYN;
			if (strcmp(rt + 2, "LIX ") == 0)
				return HELIX;
			break;
		case 'Y':
			if (strcmp(rt + 2, "DBND") == 0)
				return HYDBND;
			break;
		}
		break;

	case 'J':
		if (strcmp(rt + 1, "RNL  ") == 0)
			return JRNL;
		break;

	case 'K':
		if (strcmp(rt + 1, "EYWDS") == 0)
			return KEYWDS;
		break;

	case 'L':
		if (strcmp(rt + 1, "INK  ") == 0)
			return LINK;
		break;

	case 'M':
		switch (rt[1]) {
		case 'A':
			if (strcmp(rt + 2, "STER") == 0)
				return MASTER;
			break;
		case 'D':
			if (strcmp(rt + 2, "LTYP") == 0)
				return MDLTYP;
			break;
		case 'O':
			if (strcmp(rt + 2, "DEL ") == 0)
				return MODEL;
			if (strcmp(rt + 2, "DRES") == 0)
				return MODRES;
			break;
		case 'T':
			if (strcmp(rt + 2, "RIX1") == 0 || strcmp(rt + 2, "RIX2") == 0
					|| strcmp(rt + 2, "RIX3") == 0)
				return MTRIX;
			break;
		}
		break;

	case 'N':
		switch (rt[1]) {
		case 'U':
			if (strcmp(rt + 2, "MMDL") == 0)
				return NUMMDL;
			break;
		}
		break;

	case 'O':
		switch (rt[1]) {
		case 'B':
			if (strcmp(rt + 2, "SLTE") == 0)
				return OBSLTE;
			break;
		case 'R':
			if (strcmp(rt + 2, "IGX1") == 0 || strcmp(rt + 2, "IGX2") == 0
					|| strcmp(rt + 2, "IGX3") == 0)
				return ORIGX;
			break;
		}
		break;

	case 'R':
		if (rt[1] != 'E')
			break;
		if (strcmp(rt + 2, "MARK") == 0)
			return REMARK;
		if (strcmp(rt + 2, "VDAT") == 0)
			return REVDAT;
		break;

	case 'S':
		switch (rt[1]) {
		case 'C':
			if (strcmp(rt + 2, "ALE1") == 0 || strcmp(rt + 2, "ALE2") == 0
					|| strcmp(rt + 2, "ALE3") == 0)
				return SCALE;
			break;
		case 'E':
			if (strcmp(rt + 2, "QRES") == 0)
				return SEQRES;
			if (strcmp(rt + 2, "QADV") == 0)
				return SEQADV;
			break;
		case 'H':
			if (strcmp(rt + 2, "EET ") == 0)
				return SHEET;
			break;
		case 'I':
			if (strcmp(rt + 2, "TE  ") == 0)
				return SITE;
			if (strcmp(rt + 2, "GATM") == 0)
				return SIGATM;
			if (strcmp(rt + 2, "GUIJ") == 0)
				return SIGUIJ;
			break;
		case 'L':
			if (strcmp(rt + 2, "TBRG") == 0)
				return SLTBRG;
			break;
		case 'O':
			if (strcmp(rt + 2, "URCE") == 0)
				return SOURCE;
			break;
		case 'P':
			if (strcmp(rt + 2, "RSDE") == 0)
				return SPRSDE;
			if (strcmp(rt + 2, "LIT ") == 0)
				return SPLIT;
			break;
		case 'S':
			if (strcmp(rt + 2, "BOND") == 0)
				return SSBOND;
			break;
		}
		break;

	case 'T':
		switch (rt[1]) {
		case 'E':
			if (strcmp(rt + 2, "R   ") == 0)
				return TER;
			break;
		case 'I':
			if (strcmp(rt + 2, "TLE ") == 0)
				return TITLE;
			break;
		case 'O':
			if (strcmp(rt + 2, "RSDO") == 0)
				return END;
			break;
		case 'U':
			if (strcmp(rt + 2, "RN  ") == 0)
				return TURN;
			break;
		case 'V':
			if (strcmp(rt + 2, "ECT ") == 0)
				return TVECT;
			break;
		}
		break;
//  like most of this function, not really necessary
//	case 'U':
//		if (rt[1] == 'S' && rt[2] == 'E' && rt[3] == 'R')
//			switch (pdbrunInputVersion) {
//			case 1: case 2: case 3: case 4: case 5:
//				return pdbrun5Type(line + 6);
//			case 6: case 7:
//				return pdbrun6Type(line + 6);
//			default:
//				// bootstrap by recognizing PDBRUN
//				if (strncasecmp(line + 6, "PDBRUN ", 7) == 0)
//					return USER_PDBRUN;
//				return USER;
//			}
//		break;
	}
	return UNKNOWN;
}

#ifdef PDBTOOLS
int main(int argc, char **argv){
	PdbReader reader(argv[1]);
	reader.readCoords();
	reader.writeTransformed("mytest.pdb",{M_PI_2,0,0},{0,0,0});
}
#endif
