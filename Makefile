
BOOST_INCLUDE = -I/usr/local/include/
CUB_INCLUDE = -I/data/tools/cub-1.8.0/
#EIGEN_INCLUDE = -I/data/tools/lorm/3rdparty/include/eigen3/
EIGEN_INCLUDE = -I/data/tools/eigen-eigen-323c052e1731/
CFLAGS = $(DEBUG) -I. -dc $(CUDA_INCLUDE) -g -G 
IMP_INCLUDE = -I/data/git/imp/bin/include/ -I/data/git/imp/build/src/dependency/RMF/include/
IMP_LINKS = -L/data/git/imp/build/lib/ -lRMF -limp_core -limp_rmf -limp_kernel -limp_atom -limp_algebra 

BOOST_LINKS = -L/usr/local/lib -lboost_unit_test_framework

ifndef CUDA_LIBS
CUDA_LIBS = /usr/local/cuda-10.0/lib64/
endif

CUDA_LINKS = -L$(CUDA_LIBS) -lcusolver -lcudart -lcuda -lcudadevrt

LFLAGS=-L. $(CUDA_LINKS) $(BOOST_LINKS) -g 

LINK_LORM = -L/data/tools/lorm/build/lib -llorm -lLORM_NFFTwrapper_obj
LINK_NFFT3 = -L/usr/local/lib -lnfft3

ifndef NVCC
NVCC = nvcc
endif

ifndef DEVICE_ARCH
DEVICE_ARCH = sm_61
endif

diffmap: CMrcReader.o
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --include-path=./ --device-c -DDIFFMAP Density.cu -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link Density.o CMrcReader.o --output-file link.o
	g++ Density.o CMrcReader.o link.o -o diffmap -pthread -lcudart -L/usr/local/cuda-10.0/lib64/ -lcudart -lcudadevrt 

embed: CMrcReader.o score_helper.o Density.o
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --include-path=./ --device-c -DEMBED Density.cu -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link Density.o CMrcReader.o score_helper.o --output-file link.o
	g++ Density.o CMrcReader.o score_helper.o link.o -o embed -pthread -lcudart -L/usr/local/cuda-10.0/lib64/ -lcudart -lcudadevrt 

transform: CMrcReader.o score_helper.o
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --include-path=./ --device-c -DTRANSFORM Density.cu -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link Density.o CMrcReader.o score_helper.o --output-file link.o
	g++ Density.o CMrcReader.o score_helper.o link.o -o transform -pthread -lcudart -L/usr/local/cuda-10.0/lib64/ -lcudart -lcudadevrt 

ent_test:
	$(NVCC) --gpu-architecture=sm_61 --include-path=./ --device-c -DENTTEST CamScore.cu McEwenNodes.cu ChebychevNodes.cu Stamp.cu DensityGenerator.cu PdbReader.cu Particles.cu Engine.cu Density.cu CMrcReader.cu  -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=sm_61 --device-link CamScore.o McEwenNodes.o ChebychevNodes.o DensityGenerator.o Stamp.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o  --output-file link.o
	g++ CamScore.o McEwenNodes.o ChebychevNodes.o Stamp.o DensityGenerator.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o link.o -o ent_test -pthread -lcudart -L/usr/local/cuda-10.0/lib64/ -lcudart -lcudadevrt

test_align:
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --include-path=./ $(IMP_INCLUDE) $(EIGEN_INCLUDE) -DTEST --device-c AlignmentProtocol.cu test.cu -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link AlignmentProtocol.o test.o --output-file link.o 
	g++ AlignmentProtocol.o test.o link.o -o test_align -lnvidia-ml -L/usr/local/cuda-10.0/lib64/ -lcudart -lcudadevrt -lcublas -lcusolver -pthread $(IMP_LINKS) 

align:
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --include-path=./ $(IMP_INCLUDE) $(EIGEN_INCLUDE) --device-c AlignmentProtocol.cu -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link AlignmentProtocol.o --output-file link.o 
	g++ AlignmentProtocol.o link.o -o align -lnvidia-ml -L/usr/local/cuda-10.0/lib64/ -lcudart -lcudadevrt -lcublas -lcusolver -pthread $(IMP_LINKS) 

align_cluster:
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --include-path=./ -DCLUSTER --device-c AlignmentProtocol.cu -g -G
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link AlignmentProtocol.o --output-file link.o 
	g++ AlignmentProtocol.o link.o -o align -lcudart -lcudadevrt -lcublas -lcusolver -pthread 

molmap:
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --include-path=./ --device-c -DMOLMAP Stamp.cu DensityGenerator.cu PdbReader.cu Particles.cu Engine.cu Density.cu CMrcReader.cu -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link Stamp.o DensityGenerator.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o  --output-file link.o
	g++ Stamp.o PdbReader.o DensityGenerator.o Particles.o Engine.o Density.o CMrcReader.o link.o -o molmap -pthread -lnvidia-ml -L/usr/local/cuda-10.0/lib64/ -lcudart -lcudadevrt -lcurl -L/usr/lib64/ -ljsoncpp

molmap_bench:
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --include-path=./ --device-c -DMOLMAPBENCH CamScore.cu ChebychevNodes.cu Stamp.cu DensityGenerator.cu PdbReader.cu Particles.cu Engine.cu Density.cu CMrcReader.cu  -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link CamScore.o ChebychevNodes.o Stamp.o DensityGenerator.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o  --output-file link.o
	g++ CamScore.o ChebychevNodes.o Stamp.o DensityGenerator.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o link.o -o molmap_bench -pthread -lnvidia-ml -L/usr/local/cuda-10.0/lib64/ -lcudart -lcudadevrt #CLUSTER COPROPHAGE SUPPORT -lcurl -L/usr/lib64/ -ljsoncpp

molmap_bench_hyde:
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --include-path=./ --device-c -DMOLMAPBENCH CamScore.cu ChebychevNodes.cu Stamp.cu DensityGenerator.cu PdbReader.cu Particles.cu Engine.cu Density.cu CMrcReader.cu  -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link CamScore.o ChebychevNodes.o DensityGenerator.o Stamp.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o  --output-file link.o
	g++ CamScore.o ChebychevNodes.o Stamp.o DensityGenerator.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o link.o -o molmap_bench -pthread -lcudart -lcudadevrt #CLUSTER COPROPHAGE SUPPORT -lcurl -L/usr/lib64/ -ljsoncpp

cam_fit:
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --include-path=./ --device-c -DFITCAM score_helper.cu CamScore.cu McEwenNodes.cu ChebychevNodes.cu Stamp.cu DensityGenerator.cu PdbReader.cu Particles.cu Engine.cu Density.cu CMrcReader.cu  -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link score_helper.o CamScore.o McEwenNodes.o ChebychevNodes.o DensityGenerator.o Stamp.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o  --output-file link.o
	g++ score_helper.o CamScore.o McEwenNodes.o ChebychevNodes.o Stamp.o DensityGenerator.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o link.o -o cam_fit -pthread -lcudart -L/usr/local/cuda-10.0/lib64/ -lcudart -lcudadevrt #-lcurl -L/usr/lib64/ -ljsoncpp

score_helper.o: score_helper.cu
	$(NVCC) $(CFLAGS) --gpu-architecture=$(DEVICE_ARCH) -o $@ $< -g -G -D_GLIBCXX_USE_CXX11_ABI=0

PdbReader.o: PdbReader.cu
	$(NVCC) $(CFLAGS) --gpu-architecture=$(DEVICE_ARCH) -o $@ $< -g -G -D_GLIBCXX_USE_CXX11_ABI=0

Particles.o: Particles.cu
	$(NVCC) $(CFLAGS) --gpu-architecture=$(DEVICE_ARCH) -o $@ $< -g -G -D_GLIBCXX_USE_CXX11_ABI=0

CMrcReader.o: CMrcReader.cu
	$(NVCC) $(CFLAGS) --gpu-architecture=$(DEVICE_ARCH) -o $@ $< -g -G -D_GLIBCXX_USE_CXX11_ABI=0

Density.o: Density.cu
	$(NVCC) $(CFLAGS) --gpu-architecture=$(DEVICE_ARCH) -o $@ $< -g -G -D_GLIBCXX_USE_CXX11_ABI=0

DensityGenerator.o: DensityGenerator.cu
	$(NVCC) $(CFLAGS) --gpu-architecture=$(DEVICE_ARCH) -o $@ $< -g -G -D_GLIBCXX_USE_CXX11_ABI=0

Stamp.o: Stamp.cu
	$(NVCC) $(CFLAGS) --gpu-architecture=$(DEVICE_ARCH) -o $@ $< -g -G -D_GLIBCXX_USE_CXX11_ABI=0

McEwenNodes.o: McEwenNodes.cu
	$(NVCC) $(CFLAGS) --gpu-architecture=$(DEVICE_ARCH) -o $@ $< -g -G -D_GLIBCXX_USE_CXX11_ABI=0

ChebychevNodes.o: ChebychevNodes.cu
	$(NVCC) $(CFLAGS) --gpu-architecture=$(DEVICE_ARCH) -o $@ $< -g -G -D_GLIBCXX_USE_CXX11_ABI=0

CamScoreImproved.o: CamScoreImproved.cu
	$(NVCC) $(CFLAGS) --gpu-architecture=$(DEVICE_ARCH) -o $@ $< -g -G -D_GLIBCXX_USE_CXX11_ABI=0

CamScoreDescend.o: CamScoreDescend.cu
	$(NVCC) $(CFLAGS) --gpu-architecture=$(DEVICE_ARCH) -o $@ $< -g -G -D_GLIBCXX_USE_CXX11_ABI=0

CamFFTScan.o: CamFFTScan.cu
	$(NVCC) $(CFLAGS) --gpu-architecture=$(DEVICE_ARCH) -o $@ $< -g -G -D_GLIBCXX_USE_CXX11_ABI=0

Engine.o: Engine.cu
	$(NVCC) $(CFLAGS) --gpu-architecture=$(DEVICE_ARCH) -o $@ $< -g -G -D_GLIBCXX_USE_CXX11_ABI=0

cam_descend: PdbReader.o Particles.o CMrcReader.o Density.o DensityGenerator.o Stamp.o score_helper.o CamScoreDescend.o  McEwenNodes.o ChebychevNodes.o Engine.o 
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) $(CFLAGS) --include-path=./ --device-c -DCAM_DESCEND Engine.cu -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link score_helper.o CamScoreDescend.o McEwenNodes.o ChebychevNodes.o DensityGenerator.o Stamp.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o  --output-file link.o
	g++ score_helper.o CamScoreDescend.o McEwenNodes.o ChebychevNodes.o Stamp.o DensityGenerator.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o link.o -o cam_descend -pthread $(CUDA_LINKS)

ov_fft: Engine.o PdbReader.o Particles.o CMrcReader.o Density.o DensityGenerator.o Stamp.o score_helper.o CamFFTScan.o  McEwenNodes.o ChebychevNodes.o 
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link score_helper.o Engine.o CamFFTScan.o McEwenNodes.o ChebychevNodes.o DensityGenerator.o Stamp.o PdbReader.o Particles.o Density.o CMrcReader.o --output-file link.o
	g++ score_helper.o Engine.o CamFFTScan.o McEwenNodes.o ChebychevNodes.o Stamp.o DensityGenerator.o PdbReader.o Particles.o Density.o CMrcReader.o link.o -o ov_fft -pthread $(CUDA_LINKS) -lcufft

cam_fft: Engine.o PdbReader.o Particles.o CMrcReader.o Density.o DensityGenerator.o Stamp.o score_helper.o CamFFTScan.o  McEwenNodes.o ChebychevNodes.o 
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link score_helper.o Engine.o CamFFTScan.o McEwenNodes.o ChebychevNodes.o DensityGenerator.o Stamp.o PdbReader.o Particles.o Density.o CMrcReader.o --output-file link.o
	g++ score_helper.o Engine.o CamFFTScan.o McEwenNodes.o ChebychevNodes.o Stamp.o DensityGenerator.o PdbReader.o Particles.o Density.o CMrcReader.o link.o -o cam_fft -pthread $(CUDA_LINKS) -lcufft

cam_ent: Engine.o PdbReader.o Particles.o CMrcReader.o Density.o DensityGenerator.o Stamp.o score_helper.o CamFFTScan.o  McEwenNodes.o ChebychevNodes.o 
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link score_helper.o Engine.o CamFFTScan.o McEwenNodes.o ChebychevNodes.o DensityGenerator.o Stamp.o PdbReader.o Particles.o Density.o CMrcReader.o --output-file link.o
	g++ score_helper.o Engine.o CamFFTScan.o McEwenNodes.o ChebychevNodes.o Stamp.o DensityGenerator.o PdbReader.o Particles.o Density.o CMrcReader.o link.o -o cam_ent -pthread $(CUDA_LINKS) -lcufft

cam_improved: PdbReader.o Particles.o CMrcReader.o Density.o DensityGenerator.o Stamp.o score_helper.o CamScoreImproved.o  McEwenNodes.o ChebychevNodes.o 
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) $(CFLAGS) --include-path=./ --device-c -DCAM_IMPROVED Engine.cu -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link score_helper.o CamScoreImproved.o McEwenNodes.o ChebychevNodes.o DensityGenerator.o Stamp.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o  --output-file link.o
	g++ score_helper.o CamScoreImproved.o McEwenNodes.o ChebychevNodes.o Stamp.o DensityGenerator.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o link.o -o cam_improved -pthread $(CUDA_LINKS)

search_test: PdbReader.o Particles.o CMrcReader.o Density.o DensityGenerator.o Stamp.o score_helper.o CamScoreImproved.o  McEwenNodes.o ChebychevNodes.o
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) $(CFLAGS) --include-path=./ --device-c -DSEARCH_TEST Engine.cu -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link score_helper.o CamScoreImproved.o McEwenNodes.o ChebychevNodes.o DensityGenerator.o Stamp.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o --output-file link.o
	g++ score_helper.o CamScoreImproved.o McEwenNodes.o ChebychevNodes.o Stamp.o DensityGenerator.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o link.o -o search_test -pthread $(CUDA_LINKS)	

map_range: Engine.o PdbReader.o Particles.o CMrcReader.o Density.o DensityGenerator.o Stamp.o score_helper.o 
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) $(CFLAGS) --include-path=./ --device-c -DMAPRANGE Engine.cu -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link DensityGenerator.o Stamp.o score_helper.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o --output-file link.o
	g++ Stamp.o DensityGenerator.o PdbReader.o Particles.o Engine.o Density.o score_helper.o CMrcReader.o link.o -o map_range -pthread $(CUDA_LINKS)

chamfer_fit:
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --include-path=./ --device-c -DFITCHAMFER score_helper.cu ChamferScore.cu McEwenNodes.cu ChebychevNodes.cu Stamp.cu DensityGenerator.cu PdbReader.cu Particles.cu Engine.cu Density.cu CMrcReader.cu  -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link score_helper.o ChamferScore.o McEwenNodes.o ChebychevNodes.o DensityGenerator.o Stamp.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o  --output-file link.o
	g++ score_helper.o ChamferScore.o McEwenNodes.o ChebychevNodes.o Stamp.o DensityGenerator.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o link.o -o chamfer_fit -pthread -lcudart -L/usr/local/cuda-10.0/lib64/ -lcudart -lcudadevrt # -lcurl -L/usr/lib64/ -ljsoncpp

cam_bench:
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --include-path=./ --device-c -DFIT_BENCH CamScore.cu McEwenNodes.cu ChebychevNodes.cu Stamp.cu DensityGenerator.cu PdbReader.cu Particles.cu Engine.cu Density.cu CMrcReader.cu  -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=$(DEVICE_ARCH) --device-link CamScore.o McEwenNodes.o ChebychevNodes.o DensityGenerator.o Stamp.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o  --output-file link.o
	g++ CamScore.o McEwenNodes.o ChebychevNodes.o Stamp.o DensityGenerator.o PdbReader.o Particles.o Engine.o Density.o CMrcReader.o link.o -o cam_bench -pthread -lcudart -L/usr/local/cuda-10.0/lib64/ -lcudart -lcudadevrt #-lcurl -L/usr/lib64/ -ljsoncpp

ccl:
	$(NVCC) --gpu-architecture=sm_61 --include-path=./ --device-c -DCCL Density.cu CMrcReader.cu Labeler.cu -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=sm_61 --device-link  Density.o CMrcReader.o Labeler.o  --output-file link.o
	g++ Density.o CMrcReader.o Labeler.o link.o -o ccl -pthread -lcudart -L/usr/local/cuda-10.0/lib64/ -lcudart -lcudadevrt
	
pdb:
	$(NVCC) --gpu-architecture=sm_61 --include-path=./ --device-c -DPDBTOOLS PdbReader.cu -g -G -D_GLIBCXX_USE_CXX11_ABI=0
	$(NVCC) --gpu-architecture=sm_61 --device-link  PdbReader.o  --output-file link.o
	g++ PdbReader.o link.o -o pdb -pthread -lcudart -L/usr/local/cuda-10.0/lib64/ -lcudart -lcudadevrt


