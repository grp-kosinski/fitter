/*
 * Engine.cu
 *
 *  Created on: Dec 15, 2020
 *      Author: kkarius
 */

#include<Engine.h>
#include<Particles.h>
#include<Density.h>
#include<DensityGenerator.h>
#ifdef FIT_BENCH
#include<curl/curl.h>
#include<jsoncpp/json/json.h>
#endif
#include<stdio.h>
#include<stdlib.h>
#include<chrono>
#include<ChebychevNodes.h>
#include<CamScore.h>
#include<CamFFTScan.h>
#include<ChamferScore.h>
#include<util.h>
#include<score_helper.h>

CamScore cam_score;
CamFFTScan fft_scan;
ChamferScore chamfer_score;
ChebychevNodes scan_nodes;
ChebychevNodes trans_nodes;
McEwenNodes rot_nodes;
Density query_density;
Density target_density;
Particles pdb;
DensityGenerator generator;
Stamp stamp;

float * h_global_mins;
float * h_global_maxs;
uint * h_global_max_indexes;
float * h_global_scores;
float * h_global_quads;
float * h_global_ents;
size_t * h_global_stride;
size_t * h_initial_offsets;
float h_translation_vol;
int * total_transformations_per_gpu;
uint rotation_vol;
int target_off_surface_volume;
uint total_chunks;
uint target_pixel_vol;
uint translation_vol;
uint translation_vol_per_gpu;
size_t * max_translation_vol_per_gpu;
size_t * max_transformation_vol_per_gpu;
uint total_transformations;
std::pair<float3,float3> trans_node_scale; 
uint gpu_num;
bool MRC=false;
bool PDB_OUT=true;

#ifdef OVFFTSCAN
int * Rs;
float * h_entropies;
void init_resources_ov_fft_scan(const char * target_file_name, const char * query_file_name, const char * working_dir,	float3 t_max, float4 q_max, uint3 rot_grid, float query_threshold, const int & gpu_index){
        fft_scan.createInstance(target_file_name, query_file_name, working_dir, t_max, q_max, rot_grid, query_threshold, gpu_index);
        fft_scan.initFFTOV(Rs, gpu_index);
}

void scan(const int& gpu_index){
	fft_scan.genDistsAndAvgsOV(Rs, gpu_num, gpu_index);
}

void Engine::ovFFTScan(const char * target_mrc, const char * query_pdb, const char * working_dir, float3 t_max, float4 q_max, uint3 rot_grid, float query_threshold){
	std::unique_lock<std::mutex> lock(lock_);
	lock.unlock();
	gpu_num = _thread_count;
    Rs = new int[gpu_num];
	h_entropies = new float[vol(rot_grid)];
	for (int g=0;g<_thread_count;g++){
		//void init_resources_cam_fft_scan(const char * target_file_name, const char * query_file_name, const char * working_dir,	float3 t_max, float4 q_max, uint3 rot_grid, float query_threshold, const int & gpu_index){
		auto init_f = std::bind(init_resources_ov_fft_scan, target_mrc, query_pdb, working_dir, t_max, q_max, rot_grid, query_threshold, g);
		{
			lock.lock();
			_per_gpu_q[g].push(init_f);
			lock.unlock();
		}
	}
	while (!check_per_GPU_done()){}
	
	for (int g=0;g<_thread_count;g++){
		auto scan_f = std::bind(scan, g);
		{
			lock.lock();
			_per_gpu_q[g].push(scan_f);
			lock.unlock();
		}
	}
	_main_task_finished = true;
}
#endif
#ifdef CAMFFTSCAN
int * Rs;
float * h_entropies;
void init_resources_cam_fft_scan(const char * target_file_name, const char * query_file_name, const char * working_dir,	float3 t_max, float4 q_max, uint3 rot_grid, float query_threshold, const int & gpu_index){
	fft_scan.createInstance(target_file_name, query_file_name, working_dir, t_max, q_max, rot_grid, query_threshold, gpu_index);
        fft_scan.initFFT(Rs, gpu_index);
}

void scan(const int& gpu_index){
	fft_scan.genDistsAndAvgs(Rs, gpu_num, gpu_index);
}

void Engine::camFFTScan(const char * target_mrc, const char * query_pdb, const char * working_dir, float3 t_max, float4 q_max, uint3 rot_grid, float query_threshold, float resolution){
	std::unique_lock<std::mutex> lock(lock_);
	lock.unlock();
	gpu_num = _thread_count;
    Rs = new int[gpu_num];
	h_entropies = new float[vol(rot_grid)];
	for (int g=0;g<_thread_count;g++){
		//void init_resources_cam_fft_scan(const char * target_file_name, const char * query_file_name, const char * working_dir,	float3 t_max, float4 q_max, uint3 rot_grid, float query_threshold, float resolution, const int & gpu_index){
		auto init_f = std::bind(init_resources_cam_fft_scan, target_mrc, query_pdb, working_dir, t_max, q_max, rot_grid, query_threshold, g);
		{
			lock.lock();
			_per_gpu_q[g].push(init_f);
			lock.unlock();
		}
	}
	while (!check_per_GPU_done()){}
	
	for (int g=0;g<_thread_count;g++){
		auto scan_f = std::bind(scan, g);
		{
			lock.lock();
			_per_gpu_q[g].push(scan_f);
			lock.unlock();
		}
	}
	_main_task_finished = true;
}
#endif
#ifdef CAMENT
__global__
void entropies(float * d_ncc, float * d_entropies, float rot_average, size_t N){ 
    __shared__ float concat[512];
    float * d_curr = d_ncc + N*blockIdx.x;
    int tid = threadIdx.x;
    float f;
    concat[tid] = 0.0f;
    while (tid < N){ 
	    f = d_curr[tid]/rot_average;
	    if (f > 0)
		    concat[threadIdx.x] += -f*logf(f);
		    tid += blockDim.x;
		}   
	__syncthreads();
	if (threadIdx.x < 256) { concat[threadIdx.x] += concat[threadIdx.x + 256];} __syncthreads();
	if (threadIdx.x < 128) { concat[threadIdx.x] += concat[threadIdx.x + 128];} __syncthreads();
	if (threadIdx.x <  64) { concat[threadIdx.x] += concat[threadIdx.x +  64];} __syncthreads();
	if (threadIdx.x <  32) { concat[threadIdx.x] += concat[threadIdx.x +  32];} __syncthreads();
	if (threadIdx.x <  16) { concat[threadIdx.x] += concat[threadIdx.x +  16];} __syncthreads();
	if (threadIdx.x <   8) { concat[threadIdx.x] += concat[threadIdx.x +   8];} __syncthreads();
	if (threadIdx.x <   4) { concat[threadIdx.x] += concat[threadIdx.x +   4];} __syncthreads();
	if (threadIdx.x <   2) { concat[threadIdx.x] += concat[threadIdx.x +   2];} __syncthreads();
	if (threadIdx.x <   1) { concat[threadIdx.x] += concat[threadIdx.x +   1];} __syncthreads();
	if (threadIdx.x == 0)
		d_entropies[blockIdx.x] = concat[0];
}

void calc_entropy(float * h_entropies, uint3 rot_dim, const char * working_dir, int & g){
    CudaSafeCall(cudaSetDevice(g));
	rot_nodes.createInstance(rot_dim,g);
	//calculateEntropy(float * h_entropies, uint3 rot_dim, const char * working_dir, const int& num_gpus    , const int& gpu_index)
	//fft_scan.calculateEntropy(h_entropies, rot_dim,working_dir,gpu_num,g);
	int R_total = vol(rot_dim);
    float * h_averages = new float[R_total];
    float * d_averages;
    CudaSafeCall(cudaMalloc((void **)&d_averages,R_total*sizeof(*d_averages)));
    float rotavg;
    //get rotational average
    std::string output_dir(working_dir);
    std::string avg_basename("averages_");
    std::vector<std::string> row;
    std::string line, word;
    uint3 ncc_vol = {199,199,199};
    int index;
    float c_avg;
	float uni = (float) ncc_vol.x*ncc_vol.y*ncc_vol.z;
    for (int g=0; g < gpu_num; g++){
    	std::string avg_fname = output_dir + std::string("/") + avg_basename + std::to_string(g) + std::string(".txt");
    	std::ifstream avg_in(avg_fname.c_str(),std::ofstream::in);
    	if (avg_in.is_open())
    	{
			while(getline(avg_in,line)){
				row.clear();
				stringstream str(line);
				while(getline(str, word, ',')) row.push_back(word);
				if (row.size() == 2){
					index = std::stoi(row[0]);
					c_avg = std::stof(row[1]);
					if (index < R_total) h_averages[index] = c_avg;
					//if (index < R_total) h_averages[index] = uni;
				}
			}
		}
	}
	rot_nodes.quadrature(h_averages, rotavg, g);
    printf("Rotaverage is: %f\n",rotavg);
	//CudaSafeCall(cudaMemcpy(d_averages, h_averages, R_total*sizeof(*d_averages),cudaMemcpyHostToDevice));
    size_t max_densities = 100; //not so good, should check
    float *h_workspace = new float[max_densities*vol(ncc_vol)];
	//for (int i = 0; i < max_densities*vol(ncc_vol); i++)h_workspace[i] = 1.0f;
    float *d_workspace;
    float *d_ents;
    CudaSafeCall(cudaMalloc((void **)&d_ents,max_densities*sizeof(*d_workspace)));
    CudaSafeCall(cudaMalloc((void **)&d_workspace,max_densities*vol(ncc_vol)*sizeof(*d_workspace)));
    //load single files
    int r_offset = 0;
    int R_global_stride = max_densities*gpu_num;
	int r_vol;
    for (int l = 0; l < g; l++)
		r_offset += max_densities;
	    std::string fname;
	    while (r_offset < R_total){
		    r_vol = std::min((int) max_densities,R_total - r_offset);
			//load file contents
	        for (int r = 0; r < r_vol; r++){
		        fname = output_dir + std::string("/ncc_")+ std::to_string(r_offset + r) + std::string(".mrc");
		        CMrcReader::writeTo(fname.c_str(),h_workspace + r*vol(ncc_vol));
			}
			printf("Loaded files from %i - %i\n",r_offset, r_offset + r_vol);
			CudaSafeCall(cudaMemcpy(d_workspace, h_workspace, r_vol*vol(ncc_vol)*sizeof(*d_workspace), cudaMemcpyHostToDevice));
			entropies<<<r_vol,512>>>(d_workspace, d_ents, rotavg, vol(ncc_vol));
            //entropies<<<r_vol,512>>>(d_workspace, d_entropies, d_averages, vol(ncc_vol));
	        cudaDeviceSynchronize();
	        CudaSafeCall(cudaMemcpy(h_entropies + r_offset, d_ents, r_vol * sizeof(*h_entropies), cudaMemcpyDeviceToHost));

		    r_offset += R_global_stride;
	}
	cudaDeviceSynchronize();
}

void Engine::camEnt(uint3 rot_grid, const char * working_dir){
	std::unique_lock<std::mutex> lock(lock_);
	lock.unlock();
	gpu_num = _thread_count;
	float * h_entropies = new float[vol(rot_grid)];
	for (int g=0;g<_thread_count;g++){
		//void init_resources_cam_fft_scan(const char * target_file_name, const char * query_file_name, const char * working_dir,	float3 t_max, float4 q_max, uint3 rot_grid, float query_threshold, const int & gpu_index){
		auto calc_f = std::bind(calc_entropy, h_entropies, rot_grid, working_dir, g);
		{
			lock.lock();
			_per_gpu_q[g].push(calc_f);
			lock.unlock();
		}
	}
	while (!check_per_GPU_done()){}
	_main_task_finished = true;
	std::string sep;
	std::string fn = working_dir + std::string("/entropies.txt");
	std::ofstream fh(fn.c_str(),std::ofstream::out);
	for (int r=0; r < vol(rot_grid); r++){
		fh << sep << h_entropies[r];
		sep = std::string(",");
	}
	fh.close();
	float entropy;
	rot_nodes.quadrature(h_entropies,entropy,0);	
	printf("Total entropy: %f\n",entropy);
	_main_task_finished = true;
}
#endif

#if defined(CAM_DESCEND)
void init_query(const int& gpu_index){
	if (!MRC){
		uint3 query_pixel_dim;
		generator.generateMolecularDensity(&query_density,&pdb,&stamp,24,256,gpu_index);
		query_density.createTexture(gpu_index);
		query_density.getPixelDim(query_pixel_dim,gpu_index);
		float3 query_pixel_dimf = {(float) query_pixel_dim.x,(float) query_pixel_dim.y,(float) query_pixel_dim.z};
		std::pair<float3,float3> scan_node_scale = {-0.5f*query_pixel_dimf,query_pixel_dimf};
		scan_nodes.scale(scan_node_scale,gpu_index);
		query_density.setZeroIfAbove(2.48f,gpu_index);
		query_density.toMrc("generated_density.mrc",0,gpu_index);
	}
}
#include<CamScoreDescend.h>
CamScoreDescend cam_score_descend;
void descend_cam(const int& gpu_index){
	//Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes, size_t *const& max_transformations_per_gpu, const int & gpu_num, const int & gpu_index)
	cam_score_descend.descend(&target_density, &query_density, &scan_nodes, max_transformation_vol_per_gpu, gpu_num,gpu_index);
}

void init_resources_cam_descend(const char * target_file_name, const char * query_file_name, const float & target_threshold,
	float sigma, const int& scan_refinement_level, const float & d, const float &epsilon, const int& M, const int& trans_S, const int & rot_S, const int & gpu_index){
	float pixel_size;
	uint3 query_pixel_dim;
	uint3 target_pixel_dim;
	float c = 0.8f;
	target_density.createInstanceFromMrc(target_file_name,target_threshold,gpu_index);
	target_density.getPixelSize(pixel_size,gpu_index);
	target_density.createTexture(gpu_index);
//TODO: make parameter
	target_density.setZeroIfAbove(0.0127f,gpu_index);
	target_density.toMrc("target_crop.mrc",0,gpu_index);
	std::string query_fn(query_file_name);
	target_density.getPixelDim(target_pixel_dim,gpu_index);
	float3 target_pixel_dimf = {(float) target_pixel_dim.x,(float) target_pixel_dim.y,(float) target_pixel_dim.z};
	//scale requires offset in float pixel dim first and volume in total second. 
	trans_node_scale = {0.5*(1-c)*target_pixel_dimf,c*target_pixel_dimf};
	scan_nodes.createInstance(scan_refinement_level,gpu_index);
	if (query_fn.substr(query_fn.size()-3,query_fn.size()) == std::string("pdb"))
		MRC = false;
	if (!MRC){
		int reader_index = pdb.fromPdb(query_file_name);
		pdb.createInstanceFromReader(gpu_index,reader_index);
		generator.createInstanceFromParticles(&pdb,gpu_index);
		pdb.find_bounds(gpu_index);
		query_density.createInstanceFromBounds(pdb.volume(gpu_index),pixel_size,0,gpu_index);
	} else {
		query_density.createInstanceFromMrc(query_file_name,0.0f,gpu_index);
		query_density.createTexture(gpu_index);
		query_density.getPixelDim(query_pixel_dim,gpu_index);
		float3 query_pixel_dimf = {(float) query_pixel_dim.x,(float) query_pixel_dim.y,(float) query_pixel_dim.z};
		std::pair<float3,float3> scan_node_scale = {-0.5f*query_pixel_dimf,query_pixel_dimf};
		scan_nodes.scale(scan_node_scale,gpu_index);
	}
	cam_score_descend.createInstance(trans_node_scale,d,epsilon,M,max_transformation_vol_per_gpu, trans_S, rot_S, gpu_index);
	stamp.createInstance(sigma,pixel_size,gpu_index);
}

void Engine::camDescend(const char * target_mrc, const char * query_pdb, const char * out_p, const float & target_threshold,
	const float & resolution, const int& s_ref_level, const float& d, const float& epsilon, const int& M,const int & trans_S, const int& rot_S){
	std::unique_lock<std::mutex> lock(lock_);
	lock.unlock();
	gpu_num = _thread_count;
	max_transformation_vol_per_gpu = (size_t *) malloc(gpu_num*sizeof(*max_translation_vol_per_gpu));
	for (int g=0;g<_thread_count;g++){
		//void init_resources_cam_descend(const char * target_file_name, const char * query_file_name, const float & target_threshold,
		//	float sigma, const int& scan_refinement_level, const float & d, const float &epsilon, const int & gpu_index){
		auto init_f = std::bind(init_resources_cam_descend, target_mrc, query_pdb, target_threshold, resolution, s_ref_level, d, epsilon, M, trans_S, rot_S, g);
		{
			lock.lock();
			_per_gpu_q[g].push(init_f);
			lock.unlock();
		}
	}

	while (!check_per_GPU_done()){}
	
	for (int g=0;g<_thread_count;g++){
		auto descend_f = std::bind(descend_cam,g);
		auto init_query_f = std::bind(init_query,g);
		{
			lock.lock();
			_per_gpu_q[g].push(init_query_f);
			_per_gpu_q[g].push(descend_f);
			lock.unlock();
		}
	}

	while (!check_per_GPU_done()){}

	_main_task_finished = true;
}
#endif

#if defined(CAM_IMPROVED) || defined(SEARCH_TEST)
#include<CamScoreImproved.h>
CamScoreImproved cam_score_improved;
void score_cam_improved(const int& gpu_index){
//	void CamScoreImproved::score(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes, float *const& h_scores,
//			float *const& h_min, float *const& h_max, const int& translation_volume_per_gpu,  const int& translation_volume_total, const int& gpu_num,
//			const int & gpu_index)
	//query_density.testTexture(gpu_index);
	cam_score_improved.score(&target_density,&query_density,&scan_nodes, h_global_scores, h_global_mins,h_global_maxs,h_global_max_indexes, translation_vol_per_gpu,translation_vol, gpu_num,gpu_index);
}

void init_resources_cam_improved(const char * target_file_name, const char * query_file_name, const float & target_threshold,
	float sigma, const int& trans_refinement_level, const int& rot_refinement_level, const int& scan_refinement_level,
	const int & gpu_index){
	float pixel_size;
	uint3 query_pixel_dim;
	uint3 target_pixel_dim;
	float c = 0.8f;
	target_density.createInstanceFromMrc(target_file_name,target_threshold,gpu_index);
	target_density.getPixelSize(pixel_size,gpu_index);
	target_density.createTexture(gpu_index);
	target_density.toMrc("target_crop.mrc",0,gpu_index);
	std::string query_fn(query_file_name);
	target_density.getPixelDim(target_pixel_dim,gpu_index);
	float3 target_pixel_dimf = {(float) target_pixel_dim.x,(float) target_pixel_dim.y,(float) target_pixel_dim.z};
	//scale requires offset in float pixel dim first and volume in total second. 
	std::pair<float3,float3> trans_node_scale = {0.5*(1-c)*target_pixel_dimf,c*target_pixel_dimf};
	trans_nodes.createInstance(trans_refinement_level,gpu_index);
	trans_nodes.scale(trans_node_scale,gpu_index);
	scan_nodes.createInstance(scan_refinement_level,gpu_index);
	translation_vol = trans_nodes.getVolume(gpu_index);
	rot_nodes.createInstance(rot_refinement_level,gpu_index);
	rotation_vol = rot_nodes.getMaximumNodes(gpu_index);
	total_transformations = translation_vol*rotation_vol;
	if (query_fn.substr(query_fn.size()-3,query_fn.size()) == std::string("pdb"))
		MRC = false;
	if (!MRC){
		int reader_index = pdb.fromPdb(query_file_name);
		pdb.createInstanceFromReader(gpu_index,reader_index);
		generator.createInstanceFromParticles(&pdb,gpu_index);
		pdb.find_bounds(gpu_index);
		query_density.createInstanceFromBounds(pdb.volume(gpu_index),pixel_size,0,gpu_index);
	} else {
		query_density.createInstanceFromMrc(query_file_name,0.0f,gpu_index);
		query_density.createTexture(gpu_index);
		query_density.getPixelDim(query_pixel_dim,gpu_index);
		float3 query_pixel_dimf = {(float) query_pixel_dim.x,(float) query_pixel_dim.y,(float) query_pixel_dim.z};
		std::pair<float3,float3> scan_node_scale = {-0.5f*query_pixel_dimf,query_pixel_dimf};
		scan_nodes.scale(scan_node_scale,gpu_index);
	}
	cam_score_improved.createInstance(&trans_nodes,&rot_nodes,max_translation_vol_per_gpu,gpu_index);
	stamp.createInstance(sigma,pixel_size,gpu_index);
}

void score_min(const size_t& translation_offset, const size_t& translation_end, const int& gpu_index){
//void CamScoreImproved::scoreAndMinChunks(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes, float *const& h_scores, float *const& h_min,
//                            float *const& h_max, uint *const& h_max_index, size_t * translation_volumes_per_gpu,const size_t& translation_volume_total, const int & gpu_num, const int & gpu_index)
	cam_score_improved.scoreAndMinChunks(&target_density,&query_density,&scan_nodes,h_global_scores,h_global_mins,h_global_maxs,h_global_max_indexes,max_translation_vol_per_gpu,translation_offset,translation_end,gpu_num,gpu_index);
}

void Engine::camImproved(const char * target_mrc, const char * query_pdb, const char * out_p, const float & target_threshold,
		const int& solution_num,const float & resolution,const uint & translation_offset, const uint& translation_end, const int& t_ref_level, const int& r_ref_level, const int& s_ref_level){
	std::unique_lock<std::mutex> lock(lock_);
	lock.unlock();
	total_transformations_per_gpu = (int *) malloc(sizeof(*total_transformations_per_gpu));
	int trans_node_resolution = ipow(2,t_ref_level);
	int total_translations = ipow(trans_node_resolution,3);
	uint3 rot_abg;
	uint3 trans_xyz;
	float3 trans_shift;
	float3 rot_eulers;
	gpu_num = _thread_count;
	for (int g=0;g<_thread_count;g++){
//		void init_resources_cam_improved(const char * target_file_name, const char * query_file_name, const float & target_threshold,
//				 float sigma, const int& trans_refinement_level, const int& rot_refinement_level, const int& scan_refinement_level,
//				 const int & gpu_index)
		auto init_f = std::bind(init_resources_cam_improved,target_mrc,query_pdb,target_threshold,resolution,t_ref_level,r_ref_level,s_ref_level,g);
		{
			lock.lock();
			_per_gpu_q[g].push(init_f);
			lock.unlock();
		}
	}

	while (!check_per_GPU_done()){}

	//global result allocations
 	h_global_scores = (float *) malloc(rotation_vol*translation_vol*sizeof(*h_global_scores));
 	h_global_mins = (float *) malloc(total_chunks*sizeof(*h_global_mins));//for distributions
 	h_global_maxs = (float *) malloc(translation_vol*sizeof(*h_global_maxs));//for scoring
 	h_global_max_indexes = (uint *) malloc(translation_vol*sizeof(*h_global_max_indexes));
	h_global_stride = (size_t *) malloc(sizeof(*h_global_stride));
	h_initial_offsets= (size_t *) malloc(_thread_count*sizeof(*h_global_stride));
	//determine workload per GPU
	size_t actual_volume = max_translation_vol_per_gpu[0];
	for (int g = 1;g< gpu_num;g++)
		actual_volume = std::min<size_t>(actual_volume,max_translation_vol_per_gpu[g]);
	translation_vol_per_gpu = std::min<size_t>(actual_volume,translation_vol/gpu_num);
	total_chunks = total_transformations/translation_vol_per_gpu + 1;
	printf("Total number of chunks: %u\n",total_chunks);
//	float translation_vol;
//
//
	for (int g=0;g<_thread_count;g++){
//		score_cam_improved(float * const & h_trans_vol, const int & translation_volume_total, const int& gpu_num, const int& gpu_index)
		auto score_f = std::bind(score_min,translation_offset,translation_end,g);
		auto init_query_f = std::bind(init_query,g);
		{
			lock.lock();
			_per_gpu_q[g].push(init_query_f);
			_per_gpu_q[g].push(score_f);
			lock.unlock();
		}
	}

	while (!check_per_GPU_done()){}
        std::vector<float> rot_maxima(h_global_maxs+translation_offset,h_global_maxs + translation_end);
        std::vector<float>::iterator max_trans = std::max_element(rot_maxima.begin(),rot_maxima.end());
	size_t max_trans_index = translation_offset + std::distance(rot_maxima.begin(),max_trans);

	rot_nodes.getRotationIndex(rot_abg,h_global_max_indexes[max_trans_index],0);
	rot_nodes.getRotationEulers(rot_eulers,h_global_max_indexes[max_trans_index],0);
	trans_nodes.getTranslationIndex(trans_xyz,max_trans_index,0);
	trans_nodes.getTranslation(trans_shift,max_trans_index,0);
	printf("Max translation:\t%i - %f\n",*max_trans,max_trans_index);
	printf("Rotation eulers: \t %f %f %f\n",rot_eulers.x,rot_eulers.y,rot_eulers.z);
	printf("Translation shift: \t %f %f %f\n",trans_shift.x,trans_shift.y,trans_shift.z);
	_main_task_finished = true;
}
#endif

//====================================================CAM_HELPERS==========================================
#ifdef FITCAM
void init_resources_cam(const char * target_file_name, const char * query_file_name, const float & target_threshold,
		float sigma, const int& trans_refinement_level, const int& rot_refinement_level, const int& scan_refinement_level,const int & gpu_index){
	target_density.createInstanceFromMrc(target_file_name,target_threshold,gpu_index);
	target_density.createTexture(gpu_index);
	float pixel_size;
	target_density.getPixelSize(pixel_size,gpu_index);
	std::string query_fn(query_file_name);
	if (!MRC){
		int reader_index = pdb.fromPdb(query_file_name);
		pdb.createInstanceFromReader(gpu_index,reader_index);
		generator.createInstanceFromParticles(&pdb,gpu_index);
		pdb.find_bounds(gpu_index);
		query_density.createInstanceFromBounds(pdb.volume(gpu_index),pixel_size,0,gpu_index);
	} else {
		query_density.createInstanceFromMrc(query_file_name,0.0f,gpu_index);
		query_density.createTexture(gpu_index);
	}
	uint3 query_pixel_dim;
	uint3 target_pixel_dim;
	query_density.getPixelDim(query_pixel_dim,gpu_index);
	target_pixel_dim = query_pixel_dim;
	float3 target_pixel_dimf = {(float) target_pixel_dim.x,(float) target_pixel_dim.y,(float) target_pixel_dim.z};
	float3 query_pixel_dimf = {(float) query_pixel_dim.x,(float) query_pixel_dim.y,(float) query_pixel_dim.z};
	float c = 0.8f;
	std::pair<float3,float3> scan_node_scale = {-0.5f*query_pixel_dimf,query_pixel_dimf};
	std::pair<float3,float3> trans_node_scale = {-0.5f*c*target_pixel_dimf,c*target_pixel_dimf};
	scan_nodes.createInstance(scan_refinement_level,gpu_index);
	scan_nodes.scale(scan_node_scale,gpu_index);
	trans_nodes.createInstance(trans_refinement_level,gpu_index);
	trans_nodes.scale(trans_node_scale,gpu_index);
	translation_vol = trans_nodes.getVolume(gpu_index);
	rot_nodes.createInstance(rot_refinement_level,gpu_index);
	rotation_vol = rot_nodes.getMaximumNodes(gpu_index);
	total_transformations = translation_vol*rotation_vol;
	//assume trans_node_vol and total_gpus shares a good number of factors of 2
	cam_score_improved.createInstance(&trans_nodes,&rot_nodes,max_translation_vol_per_gpu,gpu_index);
	stamp.createInstance(sigma,pixel_size,gpu_index);
}

void score_and_min(float * const & h_min, float * const & h_trans_vol, const int & translation_offset, const int & translation_volume, const int& gpu_index){
	cam_score.scoreAndMin(&target_density,&query_density,&scan_nodes,translation_offset,translation_volume,h_min,h_trans_vol,128,256,gpu_index);
}

void score_bench(float * const & h_trans_vol, const int & translation_offset, const int & translation_volume, const int& gpu_index){
	cam_score.scoreBench(&target_density,&query_density,&scan_nodes,translation_offset,translation_volume,h_trans_vol,128,256,gpu_index);
}

void score_cam(float * const & h_trans_vol, const int & translation_offset, const int & translation_volume, const int& gpu_index){
	cam_score.scoreSimple(&target_density,&query_density,&scan_nodes,translation_offset,translation_volume,h_trans_vol,128,256,gpu_index);
}

void shift_and_quad(float * const & h_min, float * const & h_quad,const int & translation_offset, const int & translation_volume,
		const int& gpu_index){
	cam_score.shiftAndQuad(&trans_nodes,translation_offset,translation_volume,h_min,h_quad,128,256,gpu_index);
}

void div_and_ent(float * const & h_quad, float * const & h_ent,const int & translation_offset, const int & translation_volume,
		const int& gpu_index){
	cam_score.divAndEnt(&trans_nodes,translation_offset,translation_volume,h_quad,h_ent,128,256,gpu_index);
}


void copy_to_host(float * const & h_scores,const int& rot_vol, const int& translations_per_gpu,const int& gpu_vol,const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	float * d_scores = cam_score._instance_registry[gpu_index].d_v_ave;
	int translation_stride = 4*rot_vol+3;
	for (int t = 0;t<translations_per_gpu;t++){
		CudaSafeCall(cudaMemcpy(h_scores+gpu_index*gpu_vol + t*rot_vol,
								 d_scores+t*translation_stride,rot_vol*sizeof(*d_scores),
								 cudaMemcpyDeviceToHost));
	}
	cudaDeviceSynchronize();
}

void Engine::cam(const char * target_mrc, const char * query_pdb, const char * out_p, const float & target_threshold,
		const int& solution_num,const float & resolution,const int& t_ref_level, const int& r_ref_level, const int& s_ref_level){
	gpu_num = _thread_count;
	std::unique_lock<std::mutex> lock(lock_);
	lock.unlock();
	//init necessary resources
	//void init_resources_search_test(const char * test_file_name, const float & target_threshold, const int& trans_refinement_level, const int& rot_refinement_level, const int& scan_refinement_level, const int & gpu_index)	
	for (int g=0;g < _thread_count;g++){
		auto init_f = std::bind(init_resources_search_test, test_mrc, t_ref_level,
		r_ref_level, s_ref_level,g);
		{
		 	lock.lock();
		 	_per_gpu_q[g].push(init_f);
		 	lock.unlock();
		}
	}
	while (!check_per_GPU_done()){}
	
	for (int n = 0; n < num_tests; n++){
		test_t = rand()%translation_vol;
		test_r = rand()%rotation_vol;
		rot_nodes.getRotationIndex(test_rot_index,test_r,0);
		rot_nodes.getRotationEulers(test_rot,test_r,0);
		trans_nodes.getTranslationIndex(test_trans_index,test_t,0);
		trans_nodes.getTranslation(test_trans,test_t,0);
		
		for (int g=0;g < _thread_count;g++){
			auto trans_f = std::bind(transform_to, test_t, test_r, g);
			//auto trans_f = std::bind(transform_to, 0, 18, g);
			{
			 	lock.lock();
			 	_per_gpu_q[g].push(trans_f);
			 	lock.unlock();
			}
		}
		while (!check_per_GPU_done()){}

 		//global result allocations
 		h_global_scores = (float *) malloc(rotation_vol*translation_vol*sizeof(*h_global_scores));
 		h_global_mins = (float *) malloc(total_chunks*sizeof(*h_global_mins));//for distributions
 		h_global_maxs = (float *) malloc(translation_vol*sizeof(*h_global_maxs));//for scoring
 		h_global_max_indexes = (uint *) malloc(translation_vol*sizeof(*h_global_max_indexes));
		h_global_stride = (size_t *) malloc(sizeof(*h_global_stride));
		h_initial_offsets= (size_t *) malloc(_thread_count*sizeof(*h_global_stride));
		
		for (int g=0;g < _thread_count;g++){
			auto score_f = std::bind(score_min_search_test,translation_offset,translation_end, g);
			{
			 	lock.lock();
			 	_per_gpu_q[g].push(score_f);
			 	lock.unlock();
			}
		}
		while (!check_per_GPU_done()){}
        	std::vector<float> rot_maxima(h_global_maxs+translation_offset,h_global_maxs + translation_end);
        	std::vector<float>::iterator max_trans = std::max_element(rot_maxima.begin(),rot_maxima.end());
		size_t max_trans_index = translation_offset + std::distance(rot_maxima.begin(),max_trans);

		rot_nodes.getRotationIndex(rot_abg,h_global_max_indexes[max_trans_index],0);
		rot_nodes.getRotationEulers(rot_eulers,h_global_max_indexes[max_trans_index],0);
		trans_nodes.getTranslationIndex(trans_xyz,max_trans_index,0);
		trans_nodes.getTranslation(trans_shift,max_trans_index,0);
		printf("Max translation:\t%i - %f\n",*max_trans,max_trans_index);
		printf("Rotation index: \t %u: %u %u %u\n",test_r,rot_abg.x,rot_abg.y,rot_abg.z);
		printf("Translation index: \t %u: %u %u %u\n",test_t, trans_xyz.x,trans_xyz.y,trans_xyz.z);
		printf("Translation shift: \t %f %f %f\n",trans_shift.x,trans_shift.y,trans_shift.z);
		printf("Rotation eulers: \t %f %f %f\n",rot_eulers.x,rot_eulers.y,rot_eulers.z);
		float3 trans_diff = trans_shift - test_trans;
		float3 rot_diff = rot_eulers - test_rot;
		printf("Differences trans - rot: %f %f %f - %f %f %f\n",trans_diff.x,trans_diff.y, trans_diff.z, rot_diff.x,rot_diff.y,rot_diff.z);
		out_test << translation_vol << "," << rotation_vol << "," << test_t << "," << test_r << "," << trans_shift.x << "," << trans_shift.y << ","<< trans_shift.z << ","<< rot_eulers.x << ","<< rot_eulers.y<< ","<< rot_eulers.z << ","   << test_trans.x << ","<< test_trans.y << ","<< test_trans.z << "," << test_rot.x << "," << test_rot.y << "," << test_rot.z << "," << *max_trans << "," << h_global_scores[rotation_vol*test_t + test_r] << std::endl; 
	}
	out_test.close();
	total_transformations_per_gpu = (int *) malloc(sizeof(*total_transformations_per_gpu));
	max_rotations = (int *) malloc(sizeof(*max_rotations));
	int trans_node_resolution = ipow(2,t_ref_level);
	int total_translations = ipow(trans_node_resolution,3)
	int per_gpu_translations = total_translations/_thread_count;
	for (int g=0;g<_thread_count;g++){
		auto init_f = std::bind(init_resources_cam,target_mrc,query_pdb,target_threshold,resolution,t_ref_level,r_ref_level,s_ref_level,g);
		{
			lock.lock();
			_per_gpu_q[g].push(init_f);
			lock.unlock();
		}
	}
	float translation_vol;

	while (!check_per_GPU_done()){}

	for (int g=0;g<_thread_count;g++){
		auto score_f = std::bind(score_cam,&translation_vol,g*per_gpu_translations,per_gpu_translations,g);
		auto init_query_f = std::bind(init_query,g);
		{
			lock.lock();
			_per_gpu_q[g].push(init_query_f);
			_per_gpu_q[g].push(score_f);
			lock.unlock();
		}
	}
	while (!check_per_GPU_done()){}
	float * total_scores = (float *) malloc(total_transformations_per_gpu[0]*_thread_count*sizeof(*total_scores));
	for (int g=0;g<_thread_count;g++){
		auto to_host = std::bind(copy_to_host,total_scores,max_rotations[0],per_gpu_translations,total_transformations_per_gpu[0],g);
		{
			lock.lock();
			_per_gpu_q[g].push(to_host);
			lock.unlock();
		}
	}
	while (!check_per_GPU_done()){}
	std::vector<float> scores(total_transformations_per_gpu[0]*_thread_count);
	scores.assign(total_scores,total_scores+total_transformations_per_gpu[0]*_thread_count);
	std::vector<size_t> sorted_score_indexes = sort_indexes(scores);
	float3 translation;
	float3 eulers;
	std::ofstream res_file;
	res_file.open(out_p,std::fstream::out);
	res_file << target_mrc << std::endl << query_pdb << std::endl;
	for (int i = 0;i<solution_num;i++){
		translation = {0.0f,0.0f,0.0f};
		index_to_transformation(&target_density,&trans_nodes,&rot_nodes,0,sorted_score_indexes[i],&translation,&eulers,0);
//		euler_to_axis_angle(reinterpret_cast<float *>(&eulers),axis_angle);
		res_file << translation.x << "," << translation.y << "," << translation.z
				 << "," << eulers.x << ","<< eulers.y << "," << eulers.z
                 << "," << scores[sorted_score_indexes[i]] << std::endl;
		std::string fname = std::string("./test_out_") + std::to_string(i) + std::string(".pdb");
		if (PDB_OUT)
			pdb.writeTransformedPdb(fname.c_str(),eulers,translation,0);
	}
	res_file.close();
	_main_task_finished = true;
}


void set_scan_nodes(const int& s_ref_level,const int& gpu_index){
	scan_nodes.createInstance(s_ref_level,{{0.0f,0.0f,0.0f},{1.0f,1.0f,1.0f}},gpu_index);
}

void init_resources_cam_bench(int* const& total_transformations, int* const& max_rotations,const int& trans_refinement_level,
		const int& rot_refinement_level, const int& scan_refinement_level,const int & gpu_index,const int & total_gpus){
	target_density.createInstanceFromMrc("./query_padding.mrc",0.0f,gpu_index);
	target_density.createTexture(gpu_index);
	query_density.createInstanceFromMrc("./query.mrc",0.0f,gpu_index);
	query_density.createTexture(gpu_index);
	size_t trans_node_vol;
	scan_nodes.createInstance(scan_refinement_level,{{0.0f,0.0f,0.0f},{1.0f,1.0f,1.0f}},gpu_index);
	trans_nodes.createInstance(trans_refinement_level,{{0,0,0},{1,1,1}},gpu_index);
	trans_node_vol = trans_nodes.getVolume(gpu_index);
	rot_nodes.createInstance(rot_refinement_level,gpu_index);
	max_rotations[0] = rot_nodes.getMaximumNodes(gpu_index);
	total_transformations[0] = max_rotations[0]*trans_node_vol/total_gpus;
	//assume trans_node_vol and total_gpus shares a good number of factors of 2
	cam_score.createInstance(&trans_nodes,&rot_nodes,trans_node_vol/total_gpus,gpu_index);
}

void Engine::cam_benchmark(void){

	std::chrono::duration<double> elapsed;
	std::chrono::_V2::system_clock::time_point start;
	std::chrono::_V2::system_clock::time_point finish;

	std::unique_lock<std::mutex> lock(lock_);
	lock.unlock();
	h_global_mins = (float *) malloc(_thread_count*sizeof(*h_global_mins));
	h_global_quads = (float *) malloc(_thread_count*sizeof(*h_global_quads));
	h_global_ents = (float *) malloc(_thread_count*sizeof(*h_global_ents));
	total_transformations_per_gpu = (int *) malloc(sizeof(*total_transformations_per_gpu));
	max_rotations = (int *) malloc(sizeof(*max_rotations));
//	const char * out_p = "/data/eclipse-workspace/fitter/test/fitter_out.txt";
//	scan_nodes.createInstance(8,0);

	int t_ref_level = 5;
	int r_ref_level = 4;
	int s_ref_level = 3;
	int trans_node_resolution = ipow(2,t_ref_level);
	int per_gpu_translations = ipow(trans_node_resolution,3)/_thread_count;

	for (int g=0;g<_thread_count;g++){
//		void init_resources_cam_bench(int* const& total_transformations, int* const& max_rotations,const int& trans_refinement_level,
//				const int& rot_refinement_level, const int& scan_refinement_level,const int & gpu_index,const int & total_gpus)				float sigma, const int& trans_refinement_level, const int& rot_refinement_level, const int & gpu_index,const int & total_gpus){
		auto init_f = std::bind(init_resources_cam_bench,total_transformations_per_gpu,max_rotations,t_ref_level,r_ref_level,s_ref_level,g,_thread_count);
		{
			lock.lock();
			_per_gpu_q[g].push(init_f);
			lock.unlock();
		}
	}
	float translation_vol;
	while (!check_per_GPU_done()){}

	for (int g=0;g<_thread_count;g++){
	//		void init_resources_cam(const char * target_file_name, const int & gpu_index,const int & total_gpus)
			auto init_query_f = std::bind(init_query,g);
			{
				lock.lock();
				_per_gpu_q[g].push(init_query_f);
				lock.unlock();
			}
	}
	while (!check_per_GPU_done()){}

	while (s_ref_level < 7){
		for (int g=0;g<_thread_count;g++){
			auto _set_scan_nodes = std::bind(set_scan_nodes,s_ref_level,g);
			{
				lock.lock();
				_per_gpu_q[g].push(_set_scan_nodes);
				lock.unlock();
			}
		}

		start = std::chrono::high_resolution_clock::now();
		for (int g=0;g<_thread_count;g++){
			auto score_bench_f = std::bind(score_bench,&translation_vol,g*per_gpu_translations,per_gpu_translations,g);
			{
				lock.lock();
				_per_gpu_q[g].push(score_bench_f);
				lock.unlock();
			}
		}
		while (!check_per_GPU_done()){}
		finish = std::chrono::high_resolution_clock::now();
		elapsed = finish-start;
		std::cout << "Time:" << std::endl << s_ref_level << "," << elapsed.count()<< ","<< ((float) (total_transformations_per_gpu[0]*_thread_count))/elapsed.count() <<std::endl;
		s_ref_level += 1;
	}
	_main_task_finished = true;
}
#endif

//====================================================/CAM_HELPERS==========================================

#ifdef FITCHAMFER
//====================================================CHAMFER_HELPERS==========================================
void init_resources_chamfer(const char * target_file_name, const char * query_file_name,const float& target_t0,const float& target_t1,
		const float& query_t0,const float& query_t1,int* const& total_transformations, const float & target_threshold,
		int* const& max_rotations, float sigma, const int& trans_refinement_level, const int& rot_refinement_level, const int & gpu_index,const int & total_gpus){
	target_density.createInstanceFromMrc(target_file_name,target_threshold,gpu_index);
	float pixel_size;
	target_density.getPixelSize(pixel_size,gpu_index);
	std::string query_fn(query_file_name);
	if (!MRC){
		int reader_index = pdb.fromPdb(query_file_name);
		pdb.createInstanceFromReader(gpu_index,reader_index);
		generator.createInstanceFromParticles(&pdb,gpu_index);
		pdb.find_bounds(gpu_index);
		query_density.createInstanceFromBounds(pdb.volume(gpu_index),pixel_size,0,gpu_index);
		stamp.createInstance(sigma,pixel_size,gpu_index);
	} else {
		query_density.createInstanceFromMrc(query_file_name,0.0f,gpu_index);
	}
	size_t trans_node_vol;
	float3 target_coord_dim;
	target_density.getCoordDim(target_coord_dim,gpu_index);
	trans_nodes.createInstance(trans_refinement_level,{{0,0,0},{1,1,1}},gpu_index);
	trans_node_vol = trans_nodes.getVolume(gpu_index);
	rot_nodes.createInstance(rot_refinement_level,gpu_index);
	max_rotations[0] = rot_nodes.getMaximumNodes(gpu_index);
	total_transformations[0] = max_rotations[0]*trans_node_vol/total_gpus;
	//assume trans_node_vol and total_gpus shares a good number of factors of 2
	chamfer_score.createInstance(&target_density,&query_density,&trans_nodes,&rot_nodes,trans_node_vol/total_gpus,gpu_index);
	target_density.defineSurface(target_t0,target_t1,gpu_index);
	query_density.defineSurface(query_t0,query_t1,gpu_index);
	int on_surface;
	uint3 pixel_dim;
	target_density.getPixelDim(pixel_dim,gpu_index);
	target_density.getSurfaceVolume(on_surface,gpu_index);
	chamfer_score.getPaddingVolume(target_pixel_vol,gpu_index);
	target_off_surface_volume = target_pixel_vol - on_surface;
}

void distance_map_chamfer(const int& offset, const int & vol, const int& gpu_index){
	cudaSetDevice(gpu_index);
	ChamferScore_Register * chamfer_instance = &chamfer_score._instance_registry[gpu_index];
//	void createDifferences(float * const& d_differences, float * const& h_differences, uint3 *const& h_padding_pixel_dim,
//			float3 *const& h_padding_coord_dim, const uint& padding_pixel_offset, const int& offset, const int& volume, const int & gpu_index)
	target_density.createDifferences(chamfer_instance->d_diffmap,chamfer_instance->h_diffmap,chamfer_instance->h_padding_pixel_dim,
			chamfer_instance->h_padding_coord_dim,chamfer_instance->h_target_padding[0],chamfer_instance->h_padding_off_surface_indexes, offset,vol,gpu_index);
}

void sync_distance_map(float *const& h_diffmap_global, const int& offset, const int& pixel_vol,const int & gpu_index){
	cudaSetDevice(gpu_index);
	ChamferScore_Register * chamfer_instance = &chamfer_score._instance_registry[gpu_index];
	float * h_diffs_gpu = chamfer_instance->h_diffmap;
	uint * offsets = chamfer_instance->h_padding_off_surface_indexes;

	for (uint u=0; u<pixel_vol;++u){
		h_diffmap_global[offsets[u]] = h_diffs_gpu[offsets[u]];
	}
//
//		CudaSafeCall(cudaMemcpy(h_diffmap_global + chamfer_instance->h_padding_off_surface_indexes[u],
//				chamfer_instance->h_diffmap + chamfer_instance->h_padding_off_surface_indexes[u],sizeof(*h_diffmap_global),cudaMemcpyHostToHost));
	cudaDeviceSynchronize();
}

void back_sync_distance_map(float *const& h_diffmap_global, const int & gpu_index){
	cudaSetDevice(gpu_index);
	ChamferScore_Register * chamfer_instance = &chamfer_score._instance_registry[gpu_index];
	Density_Register * target_instance = &target_density._instance_registry[gpu_index];
	Density_Register * query_instance = &query_density._instance_registry[gpu_index];
	CudaSafeCall(cudaMemcpy(chamfer_instance->h_diffmap,h_diffmap_global,target_pixel_vol*sizeof(*chamfer_instance->h_diffmap),cudaMemcpyHostToHost));
	CudaSafeCall(cudaMemcpy(chamfer_instance->d_diffmap,h_diffmap_global,target_pixel_vol*sizeof(*chamfer_instance->d_diffmap),cudaMemcpyHostToDevice));
//	Density::toMrcRaw("cube_test_diffmap.mrc",chamfer_instance->h_padding_pixel_dim[0],chamfer_instance->h_padding_coord_dim[0],chamfer_instance->h_diffmap);
//	target_density.writeToMrcSurface("target_surface.mrc",gpu_index);
//	query_density.writeToMrcSurface("query_surface.mrc",gpu_index);
}

void diff_to_disk(const int& gpu_index){
	cudaSetDevice(gpu_index);
	ChamferScore_Register * chamfer_instance = &chamfer_score._instance_registry[gpu_index];
	target_density._instance_registry[gpu_index].d_data = (TDensity *) chamfer_instance->d_diffmap;
	std::string fname = std::string("diffmap_test_") + std::to_string(gpu_index) + std::string(".mrc");
	target_density.toMrc(fname.c_str(),0,gpu_index);
}

void score_chamfer(const int& translation_offset, const int& translation_volume, const int& gpu_index){
	chamfer_score.score(&target_density,&query_density,translation_offset,translation_volume,gpu_index);
}

void copy_to_host_chamfer(float * const & h_scores,const int& rot_vol, const int& translations_per_gpu,const int& gpu_vol,const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	float * d_scores = chamfer_score._instance_registry[gpu_index].d_score;
	for (int t = 0;t<translations_per_gpu;t++){
		CudaSafeCall(cudaMemcpy(h_scores+gpu_index*gpu_vol + t*rot_vol,
								 d_scores+t* chamfer_score._instance_registry[gpu_index].h_translation_voltranslation_stride[0],rot_vol*sizeof(*d_scores),
								 cudaMemcpyDeviceToHost));
	}
	cudaDeviceSynchronize();
}

void Engine::chamfer(const char * target_mrc, const char * query_pdb, const char * out_p, const float & resolution, const float & target_t0,
		const float & target_t1,const float & query_t0,const float & query_t1,const int& solution_num,const int& t_ref_level,
		const int& r_ref_level){
	std::unique_lock<std::mutex> lock(lock_);
	lock.unlock();
	total_transformations_per_gpu = (int *) malloc(sizeof(*total_transformations_per_gpu));
	max_rotations = (int *) malloc(sizeof(*max_rotations));
	int trans_node_resolution = ipow(2,t_ref_level);
	int per_gpu_translations = ipow(trans_node_resolution,3)/_thread_count;
	for (int g=0;g<_thread_count;g++){
		auto init_f = std::bind(init_resources_chamfer,target_mrc,query_pdb,target_t0,target_t1,query_t0,query_t1,
				total_transformations_per_gpu,0.0f,max_rotations,resolution,t_ref_level,r_ref_level,g,_thread_count);
		{
			lock.lock();
			_per_gpu_q[g].push(init_f);
			lock.unlock();
		}
	}
	float translation_vol;

	while (!check_per_GPU_done()){}

	printf("Submitted initiation jobs to GPUs, total transformations per gpu: %i ...\n",total_transformations_per_gpu[0]);
	printf("Chamfer target off surface pixel volume: %i ...\n",target_off_surface_volume);

	int diffs_per_gpu = target_off_surface_volume/_thread_count;
	int last_vol = target_off_surface_volume - diffs_per_gpu*(_thread_count - 1);

	for (int g=0;g<_thread_count;g++){
//		distance_map_chamfer(Density * target, ChamferScore * chamfer_score, const int& offset, const int & vol, const int& gpu_index)
		if (g != _thread_count - 1){
			auto distance_map_f = std::bind(distance_map_chamfer,g*diffs_per_gpu,diffs_per_gpu,g);
			{
				lock.lock();
				_per_gpu_q[g].push(distance_map_f);
				lock.unlock();
				printf("Initiated distance map calculations on gpu %i for pixels %i - %i ...\n",g,g*diffs_per_gpu,g*diffs_per_gpu+diffs_per_gpu);
			}
		} else {
			auto distance_map_f = std::bind(distance_map_chamfer,g*diffs_per_gpu,last_vol,g);
			{
				lock.lock();
				_per_gpu_q[g].push(distance_map_f);
				lock.unlock();
				printf("Initiated distance map calculations on gpu %i for pixels %i - %i ...\n",g,g*diffs_per_gpu,g*diffs_per_gpu+last_vol);
			}
		}

	}
	while (!check_per_GPU_done()){}

//	float * total_scores = (float *) malloc(total_transformations_per_gpu[0]*_thread_count*sizeof(*total_scores));
	float * h_diffmap_global = (float *) malloc(target_pixel_vol*sizeof(*h_diffmap_global));
	for (int i = 0; i<target_pixel_vol; i++) h_diffmap_global[i] = 0.0f;
	for (int g=0;g<_thread_count;g++){
//		sync_distance_map(const int& pixel_vol,const int & gpu_index)
		if (g != _thread_count - 1){
			auto _sync = std::bind(sync_distance_map,h_diffmap_global,g*diffs_per_gpu,diffs_per_gpu,g);
			{
				lock.lock();
				_per_gpu_q[g].push(_sync);
				printf("Initiated distance map synchronizations on gpu %i for pixels %i - %i ...\n",g,g*diffs_per_gpu,g*diffs_per_gpu+diffs_per_gpu);
				lock.unlock();
			}
		} else {
			auto _sync = std::bind(sync_distance_map,h_diffmap_global,g*diffs_per_gpu,last_vol,g);
			{
				lock.lock();
				_per_gpu_q[g].push(_sync);
				printf("Initiated distance map synchronizations on gpu %i for pixels %i - %i ...\n",g,g*diffs_per_gpu,g*diffs_per_gpu+last_vol);
				lock.unlock();
			}
		}
	}
	while (!check_per_GPU_done()){}

	for (int g=0;g<_thread_count;g++){
//		back_sync_distance_map(h_diffmap_global,const int & gpu_index)
		auto back_sync = std::bind(back_sync_distance_map,h_diffmap_global,g);
		{
			lock.lock();
			_per_gpu_q[g].push(back_sync);
			printf("Initiated distance map back synchronizations on gpu %i ... \n",g);
			lock.unlock();
		}
	}
	while (!check_per_GPU_done()){}
	std::chrono::duration<double> elapsed;
	std::chrono::_V2::system_clock::time_point start;
	std::chrono::_V2::system_clock::time_point finish;
	start = std::chrono::high_resolution_clock::now();
	while (!check_per_GPU_done()){}
	for (int g=0;g<_thread_count;g++){
//		sync_distance_map(const int& pixel_vol,const int & gpu_index)
//		auto to_disk = std::bind(diff_to_disk,g);
		auto _score = std::bind(score_chamfer, g*per_gpu_translations,per_gpu_translations, g);
		{
			lock.lock();
			_per_gpu_q[g].push(_score);
			printf("Initiated chamfer scoring on gpu %i for translations %i - %i... \n",g,g*per_gpu_translations,g*per_gpu_translations + per_gpu_translations);
			lock.unlock();
		}
	}
	while (!check_per_GPU_done()){}
	finish = std::chrono::high_resolution_clock::now();
	elapsed = finish-start;

	std::cout << "TIME:" << t_ref_level << "," << r_ref_level << "," << elapsed.count() << "," <<  ((float) (total_transformations_per_gpu[0]*_thread_count))/elapsed.count() << std::endl;

	float * total_scores = (float *) malloc(total_transformations_per_gpu[0]*_thread_count*sizeof(*total_scores));
	for (int g=0;g<_thread_count;g++){
		auto to_host = std::bind(copy_to_host_chamfer,total_scores,max_rotations[0],per_gpu_translations,total_transformations_per_gpu[0],g);
		{
			lock.lock();
			_per_gpu_q[g].push(to_host);
			lock.unlock();
		}
	}
	while (!check_per_GPU_done()){}
	std::vector<float> scores(total_transformations_per_gpu[0]*_thread_count);
	scores.assign(total_scores,total_scores+total_transformations_per_gpu[0]*_thread_count);
	std::vector<size_t> sorted_score_indexes = sort_indexes_inverse(scores);
	float3 translation;
	float3 eulers;
	float axis_angle[4];
	std::ofstream res_file;
	res_file.open(out_p,std::fstream::out);
	res_file << target_mrc << std::endl << query_pdb << std::endl;
	float3 neg_half_query_coord_dim = -0.5*query_density._instance_registry[0].h_coord_dim[0];
	for (int i = 0;i<solution_num;i++){
		translation = neg_half_query_coord_dim;
		index_to_transformation(&target_density,&trans_nodes,&rot_nodes,0,sorted_score_indexes[i],&translation,&eulers,0);
		euler_to_axis_angle(reinterpret_cast<float *>(&eulers),axis_angle);
		res_file << translation.x << "," << translation.y << "," << translation.z
				 << "," << axis_angle[0] << ","<< axis_angle[1] << "," << axis_angle[2] << ","<< axis_angle[3]
                 << "," << scores[sorted_score_indexes[i]] << std::endl;
		printf("%i %f\n",i,scores[sorted_score_indexes[i]]);
	}
	res_file.close();
	_main_task_finished = true;
}

//====================================================/CHAMFER_HELPERS==========================================
#endif

void benchmark_molmap_task(int gridDim, int blockDim, std::ofstream * benchmark,int gpu_index){
	float pixel_size = 1.35; //median obtained from embdb meta files
	std::vector<float> resolutions = {5,10,15};
	const size_t max_particles = 31764000; //from prescreening
	const size_t max_pixels = 240000000; //from prescreening
	DIR *dir;
	struct dirent *diread;
	std::vector<std::string> files;
	std::string curr;
	if ((dir = opendir("./molmap_benchmark/")) != nullptr) {
		while ((diread = readdir(dir)) != nullptr) {
			curr = std::string(diread->d_name);
			if (hasEnding(curr,std::string("pdb")))
				files.push_back(curr);
		}
	}
	closedir (dir);
	std::string pdb_path;
	std::string base_dir("./molmap_benchmark/");
	std::chrono::duration<double> elapsed;
	std::chrono::_V2::system_clock::time_point start;
	std::chrono::_V2::system_clock::time_point finish;
	Particles pdb;
	Density pdb_density;
	DensityGenerator generator;
	PdbReader reader(max_particles);
	pdb.createInstance(gpu_index,max_particles);
	pdb_density.createInstance(gpu_index,max_pixels);
	generator.createInstanceFromParticles(&pdb,gpu_index);
	std::vector<Stamp> stamps;
	for (float resolution:resolutions){
		Stamp stamp;
		stamp.createInstance(resolution,pixel_size,gpu_index);
		stamps.push_back(stamp);
	}
	double total = 0;
	int p = 0;
	int r;
	size_t ps;
	for (std::string pdb_fn:files){
		reader.setSource(std::string(base_dir + pdb_fn).c_str());
		pdb.readFromPdb(&reader,gpu_index);
		pdb.getParticleCount(&ps,gpu_index);
		if (ps <= 0)
			continue;
		pdb.find_bounds(gpu_index);
		pdb_density.setBounds(pdb.volume(gpu_index),pixel_size,gpu_index);
		r=0;
		for (Stamp stamp: stamps){
			printf("Gpu %i <%i,%i> dealing with pdb %i/%u - %s resolution %f\n",gpu_index,gridDim,blockDim,p,files.size(),pdb_fn.c_str(),resolutions[r]);
			start = std::chrono::high_resolution_clock::now();
			generator.generateMolecularDensity(&pdb_density,&pdb,&stamp,gridDim,blockDim,gpu_index);
			finish = std::chrono::high_resolution_clock::now();
			elapsed = finish-start;
			total += elapsed.count();
			r++;
		}
		p++;
	}
	pdb_density.freeAllInstances();
	pdb.freeAllInstances();
	generator.freeAllInstances();
	for (Stamp stamp:stamps){
		stamp.freeAllInstances();
	}
	(*benchmark) << gridDim << "," << blockDim << "," << total << std::endl;
}

void benchmark_molmap_particles_task(std::string file, std::ofstream * benchmark,int gpu_index){
	int gridDim = 128;
	int blockDim = 256;
	float pixel_size = 1.35f; //median obtained from embdb meta files
	float resolution = 5.0f;
	std::chrono::duration<double> elapsed;
	std::chrono::_V2::system_clock::time_point start;
	std::chrono::_V2::system_clock::time_point finish;
	Particles pdb;
	Density pdb_density;
	DensityGenerator generator;
	Stamp stamp;
	stamp.createInstance(resolution,pixel_size,gpu_index);
	size_t ps;
	float3 volume;
	int reader_index = pdb.fromPdb(file.c_str());
	pdb.createInstanceFromReader(gpu_index,reader_index);
	pdb.getParticleCount(&ps,gpu_index);
	if (ps <= 0)
		return;
	generator.createInstanceFromParticles(&pdb,gpu_index);
	pdb.find_bounds(gpu_index);
	volume = pdb.volume(gpu_index);
	pdb_density.createInstanceFromBounds(volume,pixel_size,0,gpu_index);
	stamp.createInstance(resolution,pixel_size,gpu_index);
	start = std::chrono::high_resolution_clock::now();
	generator.generateMolecularDensity(&pdb_density,&pdb,&stamp,gridDim,blockDim,gpu_index);
	finish = std::chrono::high_resolution_clock::now();
	printf("Gpu %i <%i,%i> dealing with pdb %s resolution %f\n",gpu_index,gridDim,blockDim,file.c_str(),resolution);
	elapsed = finish-start;
	(*benchmark) << file << "," << ps << ",[" << volume.x << "," << volume.y << "," <<volume.z << "],"<< elapsed.count()<< std::endl;
	pdb_density.freeAllInstances();
	pdb.freeAllInstances();
	generator.freeAllInstances();
	stamp.freeAllInstances();
}

void molmap_task(std::string file, std::string outfile_base, float resolution, float pixel_size, std::ofstream * gpu_out, int gpu_index){
	int gridDim = 128;
	int blockDim = 256;
	stamp.createInstance(resolution,pixel_size,gpu_index);
	float3 volume;
	int reader_index = pdb.fromPdb(file.c_str());
	pdb.createInstanceFromReader(gpu_index,reader_index);
	generator.createInstanceFromParticles(&pdb,gpu_index);
	pdb.find_bounds(gpu_index);
	volume = pdb.volume(gpu_index);
	query_density.createInstanceFromBounds(volume,pixel_size,0,gpu_index);
	stamp.createInstance(resolution,pixel_size,gpu_index);
	generator.generateMolecularDensity(&query_density,&pdb,&stamp,gridDim,blockDim,gpu_index);
	std::string res_path = outfile_base + std::to_string(resolution)+ std::string(".mrc");
	query_density.toMrc(res_path.c_str(),0,gpu_index);
	query_density.freeInstance(gpu_index);
	pdb.freeInstance(gpu_index);
	generator.freeInstance(gpu_index);
	stamp.freeInstance(gpu_index);
}
void benchmark_molmap_extremes_task(std::string file, float resolution, std::ofstream * benchmark,int gpu_index){
	int gridDim = 128;
	int blockDim = 256;
	float pixel_size = 1.35f; //median obtained from embdb meta files
	std::chrono::duration<double> elapsed;
	std::chrono::_V2::system_clock::time_point start;
	std::chrono::_V2::system_clock::time_point finish;
	Particles pdb;
	Density pdb_density;
	DensityGenerator generator;
	Stamp stamp;
	stamp.createInstance(resolution,pixel_size,gpu_index);
	size_t ps;
	float3 volume;
	int reader_index = pdb.fromPdb(file.c_str());
	pdb.createInstanceFromReader(gpu_index,reader_index);
	pdb.getParticleCount(&ps,gpu_index);
	if (ps <= 0)
		return;
	generator.createInstanceFromParticles(&pdb,gpu_index);
	pdb.find_bounds(gpu_index);
	volume = pdb.volume(gpu_index);
	pdb_density.createInstanceFromBounds(volume,pixel_size,0,gpu_index);
	stamp.createInstance(resolution,pixel_size,gpu_index);
	start = std::chrono::high_resolution_clock::now();
	generator.generateMolecularDensity(&pdb_density,&pdb,&stamp,gridDim,blockDim,gpu_index);
	finish = std::chrono::high_resolution_clock::now();
	printf("Gpu %i <%i,%i> dealing with pdb %s resolution %f\n",gpu_index,gridDim,blockDim,file.c_str(),resolution);
	elapsed = finish-start;
	(*benchmark) << file << "," << ps << ",[" << volume.x << "," << volume.y << "," <<volume.z << "],"<< resolution<< "," << elapsed.count()<< std::endl;
	pdb_density.freeAllInstances();
	pdb.freeAllInstances();
	generator.freeAllInstances();
	stamp.freeAllInstances();
}

void prescreen_molmap_benchmark_task(int gridDim, int blockDim, std::ofstream * benchmark,int gpu_index){
	//extremely inefficient, should only be used once, ever
	std::vector<float> resolutions = {5,10,15};
//	float resolution = 5;
	DIR *dir;
	struct dirent *diread;
	std::vector<std::string> files;
	std::string curr;
	if ((dir = opendir("./molmap_benchmark/")) != nullptr) {
		while ((diread = readdir(dir)) != nullptr) {
			curr = std::string(diread->d_name);
			if (hasEnding(curr,std::string("pdb")))
				files.push_back(curr);
		}
	}
	closedir (dir);
	Particles query;
	Density query_density;
	std::string pdb_path;
	std::string base_dir("./molmap_benchmark/");
	size_t max_particles = 0;
	float max_bounds[6] = {FLT_MAX,FLT_MAX,FLT_MAX,0,0,0};
	float current_bounds[6];
	float current_vol = 0;
	size_t count = 0;
	for (std::string pdb:files){
		printf("Current:%s | %u | %f %f %f - %f %f %f\n",pdb.c_str(),max_particles,max_bounds[0],max_bounds[1],max_bounds[2],max_bounds[3],max_bounds[4],max_bounds[5]);
		PdbReader::metrics(std::string(base_dir + pdb).c_str(),count, current_bounds);
		if ((current_bounds[3] - current_bounds[0])*(current_bounds[4] - current_bounds[1])*(current_bounds[5] - current_bounds[2])>current_vol){
			current_vol = (current_bounds[3] - current_bounds[0])*(current_bounds[4] - current_bounds[1])*(current_bounds[5] - current_bounds[2]);
			for (int i =0;i<6;i++) max_bounds[i] = current_bounds[i];
		}
		max_particles = std::max(max_particles,count);
	}
	(*benchmark) << gridDim << "," << blockDim << std::endl;
}

void Engine::estimateParameters(const int& num_gpus){
	//1. setup gpus with target and query ranges
	//2. get translational volume of every gpu
	//3. submit a number of calc entropy jobs per gpu, they do have to be synchronized

}

void Engine::prescreen_molmap_benchmark(void){
	auto op = std::bind(prescreen_molmap_benchmark_task,64,256,std::placeholders::_1,std::placeholders::_2);
	dispatch(op);
}

void Engine::cam_entropy_parameter_search_molmap(char * query_pdb, const float& r_range,const float & r0, const float& delta_r, const float& pixel_size){
	int r_d = (int)(0.5*r_range/delta_r);
	std::string working_dir = getPathName(std::string(query_pdb));
	printf("Working directory: %s\n",working_dir.c_str());
	float res;
	std::string query_file_name = baseName(query_pdb); 
	std::string query_file_base = query_file_name.substr(0,query_file_name.size()-4);
	std::string outfile_base = working_dir  + std::string("/") + query_file_base + std::string("_"); 
	std::string res_path;
	for (int i=-r_d;i<=r_d;i++){
		res = r0 + delta_r*i;
		res_path = working_dir + std::string("/") + query_file_base + std::string("_") + std::to_string(res)+ std::string(".mrc");
		if (res <= 0.0f)
		{
			printf("Invalid resolution of %f ...\n",res);
			continue;
		}
		if (file_exists(res_path)){
			printf("File %s for resolution %f already exists ...\n",res_path.c_str(),res);
		}
		else {
			printf("Generating file %s for resolution %f ...\n",res_path.c_str(),res);
			auto gen_density = std::bind(molmap_task,query_pdb,outfile_base,res,pixel_size,std::placeholders::_1,std::placeholders::_2);
		 	dispatch(gen_density);
		}
	}
}

void Engine::benchmark_molmap(void){
// this is the benchmark to test woker/thread combinations
//	std::vector<int> worker_nums = {8,16,32,64,128,256,512};
//	std::vector<int> thread_nums = {32,64,128,256,512,1024};
//	for (int w: worker_nums){
//		for (int t: thread_nums){
//			auto op = std::bind(benchmark_molmap_task,w,t,std::placeholders::_1,std::placeholders::_2);
//			dispatch(op);
//		}
//	}
// this is to test different particle numbers
//	DIR *dir;
//	struct dirent *diread;
//	std::vector<std::string> files;
//	std::string curr;
//	if ((dir = opendir("./molmap_benchmark/")) != nullptr) {
//		while ((diread = readdir(dir)) != nullptr) {
//			curr = std::string(diread->d_name);
//			if (hasEnding(curr,std::string("pdb")))
//				files.push_back(curr);
//		}
//	}
//	closedir (dir);
//	std::string pdb_path;
//	std::string base_dir("./molmap_benchmark/");
//	for (std::string pdb_fn:files){
//		pdb_path = std::string(base_dir + pdb_fn);
//		auto op = std::bind(benchmark_molmap_particles_task,pdb_path,std::placeholders::_1,std::placeholders::_2);
//		dispatch(op);
//	}
	//this to test resolutions
		std::string pdb_not_dense = "1Q55.pdb";
		std::string pdb_dense = "2KU2.pdb";
		std::string base_dir("/scratch/kosinski/molmap_benchmark/");
		std::string pdb_path;

		pdb_path = std::string(base_dir + pdb_not_dense);
		float start_res = 5.0f;
		float stop_res = 20.0f;
		float res = start_res;
		while (res < stop_res){
			auto op = std::bind(benchmark_molmap_extremes_task,pdb_path,res,std::placeholders::_1,std::placeholders::_2);
			dispatch(op);
			res += 1.0f;
		}
		pdb_path = std::string(base_dir + pdb_dense);
		res = start_res;
		while (res < stop_res){
			auto op = std::bind(benchmark_molmap_extremes_task,pdb_path,res,std::placeholders::_1,std::placeholders::_2);
			dispatch(op);
			res += 1.0f;
		}
}

Engine::Engine(std::string name, bool per_gpu): name_{std::move(name)},_per_gpu{per_gpu}
{
	CudaSafeCall(cudaGetDeviceCount(&_thread_count));
	threads_ = std::vector<std::thread>(_thread_count);
	printf("Creating dispatch queue: %s\n", name_.c_str());
	printf("Devices and dispatch threads: %zu\n", _thread_count);
	benchmarks.resize(_thread_count);
	if (_per_gpu){
		for(size_t i = 0; i < threads_.size(); i++)
		{
			threads_[i] = std::thread(&Engine::dispatch_thread_handler_per_GPU, this);
			thread_to_gpu_[threads_[i].get_id()] = i;
			std::string benchmark_fn("output_gpu_");
			benchmark_fn += std::to_string(i);
			benchmark_fn += ".csv";
			benchmarks[i] = new std::ofstream(benchmark_fn.c_str(),std::ofstream::out);
			waitflags[threads_[i].get_id()] = false;
			_per_gpu_complete_flags[threads_[i].get_id()] = true;
		}
	} else {
		for(size_t i = 0; i < threads_.size(); i++)
		{
			threads_[i] = std::thread(&Engine::dispatch_thread_handler, this);
			thread_to_gpu_[threads_[i].get_id()] = i;
			std::string benchmark_fn("output_gpu_");
			benchmark_fn += std::to_string(i);
			benchmark_fn += ".csv";
			benchmarks[i] = new std::ofstream(benchmark_fn.c_str(),std::ofstream::out);
			waitflags[threads_[i].get_id()] = true;
		}
	}
#if defined(CAM_IMPROVED) || defined(SEARCH_TEST)
	max_translation_vol_per_gpu = (size_t *) malloc (sizeof(*max_translation_vol_per_gpu)*_thread_count);
#endif
}

bool Engine::check_per_GPU_done(void){
	bool ret = true;
	std::lock_guard<std::mutex> lock(lock_);
	for (auto const & flag: _per_gpu_complete_flags){
		ret = ret && flag.second;
	}
	for (auto const & qmap: _per_gpu_q){
		ret &= qmap.second.empty();
	}
	return ret;
}

bool Engine::check_flags(void){
	bool ret = true;
	for (auto const & flag: waitflags){
		ret = ret && flag.second;
	}
	return ret;
}

void Engine::print_flags(void){
	std::string pflags("");
	for (auto const & flag: waitflags){
		pflags += std::string(flag.second?"true":"false") + std::string(",");
	}
	printf("%s\n",pflags.c_str());
}

Engine::~Engine()
{
	printf("Destructor: Destroying dispatch threads...\n");
	// Signal to dispatch threads that it's time to wrap up
	if (_per_gpu){
		while (!check_per_GPU_done()){}
	} else {
		while (q_.size() != 0 || !check_flags()){}
	}
	//close benchmark streams
	for (auto benchmark:benchmarks){
		benchmark->close();
	}
	std::unique_lock<std::mutex> lock(lock_);
	quit_ = true;
	lock.unlock();
	cv_.notify_all();
	// Wait for threads to finish before we exit
	for(size_t i = 0; i < threads_.size(); i++)
	{
		if(threads_[i].joinable())
		{
			printf("Destructor: Joining thread %zu until completion\n", i);
			threads_[i].join();
		}
	}
}

void Engine::dispatch_thread_handler_per_GPU(void)
{
	std::unique_lock<std::mutex> lock(lock_);
	lock.unlock();
	do {
		if (!_per_gpu_q[thread_to_gpu_[std::this_thread::get_id()]].empty()){
			printf("Submitting job to gpu %i, length of queue: %u...\n",thread_to_gpu_[std::this_thread::get_id()],_per_gpu_q[thread_to_gpu_[std::this_thread::get_id()]].size());
			lock.lock();
			_per_gpu_complete_flags[std::this_thread::get_id()] = false;
			auto op = std::move(_per_gpu_q[thread_to_gpu_[std::this_thread::get_id()]].front());
			_per_gpu_q[thread_to_gpu_[std::this_thread::get_id()]].pop();
			lock.unlock();
			op();
			cudaDeviceSynchronize();
			lock.lock();
			_per_gpu_complete_flags[std::this_thread::get_id()] = true;
			lock.unlock();
			printf("Finishing job at gpu %i...\n",thread_to_gpu_[std::this_thread::get_id()]);
		}
	} while (!quit_);
//		//Wait until we have data or a quit signal
//		cv_.wait(lock, [this]{
//			waitflags[std::this_thread::get_id()] = true;
//			return (q_.size() || quit_);
//		});
//		waitflags[std::this_thread::get_id()] = false;
//		//after wait, we own the lock
//		if(!quit_ && q_.size())
//		{
//			auto op = std::move(q_.front());
//			q_.pop();
//			auto op_gpu = std::bind(op,benchmarks[thread_to_gpu_[std::this_thread::get_id()]],thread_to_gpu_[std::this_thread::get_id()]);
//			//unlock now that we're done messing with the queue
//			lock.unlock();
//			op_gpu();
//			cudaDeviceSynchronize();
//			lock.lock();
//		}

}


void Engine::dispatch_thread_handler(void)
{
	std::unique_lock<std::mutex> lock(lock_);

	do {
		//Wait until we have data or a quit signal
		cv_.wait(lock, [this]{
			return (q_.size() || quit_);
		});
		waitflags[std::this_thread::get_id()] = false;
		//after wait, we own the lock
		if(!quit_ && q_.size())
		{
			auto op = std::move(q_.front());
			q_.pop();
			auto op_gpu = std::bind(op,benchmarks[thread_to_gpu_[std::this_thread::get_id()]],thread_to_gpu_[std::this_thread::get_id()]);
			//unlock now that we're done messing with the queue
			lock.unlock();
			op_gpu();
			cudaDeviceSynchronize();
			lock.lock();
		}
		waitflags[std::this_thread::get_id()] = true;

	} while (!quit_);
}

void Engine::dispatch(const fp_t_gpu_bench& op)
{
	std::unique_lock<std::mutex> lock(lock_);
	q_.push(op);

	// Manual unlocking is done before notifying, to avoid waking up
    // the waiting thread only to block again (see notify_one for details)
	lock.unlock();
	cv_.notify_one();
}

void Engine::dispatch( fp_t_gpu_bench&& op)
{
	std::unique_lock<std::mutex> lock(lock_);
	q_.push(std::move(op));
	// Manual unlocking is done before notifying, to avoid waking up
    // the waiting thread only to block again (see notify_one for details)
//	printf("Pushed\n");
	lock.unlock();
	cv_.notify_one();
}

std::size_t callback(const char* in, std::size_t size, std::size_t num,std::string* out){
	const std::size_t totalBytes(size * num);
	out->append(in, totalBytes);
	return totalBytes;
}

size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}

//call this only from main thread, thread safety and such
//#if !defined(FIT) && !defined(FIT_BENCH) && !defined(ENTTEST)//whoreshit module system cant handle it
#ifdef FIT_BENCH
void Engine::get_benchmark_molecular_map_pdbs(){
	uint num_pdbs = 100;
	CURL* curl = curl_easy_init();
	for (int i = 0; i<=20;i++){
		uint atom_num_from = 1000*i;
		uint atom_num_to = 1000*(i+1);
		std::string query =
				std::string("https://search.rcsb.org/rcsbsearch/v1/query?json=%7B%22query%22%3A%7B%22type%22%3A%22terminal%22%2C%22service%22%3A%22text%22%2C%22parameters%22%3A%7B%22attribute%22%3A%22rcsb_entry_info.deposited_atom_count%22%2C%22negation%22%3Afalse%2C%22operator%22%3A%22range%22%2C%22value%22%3A%5B")
				+ std::to_string(atom_num_from) + std::string("%2C") + std::to_string(atom_num_to) + std::string("%5D%7D%7D%2C%22return_type%22%3A%22entry%22%2C%22request_options%22%3A%7B%22pager%22%3A%7B%22start%22%3A0%2C%22rows%22%3A")
				+std::to_string(num_pdbs)+std::string("%7D%2C%22scoring_strategy%22%3A%22combined%22%2C%22sort%22%3A%5B%7B%22sort_by%22%3A%22score%22%2C%22direction%22%3A%22desc%22%7D%5D%7D%7D");


		std::string pdb_download_base_url("https://files.rcsb.org/download/");


		make_dir(test_dir);
		curl_easy_setopt(curl, CURLOPT_URL, query.c_str());
		// Don't bother trying IPv6, which would increase DNS resolution time.
		curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		// Don't wait forever, time out after 10 seconds.
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);
		// Follow HTTP redirects if necessary.
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		// Response information.
		long httpCode(0);
		std::unique_ptr<std::string> httpData(new std::string());

		// Hook up data handling function.
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);

		// Hook up data container (will be passed as the last parameter to the
		// callback handling function).  Can be any pointer type, since it will
		// internally be passed as a void pointer.
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData.get());

		// Run our HTTP GET command, capture the HTTP response code, and clean up.
		curl_easy_perform(curl);
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
	//	curl_easy_cleanup(curl);
		std::vector<std::string> pdb_accessions;
		if (httpCode == 200)
		{
			std::cout << "\nGot successful response from " << query.c_str() << std::endl;
			// Response looks good - done using Curl now.  Try to parse the results
			// and print them out.
			Json::Value jsonData;
			Json::Reader jsonReader;
			if (jsonReader.parse(*httpData.get(), jsonData))
			{
				for(Json::Value::iterator it=jsonData["result_set"].begin(); it!=jsonData["result_set"].end(); ++it){
					pdb_accessions.push_back(it->get("identifier","").asString());
				}
			}
			else
			{
				std::cout << "Could not parse HTTP data as JSON" << std::endl;
				std::cout << "HTTP data was:\n" << *httpData.get() << std::endl;
			}
			printf("Got %i accessions, starting download ...\n",pdb_accessions.size());

//			CURLcode res;
			FILE *fp;
			std::string outfilename;
			std::string url;
			for (int i = 0; i<pdb_accessions.size();i++){
				url = pdb_download_base_url + pdb_accessions[i]+std::string(".pdb");
				outfilename = std::string(test_dir)+ std::string("/")+ pdb_accessions[i]+std::string(".pdb");
				printf("Requesting %s and writing to file %s ...\n",url.c_str(),outfilename.c_str());
				fp = fopen(outfilename.c_str(),"wb");
				curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				curl_easy_perform(curl);
				fclose(fp);
			}
		}
		else
		{
			std::cout << "Couldn't GET from " << query.c_str() << " - exiting" << std::endl;
		}
	}
	curl_easy_cleanup(curl);
}
#endif

#ifdef SEARCH_TEST 
void init_resources_search_test(const char * test_file_name, const int& trans_refinement_level, const int& rot_refinement_level, const int& scan_refinement_level, const int & gpu_index){
	//what is needed for this
	//1. a set of trans and rot nodes to provide a basis for transformations
	//2. a target density that will be transformed according to a random transformation identified by r and t
	//3. a cam score search instance that will conduct a search and propose a best transformation 
	//4. a query density that will be used to search 
	query_density.createInstanceFromMrc(test_file_name,0.01f,gpu_index);
	query_density.createTexture(gpu_index);
	uint3 query_pixel_dim;
	uint3 target_pixel_dim;
	query_density.getPixelDim(query_pixel_dim,gpu_index);
	target_pixel_dim = query_pixel_dim;
	float3 target_pixel_dimf = {(float) target_pixel_dim.x,(float) target_pixel_dim.y,(float) target_pixel_dim.z};
	float3 query_pixel_dimf = {(float) query_pixel_dim.x,(float) query_pixel_dim.y,(float) query_pixel_dim.z};
	float c = 0.2f;
	std::pair<float3,float3> scan_node_scale = {-0.5f*query_pixel_dimf,query_pixel_dimf};
	std::pair<float3,float3> trans_node_scale = {-0.5f*c*target_pixel_dimf,c*target_pixel_dimf};
	scan_nodes.createInstance(scan_refinement_level,gpu_index);
	scan_nodes.scale(scan_node_scale,gpu_index);
	trans_nodes.createInstance(trans_refinement_level,gpu_index);
	trans_nodes.scale(trans_node_scale,gpu_index);
	translation_vol = trans_nodes.getVolume(gpu_index);
	rot_nodes.createInstance(rot_refinement_level,gpu_index);
	rotation_vol = rot_nodes.getMaximumNodes(gpu_index);
	total_transformations = translation_vol*rotation_vol;
	//assume trans_node_vol and total_gpus shares a good number of factors of 2
	cam_score_improved.createInstance(&trans_nodes,&rot_nodes,max_translation_vol_per_gpu,gpu_index);
}

void transform_to(const uint& trans_index, const uint& rot_index, const int& gpu_index){
	//void Density::transformTo(Density *const& target, ChebychevNodes *const& trans_nodes, McEwenNodes *const& rot_nodes, const uint& trans_index, const uint& rot_index, const int& gpu_index)	
	query_density.transformTo(&target_density,&trans_nodes,&scan_nodes,&rot_nodes,trans_index,rot_index,gpu_index);
	std::string fn = std::string("trans_test") + std::to_string(gpu_index) + std::string(".mrc");
	target_density.toMrc(fn.c_str(),0,gpu_index);
}

void Engine::cam_search_test(char * test_mrc, const char * out_p, const int& t_ref_level, const int& r_ref_level, const int& s_ref_level, const int& num_tests, const size_t& translation_offset, const size_t& translation_end){
	gpu_num = _thread_count;
	std::unique_lock<std::mutex> lock(lock_);
	lock.unlock();
	srand (time(NULL));
	//init necessary resources
	//void init_resources_search_test(const char * test_file_name, const float & target_threshold, const int& trans_refinement_level, const int& rot_refinement_level, const int& scan_refinement_level, const int & gpu_index)	
	for (int g=0;g < _thread_count;g++){
		auto init_f = std::bind(init_resources_search_test, test_mrc, t_ref_level,
		r_ref_level, s_ref_level,g);
		{
		 	lock.lock();
		 	_per_gpu_q[g].push(init_f);
		 	lock.unlock();
		}
	}
	while (!check_per_GPU_done()){}
	
	std::ofstream out_test;
	out_test.open("./cam_search_test_out.csv");
	uint test_t;
	uint3 test_trans_index;
	float3 test_trans;
	uint test_r;
	uint3 test_rot_index;
	float3 test_rot;
	uint3 rot_abg;
	uint3 trans_xyz;
	float3 trans_shift;
	float3 rot_eulers;
	for (int n = 0; n < num_tests; n++){
		test_t = rand()%translation_vol;
		test_r = rand()%rotation_vol;
		rot_nodes.getRotationIndex(test_rot_index,test_r,0);
		rot_nodes.getRotationEulers(test_rot,test_r,0);
		trans_nodes.getTranslationIndex(test_trans_index,test_t,0);
		trans_nodes.getTranslation(test_trans,test_t,0);
		
		for (int g=0;g < _thread_count;g++){
			auto trans_f = std::bind(transform_to, test_t, test_r, g);
			//auto trans_f = std::bind(transform_to, 0, 18, g);
			{
			 	lock.lock();
			 	_per_gpu_q[g].push(trans_f);
			 	lock.unlock();
			}
		}
		while (!check_per_GPU_done()){}

 		//global result allocations
 		h_global_scores = (float *) malloc(rotation_vol*translation_vol*sizeof(*h_global_scores));
 		h_global_mins = (float *) malloc(total_chunks*sizeof(*h_global_mins));//for distributions
 		h_global_maxs = (float *) malloc(translation_vol*sizeof(*h_global_maxs));//for scoring
 		h_global_max_indexes = (uint *) malloc(translation_vol*sizeof(*h_global_max_indexes));
		h_global_stride = (size_t *) malloc(sizeof(*h_global_stride));
		h_initial_offsets= (size_t *) malloc(_thread_count*sizeof(*h_global_stride));
		
		for (int g=0;g < _thread_count;g++){
			auto score_f = std::bind(score_min_search_test,translation_offset,translation_end, g);
			{
			 	lock.lock();
			 	_per_gpu_q[g].push(score_f);
			 	lock.unlock();
			}
		}
		while (!check_per_GPU_done()){}
        	std::vector<float> rot_maxima(h_global_maxs+translation_offset,h_global_maxs + translation_end);
        	std::vector<float>::iterator max_trans = std::max_element(rot_maxima.begin(),rot_maxima.end());
		size_t max_trans_index = translation_offset + std::distance(rot_maxima.begin(),max_trans);

		rot_nodes.getRotationIndex(rot_abg,h_global_max_indexes[max_trans_index],0);
		rot_nodes.getRotationEulers(rot_eulers,h_global_max_indexes[max_trans_index],0);
		trans_nodes.getTranslationIndex(trans_xyz,max_trans_index,0);
		trans_nodes.getTranslation(trans_shift,max_trans_index,0);
		printf("Max translation:\t%i - %f\n",*max_trans,max_trans_index);
		printf("Rotation index: \t %u: %u %u %u\n",test_r,rot_abg.x,rot_abg.y,rot_abg.z);
		printf("Translation index: \t %u: %u %u %u\n",test_t, trans_xyz.x,trans_xyz.y,trans_xyz.z);
		printf("Translation shift: \t %f %f %f\n",trans_shift.x,trans_shift.y,trans_shift.z);
		printf("Rotation eulers: \t %f %f %f\n",rot_eulers.x,rot_eulers.y,rot_eulers.z);
		float3 trans_diff = trans_shift - test_trans;
		float3 rot_diff = rot_eulers - test_rot;
		printf("Differences trans - rot: %f %f %f - %f %f %f\n",trans_diff.x,trans_diff.y, trans_diff.z, rot_diff.x,rot_diff.y,rot_diff.z);
		out_test << translation_vol << "," << rotation_vol << "," << test_t << "," << test_r << "," << trans_shift.x << "," << trans_shift.y << ","<< trans_shift.z << ","<< rot_eulers.x << ","<< rot_eulers.y<< ","<< rot_eulers.z << ","   << test_trans.x << ","<< test_trans.y << ","<< test_trans.z << "," << test_rot.x << "," << test_rot.y << "," << test_rot.z << "," << *max_trans << "," << h_global_scores[rotation_vol*test_t + test_r] << std::endl; 
	}
	out_test.close();
	//h_global_stride[0] = 0;
	//for (int g=0; g< _thread_count; g++){
	//	h_global_stride[0] += max_translation_vol_per_gpu[g]; 
	//	h_initial_offsets[g] = 0;
	//	for (int s = 0; s < g; s++){
	//		h_initial_offsets[g] += max_translation_vol_per_gpu[s];
	//	}
	//}
	
//	for (int i=-r_d;i<=r_d;i++){
//		res = r0 + delta_r*i;
//		//if (!(abs(r0-res) <= 0.5f ))
//		res_path = working_dir + std::string("/") + query_file_base + std::string("_") + std::to_string(res)+ std::string(".mrc");
//		if (res <= 0.0f)
//		{
//			printf("Invalid resolution of %f ...\n",res);
//			continue;
//		}
//		for (int g=0;g < _thread_count;g++){
//		//void calc_entropy_cam_parasearch(const char * query_file_name, const char * out_file_name, const int & gpu_index)
//			auto calc_ent_f = std::bind(calc_entropy_cam_parasearch,res_path.c_str(), out_p,g);
//			{
//				lock.lock();
//			 	_per_gpu_q[g].push(calc_ent_f);
//			 	lock.unlock();
//			}
//		}
//		while (!check_per_GPU_done()){}
//
// 	//determine workload per GPU
// 		size_t actual_volume = max_translation_vol_per_gpu[0];
// 		for (int g = 1;g< gpu_num;g++)
// 			actual_volume = std::min<size_t>(actual_volume,max_translation_vol_per_gpu[g]);
// 		translation_vol_per_gpu = std::min<size_t>(actual_volume,translation_vol/gpu_num);
// 		total_chunks = total_transformations/translation_vol_per_gpu + 1;
// 		printf("Actual translation volume per GPU #%i: %zu\n",translation_vol_per_gpu);
// 		printf("Total number of chunks: %u\n",total_chunks);
// 		for (int g=0;g<_thread_count;g++){
// //			score_cam_improved(float * const & h_trans_vol, const int & translation_volume_total, const int& gpu_num, const int& gpu_index)
// 			auto score_f = std::bind(score_cam_improved,g);
// 			{
// 				lock.lock();
// 				_per_gpu_q[g].push(score_f);
// 				lock.unlock();
// 			}
// 		}
//
//		
//		float * h_global_min = (float *)malloc(sizeof(*h_global_min));
//		h_global_min[0] = FLT_MAX;
//		for (int c = 0; c < total_chunks; ++c){
//			h_global_min[0] = std::min(h_global_min[0], h_global_mins[c]);
//			
//		}
//
//	}
//	//total_transformations_per_gpu = (int *) malloc(sizeof(*total_transformations_per_gpu));
//	//int trans_node_resolution = ipow(2,t_ref_level);
//	//int total_translations = ipow(trans_node_resolution,3);
//	for (int i=-r_d;i<=r_d;i++){
//		res = r0 + delta_r*i;
//		res_path = working_dir + std::string("/") + query_file_base + std::string("_") + std::to_string(res)+ std::string(".mrc");
//		//printf("%s\n",res_path.c_str());
//	
//	}
//
//	while (!check_per_GPU_done()){}
	
}
#endif


#include <stdio.h>
#include <getopt.h>

int main(int argc, char ** argv)
{

#ifdef SEARCH_TEST 
	int opt;
	if (argc < 2){
		printf("Usage:\n");
		printf("para_search search.mrc\n");
		std::exit(0);
	}
	int trans_ref_level = 4;
	int rot_ref_level = 3;
	int scan_ref_level = 4;
	int translation_offset = 0;
	//necessary parameters
	char test_mrc[PATH_MAX+1];
	//char * target_mrc;
	realpath(argv[1],test_mrc);
	//char * query_pdb;
	const char * out_csv = "search_test.csv";	
    	int num_tests = 100;
	int translation_end = 0;
	while(1) 
    	{
		opt = getopt(argc, argv, "R:T:S:f:t:");
	      	if (opt == -1) break; /* end of list */
        	switch(opt) 
        	{ 
            	case 'T': 
            		trans_ref_level = std::stoi(optarg);
			break;
		case 'R':
            		rot_ref_level = std::stoi(optarg);
			break;
            	case 'S': 
            		scan_ref_level = std::stoi(optarg);
			break;
            	case 'f': 
            		translation_offset = std::stoi(optarg);
			break;
            	case 't': 
            		translation_end = std::stoi(optarg);
			break;
            	case ':': 
                	printf("option needs a value\n"); 
                	break; 
            	case '?': 
                	printf("unknown option: %c\n", optopt);
                	break; 
        	} 
    	}

	if (translation_end == 0)
		translation_end = ipow(2,3*trans_ref_level);

	printf("=================================================\n");
	printf("Attempting search test with the following inputs:\n");
	printf("Test density:\t\t\t\t%s\n",&test_mrc[0]);
	printf("Test number:\t\t\t\t%i\n",num_tests);
	printf("Search grid translation refinement:\t%i\n",trans_ref_level);
	printf("Search grid rotation refinement:\t%i\n",rot_ref_level);
	printf("Scan grid translation refinement:\t%i\n",scan_ref_level);
	printf("=================================================\n");
	
	Engine engine("map_generation",true);
	engine.cam_search_test(test_mrc, out_csv, trans_ref_level, rot_ref_level, scan_ref_level, num_tests, translation_offset,translation_end);


#endif
#ifdef MAPRANGE
	if (argc < 5){
		printf("Usage:\n");
		printf("map_range query.pdb pixel_size resolution_0 delta_resolution\n");
		std::exit(0);
	}
	//necessary parameters
	//char * query_pdb;
	char query_pdb[PATH_MAX+1];
	realpath(argv[1],query_pdb);
	float pixel_size = std::stof(argv[2]);
	float r0 = std::stof(argv[3]);
	float delta_r = std::stof(argv[4]);
	//define defaults
	float r_range = 3.0f;
	printf("=================================================\n");
	printf("Attempting parameter search with the following inputs:\n");
	printf("Target pixel size:\t\t\t%f\n",pixel_size);
	printf("Query structure:\t\t\t%s\n",&query_pdb[0]);
	printf("Query base resolution:\t\t\t%f\n",r0);
	printf("Query resolution delta:\t\t\t%f\n",delta_r);
	printf("Query resolution range:\t\t\t%f\n",r_range);
	printf("=================================================\n");
	
	{
		Engine engine("map_generation",false);
		engine.cam_entropy_parameter_search_molmap(&query_pdb[0], r_range,r0, delta_r,pixel_size);
	}

#endif
#ifdef FIT_BENCH
	Engine engine("kaingine",true);
	engine.cam_benchmark();
#endif
#ifdef FITCHAMFER
	Engine engine("kaingine",true);
	if (argc == 12){
		float sigma = std::stof(argv[4]);
		float target_t0 = std::stof(argv[5]);
		float target_t1 = std::stof(argv[6]);
		float query_t0 = std::stof(argv[7]);
		float query_t1 = std::stof(argv[8]);
		int solution_num = std::stoi(argv[9]);
		int trans_ref_level = std::stoi(argv[10]);
		int rot_ref_level = std::stoi(argv[11]);
		engine.chamfer(argv[1],argv[2],argv[3],sigma,target_t0,target_t1,query_t0,query_t1,solution_num,trans_ref_level,rot_ref_level);
	} else if (argc == 11){
		MRC = true;
		float target_t0 = std::stof(argv[4]);
		float target_t1 = std::stof(argv[5]);
		float query_t0 = std::stof(argv[6]);
		float query_t1 = std::stof(argv[7]);
		int solution_num = std::stoi(argv[8]);
		int trans_ref_level = std::stoi(argv[9]);
		int rot_ref_level = std::stoi(argv[10]);
		engine.chamfer(argv[1],argv[2],argv[3],0.0f,target_t0,target_t1,query_t0,query_t1,solution_num,trans_ref_level,rot_ref_level);
	}
#endif
#ifdef FITCAM
	Engine engine("kaingine",true);
	if (argc == 10){
		float target_treshold = std::stof(argv[4]);
		float sigma = std::stof(argv[5]);
		int solution_num = std::stoi(argv[6]);
		int trans_ref_level = std::stoi(argv[7]);
		int rot_ref_level = std::stoi(argv[8]);
		int scan_ref_level = std::stoi(argv[9]);
		engine.cam(argv[1],argv[2],argv[3],target_treshold,solution_num,sigma,trans_ref_level,rot_ref_level,scan_ref_level);
	} else if (argc == 9){
		MRC = true;
		float target_treshold = std::stof(argv[4]);
		int solution_num = std::stoi(argv[5]);
		int trans_ref_level = std::stoi(argv[6]);
		int rot_ref_level = std::stoi(argv[7]);
		int scan_ref_level = std::stoi(argv[8]);
		engine.cam(argv[1],argv[2],argv[3],target_treshold,solution_num,0.0f,trans_ref_level,rot_ref_level,scan_ref_level);
	}
#endif
#ifdef CAM_DESCEND
	//timing
	std::chrono::duration<double> elapsed;
	std::chrono::_V2::system_clock::time_point start;
	std::chrono::_V2::system_clock::time_point finish;
	int opt;
	if (argc < 4){
		printf("Usage:\n");
		printf("cam_improved target.mrc query_file target_threshold\n");
		std::exit(0);
	}
	int scan_ref_level = 5;
	//necessary parameters
	char target_file_name[PATH_MAX+1];
	char query_file_name[PATH_MAX+1];
	realpath(argv[1],target_file_name);
	realpath(argv[2],query_file_name);
	float target_threshold = std::stof(argv[3]);
	//not so necessary
	const char * out_csv = "solutions.csv";	
	float resolution = 0.0f;
	while(1) 
    	{
		opt = getopt(argc, argv, "S:r:");
	      	if (opt == -1) break; /* end of list */
        	switch(opt) 
        	{ 
            	case 'S': 
            		scan_ref_level = std::stoi(optarg);
			break;
            	case 'r': 
            		resolution = std::stof(optarg);
			break;
            	case ':': 
                	printf("option needs a value\n"); 
                	break; 
            	case '?': 
                	printf("unknown option: %c\n", optopt);
                	break; 
        	} 
    	}

	float d = 30.0f;
	float epsilon = 0.001f;
	int M = 2;

	printf("=================================================\n");
	printf("Attempting cam score descend with the following inputs:\n");
	printf("Target density:\t\t\t\t%s\n",&target_file_name[0]);
	printf("Query file:\t\t\t\t%s\n",&query_file_name[0]);
	printf("Target threshold:\t\t\t%f\n",target_threshold);
	printf("Scan grid translation refinement:\t%i\n",scan_ref_level);
	printf("=================================================\n");
	
	Engine engine("cam_improved",true);
	MRC = true;
	start = std::chrono::high_resolution_clock::now();
	engine.camDescend(&target_file_name[0],&query_file_name[0],out_csv,target_threshold,resolution,scan_ref_level,d,epsilon,M,1,1);
	finish = std::chrono::high_resolution_clock::now();
	elapsed = finish-start;
	std::cout << "Time:" << std::endl << elapsed.count()<< std::endl;
#endif
#ifdef CAMENT
	//timing
	std::chrono::duration<double> elapsed;
	std::chrono::_V2::system_clock::time_point start;
	std::chrono::_V2::system_clock::time_point finish;
	int opt;
	if (argc < 2){
		printf("Usage:\n");
		printf("cam_ent working_dir\n");
		std::exit(0);
	}
	//necessary parameters
	char working_dir[PATH_MAX+1];
	realpath(argv[1],working_dir);
	//float target_threshold = std::stof(argv[3]);
	//not so necessary
	while(1) 
    	{
		opt = getopt(argc, argv, "r:");
	      	if (opt == -1) break; /* end of list */
        	switch(opt) 
        	{ 
            	case ':': 
                	printf("option needs a value\n"); 
                	break; 
            	case '?': 
                	printf("unknown option: %c\n", optopt);
                	break; 
        	} 
    	}

	printf("=================================================\n");
	printf("Working directory:\t\t\t%s\n",&working_dir[0]);
	printf("=================================================\n");

	//float3 t_max = {29.9078,64.0547,61.1141};
	//float4 q_max = {0.24624987900979975, 0.36173160246457553, 0.8387493118254792, -0.32405375600099817}; 
	//float query_threshold = 2.48f;
	uint3 rot_grid = {32,16,32};	
	
	Engine engine("cam_fft_scan",true);
	MRC = true;
	start = std::chrono::high_resolution_clock::now();
	engine.camEnt(rot_grid,working_dir);
	finish = std::chrono::high_resolution_clock::now();
	elapsed = finish-start;
	std::cout << "Time:" << std::endl << elapsed.count()<< std::endl;
#endif
#ifdef OVFFTSCAN
	//timing
	std::chrono::duration<double> elapsed;
	std::chrono::_V2::system_clock::time_point start;
	std::chrono::_V2::system_clock::time_point finish;
	int opt;
	if (argc < 4){
		printf("Usage:\n");
		printf("ov_fft target.mrc query_file working_dir\n");
		std::exit(0);
	}
	//necessary parameters
	char target_file_name[PATH_MAX+1];
	char query_file_name[PATH_MAX+1];
	char working_dir[PATH_MAX+1];
	realpath(argv[1],target_file_name);
	realpath(argv[2],query_file_name);
	realpath(argv[3],working_dir);
	//float target_threshold = std::stof(argv[3]);
	//not so necessary
	float resolution = 0.0f;
	while(1) 
    	{
		opt = getopt(argc, argv, "r:");
	      	if (opt == -1) break; /* end of list */
        	switch(opt) 
        	{ 
            	case 'r': 
            		resolution = std::stof(optarg);
			break;
            	case ':': 
                	printf("option needs a value\n"); 
                	break; 
            	case '?': 
                	printf("unknown option: %c\n", optopt);
                	break; 
        	} 
    	}

	printf("=================================================\n");
	printf("Attempting cam score descend with the following inputs:\n");
	printf("Target density:\t\t\t\t%s\n",&target_file_name[0]);
	printf("Query file:\t\t\t\t%s\n",&query_file_name[0]);
	printf("Working directory:\t\t\t%s\n",&working_dir[0]);
	printf("=================================================\n");

	float3 t_max = {29.9078,64.0547,61.1141};
	float4 q_max = {0.24624987900979975, 0.36173160246457553, 0.8387493118254792, -0.32405375600099817}; 
	float query_threshold = 2.48f;
	uint3 rot_grid = {32,16,32};	
	
	Engine engine("cam_fft_scan",true);
	MRC = true;
	start = std::chrono::high_resolution_clock::now();
	engine.ovFFTScan(target_file_name, query_file_name, working_dir, t_max, q_max, rot_grid, query_threshold);
	finish = std::chrono::high_resolution_clock::now();
	elapsed = finish-start;
	std::cout << "Time:" << std::endl << elapsed.count()<< std::endl;
#endif
#ifdef CAMFFTSCAN
	//timing
	std::chrono::duration<double> elapsed;
	std::chrono::_V2::system_clock::time_point start;
	std::chrono::_V2::system_clock::time_point finish;
	int opt;
	if (argc < 4){
		printf("Usage:\n");
		printf("cam_improved target.mrc query_file working_dir\n");
		std::exit(0);
	}
	//necessary parameters
	char target_file_name[PATH_MAX+1];
	char query_file_name[PATH_MAX+1];
	char working_dir[PATH_MAX+1];
	realpath(argv[1],target_file_name);
	realpath(argv[2],query_file_name);
	realpath(argv[3],working_dir);
	//float target_threshold = std::stof(argv[3]);
	//not so necessary
	float resolution = 0.0f;
	while(1) 
    	{
		opt = getopt(argc, argv, "r:");
	      	if (opt == -1) break; /* end of list */
        	switch(opt) 
        	{ 
            	case 'r': 
            		resolution = std::stof(optarg);
			break;
            	case ':': 
                	printf("option needs a value\n"); 
                	break; 
            	case '?': 
                	printf("unknown option: %c\n", optopt);
                	break; 
        	} 
    	}

	printf("=================================================\n");
	printf("Attempting cam score descend with the following inputs:\n");
	printf("Target density:\t\t\t\t%s\n",&target_file_name[0]);
	printf("Query file:\t\t\t\t%s\n",&query_file_name[0]);
	printf("Working directory:\t\t\t%s\n",&working_dir[0]);
	printf("=================================================\n");

	float3 t_max = {29.9078,64.0547,61.1141};
	float4 q_max = {0.24624987900979975, 0.36173160246457553, 0.8387493118254792, -0.32405375600099817}; 
	float query_threshold = 2.48f;
	uint3 rot_grid = {32,16,32};	
	
	Engine engine("cam_fft_scan",true);
	MRC = true;
	start = std::chrono::high_resolution_clock::now();
	engine.camFFTScan(target_file_name, query_file_name, working_dir, t_max, q_max, rot_grid, query_threshold,resolution);
	finish = std::chrono::high_resolution_clock::now();
	elapsed = finish-start;
	std::cout << "Time:" << std::endl << elapsed.count()<< std::endl;
#endif
#ifdef CAM_IMPROVED
	//timing
	std::chrono::duration<double> elapsed;
	std::chrono::_V2::system_clock::time_point start;
	std::chrono::_V2::system_clock::time_point finish;
	int opt;
	if (argc < 4){
		printf("Usage:\n");
		printf("cam_improved target.mrc query_file target_threshold\n");
		std::exit(0);
	}
	int trans_ref_level = 4;
	int rot_ref_level = 3;
	int scan_ref_level = 5;
	int translation_offset = 0;
	//necessary parameters
	char target_file_name[PATH_MAX+1];
	char query_file_name[PATH_MAX+1];
	realpath(argv[1],target_file_name);
	realpath(argv[2],query_file_name);
	float target_threshold = std::stof(argv[3]);
	//not so necessary
	const char * out_csv = "solutions.csv";	
	int translation_end = 0;
	float resolution = 0.0f;
	int solution_num = 100;
	while(1) 
    	{
		opt = getopt(argc, argv, "R:T:S:f:t:n:r:");
	      	if (opt == -1) break; /* end of list */
        	switch(opt) 
        	{ 
            	case 'T': 
            		trans_ref_level = std::stoi(optarg);
			break;
		case 'R':
            		rot_ref_level = std::stoi(optarg);
			break;
            	case 'S': 
            		scan_ref_level = std::stoi(optarg);
			break;
            	case 'f': 
            		translation_offset = std::stoi(optarg);
			break;
            	case 't': 
            		translation_end = std::stoi(optarg);
			break;
            	case 'n': 
            		solution_num = std::stoi(optarg);
			break;
            	case 'r': 
            		resolution = std::stof(optarg);
			break;
            	case ':': 
                	printf("option needs a value\n"); 
                	break; 
            	case '?': 
                	printf("unknown option: %c\n", optopt);
                	break; 
        	} 
    	}

	if (translation_end == 0)
		translation_end = ipow(2,3*trans_ref_level);

	printf("=================================================\n");
	printf("Attempting search test with the following inputs:\n");
	printf("Target density:\t\t\t\t%s\n",&target_file_name[0]);
	printf("Query file:\t\t\t\t%s\n",&query_file_name[0]);
	printf("Target threshold:\t\t\t%f\n",target_threshold);
	printf("Solution number:\t\t\t%i\n",solution_num);
	printf("Search grid translation refinement:\t%i\n",trans_ref_level);
	printf("Search grid rotation refinement:\t%i\n",rot_ref_level);
	printf("Scan grid translation refinement:\t%i\n",scan_ref_level);
	printf("=================================================\n");
	
	Engine engine("cam_improved",true);
	MRC = true;
	start = std::chrono::high_resolution_clock::now();
	engine.camImproved(&target_file_name[0],&query_file_name[0],out_csv,target_threshold,solution_num,resolution,translation_offset,translation_end,trans_ref_level,rot_ref_level,scan_ref_level);
	finish = std::chrono::high_resolution_clock::now();
	elapsed = finish-start;
	std::cout << "Time:" << std::endl << elapsed.count()<< std::endl;
#endif
#ifdef MOLMAP
    Engine engine("map_generator",false);
    if (argc == 6)
    {
    	float sigma = std::stof(argv[3]);
    	float pixel_size = std::stof(argv[4]);
    	uint padding = std::stoi(argv[5]);
    	auto op = std::bind(engine.generate_molecular_map,argv[1],argv[2],sigma,pixel_size,padding,std::placeholders::_2);
    	engine.dispatch(op);
    }
#endif
#ifdef MOLMAPBENCH
    //    engine.get_benchmark_molecular_map_pdbs();
    //    engine.prescreen_molmap_benchmark();
	Engine engine("kaingine",true);
    engine.benchmark_molmap();
#endif
#ifdef ENTTEST
    int gpu_index = 0;
    int trans_refinement_level = 4;
    int rot_refinement_level = 3;
    trans_nodes.createInstance(trans_refinement_level,{{0,0,0},{1,1,1}},gpu_index);
   	size_t trans_node_vol = trans_nodes.getVolume(gpu_index);
   	rot_nodes.createInstance(rot_refinement_level,gpu_index);
   	cam_score.createInstance(&trans_nodes,&rot_nodes,trans_node_vol,gpu_index);
   	cam_score.entropy_test_zero(gpu_index);
   	cam_score.entropy_test_one(gpu_index);
	cam_score.entropy_test_two(gpu_index);
	cam_score.entropy_test_three(gpu_index);
	cam_score.entropy_test_four(gpu_index);
#endif
    return 0;
}
