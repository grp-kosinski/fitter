/*
 * Density.h
 *
 *  Created on: Sep 17, 2019
 *      Author: kkarius
 */

#ifndef DENSITY_H_
#define DENSITY_H_
#include<cuda_util_include/cutil_math.h>
#include<cuda.h>
#include<cuda_runtime.h>
#include<cuda_device_runtime_api.h>

#include<thrust/pair.h>
#include<thrust/copy.h>
#include<thrust/device_malloc.h>
#include<thrust/device_ptr.h>
#include<thrust/extrema.h>
#include<thrust/execution_policy.h>
#include<thrust/reduce.h>
#include<thrust/functional.h>
#include<thrust/transform.h>
#include<thrust/device_vector.h>

#include<string.h>
#include<iostream>
#include<vector>
#include<cstdlib>
#include<sys/types.h>
#include<dirent.h>
#include<fstream>
#include<map>
#include<CMrcReader.h>
#include<Particles.h>
#include<util.h>
#include<thrust/transform_reduce.h>
#include<cuda_util_include/cutil_math.h>
#include<stdexcept>
#include<stdlib.h>
#include<derived_atomic_functions.h>
#include<chrono>

#include<ChebychevNodes.h>
#include<McEwenNodes.h>
#include<cufft.h>

struct Density_Register {
	float3 * h_coord_dim = nullptr;
	float3 * d_coord_dim = nullptr;
	float3 * h_coord_offset = nullptr;
	float3 * d_coord_offset = nullptr;
	float3 * h_molmap_padding = nullptr;
	uint3 * h_pixel_dim = nullptr;
	uint3 * d_pixel_dim = nullptr;
	TDensity * h_data = nullptr;
	TDensity * d_data = nullptr;
	cufftComplex * d_cdata = nullptr;
	float * h_pixel_size = nullptr;
	float * d_pixel_size = nullptr;
	uint3 * h_crop_pixel_offset = nullptr;
	size_t * h_external_memory = nullptr;
	cudaArray * d_texture = nullptr;
	cudaTextureObject_t * textureObj = nullptr;
	//for surfaces
	uint * d_off_surface = nullptr;
	uint * d_on_surface = nullptr;
	uint * h_on_surface_vol = nullptr;
	uint * d_on_surface_vol = nullptr;
};

__host__ __device__
void normalize_and_rotation_inverse(float4 *const& q, float *const& rotation);

class Density {
public:
	Density(){};
	virtual ~Density(){};
	void createInstance(const int &gpu_index, const size_t &extern_memory);
	void createMultiInstance(const uint3 &pix_dim, const float &pixel_size, const int& N, const int &gpu_index);
	void createInstanceGaussian(const uint3 & pixel_dim, const float & pixel_size, const int &gpu_index);
	void createInstanceTestFunction(const float & a, const uint & pixels, const int &gpu_index);
	void createInstanceFromBounds(const float3 &coord_dim, const float &pixel_size, const size_t &extern_memory, const int &gpu_index);
	void createInstanceFromDensity(Density * const & density, const int &gpu_index);
	void createInstanceCut(Density *const& source, const float3 & offset, const float3 & part, const int &gpu_index);
	void createInstanceCutAround(Density *const& source, const float3 & position, const uint3 & pix_dim, const int &gpu_index);
	void createInstanceMask(Density *const& source, const float& treshold, const int &gpu_index);
	void createInstanceFromMrc(const char * mrc_path, float crop, int gpu_index);
	void approximateVolume(ChebychevNodes * const& chebychev_nodes, float * h_result, float * d_result, int gridDim,int blockDim, const int & gpu_index);
	void approximateAverage(ChebychevNodes * const& chebychev_nodes, float * h_result, float * d_result, int gridDim,int blockDim, const int & gpu_index);
	void pixelVolume(const TDensity & threshold, float *const& h_result, float * const& d_result, const int &gpu_index);
	void pixelsAbove(const TDensity & threshold, int *const& h_result, int *const& d_result, const int &gpu_index);
	void setBounds(const float3 &coord_dim, const float &pixel_size, const int &gpu_index);
	void setPixelCube(const uint3 &offset, const uint3 &dimension,  const float& value, const int &gpu_index);
	void shiftBy(TDensity * const& d_shift_value, const TDensity & threshold,const int & gpu_index);
	void flip(const int & gpu_index);
	void mask(const float & threshold, const int & gpu_index);
	void createDensityMask(const TDensity & threshold, int *const& d_mask,const int& gpu_index);
	void writeLabelToMrc(const char * path_name_prefix, uint *const& d_labels, const uint& label,const uint &log_2_dim, const int& gpu_index);
	void prepareQueryForCam(const TDensity & threshold, const int & gpu_index);
	void createDifferences(float * const& d_differences, const int& offset, const int& vol, const int & gpu_index);
	void createDifferences(float * const& d_differences, float * const& h_differences, uint3 *const& h_padding_pixel_dim,
	float3 *const& h_padding_coord_dim, const uint& padding, uint *& h_padding_off_surface_indexes,
		const int& offset, const int& volume, const int & gpu_index);
	void writeToMrcOnly(const char * file_name, thrust::device_vector<uint> & td_indeces, const int & gpu_index);
	void writeToMrcOnly(const char * file_name, uint * d_indeces, uint volume, const int & gpu_index);
	void writeToMrcSurface(const char * mrc_path, const int & gpu_index);
	void createTexture(int gpu_index);
	void testTexture(int gpu_index);
	void transformTo(Density *const& target, ChebychevNodes *const& trans_nodes, ChebychevNodes *const& scan_nodes, McEwenNodes *const& rot_nodes, const uint& trans_index, const uint& rot_index, const int& gpu_index);
	void transformTo(Density *const& target, const float3& trans_shift, const float3& eulers, const int& gpu_index) ;
	void embed(Density *const& target, const float3& trans_shift, const float3& eulers, const int& gpu_index);
	void embed(Density *const& target, const float3& translation, const float4& q, const int& gpu_index);
	float average(Density *const& mask, const int & gpu_index);
	void average(const float& threshold, float &average, float &norm, float&sum, const int & gpu_index);
	void freeAllInstances(void);
	void cropToThreshold(const float & threshold,int gridDim,int blockDim,int gpu_index);
	void setZeroIfAbove(const float & threshold,int gpu_index);
	void getPixelDim(uint3 & pixel_dim, const int& gpu_index);
	void getMaxPixelDim(uint & max_pixel_dim, const int& gpu_index);
	void getCoordDim(float3 & coord_dim, const int& gpu_index);
	void getPixelSize(float& pixel_size, const int & gpu_index);
	void getSurfaceVolume(int &volume,const int& gpu_index);
	void defineSurface(const float& threshold0, const float& threshold1, const int& gpu_index);
	void toMrc(const char * mrc_path, const uint &padding,int gpu_index);
	void static toMrcRaw(const char * mrc_path, uint3 pixel_dim, float3 coord_dim, float *const& h_data);
	void freeInstance(const int& gpu_index);
	__host__ __device__ static uint index_to_linear(const uint3 &pixel_index,uint3 * const& pixel_dim);
	__host__ __device__ static uint3 linear_to_index(const uint &linear_index, uint3 * const& pixel_dim);
	std::map<int,Density_Register> _instance_registry;
	std::string file_path;
};

#endif /* DENSITY_H_ */
/*
 * CPdbReader.h
 *
 *  Created on: Feb 25, 2019
 *      Author: kkarius
 */



