/*
 * ChebychevNodes.h
 *
 *  Created on: Mar 9, 2021
 *      Author: kkarius
 */

#ifndef CHEBYCHEVNODES_H_
#define CHEBYCHEVNODES_H_

#include <string>
#include <vector>
#include <cmath>
#include <cstdio>
#include <cctype>
#include <iostream>
#include <util.h>
#include <map>

#include <cuda_util_include/cutil_math.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <cuda_device_runtime_api.h>


struct ChebychevNodes_Register {
	uint3 * h_node_dim = nullptr;
	uint3 * d_node_dim = nullptr;
	float * h_node_list = nullptr;
	float * d_node_list = nullptr;
	float * h_weights = nullptr;
	float * d_weights = nullptr;
	size_t * h_node_vol = nullptr;
};

class ChebychevNodes {
public:
	ChebychevNodes(){};
	static int get_node_dim(const float& diff,const uint& dim);
	void createInstance(const int & trans_ref_level,int gpu_index);
	void scale(std::pair<float3,float3>& scale,int gpu_index);
	size_t getVolume(const int & gpu_index);
	virtual ~ChebychevNodes(){};
	void getTranslationIndex(uint3 & xyz,const uint& t,const int& gpu_index);
	void getTranslation(float3 & xyz,const uint& t,const int& gpu_index);
	std::map<int,ChebychevNodes_Register> _instance_registry;
};

#endif /* CHEBYCHEVNODES_H_ */
