/*
 * CMrcReader.h
 *
 *  Created on: Jun 6, 2019
 *      Author: kkarius
 */

#ifndef CMRCREADER_H_
#define CMRCREADER_H_
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
//'?'	boolean
//'b'	(signed) byte
//'B'	unsigned byte
//'i'	(signed) integer
//'u'	unsigned integer
//'f'	floating-point
//'c'	complex-floating point
//'m'	timedelta
//'M'	datetime
//'O'	(Python) objects
//'S', 'a'	zero-terminated bytes (not recommended)
//'U'	Unicode string
//'V'	raw data (void)

#pragma pack(push, r1, 1)
struct SCellA {
	//    ('cella', [            # Cell size in angstroms
	//        ('x', 'f4'),
	//        ('y', 'f4'),
	//        ('z', 'f4')
	//    ]),
	SCellA() :
			x(0), y(0), z(0) {
	}
	float x;
	float y;
	float z;
};

struct SCellB {
	//    ('cellb', [            # Cell angles in degrees
	//        ('alpha', 'f4'),
	//        ('beta', 'f4'),
	//        ('gamma', 'f4')
	//    ]),

	SCellB() :
			alpha(90), beta(90), gamma(90) {
	}
	float alpha;
	float beta;
	float gamma;

};

struct SMrcHeader {
	SMrcHeader() :
			cella(SCellA()), cellb(SCellB()), nx(0), ny(0), nz(0), mx(0), my(0), mz(
					0), mode(2), nxstart(0), nystart(0), nzstart(0), mapc(1), mapr(2), maps(
					3), dmin(0.0),dmax(0.0),dmean(0.0),ispg(0), nsymbt(0), rms(0), nlabl(0), origin(SCellA()), nversion(
					0) {
	}

	//    ('cella', [            # Cell size in angstrom
	SCellA cella;
	//
	//    ('cellb', [            # Cell angles in degrees
	SCellB cellb;
	//
	//    ('nx', 'i4'),          # Number of columns
	//    ('ny', 'i4'),          # Number of rows
	//    ('nz', 'i4'),          # Number of sections
	int nx;
	int ny;
	int nz;

	//
	//    ('mx', 'i4'),          # Grid size in X, Y and Z
	//    ('my', 'i4'),
	//    ('mz', 'i4'),
	int mx;
	int my;
	int mz;
	//    ('mode', 'i4'),        # Mode; indicates type of values stored in data block
	int mode;
	//    ('nxstart', 'i4'),     # Starting point of sub-image
	//    ('nystart', 'i4'),
	//    ('nzstart', 'i4'),
	int nxstart;
	int nystart;
	int nzstart;

	//

	//    ('mapc', 'i4'),        # map column  1=x,2=y,3=z.
	//    ('mapr', 'i4'),        # map row     1=x,2=y,3=z.
	//    ('maps', 'i4'),        # map section 1=x,2=y,3=z.
	int mapc;
	int mapr;
	int maps;
	//
	//    ('dmin', 'f4'),        # Minimum pixel value
	//    ('dmax', 'f4'),        # Maximum pixel value
	//    ('dmean', 'f4'),       # Mean pixel value
	float dmin;
	float dmax;
	float dmean;

	//    ('ispg', 'i4'),        # space group number
	//    ('nsymbt', 'i4'),      # number of bytes in extended header
	int ispg;
	int nsymbt;
	//    ('extra1', 'V8'),      # extra space, usage varies by application
	int extra1[2];
	//    ('exttyp', 'S4'),      # code for the type of extended header
	char exttype[4];
	//    ('nversion', 'i4'),    # version of the MRC format
	int nversion;
	//    ('extra2', 'V84'),     # extra space, usage varies by application
	int extra2[42];
	//
	//    ('origin', [           # Origin of image
	//        ('x', 'f4'),float
	//        ('y', 'f4'),
	//        ('z', 'f4')
	//    ]),
	//same shit
	SCellA origin;

	//    ('map', 'S4'),         # Contains 'MAP ' to identify file type
	char map[4];
	//    ('machst', 'u1', 4),   # Machine stamp; identifies byte order
	char machst[4];
	//
	//    ('rms', 'f4'),        # RMS deviation of densities from mean density
	float rms;
	//
	//    ('nlabl', 'i4'),       # Number of labels with useful data
	int nlabl;
	//    ('label', 'S80', 10)   # 10 labels of 80 characters
	char label[800];

};
#pragma pack(pop, r1)

typedef unsigned char uchar;

class CMrcReader {
public:
	CMrcReader();
	SMrcHeader header;
	const char * mrc_file_name = nullptr;
	float * data = nullptr;
	virtual ~CMrcReader();
	void setFileName(const char * file_name){mrc_file_name = file_name;}
	void readHeader();
	void write();
	void printHeader();
	int readData(void);
	void setDimensions(float dim[3],uint pixel_dim[3]);
	void makeCube(float dim[3],uint pixel_dim[3],float pos[3],float cube_dim[3],
			float cube_val, float non_cube_val);
	void addCube(float dim[3],uint pixel_dim[3],float pos[3],float cube_dim[3],
			float cube_val);
	size_t pixel_vol(void){return header.nx*header.ny*header.nz;}
	static float getPixelSize(const char * file_name);
	static void writeTo(const char * file_name, float *const& data);
	float average(void);
private:
	bool x_HeaderRead = false;
};

#endif /* CMRCREADER_H_ */

//HEADER_DTYPE = np.dtype([
//
//])
//
//
//VOXEL_SIZE_DTYPE = np.dtype([
//    ('x', 'f4'),
//    ('y', 'f4'),
//    ('z', 'f4')
//])
//
//
//# FEI extended header dtype for metadata version 0, as described in the EPU
//# manual. Note that the FEI documentation is unclear about the endianness of
//# the data in the extended header. Probably, it is always little-endian, and
//# therefore the data might be misinterpreted on a big-endian machine, but this
//# has not been tested.
//FEI_EXTENDED_HEADER_DTYPE = np.dtype([
//    ('Metadata size', 'i4'),
//    ('Metadata version', 'i4'),
//    ('Bitmask 1', 'u4'),
//    ('Timestamp', 'f8'),  # Not specified, but suspect this is in days after 1/1/1900
//    ('Microscope type', 'S16'),
//    ('D-Number', 'S16'),
//    ('Application', 'S16'),
//    ('Application version', 'S16'),
//    ('HT', 'f8'),
//    ('Dose', 'f8'),
//    ('Alpha tilt', 'f8'),
//    ('Beta tilt', 'f8'),
//    ('X-Stage', 'f8'),
//    ('Y-Stage', 'f8'),
//    ('Z-Stage', 'f8'),
//    ('Tilt axis angle', 'f8'),
//    ('Dual axis rotation', 'f8'),float
//    ('Pixel size X', 'f8'),
//    ('Pixel size Y', 'f8'),
//    ('Unused range', 'S48'),
//    ('Defocus', 'f8'),
//    ('STEM Defocus', 'f8'),
//    ('Applied defocus', 'f8'),
//    ('Instrument mode', 'i4'),
//    ('Projection mode', 'i4'),
//    ('Objective lens mode', 'S16'),
//    ('High magnification mode', 'S16'),
//    ('Probe mode', 'i4'),
//    ('EFTEM On', '?'),
//    ('Magnification', 'f8'),
//    ('Bitmask 2', 'u4'),
//    ('Camera length', 'f8'),
//    ('Spot index', 'i4'),
//    ('Illuminated area', 'f8'),
//    ('Intensity', 'f8'),
//    ('Convergence angle', 'f8'),
//    ('Illumination mode', 'S16'),
//    ('Wide convergence angle range', '?'),
//    ('Slit inserted', '?'),
//    ('Slit width', 'f8'),
//    ('Acceleration voltage offset', 'f8'),
//    ('Drift tube voltage', 'f8'),
//    ('Energy shift', 'f8'),
//    ('Shift offset X', 'f8'),
//    ('Shift offset Y', 'f8'),
//    ('Shift X', 'f8'),
//    ('Shift Y', 'f8'),
//    ('Integration time', 'f8'),
//    ('Binning Width', 'i4'),
//    ('Binning Height', 'i4'),
//    ('Camera name', 'S16'),
//    ('Readout area left', 'i4'),
//    ('Readout area top', 'i4'),
//    ('Readout area right', 'i4'),
//    ('Readout area bottom', 'i4'),
//    ('Ceta noise reduction', '?'),
//    ('Ceta frames summed', 'i4'),
//    ('Direct detector electron counting', '?'),
//    ('Direct detector align frames', '?'),
//    ('Camera param reserved 0', 'i4'),
//    ('Camera param reserved 1', 'i4'),
//    ('Camera param reserved 2', 'i4'),
//    ('Camera param reserved 3', 'i4'),
//    ('Bitmask 3', 'u4'),
//    ('Camera param reserved 4', 'i4'),
//    ('Camera param reserved 5', 'i4'),
//    ('Camera param reserved 6', 'i4'),
//    ('Camera param reserved 7', 'i4'),
//    ('Camera param reserved 8', 'i4'),
//    ('Camera param reserved 9', 'i4'),
//    ('Phase Plate', '?'),
//    ('STEM Detector name', 'S16'),
//    ('Gain', 'f8'),
//    ('Offset', 'f8'),
//    ('STEM param reserved 0', 'i4'),
//    ('STEM param reserved 1', 'i4'),
//    ('STEM param reserved 2', 'i4'),
//    ('STEM param reserved 3', 'i4'),
//    ('STEM param reserved 4', 'i4'),
//    ('Dwell time', 'f8'),
//    ('Frame time', 'f8'),
//    ('Scan size left', 'i4'),
//    ('Scan size top', 'i4'),
//    ('Scan size right', 'i4'),
//    ('Scan size bottom', 'i4'),
//    ('Full scan FOV X', 'f8'),
//    ('Full scan FOV Y', 'f8'),
//    ('Element', 'S16'),
//    ('Energy interval lower', 'f8'),
//    ('Energy interval higher', 'f8'),
//    ('Method', 'i4'),
//    ('Is dose fraction', '?'),
//    ('Fraction number', 'i4'),
//    ('Start frame', 'i4'),
//    ('End frame', 'i4'),
//    ('Input stack filename', 'S80'),
//    ('Bitmask 4', 'u4'),
//    ('Alpha tilt min', 'f8'),
//    ('Alpha tilt max', 'f8')
//])
