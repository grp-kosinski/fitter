/*
 * CRotationGrid.h
 *
 *  Created on: Sep 17, 2019
 *      Author: kkarius
 */

#ifndef TRANSFORMATIONGRID_H_
#define TRANSFORMATIONGRID_H_
#include <cuda_util_include/cutil_math.h>
#include <cstdlib>
#include <utility>
#include <cuda_runtime.h>
#include <CRotationGrid.h>
#include <png.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <iomanip>
#include <limits>

#include <thrust/execution_policy.h>
#include <thrust/extrema.h>
#include <thrust/transform.h>
#include <thrust/tuple.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sort.h>
#include <thrust/execution_policy.h>
#include <thrust/device_ptr.h>
#include <thrust/reduce.h>
#include <thrust/functional.h>
#include <thrust/for_each.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/constant_iterator.h>

#include <util.h>

//#include "gDel3D/GpuDelaunay.h"
//
//#include "DelaunayChecker.h"
//

enum ScoreType {Chamfer,Envelope,PartialSurface,CorrelationAboutMean,CorreclationCoefficient};
typedef float(*pointFloatToFloat_t)(float);
typedef float TScore;

class TransformationGrid{
public:
	__device__ static void d_euler_to_quat(const float3 &euler,float4 &quat);
	__host__ static void h_euler_to_quat(const float3 &euler,float4 &quat);
	__host__ static void h_quat_to_euler(const float4 & rotations_quat,float3* const& euler);
	__host__ static void h_matrix_to_quat(float * const& m,float4 & quaternion);
	__host__ static void read_matrix_SO3_sampling(const char * file_name,float4 *& h_rotations, float3 *& h_rotations_euler, uint *& h_num_rotations);
	__host__ static void shift_and_scale_envelope_score(float * d_score, float * d_normalized_score,float *d_min, float *d_max, size_t num_scores);
	__host__ static void shift_and_scale_chamfer_score(float * d_score, float * d_normalized_score,float *d_min, float *d_max, size_t num_scores);
	TransformationGrid(const std::pair<float3,float3> bounding_box, const float tau, float * const& translation_offset);
	virtual ~TransformationGrid();
	__host__ void print_transformations(void);
	uint * h_num_translations;
	uint * d_num_translations;
	uint * h_num_rotations;
	uint * d_num_rotations;
	uint * h_translation_refinement;
	uint * d_translation_refinement;
	uint * h_translation_refinement_dim;
	uint * d_translation_refinement_dim;
	float3 * d_translation_total_shift;
	float3 * h_translation_total_shift;
	float4 * h_translation_offset;
	float4 * d_translation_offset;
	float4 * h_rotations;
	float3 * h_rotations_euler;
	float4 * d_rotations;
	float * h_chebychev_nodes;
	float * d_chebychev_nodes;
	float * h_chebychev_weights;
	float * d_chebychev_weights;
//	float * h_tmp_rot_quadrature_scores;
	float * d_tmp_rot_quadrature_scores;
	TScore * h_scores;
	TScore * d_scores;
	TScore * h_distribution;
	TScore * d_distribution;
	uint * h_num_transformations;
	uint * d_num_transformations;
	pointFloatToFloat_t h_identity;
	pointFloatToFloat_t h_entropy;
	__device__ int4 d_transformation_index_from_linear(const uint& linear_index);
	__host__ int4 h_transformation_index_from_linear(const uint& linear_index);
	__device__ void d_transformation_to_memory(const uint& i, float4 * const& rotation, float4 * const& translation);
	__host__ void h_transformation_to_memory(const uint& i, float4 * const& rotation, float4 * const& translation);
	__host__ void write_to_csv(const char * file_name);
	__host__ int write_to_png(const char * filename);
	__host__ void normalize_chamfer_score(void);
	__host__ void refine_translation_grid(uint & i, float * h_translation);
	__host__ void calculate_current_chebychev_nodes(void);
	__host__ void gaussian(void);
	__host__ void quadrature_test_function(void);
	__host__ void quadrature(float* const &result, pointFloatToFloat_t pfunction);
	__host__ void print_quadrature_weights(void);
	__host__ void print_quadrature_nodes(void);
	__host__ void create_distribution(void);
//	static __host__ void evaluate(const char * file_name);
	__host__ static thrust::device_vector<thrust::tuple<float4,float3,float>> n_best_from_file(const char * file_name, uint n);
	__host__ static float3 transform(const float4& t);
	void print(void);
private:
	std::pair<float3,float3> bounding_box;
};
#endif /* TRANSFORMATIONGRID_H_ */
