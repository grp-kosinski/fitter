#/usr/bin/python
import RMF,IMP,IMP.rmf
fh = RMF.open_rmf_file_read_only("./concat.rmf3")
m = IMP.Model()
h = IMP.rmf.create_hierarchies(fh,m)[0]
ofh=RMF.create_rmf_file('./py_concat_n.rmf')
IMP.rmf.add_hierarchy(ofh,h)
rbs = [e for e in IMP.atom.get_leaves(h) if IMP.core.RigidBody_get_is_setup(e)]
for rb in rbs:
    IMP.core.transform(IMP.core.RigidBody(rb),IMP.algebra.Transformation3D(IMP.algebra.Vector3D(-30,-30,-30)))
m.update()
IMP.rmf.save_frame(ofh)
for rb in rbs:
    IMP.core.transform(IMP.core.RigidBody(rb),IMP.algebra.Transformation3D(IMP.algebra.Vector3D(-30,-30,-30)))
m.update()
IMP.rmf.save_frame(ofh)
for rb in rbs:
    IMP.core.transform(IMP.core.RigidBody(rb),IMP.algebra.Transformation3D(IMP.algebra.Vector3D(-30,-30,-30)))
m.update()
IMP.rmf.save_frame(ofh)
del ofh

