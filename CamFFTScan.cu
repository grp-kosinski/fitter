/*
 * CamScoreDescend.cu
 *
 *  Created on: Sep 14, 2021
 *      Author: kkarius
 */

#include<CamFFTScan.h>
#include<score_helper.h>
#include<cuda_runtime.h>
#include<McEwenNodes.h>
#include<score_helper.h>
#include<fstream>

__global__
void fft_prepare(cudaTextureObject_t texobj, cufftComplex * d_data, cufftComplex * d_mask, float threshold, float4 * qs, uint3 cut_size, uint3 fft_size, int R){
        __shared__ float rotation[9];
	cufftComplex * current_data;
	cufftComplex * current_mask;
        int tidx = threadIdx.x + blockIdx.x * blockDim.x;
        int tidy = threadIdx.y + blockIdx.y * blockDim.y;
        int tidz = threadIdx.z + blockIdx.z * blockDim.z;
	float value;
        if ( tidx >= cut_size.x || tidy >= cut_size.y || tidz >= cut_size.z )
                return;
	int r = 0;
       	float3 half_volume = {0.5f*__uint2float_rd(cut_size.x), 0.5f*__uint2float_rd(cut_size.y), 0.5f*__uint2float_rd(cut_size.z)};
       	float3 coords_pixel = {__uint2float_rd(tidx)+0.5f,
       	                       __uint2float_rd(tidy)+0.5f,
       	                       __uint2float_rd(tidz)+0.5f};
       	coords_pixel -= half_volume;
	float3 coords_pixel_r;
	//flip here
       	size_t oidx = (cut_size.x - tidx - 1) + (cut_size.y - tidy - 1)*fft_size.x + (cut_size.z - tidz - 1)*fft_size.x*fft_size.y;
	while (r < R){
		current_data = d_data + r*vol(fft_size);
		current_mask = d_mask + r*vol(fft_size);
        	if (threadIdx.x == 0 && threadIdx.y == 0 && threadIdx.z == 0){
        	        //write rotation to memory
        	        normalize_and_rotation_inverse(qs + r,&rotation[0]);
        	}
        	__syncthreads();
        	rotate(coords_pixel_r,coords_pixel,&rotation[0]);
        	coords_pixel_r += half_volume;
		value = tex3D<float>(texobj, coords_pixel_r.x, coords_pixel_r.y, coords_pixel_r.z);
        	current_data[oidx] = {value,0.0f};
		if (value > threshold){
			current_mask[oidx] = {1.0f,0.0f};
		} 
		r++;
	}
}

__global__
void fft_prepareOV(cudaTextureObject_t texobj, cufftComplex * d_data, float threshold, float4 * qs, uint3 cut_size, uint3 fft_size, int R){
        __shared__ float rotation[9];
	cufftComplex * current_data;
        int tidx = threadIdx.x + blockIdx.x * blockDim.x;
        int tidy = threadIdx.y + blockIdx.y * blockDim.y;
        int tidz = threadIdx.z + blockIdx.z * blockDim.z;
	float value;
        if ( tidx >= cut_size.x || tidy >= cut_size.y || tidz >= cut_size.z )
                return;
	int r = 0;
       	float3 half_volume = {0.5f*__uint2float_rd(cut_size.x), 0.5f*__uint2float_rd(cut_size.y), 0.5f*__uint2float_rd(cut_size.z)};
       	float3 coords_pixel = {__uint2float_rd(tidx)+0.5f,
       	                       __uint2float_rd(tidy)+0.5f,
       	                       __uint2float_rd(tidz)+0.5f};
       	coords_pixel -= half_volume;
	float3 coords_pixel_r;
	//flip here
       	size_t oidx = (cut_size.x - tidx - 1) + (cut_size.y - tidy - 1)*fft_size.x + (cut_size.z - tidz - 1)*fft_size.x*fft_size.y;
	while (r < R){
		current_data = d_data + r*vol(fft_size);
        	if (threadIdx.x == 0 && threadIdx.y == 0 && threadIdx.z == 0){
        	        //write rotation to memory
        	        normalize_and_rotation_inverse(qs + r,&rotation[0]);
        	}
        	__syncthreads();
        	rotate(coords_pixel_r,coords_pixel,&rotation[0]);
        	coords_pixel_r += half_volume;
		value = tex3D<float>(texobj, coords_pixel_r.x, coords_pixel_r.y, coords_pixel_r.z);
        	current_data[oidx] = {value,0.0f};
		r++;
	}
}
__global__
void fft_embed(cufftComplex * d_fft_area, float * d_data, uint3 fft_dim, uint3 data_dim, size_t N){
	uint3 tid = {blockIdx.x*blockDim.x + threadIdx.x,blockIdx.y*blockDim.y + threadIdx.y, blockIdx.z*blockDim.z + threadIdx.z};
	if (tid.x < data_dim.x && tid.y < data_dim.y && tid.z < data_dim.z)
		d_fft_area[tid.x + tid.y*fft_dim.x + tid.z*fft_dim.x*fft_dim.y] = {d_data[tid.x + tid.y*data_dim.x + tid.z*data_dim.x*data_dim.y],0.0f};
}

__global__
void pointwise_product(cufftComplex * d_A, cufftComplex * d_B, size_t N, int R){
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	float normalize = 1/((float) N);
	cufftComplex tmp;
	cufftComplex tmp_r;
	int r = 0;
	if (tid < N){
		tmp = d_A[tid];
		while (r < R){
			tmp_r = d_B[r*N + tid];
			d_B[r*N + tid] = {normalize*(tmp.x*tmp_r.x - tmp.y*tmp_r.y),normalize*(tmp.x*tmp_r.y + tmp.y*tmp_r.x)};
			r++;
		}	
	}
}

__global__
void pointwise_product(cufftComplex * d_A, cufftComplex * d_B, cufftComplex * d_C, size_t N, int R){
	//first d_A times many d_B to many d_C
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	float normalize = 1/((float) N);
	cufftComplex tmp;
	int r = 0;
	if (tid < N){
		tmp = d_A[tid];
		while (r < R){
			d_C[r*N + tid].x = normalize*(tmp.x*d_B[r*N + tid].x - tmp.y*d_B[r*N + tid].y);
			d_C[r*N + tid].y = normalize*(tmp.x*d_B[r*N + tid].y + tmp.y*d_B[r*N + tid].x);
			r++;
		}
	}
}

__global__
void square_real(cufftComplex * d_in, cufftComplex * d_out,uint3 cut_size, uint3 fft_size){
	uint3 tid = {blockIdx.x*blockDim.x + threadIdx.x,blockIdx.y*blockDim.y + threadIdx.y, blockIdx.z*blockDim.z + threadIdx.z};
	size_t lin = tid.x + tid.y*fft_size.x + tid.z*fft_size.x*fft_size.y;
	if (tid.x < cut_size.x && tid.y < cut_size.y && tid.z < cut_size.z){
		d_out[lin].x = d_in[lin].x*d_in[lin].x;
	}
}

__global__
void ov(float * d_ov, cufftComplex * d_iF1F2, uint3 ncc_offset, uint3 ncc_vol, uint3 fft_size, size_t R){
        uint3 tid = {blockIdx.x*blockDim.x + threadIdx.x,blockIdx.y*blockDim.y + threadIdx.y,blockIdx.z*blockDim.z + threadIdx.z};
	if (tid.x >= ncc_vol.x || tid.y >= ncc_vol.y || tid.z >= ncc_vol.z)
		return;
	uint3 tid_from;
	size_t lin_out;
	size_t lin;
	size_t lin_r;
	size_t lin_out_r;
    lin_out = Density::index_to_linear(tid, &ncc_vol);
    tid_from = tid + ncc_offset;
    lin = Density::index_to_linear(tid_from,&fft_size);
	size_t fft_vol = vol(fft_size);
	size_t _ncc_vol = vol(ncc_vol);
	int r = 0;
	while (r < R){
		lin_r = lin + r*fft_vol;
		lin_out_r = lin_out + r*_ncc_vol;
		d_ov[lin_out_r] = 0.0f;
		//numbers reflect densities
		if (d_iF1F2[lin_r].x > 0.0f){
			d_ov[lin_out_r] = d_iF1F2[lin_r].x;
		}
		r++;
	}
}

__global__
void ncc(float * d_ncc, cufftComplex * d_iF1F2, cufftComplex * d_iF1M, cufftComplex * d_iF1sqM, float f2avg, float f2normsq, float msum, uint3 ncc_offset, uint3 ncc_vol, uint3 fft_size, size_t R, float eps, float overlap_threshold){
        uint3 tid = {blockIdx.x*blockDim.x + threadIdx.x,blockIdx.y*blockDim.y + threadIdx.y,blockIdx.z*blockDim.z + threadIdx.z};
	if (tid.x >= ncc_vol.x || tid.y >= ncc_vol.y || tid.z >= ncc_vol.z)
		return;
	uint3 tid_from;
	size_t lin_out;
	size_t lin;
	size_t lin_r;
	size_t lin_out_r;
    lin_out = Density::index_to_linear(tid, &ncc_vol);
    tid_from = tid + ncc_offset;
    lin = Density::index_to_linear(tid_from,&fft_size);
	size_t fft_vol = vol(fft_size);
	size_t _ncc_vol = vol(ncc_vol);
	float den_1;
	float num;
	float ncc;
	int r = 0;
	while (r < R){
		lin_r = lin + r*fft_vol;
		lin_out_r = lin_out + r*_ncc_vol;
		d_ncc[lin_out_r] = 0.0f;
		//numbers reflect densities
		if (d_iF1F2[lin_r].x < overlap_threshold){
			r++;
			continue;
		}
		den_1 = d_iF1sqM[lin_r].x - d_iF1M[lin_r].x*d_iF1M[lin_r].x/msum;
		if (den_1 > eps){
			num = d_iF1F2[lin_r].x - f2avg*d_iF1M[lin_r].x;
			ncc = num/(f2normsq * sqrtf(den_1));
			if (ncc > 0.0f && ncc < 1.0f)
				d_ncc[lin_out_r] = num/(f2normsq * sqrtf(den_1));
		}
		r++;
	}
}

void toMRCComplex(const char * fname, cufftComplex * d_data, uint3 pixel_dim, float pixel_size){
	size_t _vol = vol(pixel_dim);
	cufftComplex * h_dataC = new cufftComplex[_vol];
	float * h_data = new float[_vol];
	CudaSafeCall(cudaMemcpy(h_dataC,d_data,_vol*sizeof(h_dataC),cudaMemcpyDeviceToHost));
	cudaDeviceSynchronize();
	for (int i = 0; i < _vol; i++){
		h_data[i] = h_dataC[i].x;
	}
	//void static toMrcRaw(const char * mrc_path, uint3 pixel_dim, float3 coord_dim, float *const& h_data);
	Density::toMrcRaw(fname,pixel_dim,{pixel_size*pixel_dim.x,pixel_size*pixel_dim.y,pixel_size*pixel_dim.z},h_data);	
}

__global__
void averages(float * d_ncc, float * d_averages, size_t N){
	__shared__ float concat[512];
	float * d_curr = d_ncc + N*blockIdx.x;
	int tid = threadIdx.x;
	concat[tid] = 0.0f;
	while (tid < N){
		concat[threadIdx.x] += d_curr[tid];
		tid += blockDim.x;
	}
	__syncthreads();
 	if (threadIdx.x < 256) { concat[threadIdx.x] += concat[threadIdx.x + 256];} __syncthreads();
        if (threadIdx.x < 128) { concat[threadIdx.x] += concat[threadIdx.x + 128];} __syncthreads();
        if (threadIdx.x <  64) { concat[threadIdx.x] += concat[threadIdx.x +  64];} __syncthreads();
        if (threadIdx.x <  32) { concat[threadIdx.x] += concat[threadIdx.x +  32];} __syncthreads();
        if (threadIdx.x <  16) { concat[threadIdx.x] += concat[threadIdx.x +  16];} __syncthreads();
        if (threadIdx.x <   8) { concat[threadIdx.x] += concat[threadIdx.x +   8];} __syncthreads();
        if (threadIdx.x <   4) { concat[threadIdx.x] += concat[threadIdx.x +   4];} __syncthreads();
        if (threadIdx.x <   2) { concat[threadIdx.x] += concat[threadIdx.x +   2];} __syncthreads();
        if (threadIdx.x <   1) { concat[threadIdx.x] += concat[threadIdx.x +   1];} __syncthreads();
	if (threadIdx.x == 0)
		d_averages[blockIdx.x] = concat[0];
}

__global__
void entropies(float * d_ncc, float * d_entropies, float * d_averages, size_t N){
	__shared__ float concat[512];
	float * d_curr = d_ncc + N*blockIdx.x;
	int tid = threadIdx.x;
	float f;
	concat[tid] = 0.0f;
	while (tid < N){
		f = d_curr[tid]/d_averages[blockIdx.x];
		if (f > 0)
			concat[threadIdx.x] += -f*logf(f);
		tid += blockDim.x;
	}
	__syncthreads();
 	if (threadIdx.x < 256) { concat[threadIdx.x] += concat[threadIdx.x + 256];} __syncthreads();
        if (threadIdx.x < 128) { concat[threadIdx.x] += concat[threadIdx.x + 128];} __syncthreads();
        if (threadIdx.x <  64) { concat[threadIdx.x] += concat[threadIdx.x +  64];} __syncthreads();
        if (threadIdx.x <  32) { concat[threadIdx.x] += concat[threadIdx.x +  32];} __syncthreads();
        if (threadIdx.x <  16) { concat[threadIdx.x] += concat[threadIdx.x +  16];} __syncthreads();
        if (threadIdx.x <   8) { concat[threadIdx.x] += concat[threadIdx.x +   8];} __syncthreads();
        if (threadIdx.x <   4) { concat[threadIdx.x] += concat[threadIdx.x +   4];} __syncthreads();
        if (threadIdx.x <   2) { concat[threadIdx.x] += concat[threadIdx.x +   2];} __syncthreads();
        if (threadIdx.x <   1) { concat[threadIdx.x] += concat[threadIdx.x +   1];} __syncthreads();
	if (threadIdx.x == 0)
		d_entropies[blockIdx.x] = concat[0];
}

bool check_files(std::string output_dir, std::string basename, size_t r_offset , size_t _vol){
	bool ret = true;
	std::string fname;
	for (int r = 0; r < _vol; r++){
		fname = output_dir +std::string("/") + basename + std::to_string(r_offset + r) + std::string(".mrc");
		ret &= file_exists(fname);
	}
	return ret;
}


void CamFFTScan::createInstance(const char * target_fn,const char * query_fn, const char * _output_dir, float3 _tmax, float4 _qmax, uint3 rot_grid, float _query_threshold, const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
        if (_instance_registry.find(gpu_index) == _instance_registry.end()){
                CamFFTScan_Register instance;
		instance.R = new int[1];

		output_dir = std::string(_output_dir);
		ncc_basename = std::string("ncc_");
		ov_basename = std::string("ov_");
		avg_basename = std::string("averages_");
		R_total = vol(rot_grid);
		query_threshold = _query_threshold;
		rot_nodes.createInstance(rot_grid, gpu_index);
		q_max = _qmax;
		t_max = _tmax;
		cut_size = {100,100,100};
		fft_size = 2*cut_size - 1;
		ncc_offset = {0,0,0};
		ncc_vol = fft_size;
		target.createInstanceFromMrc(target_fn, 0, gpu_index);
		query.createInstanceFromMrc(query_fn, 0 ,gpu_index);
		query.createTexture(gpu_index);
		target_cut.createInstanceCutAround(&target, t_max, cut_size, gpu_index);
		query_cut.createInstanceFromDensity(&target_cut, gpu_index);
		target_cut.createInstanceCutAround(&target,t_max,cut_size, gpu_index);
		query_cut.createInstanceFromDensity(&target_cut, gpu_index);
		_instance_registry[gpu_index] = instance;	
	}
}

void CamFFTScan::initFFTOV(int *const& Rs, const int& gpu_index){
    CudaSafeCall(cudaSetDevice(gpu_index));
    CamFFTScan_Register * instance = &_instance_registry[gpu_index];
	query.average(query_threshold, f2avg, f2normsq, msum, gpu_index);
	//void createInstanceCutAround(Density *const& source, const float3 & position, const uint3 & pix_dim, const int &gpu_index);

	CudaSafeCall(cudaMalloc((void**)&instance->d_f1, vol(fft_size) * sizeof(cufftComplex)));
	CudaSafeCall(cudaMalloc((void**)&instance->d_f1sq, vol(fft_size) * sizeof(cufftComplex)));
	
	cufftHandle fwplan;
	cufftPlan3d(&fwplan, fft_size.z, fft_size.y, fft_size.x, CUFFT_C2C);
	//__global__
	//fft_embed(cufftComplex * d_fft_area, float * d_data, uint3 fft_dim, uint3 data_dim, size_t N)
	uint3 block_dim = {8,8,8};
	uint3 grid_dim = {cut_size.x/block_dim.x + 1, cut_size.y/block_dim.y + 1, cut_size.z/block_dim.z + 1};
	fft_embed<<<grid_dim,block_dim>>>(instance->d_f1, target_cut._instance_registry[gpu_index].d_data, fft_size, cut_size, vol(fft_size));
	square_real<<<grid_dim,block_dim>>>(instance->d_f1,instance->d_f1sq,cut_size,fft_size);	
	cudaDeviceSynchronize();
	cufftExecC2C(fwplan, instance->d_f1,instance-> d_f1, CUFFT_FORWARD);
	cufftExecC2C(fwplan, instance->d_f1sq, instance->d_f1sq, CUFFT_FORWARD);
	cufftDestroy(fwplan);
	
	//parameters to be used later for cufft calls and planning!
	int n[3] = {(int) fft_size.x, (int) fft_size.y, (int) fft_size.z};
	int inembed[] = {(int) fft_size.z, (int) fft_size.y, (int) fft_size.x};
   	int onembed[] = {(int) fft_size.z, (int) fft_size.y, (int) fft_size.x};
	
	//for memory assessment
	int R = 0;
	size_t _free;
	size_t _total;
	size_t workSize = 0;
	//ncc, f2_multi 
	//float, cufftComplex, cufftComplex, cufftComplex
	size_t dataSize = sizeof(float) + sizeof(cufftComplex);
	CudaSafeCall(cudaMemGetInfo ( &_free, &_total ));
	_free = (size_t)(0.9f*_free);
	while (workSize < _free){
		R += 1;
		cufftEstimateMany(3, n,inembed, 1, vol(fft_size), onembed, 1, vol(fft_size), CUFFT_C2C , R, &workSize);
		workSize += R*vol(fft_size)*dataSize;
	}
	instance->R[0] = R;
	Rs[gpu_index] = R;	
	printf("Rotations feasible on gpu %i: %i ...\n",gpu_index,R);
}

void CamFFTScan::initFFT(int *const& Rs, const int& gpu_index){
    CudaSafeCall(cudaSetDevice(gpu_index));
    CamFFTScan_Register * instance = &_instance_registry[gpu_index];
	query.average(query_threshold, f2avg, f2normsq, msum, gpu_index);
	//void createInstanceCutAround(Density *const& source, const float3 & position, const uint3 & pix_dim, const int &gpu_index);

	CudaSafeCall(cudaMalloc((void**)&instance->d_f1, vol(fft_size) * sizeof(cufftComplex)));
	CudaSafeCall(cudaMalloc((void**)&instance->d_f1sq, vol(fft_size) * sizeof(cufftComplex)));
	
	cufftHandle fwplan;
	cufftPlan3d(&fwplan, fft_size.z, fft_size.y, fft_size.x, CUFFT_C2C);
	//__global__
	//fft_embed(cufftComplex * d_fft_area, float * d_data, uint3 fft_dim, uint3 data_dim, size_t N)
	uint3 block_dim = {8,8,8};
	uint3 grid_dim = {cut_size.x/block_dim.x + 1, cut_size.y/block_dim.y + 1, cut_size.z/block_dim.z + 1};
	fft_embed<<<grid_dim,block_dim>>>(instance->d_f1, target_cut._instance_registry[gpu_index].d_data, fft_size, cut_size, vol(fft_size));
	square_real<<<grid_dim,block_dim>>>(instance->d_f1,instance->d_f1sq,cut_size,fft_size);	
	cudaDeviceSynchronize();
	cufftExecC2C(fwplan, instance->d_f1,instance-> d_f1, CUFFT_FORWARD);
	cufftExecC2C(fwplan, instance->d_f1sq, instance->d_f1sq, CUFFT_FORWARD);
	cufftDestroy(fwplan);
	
	//parameters to be used later for cufft calls and planning!
	int n[3] = {(int) fft_size.x, (int) fft_size.y, (int) fft_size.z};
	int inembed[] = {(int) fft_size.z, (int) fft_size.y, (int) fft_size.x};
   	int onembed[] = {(int) fft_size.z, (int) fft_size.y, (int) fft_size.x};
	
	//for memory assessment
	int R = 0;
	size_t _free;
	size_t _total;
	size_t workSize = 0;
	//ncc, d_F1M, f2_multi, m_multi 
	//float, cufftComplex, cufftComplex, cufftComplex
	size_t dataSize = sizeof(float) + 3*sizeof(cufftComplex);
	CudaSafeCall(cudaMemGetInfo ( &_free, &_total ));
	_free = (size_t)(0.9f*_free);
	while (workSize < _free){
		R += 1;
		cufftEstimateMany(3, n,inembed, 1, vol(fft_size), onembed, 1, vol(fft_size), CUFFT_C2C , R, &workSize);
		workSize += R*vol(fft_size)*dataSize;
	}
	instance->R[0] = R;
	Rs[gpu_index] = R;	
	printf("Rotations feasible on gpu %i: %i ...\n",gpu_index,R);
}

void CamFFTScan::calculateEntropy(float *const& h_entropies, uint3 rot_dim, const char * working_dir, const int& num_gpus, const int& gpu_index){
    CudaSafeCall(cudaSetDevice(gpu_index));
	int R_total = vol(rot_dim);
	float * h_averages = new float[R_total];	
	float * d_averages;
	CudaSafeCall(cudaMalloc((void **)&d_averages,R_total*sizeof(*d_averages)));
	float rotavg;
	//get rotational average
	std::string output_dir(working_dir);
	std::string avg_basename("averages_");
	std::vector<std::string> row;
	std::string line, word;
	int index;
	float c_avg;
	for (int g=0; g < num_gpus; g++){
		std::string avg_fname = output_dir + std::string("/") + avg_basename + std::to_string(g) + std::string(".txt");
		std::ifstream avg_in(avg_fname.c_str(),std::ofstream::in);
		if (avg_in.is_open())
		{
			while(getline(avg_in,line)){
				row.clear();
				stringstream str(line);
				while(getline(str, word, ','))
					row.push_back(word);
				index = std::stoi(row[0]);
				c_avg = std::stof(row[1]);
				h_averages[index] = c_avg;
			}
		}
	}
	rot_nodes.quadrature(h_averages, rotavg, gpu_index);
	printf("Rotaverage is: %f\n",rotavg);
	//CudaSafeCall(cudaMemcpy(d_averages, h_averages, R_total*sizeof(*d_averages),cudaMemcpyHostToDevice));
	size_t max_densities = 100; //not so good, should check
	uint3 ncc_vol = {199,199,199};
	float *h_workspace = new float[max_densities*vol(ncc_vol)];
	float *d_workspace;
	float *d_entropies;
	CudaSafeCall(cudaMalloc((void **)&d_workspace,max_densities*vol(ncc_vol)*sizeof(*d_workspace)));
	CudaSafeCall(cudaMalloc((void **)&d_entropies,max_densities*sizeof(*d_entropies)));
	
	//load single files
	int r_offset = 0;
	int R_global_stride = max_densities*num_gpus;
	for (int l = 0; l < gpu_index; l++)
		r_offset += max_densities;
	
	int r_vol;
	std::string fname;
	while (r_offset < R_total){
		r_vol = std::min((int) max_densities,R_total - r_offset);
		//load file contents
		for (int r = 0; r < r_vol; r++){
			fname = output_dir + std::string("/ncc_")+ std::to_string(r_offset + r) + std::string(".mrc");
			CMrcReader::writeTo(fname.c_str(),h_workspace + r*vol(ncc_vol));
		}
		printf("Loaded files from %i - %i\n",r_offset, r_offset + r_vol);
		CudaSafeCall(cudaMemcpy(d_workspace, h_workspace, r_vol*vol(ncc_vol)*sizeof(*d_workspace), cudaMemcpyHostToDevice));
		//entropies<<<r_vol,512>>>(d_workspace, d_entropies, rotavg, vol(ncc_vol));
		
		//entropies<<<r_vol,512>>>(d_workspace, d_entropies, d_averages, vol(ncc_vol));
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(h_entropies + r_offset, d_entropies, r_vol * sizeof(*h_entropies), cudaMemcpyDeviceToHost)); 
		r_offset += R_global_stride;	
	}
	cudaDeviceSynchronize();
}

void CamFFTScan::genDistsAndAvgs(int * Rs, int num_gpus, const int& gpu_index){
    CudaSafeCall(cudaSetDevice(gpu_index));
 	CamFFTScan_Register * instance = &_instance_registry[gpu_index];
	
	float pix_size;
	target.getPixelSize(pix_size,gpu_index);
	
	std::string avg_fname = output_dir + std::string("/") + avg_basename + std::to_string(gpu_index) + std::string(".txt");
	std::ofstream avg_out(avg_fname.c_str(),std::ofstream::out);
	//changes per rot
	cufftComplex  *d_F1M;
	float *h_ncc,*d_ncc;
	float4 q_0;
	float4 q_offset;
	rot_nodes.getRotations(&q_0,0,1,gpu_index);
	q_offset = q_div(q_max,q_0);
	//parameters to be used later for cufft calls
	int n[3] = {(int) fft_size.x, (int) fft_size.y, (int) fft_size.z};
	int inembed[] = {(int) fft_size.z, (int) fft_size.y, (int) fft_size.x};
   	int onembed[] = {(int) fft_size.z, (int) fft_size.y, (int) fft_size.x};
	
	cufftHandle fwplan;
	cufftPlan3d(&fwplan, fft_size.z, fft_size.y, fft_size.x, CUFFT_C2C);
	//__global__
	//fft_embed(cufftComplex * d_fft_area, float * d_data, uint3 fft_dim, uint3 data_dim, size_t N)
	uint3 block_dim = {8,8,8};
	uint3 grid_dim = {cut_size.x/block_dim.x + 1, cut_size.y/block_dim.y + 1, cut_size.z/block_dim.z + 1};
	int R = instance->R[0];
	float4 qs[R];
	float4 * d_qs;
	CudaSafeCall(cudaMalloc((void **) &d_qs,R*sizeof(*d_qs)));
	h_ncc = new float[R*vol(ncc_vol)];
	CudaSafeCall(cudaMalloc((void**)&d_ncc, R*vol(ncc_vol) * sizeof(*d_ncc)));
	CudaSafeCall(cudaMalloc((void**)&d_F1M, R*vol(fft_size) * sizeof(cufftComplex)));

	f2_multi.createMultiInstance(fft_size, pix_size, R, gpu_index);
	m_multi.createMultiInstance(fft_size, pix_size, R, gpu_index);

	float * h_averages, *d_averages;
	h_averages = new float[R_total];
	CudaSafeCall(cudaMalloc((void **)&d_averages,R_total*sizeof(*d_averages)));
	
	cufftHandle fwplanMany;
	cufftPlanMany(&fwplanMany, 3, n, inembed, 1, vol(fft_size), onembed, 1, vol(fft_size), CUFFT_C2C, R);
	int threads = 256;
	int blocks = (R*vol(fft_size))/threads + 1;
	block_dim = {8,8,8};
	int r_offset = 0;
	int R_global_stride = 0;
	for (int l = 0; l < gpu_index; l++)
		r_offset += Rs[l];
	for (int g = 0; g < num_gpus; g++)
		R_global_stride += Rs[g];
	grid_dim = {ncc_vol.x/block_dim.x + 1, ncc_vol.y/block_dim.y + 1, ncc_vol.z/block_dim.z + 1};
	std::string fname;
	while (r_offset < R_total){
		//check if some of the work is already done
		//check_files(std::string output_dir, std::string basename, size_t offset , size_t _vol)
		if (check_files(output_dir, ncc_basename, r_offset, R)){
			r_offset += R_global_stride;
			continue;
		}
		//init workspace
		initValue<cufftComplex><<<128,512>>>(f2_multi._instance_registry[gpu_index].d_cdata,R*vol(fft_size),{0.0f,0.0f});
		initValue<cufftComplex><<<128,512>>>(m_multi._instance_registry[gpu_index].d_cdata,R*vol(fft_size),{0.0f,0.0f});
		
		rot_nodes.getRotations(qs,r_offset, R, gpu_index);
		for (int r = 0; r < R; ++r)
		{
			qs[r] = q_mult(q_offset,qs[r]);
		}
		//rotates
		CudaSafeCall(cudaMemcpy(d_qs,qs,R*sizeof(*d_qs),cudaMemcpyHostToDevice));
		//void fft_prepare(cudaTextureObject_t texobj, float * d_data, float * d_mask, float threshold, float4 * qs, uint3 cut_size, uint3 fft_size, int R){
		fft_prepare<<<grid_dim,block_dim>>>(query._instance_registry[gpu_index].textureObj[0],f2_multi._instance_registry[gpu_index].d_cdata, m_multi._instance_registry[gpu_index].d_cdata, query_threshold, d_qs, cut_size, fft_size, R);
		cufftExecC2C(fwplanMany, f2_multi._instance_registry[gpu_index].d_cdata, f2_multi._instance_registry[gpu_index].d_cdata, CUFFT_FORWARD);
		cufftExecC2C(fwplanMany, m_multi._instance_registry[gpu_index].d_cdata, m_multi._instance_registry[gpu_index].d_cdata, CUFFT_FORWARD);
		
		//order! 
		pointwise_product<<<blocks, threads>>>(instance->d_f1, m_multi._instance_registry[gpu_index].d_cdata, d_F1M, vol(fft_size),R);
		pointwise_product<<<blocks, threads>>>(instance->d_f1, f2_multi._instance_registry[gpu_index].d_cdata, vol(fft_size),R);
		pointwise_product<<<blocks, threads>>>(instance->d_f1sq, m_multi._instance_registry[gpu_index].d_cdata, vol(fft_size),R);
		cudaDeviceSynchronize();
		cufftExecC2C(fwplanMany, f2_multi._instance_registry[gpu_index].d_cdata, f2_multi._instance_registry[gpu_index].d_cdata, CUFFT_INVERSE);
		cufftExecC2C(fwplanMany, m_multi._instance_registry[gpu_index].d_cdata, m_multi._instance_registry[gpu_index].d_cdata, CUFFT_INVERSE);
		cufftExecC2C(fwplanMany, d_F1M, d_F1M, CUFFT_INVERSE);
		cudaDeviceSynchronize();
		
		//void ncc(float * d_ncc, cufftComplex * d_iF1F2, cufftComplex * d_iF1M2, cufftComplex * d_iF1sqM1, float f2avg, float f2normsq, float m2sum, uint3 ncc_offset, uint3 ncc_vol, uint3 fft_size, size_t R, float eps){
		ncc<<<grid_dim,block_dim>>>(d_ncc, f2_multi._instance_registry[gpu_index].d_cdata, d_F1M, m_multi._instance_registry[gpu_index].d_cdata, f2avg, f2normsq, msum, ncc_offset, ncc_vol, fft_size, R, 1e-6, 1000.0f);
		cudaDeviceSynchronize();
		//__global__
		//void averages(float * d_ncc, float * d_averages, size_t N)
		averages<<<R,512>>>(d_ncc, d_averages, vol(ncc_vol));
		CudaCheckError();
		CudaSafeCall(cudaMemcpy(h_ncc, d_ncc, R * vol(ncc_vol) * sizeof(*h_ncc), cudaMemcpyDeviceToHost)); 
		CudaSafeCall(cudaMemcpy(h_averages, d_averages, R  * sizeof(*h_averages), cudaMemcpyDeviceToHost)); 

		for (int r = 0; r < R; r++){
			avg_out  << r_offset + r << ","  << h_averages[r] << std::endl;
		}
		avg_out.flush();
		for (int r = 0; r < R; r++){
			fname = output_dir + std::string("/") + ncc_basename + std::to_string(r_offset + r) + std::string(".mrc");
			Density::toMrcRaw(fname.c_str(),ncc_vol,{pix_size * ncc_vol.x,pix_size * ncc_vol.y,pix_size * ncc_vol.z},h_ncc + r*vol(ncc_vol));
		}
		r_offset += R_global_stride;
	}
	cudaDeviceSynchronize();
	//free most stuff for space
	CudaSafeCall(cudaFree(instance->d_f1));
	CudaSafeCall(cudaFree(instance->d_f1sq));
	CudaSafeCall(cudaFree(d_ncc));
	CudaSafeCall(cudaFree(d_averages));
	delete h_averages;
	CudaSafeCall(cudaFree(d_F1M));
	f2_multi.freeInstance(gpu_index);
	m_multi.freeInstance(gpu_index);
	cufftDestroy(fwplanMany);
}

void CamFFTScan::genDistsAndAvgsOV(int * Rs, int num_gpus, const int& gpu_index){
    CudaSafeCall(cudaSetDevice(gpu_index));
 	CamFFTScan_Register * instance = &_instance_registry[gpu_index];
	
	float pix_size;
	target.getPixelSize(pix_size,gpu_index);
	
	std::string avg_fname = output_dir + std::string("/") + avg_basename + std::to_string(gpu_index) + std::string(".txt");
	std::ofstream avg_out(avg_fname.c_str(),std::ofstream::out);
	//changes per rot
	float *h_ov,*d_ov;
	float4 q_0;
	float4 q_offset;
	rot_nodes.getRotations(&q_0,0,1,gpu_index);
	q_offset = q_div(q_max,q_0);
	//parameters to be used later for cufft calls
	int n[3] = {(int) fft_size.x, (int) fft_size.y, (int) fft_size.z};
	int inembed[] = {(int) fft_size.z, (int) fft_size.y, (int) fft_size.x};
   	int onembed[] = {(int) fft_size.z, (int) fft_size.y, (int) fft_size.x};
	
	cufftHandle fwplan;
	cufftPlan3d(&fwplan, fft_size.z, fft_size.y, fft_size.x, CUFFT_C2C);
	//__global__
	//fft_embed(cufftComplex * d_fft_area, float * d_data, uint3 fft_dim, uint3 data_dim, size_t N)
	uint3 block_dim = {8,8,8};
	uint3 grid_dim = {cut_size.x/block_dim.x + 1, cut_size.y/block_dim.y + 1, cut_size.z/block_dim.z + 1};
	int R = instance->R[0];
	float4 qs[R];
	float4 * d_qs;
	CudaSafeCall(cudaMalloc((void **) &d_qs,R*sizeof(*d_qs)));
	h_ov = new float[R*vol(ncc_vol)];
	CudaSafeCall(cudaMalloc((void**)&d_ov, R*vol(ncc_vol) * sizeof(*d_ov)));

	f2_multi.createMultiInstance(fft_size, pix_size, R, gpu_index);

	float * h_averages, *d_averages;
	h_averages = new float[R_total];
	CudaSafeCall(cudaMalloc((void **)&d_averages,R_total*sizeof(*d_averages)));
	
	cufftHandle fwplanMany;
	cufftPlanMany(&fwplanMany, 3, n, inembed, 1, vol(fft_size), onembed, 1, vol(fft_size), CUFFT_C2C, R);
	int threads = 256;
	int blocks = (R*vol(fft_size))/threads + 1;
	block_dim = {8,8,8};
	int r_offset = 0;
	int R_global_stride = 0;
	for (int l = 0; l < gpu_index; l++)
		r_offset += Rs[l];
	for (int g = 0; g < num_gpus; g++)
		R_global_stride += Rs[g];
	grid_dim = {ncc_vol.x/block_dim.x + 1, ncc_vol.y/block_dim.y + 1, ncc_vol.z/block_dim.z + 1};
	std::string fname;
	while (r_offset < R_total){
		//check if some of the work is already done
		//check_files(std::string output_dir, std::string basename, size_t offset , size_t _vol)
		if (check_files(output_dir, ncc_basename, r_offset, R)){
			r_offset += R_global_stride;
			continue;
		}
		//init workspace
		initValue<cufftComplex><<<128,512>>>(f2_multi._instance_registry[gpu_index].d_cdata,R*vol(fft_size),{0.0f,0.0f});
		rot_nodes.getRotations(qs,r_offset, R, gpu_index);
		for (int r = 0; r < R; ++r)
		{
			qs[r] = q_mult(q_offset,qs[r]);
		}
		//rotates
		CudaSafeCall(cudaMemcpy(d_qs,qs,R*sizeof(*d_qs),cudaMemcpyHostToDevice));
		//void fft_prepare(cudaTextureObject_t texobj, float * d_data, float threshold, float4 * qs, uint3 cut_size, uint3 fft_size, int R){
		fft_prepareOV<<<grid_dim,block_dim>>>(query._instance_registry[gpu_index].textureObj[0],f2_multi._instance_registry[gpu_index].d_cdata, query_threshold, d_qs, cut_size, fft_size, R);
		cufftExecC2C(fwplanMany, f2_multi._instance_registry[gpu_index].d_cdata, f2_multi._instance_registry[gpu_index].d_cdata, CUFFT_FORWARD);
		
		//order! 
		pointwise_product<<<blocks, threads>>>(instance->d_f1, f2_multi._instance_registry[gpu_index].d_cdata, vol(fft_size),R);
		cudaDeviceSynchronize();
		cufftExecC2C(fwplanMany, f2_multi._instance_registry[gpu_index].d_cdata, f2_multi._instance_registry[gpu_index].d_cdata, CUFFT_INVERSE);
		cudaDeviceSynchronize();
		
		ov<<<grid_dim,block_dim>>>(d_ov, f2_multi._instance_registry[gpu_index].d_cdata,  ncc_offset, ncc_vol, fft_size, R);
		cudaDeviceSynchronize();
		//__global__
		//void averages(float * d_ncc, float * d_averages, size_t N)
		averages<<<R,512>>>(d_ov, d_averages, vol(ncc_vol));
		CudaCheckError();
		CudaSafeCall(cudaMemcpy(h_ov, d_ov, R * vol(ncc_vol) * sizeof(*h_ov), cudaMemcpyDeviceToHost)); 
		CudaSafeCall(cudaMemcpy(h_averages, d_averages, R  * sizeof(*h_averages), cudaMemcpyDeviceToHost)); 

		for (int r = 0; r < R; r++){
			avg_out  << r_offset + r << ","  << h_averages[r] << std::endl;
		}
		avg_out.flush();
		for (int r = 0; r < R; r++){
			fname = output_dir + std::string("/") + ncc_basename + std::to_string(r_offset + r) + std::string(".mrc");
			Density::toMrcRaw(fname.c_str(),ncc_vol,{pix_size * ncc_vol.x,pix_size * ncc_vol.y,pix_size * ncc_vol.z},h_ov + r*vol(ncc_vol));
		}
		r_offset += R_global_stride;
	}
	cudaDeviceSynchronize();
	//free most stuff for space
	CudaSafeCall(cudaFree(instance->d_f1));
	CudaSafeCall(cudaFree(instance->d_f1sq));
	CudaSafeCall(cudaFree(d_ov));
	CudaSafeCall(cudaFree(d_averages));
	delete h_averages;
	f2_multi.freeInstance(gpu_index);
	cufftDestroy(fwplanMany);
}

