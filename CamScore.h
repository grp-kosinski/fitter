/*
 * CamScore.h
 *
 *  Created on: Mar 13, 2021
 *      Author: kkarius
 */

#ifndef CAMSCORE_H_
#define CAMSCORE_H_

#include<map>
#include<util.h>
#include<cuda_util_include/cutil_math.h>
#include<derived_atomic_functions.h>

#include<ChebychevNodes.h>
#include<McEwenNodes.h>
#include<Density.h>

struct CamScore_Register {
	ChebychevNodes_Register * trans_node = nullptr;
	McEwenNodes_Register * rot_node = nullptr;
	float * d_v_ave = nullptr;
	float * d_v_m_v_ave = nullptr;
	float * d_u_t_v = nullptr;
	float * d_dist = nullptr;
	float * d_working_mem = nullptr;
	float * d_entropies = nullptr;
	float * h_cam = nullptr;
	float * h_rot_nodes = nullptr;
	float * d_rot_nodes = nullptr;
	size_t * h_rot_vol = nullptr;
	size_t * d_rot_vol = nullptr;
	size_t * h_translation_vol = nullptr;
	size_t * d_translation_vol = nullptr;

};


class CamScore {
public:
	CamScore(){};
	virtual ~CamScore(){};
	void createInstance(ChebychevNodes *const& trans_nodes, McEwenNodes *const& rot_nodes, const size_t & translation_vol, const int & gpu_index);
	void createInstance(ChebychevNodes *const& trans_nodes, const char * file_rot_nodes, const size_t & translation_vol, const int & gpu_index);
	void score(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes,
	const int& translation_offset, const int& translation_volume, const int& gridDim, const int& blockDim, const int & gpu_index);
	void divAndEnt(ChebychevNodes * const& trans_nodes,const int& translation_offset,
			const int& translation_volume, float * const& h_quad, float * const& h_result, const int& gridDim,
			const int& blockDim, const int & gpu_index);
	void scoreAndMin(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes,
			const int& translation_offset, const int& translation_volume, float * const& h_result,float * const& h_translation_vol,
			const int& gridDim, const int& blockDim, const int & gpu_index);
	void scoreBench(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes,
				const int& translation_offset, const int& translation_volume,float * const& h_translation_vol,
				const int& gridDim, const int& blockDim, const int & gpu_index);
	void scoreSimple(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes,
					const int& translation_offset, const int& translation_volume,float * const& h_translation_vol,
					const int& gridDim, const int& blockDim, const int & gpu_index);
	void shiftAndQuad(ChebychevNodes * const& trans_nodes,const int& translation_offset,
			const int& translation_volume, float * const& h_min, float * const& h_result, const int& gridDim,
			const int& blockDim, const int & gpu_index);
	size_t getMaxTranslations(McEwenNodes *const& rot_nodes,const int& gpu_index);
	void destroyInstance(const int & gpu_index);
	void score_filenodes(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes, const char * out_file,
	const int& translation_offset, const int& translation_volume, const int& gridDim, const int& blockDim, const int & gpu_index);
//	void index_to_transformation(Density *const& target, int s, const size_t& index, float3 * const& translation, float3 * const& eulers,const int& gpu_index);
	void entropy_test_zero(const int & gpu_index);
	void entropy_test_one(const int & gpu_index);
	void entropy_test_two(const int & gpu_index);
	void entropy_test_three(const int & gpu_index);
	void entropy_test_four(const int & gpu_index);
	std::map<int,CamScore_Register> _instance_registry;
};

#endif /* CAMSCORE_H_ */
