#define BOOST_TEST_MODULE ccl_test
#include <boost/test/included/unit_test.hpp>
//#include <Density.h>
//#include <Labeler.h>
//#include <Kernels.h>
////#include <Sampler.h>
//#include <chrono>
//
//#include <thrust/device_vector.h>
//#include <thrust/replace.h>
//#include <thrust/iterator/counting_iterator.h>
//#include <thrust/iterator/transform_iterator.h>
//#include <thrust/sort.h>
//#include <thrust/device_ptr.h>
//#include <thrust/adjacent_difference.h>
//#include <thrust/transform_reduce.h>
//#include <thrust/functional.h>
#include <cuda.h>
//
//#include <cub/device/device_radix_sort.cuh>
//
//#include <functional>
//#include <derived_atomic_functions.h>
//#include <cuda_util_include/cutil_math.h>
//#include <TransformationGrid.h>
//
//#include <fstream>

//
//__global__
//void shift_coords(float4 * d_coords_4,size_t num_coords, float4 shift){
//        int coord_id = blockIdx.x*blockDim.x + threadIdx.x;
//        if (coord_id>=num_coords)
//                return;
//        d_coords_4[coord_id] += shift;
//}
//
//
//__device__
//void rotate(float4 *  position_0, float4 * position_r, float4 * rotation){
//	// rot_quat = [a,b,c,d]
//	// pos_quat = [0,x,y,z] - in memory as [x,y,z,0]
//	//  (a^2*x + 2*a*c*z - 2*a*d*y + b^2*x + 2*b*c*y + 2*b*d*z - c^2*x - d^2*x)*i
//	//+ (a^2*y - 2*a*b*z + 2*a*d*x - b^2*y + 2*b*c*x + 2*c*d*z + c^2*y - d^2*y)*j
//	//+ (a^2*z + 2*a*b*y - 2*a*c*x - b^2*z + 2*b*d*x - c^2*z + 2*c*d*y + d^2*z)*k
//
//	//x (a^2*x + 2*a*c*z - 2*a*d*y + b^2*x + 2*b*c*y + 2*b*d*z - c^2*x - d^2*x)
//	position_r->x = rotation->x*rotation->x*position_0->x
//	    + 2*rotation->x*rotation->z*position_0->z
//	    - 2*rotation->x*rotation->w*position_0->y
//	    + rotation->y*rotation->y*position_0->x
//            + 2*rotation->y*rotation->z*position_0->y
//  	    + 2*rotation->y*rotation->w*position_0->z
//	    - rotation->z*rotation->z*position_0->x
//	    - rotation->w*rotation->w*position_0->x;
//	//y (a^2*y - 2*a*b*z + 2*a*d*x - b^2*y + 2*b*c*x + 2*c*d*z + c^2*y - d^2*y)
//	position_r->y = rotation->x*rotation->x*position_0->y
//	    - 2*rotation->x*rotation->y*position_0->z
//	    + 2*rotation->x*rotation->w*position_0->x
//	    - rotation->y*rotation->y*position_0->y
//	    + 2*rotation->y*rotation->z*position_0->x
//	    + 2*rotation->z*rotation->w*position_0->z
//            + rotation->z*rotation->z*position_0->y
//	    - rotation->w*rotation->w*position_0->y;
//	//z (a^2*z + 2*a*b*y - 2*a*c*x - b^2*z + 2*b*d*x + 2*c*d*y - c^2*z + d^2*z)
//	position_r->z = rotation->x*rotation->x*position_0->z
//	    + 2*rotation->x*rotation->y*position_0->y
//	    - 2*rotation->x*rotation->z*position_0->x
//	    - rotation->y*rotation->y*position_0->z
//	    + 2*rotation->y*rotation->z*position_0->x
//	    + 2*rotation->z*rotation->w*position_0->y
//	    - rotation->z*rotation->z*position_0->z
//	    + rotation->w*rotation->w*position_0->z;
////	printf("%f %f\n",position_0->x*position_0->x+position_0->y*position_0->y+position_0->z*position_0->z,position_r->x*position_r->x+position_r->y*position_r->y+position_r->z*position_r->z);
////	printf("%f %f %f -> %f %f %f |%f %f\n",position_0->x,position_0->y,position_0->z,position_r->x,position_r->y,position_r->z,position_0->x*position_0->x+position_0->y*position_0->y+position_0->z*position_0->z,position_r->x*position_r->x+position_r->y*position_r->y+position_r->z*position_r->z);
//}
//
//
//struct linear_offset_to_coord : public thrust::unary_function<uint,float4>
//{
//	uint * d_pixel_dim;
//	float pixel_size;
//    __device__
//	float4 operator()(const uint& u) const {
//    	float4 ret;
//    	uint3 index;
//    	index.z = u/(d_pixel_dim[0]*d_pixel_dim[1]);
//    	index.y = (u - index.z*d_pixel_dim[0]*d_pixel_dim[1])/d_pixel_dim[0];
//    	index.x = u - index.z*d_pixel_dim[0]*d_pixel_dim[1] - index.y*d_pixel_dim[0];
//    	ret.x = (__uint2float_rn(index.x) + 0.5)*pixel_size;
//    	ret.y = (__uint2float_rn(index.y) + 0.5)*pixel_size;
//    	ret.z = (__uint2float_rn(index.z) + 0.5)*pixel_size;
//    	ret.w = 0.0;
//    	return ret;
//	}
//};
//


//template <size_t block_size>
//__global__
//void envelope_score(Density binarized_map, float * d_workspaces, float * d_value_buffers, uint * d_linear_offsets, float4 * d_coords,size_t num_particles , TransformationGrid grid){
//	__shared__ float4 rotation;
//	__shared__ float4 translation;
//	__shared__ float reduce[block_size];
//	float * d_data = binarized_map.d_data;
//	uint pixel_vol = binarized_map.d_pixel_vol();
//	float * d_workspace = d_workspaces + blockIdx.x*pixel_vol;
//	float * d_value_buffer = d_value_buffers + blockIdx.x*num_particles;
//	uint * d_linear_offset = d_linear_offsets + blockIdx.x*num_particles;
//	int t,b;
//	uint particle_pixel_linear;
//	uint * d_pixel_dim = binarized_map.d_pixel_dim;
//	float * d_pixel_size = binarized_map.d_pixel_size;
//
//	float4 coord;
//	float4 coord_rot;
//
//	//binarized data to workspace
//	t = threadIdx.x;
//	while(t < pixel_vol){
//		d_workspace[t] = d_data[t];
//		t += blockDim.x;
//	}
//	__syncthreads();
//
//	b = blockIdx.x;
//	while (b < grid.d_num_transformations[0]){
//		//transformation to shared
//		if(threadIdx.x == 0){
//			grid.d_transformation_to_memory(b,&rotation,&translation);
//			printf("Block %i handling, iteration %i transformation %f %f %f %f - %f %f %f\n",blockIdx.x,b,rotation.x,rotation.y,rotation.z,rotation.w,translation.x,translation.y,translation.z);
//		}
//		__syncthreads();
//		//imprint particle ids, iterate over particles
//		t = threadIdx.x;
//		while(t < num_particles){
//			coord = d_coords[t];
//			//rotate using existing coordinate
//			rotate(&coord, &coord_rot, &rotation);
//			coord_rot += translation;
//			//linearize and imprint
//			particle_pixel_linear = coord_to_linear_offset(&coord_rot,d_pixel_dim,d_pixel_size);
//			d_linear_offset[t] = particle_pixel_linear;
//			d_value_buffer[t] = d_data[particle_pixel_linear];
////			d_value_buffer[t] += blockIdx.x;
//			if (d_workspace[particle_pixel_linear] == -1){
//				d_workspace[particle_pixel_linear] = 2;
//			}
//			else if (d_workspace[particle_pixel_linear] == 0){
//				d_workspace[particle_pixel_linear] = -2;
//			}
//			t += blockDim.x;
//		}
//
//
//		__syncthreads();
//		//reduce, iterate over pixels
//		t = threadIdx.x;
//		reduce[threadIdx.x] = 0;
//		while(t < pixel_vol){
//			reduce[threadIdx.x] += d_workspace[t];
//			t += blockDim.x;
//		}
//		__syncthreads();
//
////		if (threadIdx.x < 512) reduce[threadIdx.x] += reduce[threadIdx.x + 512]; __syncthreads();
////		if (threadIdx.x < 256) reduce[threadIdx.x] += reduce[threadIdx.x + 256]; __syncthreads();
//		if (threadIdx.x < 128) reduce[threadIdx.x] += reduce[threadIdx.x + 128]; __syncthreads();
//		if (threadIdx.x < 64) reduce[threadIdx.x] += reduce[threadIdx.x + 64]; __syncthreads();
//		if (threadIdx.x < 32) reduce[threadIdx.x] += reduce[threadIdx.x + 32]; __syncthreads();
//		if (threadIdx.x < 16) reduce[threadIdx.x] += reduce[threadIdx.x + 16]; __syncthreads();
//		if (threadIdx.x < 8) reduce[threadIdx.x] += reduce[threadIdx.x + 8]; __syncthreads();
//		if (threadIdx.x < 4) reduce[threadIdx.x] += reduce[threadIdx.x + 4]; __syncthreads();
//		if (threadIdx.x < 2) reduce[threadIdx.x] += reduce[threadIdx.x + 2]; __syncthreads();
//		if (threadIdx.x < 1) reduce[threadIdx.x] += reduce[threadIdx.x + 1]; __syncthreads();
//		grid.d_scores[b] = reduce[0];
//		__syncthreads();
////		if (threadIdx.x == 0 && blockIdx.x == 0){
////			printf("%i %i %f\n",blockIdx.x,b,reduce[0]);
////		}
////		//replace, iterate over particles
//		t = threadIdx.x;
//		while(t < num_particles){
//			d_workspace[d_linear_offset[t]] = d_data[d_linear_offset[t]];
//			t += blockDim.x;
//		}
//		__syncthreads();
////		if (blockIdx.x == 0 && threadIdx.x == 0){
////			float s = 0;
////			for (int i=0;i<pixel_vol;i++) s+= d_workspace[i];
////			printf("%i %f\n",b,s);
////		}
//
//		b+=gridDim.x;
//	}
//}
//
//
__device__
uint3 d_linear_index_to_pixel_index(const uint & linear_index, uint * const &d_pixel_dim){
	uint3 ret;
	ret.z = linear_index/(d_pixel_dim[0] * d_pixel_dim[1]);
	ret.y = (linear_index - ret.z*(d_pixel_dim[0] * d_pixel_dim[1]))/d_pixel_dim[0];
	ret.x = linear_index - ret.z*(d_pixel_dim[0] * d_pixel_dim[1]) - ret.y*d_pixel_dim[0];
	return ret;
}

__device__
uint d_pixel_index_to_linear_index(const uint3 & pixel_index, uint * const &d_pixel_dim){
	return pixel_index.z*d_pixel_dim[0]*d_pixel_dim[1] + pixel_index.y*d_pixel_dim[0] + pixel_index.x;
}
//
__device__
uint d_linear_to_linear_index(const uint & linear_index, uint * const &d_pixel_dim_old, uint * const &d_pixel_dim_new){
	return d_pixel_index_to_linear_index(d_linear_index_to_pixel_index(linear_index,d_pixel_dim_old),d_pixel_dim_new);
}
//
//struct out_of_vol : public thrust::unary_function<uint,bool>
//{
//	uint * d_pixel_dim;
//	uint * d_pixel_dim_padding;
//    __device__
//	bool operator()(const uint& u) const {
//    	uint3 index_address = d_linear_index_to_pixel_index(u,d_pixel_dim_padding);
//    	bool ret = !((index_address.x<d_pixel_dim[0]) && (index_address.y<d_pixel_dim[1]) && (index_address.z<d_pixel_dim[2]));
//    	printf("%u %u %u < %u %u %u | %s\n",index_address.x,index_address.y,index_address.z,d_pixel_dim[0],d_pixel_dim[1],d_pixel_dim[2],ret ? "true":"false");
//    	return ret;
//	}
//};
//
//struct greater_than_zero : public thrust::unary_function<uint,bool>{
//	    __device__
//	bool operator()(const uint& u) const {
//	    return u > 0;
//  	}
//};
//
struct mod_minus : public thrust::binary_function<uint,uint,uint>{
	    __device__
	uint operator()(const uint& u,const uint& v) const {
	    	if (v == 0){
	    		return 0;
	    	} else  {
	    		return u-v;
	    	}
  	}
};
//
__global__
void partial_surface_score(float4 * d_coords, int * d_segmentations, uint * d_labels, float distance_threshold,
		Density difference_map, float base, uint * padding_dim , size_t num_particles,
		int * d_test, TransformationGrid grid, void * d_temp_storage,size_t temp_storage_bytes){
	__shared__ float4 rotation;
	__shared__ float4 translation;
	__shared__ float reduce[128];
	int b = blockIdx.x;
	int t;
	float score;
	uint * d_pixel_dim = difference_map.d_pixel_dim;
	uint num_pixel_padding = padding_dim[0]*padding_dim[1]*padding_dim[2];
	uint num_pixels = d_pixel_dim[0]*d_pixel_dim[1]*d_pixel_dim[2];

	//workspace allocation
	int * segmentation_workspace = d_segmentations + blockIdx.x*num_pixel_padding;
	uint * label_workspace = d_labels + 2*blockIdx.x*num_pixel_padding;
	uint * label_workspace_sort = d_labels + (2*blockIdx.x + 1)*num_pixel_padding;
	void * temp_storage = d_temp_storage + blockIdx.x*temp_storage_bytes;

	float * d_data = difference_map.d_data;
	float * d_pixel_size = difference_map.d_pixel_size;
	float4 coord;
	float4 coord_rot;
	uint linear_address;
	uint linear_address_padding;
	//iterators
//	thrust::counting_iterator<uint> linear_addresses(0);
	thrust::device_vector<uint>::iterator unique_end;

	while (b < grid.d_num_transformations[0]){
		//transformation to shared
		if (threadIdx.x == 0) grid.d_transformation_to_memory(b,&rotation,&translation);
		if (threadIdx.x == 0) printf("%u: %f %f %f %f:",b,rotation.x,rotation.y,rotation.z,rotation.w);
	//	if (threadIdx.x == 0) printf("%u: %f %f %f \n",b,translation.x,translation.y,translation.z);
		__syncthreads();
		//set segments to 0 - referring to padded volume
		t = threadIdx.x;
		while(t<num_pixel_padding){
			segmentation_workspace[t] = 0;
			t += blockDim.x;
		}
		__syncthreads();
		//imprint particles
		t = threadIdx.x;
		while(t < num_particles){
			coord = d_coords[t];
			//rotate using existing coordinate
			rotate(&coord, &coord_rot, &rotation);
			coord_rot += translation;
			//calculate linear offset and imprint segments
			linear_address = coord_to_linear_offset(&coord_rot,d_pixel_dim,d_pixel_size);
			if (d_data[linear_address] <= distance_threshold){
					linear_address_padding = d_linear_to_linear_index(linear_address, d_pixel_dim, padding_dim);
					segmentation_workspace[linear_address_padding] = 1;
			}
			t += blockDim.x;
		}
		__syncthreads();
		//perform ccl
		//void ccl_kernel(Density density, int * seg_data, uint * labels,uint log_2_dim);
		//labels are filled in kernel itself, pixel_dim is already assumed to be log2-whole
		if(threadIdx.x == 0)
			ccl_kernel<<<1,1>>>(segmentation_workspace,label_workspace,padding_dim[0]);
		__syncthreads();
		cudaDeviceSynchronize();
		//get rid of closed spaces
		t = threadIdx.x;
		while(t<num_pixel_padding){
			label_workspace[t] = label_workspace[t]*segmentation_workspace[t];
			t += blockDim.x;
		}
		__syncthreads();
		//postprocess labeling and score
		t = threadIdx.x;
		while(t<num_pixels){
			linear_address = d_linear_to_linear_index(t,d_pixel_dim, padding_dim);
			label_workspace_sort[t] = label_workspace[linear_address];
			t += blockDim.x;
		}
		__syncthreads();
		if (threadIdx.x == 0)
			cub::DeviceRadixSort::SortKeys(temp_storage, temp_storage_bytes,
					 label_workspace_sort,label_workspace, num_pixels);
		__syncthreads();
		cudaDeviceSynchronize();
		if (threadIdx.x == 0)
			thrust::adjacent_difference(thrust::device,thrust::device_pointer_cast(&label_workspace[0]),thrust::device_pointer_cast(&label_workspace[num_pixels]),thrust::device_pointer_cast(&label_workspace[0]));
		__syncthreads();
		cudaDeviceSynchronize();
		//replace non zeros with position
		t = threadIdx.x;
		while(t<num_pixels){
			if (label_workspace[t] != 0){
				label_workspace[t] = t;
			}
			t += blockDim.x;
		}
		__syncthreads();
		//swapping workspaces
		if (threadIdx.x == 0)
			cub::DeviceRadixSort::SortKeys(temp_storage, temp_storage_bytes,
					label_workspace,label_workspace_sort-1,  num_pixels);
		__syncthreads();
		cudaDeviceSynchronize();
		if (threadIdx.x == 0)
				label_workspace_sort[num_pixels-1] = num_pixels;
		__syncthreads();
		if (threadIdx.x == 0)
				thrust::adjacent_difference(thrust::device,thrust::device_pointer_cast(&label_workspace_sort[0]),thrust::device_pointer_cast(&label_workspace_sort[num_pixels]),thrust::device_pointer_cast(&label_workspace_sort[0]),mod_minus());
		__syncthreads();
		cudaDeviceSynchronize();
		t = threadIdx.x;
		reduce[threadIdx.x] = 0;
		while(t < num_pixels){
			if (label_workspace_sort[t] != 0){
				reduce[threadIdx.x] += powf(base,__uint2float_rn(label_workspace_sort[t]));
			}
			t += blockDim.x;
		}
		__syncthreads();
//		if (threadIdx.x < 512) reduce[threadIdx.x] += reduce[threadIdx.x + 512]; __syncthreads();
//		if (threadIdx.x < 256) reduce[threadIdx.x] += reduce[threadIdx.x + 256]; __syncthreads();
//		if (threadIdx.x < 128) reduce[threadIdx.x] += reduce[threadIdx.x + 128]; __syncthreads();
		if (threadIdx.x < 64) reduce[threadIdx.x] += reduce[threadIdx.x + 64]; __syncthreads();
		if (threadIdx.x < 32) reduce[threadIdx.x] += reduce[threadIdx.x + 32]; __syncthreads();
		if (threadIdx.x < 16) reduce[threadIdx.x] += reduce[threadIdx.x + 16]; __syncthreads();
		if (threadIdx.x < 8) reduce[threadIdx.x] += reduce[threadIdx.x + 8]; __syncthreads();
		if (threadIdx.x < 4) reduce[threadIdx.x] += reduce[threadIdx.x + 4]; __syncthreads();
		if (threadIdx.x < 2) reduce[threadIdx.x] += reduce[threadIdx.x + 2]; __syncthreads();
		if (threadIdx.x < 1) reduce[threadIdx.x] += reduce[threadIdx.x + 1]; __syncthreads();
		grid.d_scores[b] = reduce[0];
		b+=gridDim.x;
		if (threadIdx.x == 0){
			printf("%f:\n",reduce[0]);
		}
	}
}

#include<nvml.h>
#include<cuda_util_include/cutil_math.h>

BOOST_AUTO_TEST_CASE(kai_test)
	{
		printf(" ============================= Testing Kai score ===============================\n");
		printf(" ============================= +++++++++++++++++ ===============================\n");
		//memory management
		nvmlInit_v2();
	    //checkout free memory
	    nvmlMemory_t memory_info;
		nvmlDevice_t device_handle;
		nvmlDeviceGetHandleByIndex_v2(0,&device_handle);
		nvmlDeviceGetMemoryInfo(device_handle, &memory_info);

		float coord_dim_outer[3] = {70,70,70};
		float coord_dim[3] = {50,50,50};
		float distance_threshold = 0.5;
		uint pixel_dim_outer[3] = {70,70,70};
		uint pixel_dim[3] = {50,50,50};
		uint3 cube_pixel_dim= {30,30,30};
		Density density;
		Density::from_bounds(density,&coord_dim_outer[0],&pixel_dim_outer[0]);
		density.init_device();
		density.to_device();
		density.mid_cube_with_surface(cube_pixel_dim,1,1.0,1.5);

		Density to_fit_density;
		Density::from_bounds(to_fit_density,&coord_dim[0],&pixel_dim[0]);
		to_fit_density.mid_cube_with_surface(cube_pixel_dim,1,1.0,1.5);

		thrust::device_vector<uint> td_on_surface;
		size_t on_surface_num;
		density.indices_in_range(td_on_surface,true,1.25,2,on_surface_num);

		thrust::device_vector<uint> td_on_surface_to_fit;
		size_t on_surface_num_to_fit;
		to_fit_density.indices_in_range(td_on_surface_to_fit,true,1.25,2,on_surface_num_to_fit);

		Particles pixel_coords = to_fit_density.particles_from_density(td_on_surface_to_fit);
		pixel_coords.origin_to_center();

		Density difference_map;
		density.difference_map_density_threshold(difference_map,1.4,1.6,td_on_surface);
		CudaCheckError();

		float tau_in = 1.0;
		uint n_rot_test = 30;
		std::pair<float3,float3> bounding_box_outer = density.get_bounding_box();
		std::pair<float3,float3> bounding_box_inner = pixel_coords.get_rotationally_safe_bounding_box();
		TransformationGrid transformation_grid(bounding_box_outer,bounding_box_inner,density.h_mid_point,tau_in,"rot_samples/N23_M5880_IcoC7.dat");
		printf("Transformation num: %u\n",transformation_grid.h_num_transformations[0]);
		printf("Translation num: %u\n",transformation_grid.h_num_translations[0]);
		printf("Translational shift: %f %f %f\n",transformation_grid.h_translation_step_shift->x,transformation_grid.h_translation_step_shift->y,transformation_grid.h_translation_step_shift->z);
		printf("Rotation num: %u\n",transformation_grid.h_num_rotations[0]);

		//determine padding
		uint max_dim = std::max(difference_map.h_pixel_dim[0],density.h_pixel_dim[1]);
		max_dim = std::max(max_dim,difference_map.h_pixel_dim[2]);
		uint log_2_dim = ipow(2,(int)(std::log((float) max_dim)/std::log(2))+1);
		uint * h_pixel_dim_padding = (uint *) malloc(3*sizeof(*h_pixel_dim_padding));
		uint * d_pixel_dim_padding;
		cudaMalloc((void **)&d_pixel_dim_padding,sizeof(*d_pixel_dim_padding)*3);
		for (int i=0;i<3;i++)h_pixel_dim_padding[i] = log_2_dim;
		cudaMemcpy(d_pixel_dim_padding,h_pixel_dim_padding,sizeof(*d_pixel_dim_padding)*3,cudaMemcpyHostToDevice);
		uint padding_vol = log_2_dim*log_2_dim*log_2_dim;
		printf("Padding dim:%u\n",log_2_dim);

		float test_coord_dim[3] = {64,64,64};
		uint test_pixel_dim[3] = {64,64,64};
		Density test_density;
		Density::from_bounds(test_density,&test_coord_dim[0],&test_pixel_dim[0]);
		test_density.init_device();
		test_density.to_device();
		int * h_test = (int *) malloc(64*64*64*sizeof(int));
		int * d_test;
		cudaMalloc((void **)&d_test,64*64*64*sizeof(int));

		// Determine temporary device storage requirements
		void     *d_temp_storage = NULL;
		size_t   temp_storage_bytes = 0;
		float memory_usage_factor = 0.5;
		//segments/labels + sorting temp mem - N for sorting elements and P number of SMs -TODO: automatize
		size_t memory_usage = padding_vol*(sizeof(int)+2*sizeof(uint)+sizeof(uint)) + 10*sizeof(uint);
		unsigned long int max = 1;
		uint work_spaces = std::min(max,((uint)(((float)memory_info.free)*memory_usage_factor))/memory_usage);
		uint * d_labels;
		cudaMalloc((void **) &d_labels,2*padding_vol*work_spaces*sizeof(*d_labels));
		int * d_segmentations;
		cudaMalloc((void **) &d_segmentations, padding_vol*work_spaces*sizeof(*d_segmentations));
		CudaCheckError();

		cub::DeviceRadixSort::SortKeys(d_temp_storage, temp_storage_bytes,
				d_labels, d_labels+padding_vol,  padding_vol);
		// Allocate temporary storage
		cudaMalloc(&d_temp_storage, work_spaces*temp_storage_bytes);
		cudaDeviceSynchronize();

		size_t kai_kernel_block_dim = 128;
		size_t kai_kernel_grid_dim = work_spaces;
		float LIMIT = powf(10,20);
		float base = powf(LIMIT,1.0/((float) on_surface_num));
		partial_surface_score<<<kai_kernel_grid_dim,kai_kernel_block_dim>>>
				(pixel_coords.d_data,d_segmentations,d_labels,distance_threshold,
				difference_map,base, d_pixel_dim_padding,
				pixel_coords.particle_count(),d_test,transformation_grid,d_temp_storage,
				temp_storage_bytes);
		cudaDeviceSynchronize();
		transformation_grid.write_to_csv("test/partial_surface_score_5880.csv");
		CudaCheckError();
		nvmlShutdown();
		printf("\n\n");
}
//
//BOOST_AUTO_TEST_CASE(envelope_test_simpler)
//	{
//		printf(" ============================= Testing Envelope score ==========================\n");
//		printf(" ============================= ++++++++++++++++++++++ ==========================\n");
//		//memory management
//		nvmlInit_v2();
//	    //checkout free memory
//	    nvmlMemory_t memory_info;
//		nvmlDevice_t device_handle;
//		nvmlDeviceGetHandleByIndex_v2(0,&device_handle);
//		nvmlDeviceGetMemoryInfo(device_handle, &memory_info);
//
//		float above_value = -1.0;
//		float below_value = 0.0;
//		float coord_dim_outer[3] = {70,70,70};
//		float coord_dim[3] = {50,50,50};
//		uint pixel_dim_outer[3] = {70,70,70};
//		uint pixel_dim[3] = {50,50,50};
//		uint3 cube_pixel_dim= {30,30,30};
//		Density density;
//		Density::from_bounds(density, &coord_dim_outer[0],&pixel_dim_outer[0]);
////			Density density = Density::from_mrc("test/tauA_Brf1_TBP_negstain_30A.mrc");
//		density.init_device();
//		density.to_device();
//		density.mid_cube_with_surface(cube_pixel_dim,1,1.0,1.5);
//
//		Density binarized_map;
//		density.cut_and_binarize(binarized_map,1.0,above_value,below_value,{20,20,20});
//
//		Density to_fit_density;
//		Density::from_bounds(to_fit_density, &coord_dim[0],&pixel_dim[0]);
//		to_fit_density.mid_cube_with_surface(cube_pixel_dim,1,1.0,1.5);
//
//		//every pixel a particle, for test reasons
//		thrust::device_vector<uint> td_on_surface_to_fit;
//		size_t on_surface_num_to_fit;
//		to_fit_density.indices_in_range(td_on_surface_to_fit,true,1.25,2,on_surface_num_to_fit);
//
//		Particles pixel_coords = to_fit_density.particles_from_density(td_on_surface_to_fit);
//		pixel_coords.origin_to_center();
//
//		float memory_usage_factor = 0.5;
//		size_t density_memory_usage = pixel_coords.particle_count()*sizeof(uint) + binarized_map.h_pixel_vol()*sizeof(float);
//		unsigned long int max = 100;
//		uint work_spaces = std::min(max,((uint)(((float)memory_info.free)*memory_usage_factor))/density_memory_usage);
//
//		//set up TransformationGrid
//		float tau_in = 1.0;
//		std::pair<float3,float3> bounding_box_outer = density.get_bounding_box();
//		std::pair<float3,float3> bounding_box_inner = pixel_coords.get_rotationally_safe_bounding_box();
//		std::pair<float3,float3> effective_translational_shift;
//		effective_translational_shift.first = bounding_box_outer.first - bounding_box_inner.first;
//		effective_translational_shift.second = bounding_box_outer.second - bounding_box_inner.second;
//		TransformationGrid transformation_grid(effective_translational_shift,tau_in,binarized_map.h_mid_point);
//		CudaCheckError();
//		printf("Transformation num: %u\n",transformation_grid.h_num_transformations[0]);
//		printf("Translation num: %u\n",transformation_grid.h_num_translations[0]);
//		printf("Rotation num: %u\n",transformation_grid.h_num_rotations[0]);
//
////    	TransformationGrid transformation_grid(binarized_map.h_mid_point);
//		const size_t envelope_score_block_dim = 256;
//		size_t envelope_score_grid_dim = work_spaces;
//		size_t envelope_workspace_replaced_indices_volume = work_spaces*pixel_coords.particle_count()*sizeof(uint);
//		size_t envelope_workspace_replaced_values_buffer_volume = work_spaces*pixel_coords.particle_count()*sizeof(float);
//		//allocate workspace resources
//
//		float * d_replaced_value_buffer;
//		cudaMalloc((void **)&d_replaced_value_buffer,envelope_workspace_replaced_values_buffer_volume);
//		uint * d_replaced_linear_indices;
//		cudaMalloc((void **)&d_replaced_linear_indices,envelope_workspace_replaced_indices_volume);
//		CudaCheckError();
//		float * d_workspaces;
//		cudaMalloc((void **)&d_workspaces,work_spaces*binarized_map.h_pixel_vol()*sizeof(float));
//		printf("Allocating %u workspaces for %u pixels each occupying %u bytes of memory ...\n",work_spaces,binarized_map.h_pixel_vol(),envelope_workspace_replaced_indices_volume + envelope_workspace_replaced_values_buffer_volume);
//		CudaCheckError();
//		//(Density binarized_map, float * d_workspaces, uint * d_linear_offsets, float4 * d_coords,size_t num_particles , TransformationGrid grid)
//		envelope_score<envelope_score_block_dim><<<envelope_score_grid_dim,envelope_score_block_dim>>>
//				(binarized_map, d_workspaces,d_replaced_value_buffer, d_replaced_linear_indices,pixel_coords.d_data,pixel_coords.particle_count(),transformation_grid);
////		transformation_grid.write_to_csv("test/env_score.csv");
//		float * d_normalized_scores;
//		cudaMalloc((void **)&d_normalized_scores,transformation_grid.h_num_transformations[0]*sizeof(*d_normalized_scores));
//		float * d_min;
//		cudaMalloc((void **)&d_min,sizeof(*d_min));
//		float * d_max;
//		cudaMalloc((void **)&d_max,sizeof(*d_max));
//		transformation_grid.shift_and_scale_envelope_score(transformation_grid.d_scores,d_normalized_scores,d_min,d_max,transformation_grid.h_num_transformations[0]);
//		transformation_grid.write_to_csv("test/env_score_5880.csv");
//		printf("Finished testing envelope score ... \n\n");
//}
//


//BOOST_AUTO_TEST_CASE(chamfer_test_simpler)
//{
//	printf(" ============================= Testing Chamfer score ===========================\n");
//	printf(" ============================= +++++++++++++++++++++ ===========================\n");
//
//	//memory management
//	nvmlInit_v2();
//    //checkout free memory
//    nvmlMemory_t memory_info;
//	nvmlDevice_t device_handle;
//	nvmlDeviceGetHandleByIndex_v2(0,&device_handle);
//	nvmlDeviceGetMemoryInfo(device_handle, &memory_info);
//
//	float coord_dim_outer[3] = {70,70,70};
//	float coord_dim[3] = {50,50,50};
//	float distance_threshold = 0.5;
//	uint pixel_dim_outer[3] = {70,70,70};
//	uint pixel_dim[3] = {50,50,50};
//	uint3 cube_pixel_dim= {30,30,30};
//	Density density;
//	Density::from_bounds(density,&coord_dim_outer[0],&pixel_dim_outer[0]);
//	density.init_device();
//	density.to_device();
//	density.mid_cube_with_surface(cube_pixel_dim,1,1.0,1.5);
//
//	Density to_fit_density;
//	Density::from_bounds(to_fit_density,&coord_dim[0],&pixel_dim[0]);
//	to_fit_density.mid_cube_with_surface(cube_pixel_dim,1,1.0,1.5);
//
//	thrust::device_vector<uint> td_on_surface;
//	size_t on_surface_num;
//	density.indices_in_range(td_on_surface,true,1.25,2,on_surface_num);
//
//	thrust::device_vector<uint> td_on_surface_to_fit;
//	size_t on_surface_num_to_fit;
//	to_fit_density.indices_in_range(td_on_surface_to_fit,true,1.25,2,on_surface_num_to_fit);
//
//	Particles pixel_coords = to_fit_density.particles_from_density(td_on_surface_to_fit);
//	pixel_coords.origin_to_center();
//
//	Density difference_map;
//	density.difference_map_density_threshold(difference_map,1.4,1.6,td_on_surface);
//	CudaCheckError();
//
//	float tau_in = 1.0;
//	uint n_rot_test = 30;
//	std::pair<float3,float3> bounding_box_outer = density.get_bounding_box();
//	std::pair<float3,float3> bounding_box_inner = pixel_coords.get_rotationally_safe_bounding_box();
//	TransformationGrid transformation_grid(bounding_box_outer,bounding_box_inner,density.h_mid_point,tau_in,"rot_samples/N23_M5880_IcoC7.dat");
//	printf("Transformation num: %u\n",transformation_grid.h_num_transformations[0]);
//	printf("Translation num: %u\n",transformation_grid.h_num_translations[0]);
//	printf("Translational shift: %f %f %f\n",transformation_grid.h_translation_step_shift->x,transformation_grid.h_translation_step_shift->y,transformation_grid.h_translation_step_shift->z);
//	printf("Rotation num: %u\n",transformation_grid.h_num_rotations[0]);
//
//	size_t chamfer_kernel_block_dim = 128;
//	size_t chamfer_kernel_grid_dim = 32;
//	size_t chamfer_kernel_shared_mem = sizeof(float4) + sizeof(float4);
//	auto start = std::chrono::high_resolution_clock::now();
//	chamfer_score<<<chamfer_kernel_grid_dim,chamfer_kernel_block_dim,chamfer_kernel_shared_mem>>>
//			(pixel_coords.d_data,difference_map,pixel_coords.particle_count(),transformation_grid);
//	cudaDeviceSynchronize();
//	auto finish = std::chrono::high_resolution_clock::now();
//	std::chrono::duration<double> elapsed = finish - start;
//	std::cout << "Elapsed time: " << elapsed.count() << " s\n";
////	transformation_grid.normalize_chamfer_score();
//	transformation_grid.write_to_csv("test/chamfer_score_5880.csv");
//	printf("\n\n");
//}


//BOOST_AUTO_TEST_CASE(chamfer_test_simple)
//{
//	float deviation = 0.005;
//	float inner_rho = 1.0;
//	float edge_rho = 0.5;
//	float coord_dim[3] = {100,100,100};
//	uint pixel_dim[3] = {50,50,50};
//	float coord_dim_to_fit[3] = {40,40,40};
//	uint pixel_dim_to_fit[3] = {20,20,20};
//	uint3 cube_pixel_dim= {20,20,20};
//	uint3 cube_pixel_dim_to_fit = {18,18,18};
//	Density density = Density::from_bounds(&coord_dim[0],&pixel_dim[0]);
//	density.mid_cube_with_surface(cube_pixel_dim,2,inner_rho,edge_rho);
//	density.to_mrc("test/surface_cube.mrc");
//
//	Density to_fit_density = Density::from_bounds(&coord_dim_to_fit[0],&pixel_dim_to_fit[0]);
//	to_fit_density.mid_cube_with_surface(cube_pixel_dim_to_fit,2,inner_rho,edge_rho);
//
//	thrust::device_vector<uint> td_on_surface;
//	size_t on_surface_num;
//	density.indices_in_range(td_on_surface,true,edge_rho - deviation,edge_rho+deviation,on_surface_num);
//
//	thrust::device_vector<uint> td_on_surface_to_fit;
//	size_t on_surface_num_to_fit;
//	to_fit_density.indices_in_range(td_on_surface_to_fit,true,edge_rho - deviation,edge_rho+deviation,on_surface_num_to_fit);
//
//	Particles pixel_coords = to_fit_density.particles_from_density(td_on_surface_to_fit);
//	pixel_coords.origin_to_center();
////	sub_density.to_mrc("test/fit_in.mrc");
////	Particles particles = Particles::from_pdb("test/tauA.pdb",true);
////	Density structure_density = sub_density.from_particles_on_grid(particles,5.0);
////	thrust::device_vector<uint> td_on_surface;
//	Density difference_map = density.difference_map_density_threshold(edge_rho-deviation,edge_rho + deviation,td_on_surface);
////	difference_map.to_mrc("test/surface_cube_diff.mrc");
////	Particles pixel_coords = structure_density.particles_from_density(td_on_surface);
////	pixel_coords.center_on_density(sub_density.h_coord_dim);
//////	TransformationGrid::TransformationGrid(const std::pair<float3,float3> & bounding_box_outer,
//////			const std::pair<float3,float3> & bounding_box_inner,const float4 & angle_box, const float & alpha, const float & tau_in)
//	TransformationGrid transformation_grid(density.get_bounding_box(),pixel_coords.get_rotationally_safe_bounding_box(),{0,0,0,0},20,20);
////	transformation_grid.write_to_png("test/test.png");
////	std::exit(0);
//	printf("Transformation num: %u\n",transformation_grid.h_num_transformations[0]);
//	printf("Translation num: %u\n",transformation_grid.h_num_translations[0]);
//	printf("Rotation num: %u\n",transformation_grid.h_num_rotations[0]);
//////	void chamfer_score(float4 * d_coords, float * d_difference_map, uint * d_pixel_dim,
//////			float pixel_size, size_t num_particles,TransformationGrid grid)
//	size_t chamfer_kernel_block_dim = 128;
//	size_t chamfer_kernel_grid_dim = 32;
//	size_t chamfer_kernel_shared_mem = sizeof(float4) + sizeof(float3);
//	auto start = std::chrono::high_resolution_clock::now();
//	chamfer_score<<<chamfer_kernel_grid_dim,chamfer_kernel_block_dim,chamfer_kernel_shared_mem>>>
//			(pixel_coords.d_data,difference_map.d_data,difference_map.d_pixel_dim,difference_map.pixel_size,
//			pixel_coords.particle_count(),transformation_grid);
//	cudaDeviceSynchronize();
//	auto finish = std::chrono::high_resolution_clock::now();
//	std::chrono::duration<double> elapsed = finish - start;
//	std::cout << "Elapsed time: " << elapsed.count() << " s\n";
//	transformation_grid.normalize_chamfer_score();
//	transformation_grid.write_to_csv("test/first_scoring.csv");
//}

//BOOST_AUTO_TEST_CASE(chamfer_test_simpler)
//{
//	float deviation = 0.005;
//	float inner_rho = 2.0;
//	float edge_rho = 1.0;
//	float coord_dim[3] = {100,100,100};
//	uint pixel_dim[3] = {50,50,50};
//	uint3 cube_pixel_dim= {30,30,30};
//	Density density = Density::from_bounds(&coord_dim[0],&pixel_dim[0]);
//	density.mid_cube_with_surface(cube_pixel_dim,2,inner_rho,edge_rho);
//	density.to_mrc("test/surface_cube.mrc");
//
//	Density to_fit_density = Density::from_bounds(&coord_dim[0],&pixel_dim[0]);
//	to_fit_density.mid_cube_with_surface(cube_pixel_dim,2,inner_rho,edge_rho);
//
//	thrust::device_vector<uint> td_on_surface;
//	size_t on_surface_num;
//	density.indices_in_range(td_on_surface,true,edge_rho - deviation,edge_rho+deviation,on_surface_num);
//
//	thrust::device_vector<uint> td_on_surface_to_fit;
//	size_t on_surface_num_to_fit;
//	to_fit_density.indices_in_range(td_on_surface_to_fit,true,edge_rho - deviation,edge_rho+deviation,on_surface_num_to_fit);
//
//	Particles pixel_coords = to_fit_density.particles_from_density(td_on_surface_to_fit);
//	pixel_coords.origin_to_center();
//	Density to_fit_surface = to_fit_density.only_linear_indeces(td_on_surface_to_fit);
//	to_fit_surface.to_mrc("test/surface_cube_surface.mrc");
//
//	Density difference_map = density.difference_map_density_threshold(edge_rho-deviation,edge_rho + deviation,td_on_surface);
//	difference_map.to_mrc("test/surface_cube_diff.mrc");
//////	Particles pixel_coords = structure_density.particles_from_density(td_on_surface);
//////	pixel_coords.center_on_density(sub_density.h_coord_dim);
////////	TransformationGrid::TransformationGrid(const std::pair<float3,float3> & bounding_box_outer,
////////			const std::pair<float3,float3> & bounding_box_inner,const float4 & angle_box, const float & alpha, const float & tau_in)
//	TransformationGrid transformation_grid(density.get_bounding_box(),pixel_coords.get_rotationally_safe_bounding_box(),difference_map.h_mid_point,{0,0,0,0},20,1);
//////	transformation_grid.write_to_png("test/test.png");
//////	std::exit(0);
//	printf("Transformation num: %u\n",transformation_grid.h_num_transformations[0]);
//	printf("Translation num: %u\n",transformation_grid.h_num_translations[0]);
//	printf("Translational shift: %f %f %f\n",transformation_grid.h_translation_step_shift->x,transformation_grid.h_translation_step_shift->y,transformation_grid.h_translation_step_shift->z);
//	printf("Rotation num: %u\n",transformation_grid.h_num_rotations[0]);
//	size_t chamfer_kernel_block_dim = 128;
//	size_t chamfer_kernel_grid_dim = 32;
//	size_t chamfer_kernel_shared_mem = sizeof(float4) + sizeof(float4);
//	auto start = std::chrono::high_resolution_clock::now();
////	void chamfer_score(float4 * d_coords, Density difference_map, size_t num_particles,TransformationGrid grid)
//	chamfer_score<<<chamfer_kernel_grid_dim,chamfer_kernel_block_dim,chamfer_kernel_shared_mem>>>
//			(pixel_coords.d_data,difference_map,pixel_coords.particle_count(),transformation_grid);
//	cudaDeviceSynchronize();
//	auto finish = std::chrono::high_resolution_clock::now();
//	std::chrono::duration<double> elapsed = finish - start;
//	std::cout << "Elapsed time: " << elapsed.count() << " s\n";
//	transformation_grid.normalize_chamfer_score();
//	transformation_grid.write_to_csv("test/cube_scoring.csv");
//
//	uint n = 6;
//	thrust::host_vector<thrust::tuple<float4,float3,float>> h_best_n(n);
//	thrust::device_vector<thrust::tuple<float4,float3,float>> best_n = TransformationGrid::n_best_from_file("test/cube_scoring.csv",n);
//	h_best_n = best_n;
//	thrust::tuple<float4,float3,float> curr;
//	float4 rot;
//	float3 trans;
//	float score;
//	for (int i=0;i<n;i++){
//		curr = h_best_n[i];
//		rot = thrust::get<0>(curr);
//		trans = thrust::get<1>(curr);
//		score = thrust::get<2>(curr);
//		printf("%f %f %f %f | %f %f %f | %f\n",rot.x,rot.y,rot.z,rot.w,trans.x,trans.y,trans.z,score);
//	}
//}

// EOF
