/*
 * CLabeler.h
 *
 *  Created on: Feb 25, 2019
 *      Author: kkarius
 */

#ifndef LABELER_H_
#define LABELER_H_

#include <cctype>
#include <list>

#include <cuda_util_include/cutil_math.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <cuda_device_runtime_api.h>
#include <thrust/fill.h>
#include <thrust/device_vector.h>
#include <thrust/find.h>
#include <thrust/count.h>
#include <thrust/sort.h>
#include <thrust/binary_search.h>
#include <thrust/unique.h>
#include <thrust/adjacent_difference.h>
#include <thrust/iterator/counting_iterator.h>

#include <Density.h>
#include <util.h>

struct label_search_info {
	uint label;
	size_t label_start;
	size_t label_stop;
};

__global__
void ccl_kernel(int * seg_data, uint * labels,uint log_2_dim);


class Labeler {
public:
	Labeler(){};
	virtual ~Labeler(){};
//	static void unique_labels_to_mrc(Density density,uint *const& d_labels,uint log_2_dim,const int& gpu_index);
	static void ccl(int * d_seg_data,uint * d_labels, uint log_2_dim,const int & gpu_index);
	static std::map<uint,uint> label_occurrence(Density *const& density,uint *const& d_labels,const uint & log_2_dim,
			const char * prefix, const bool& to_mrc,const int& gpu_index);
};
#endif /* LABELER_H_ */


