/*
 * CRotationGrid.h
 *
 *  Created on: Sep 17, 2019
 *      Author: kkarius
 */

#ifndef CROTATIONGRID_H_
#define CROTATIONGRID_H_
#include <cuda_util_include/cutil_math.h>
#include <string.h>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <boost/algorithm/string.hpp>
#include <sys/types.h>
#include <dirent.h>
#include <fstream>
#include <map>

#define MAX_CHUNKSIZE 1024

enum ERotationFormat {
	eMatrix
};

class CRotationGrid {
public:
	CRotationGrid(std::string rot_file_path, size_t chunksize);
	virtual ~CRotationGrid();
	size_t rotation_num;
	float covering_radius;
	CRotationGrid& operator++(int);
	float *&operator*();
	bool operator!();
	size_t current_chunk_size();
	void reset();
	void print_current();
	static void random_close_to_unity(float * &matrix,size_t offset=0);
	static void find_nearest_rotation_set(float alpha_approx, float4 *& rotations,uint *& n_rotations, float & alpha);
	static void quat_to_euler(float4 *& rotations_quat,float3 *& rotations_euler, int n);
private:
	size_t m_chunksize;
	ERotationFormat m_rotation_format;

	std::string m_filepath;
	std::ifstream m_filestream;
	std::string m_current_line;
	bool m_beginning_found;
	float * m_matrix_chunk;
	float m_current_matrix[9];
	float4 m_current_quat;
	int m_current_chunk_num=0;
	size_t m_current_rot_num=0;

	bool x_find_beginning(void);
	bool x_parse_line(void);
	bool x_quat_to_rot_mat();
};

inline float scaled_rand(int _rand_max, float bound) {
	return (float) (rand() % _rand_max) / (float) _rand_max * bound;
}



#endif /* CROTATIONGRID_H_ */
