# from scipy.interpolate import RegularGridInterpolator
from numpy import linspace, zeros, array
from matplotlib import cm # for 3d poltting
from mpl_toolkits.mplot3d.axes3d import Axes3D # for 3d poltting
import matplotlib.pyplot as plt
import numpy as np
from struct import unpack,pack
from itertools import product

frac = lambda x:x-np.floor(x)

def mat_from_euler(angle):
    cosa = [np.cos(a) for a in angle]
    sina = [np.sin(a) for a in angle]
    m = np.ndarray((3,3))
    m[0,0] = cosa[0]*cosa[1]*cosa[2] - sina[0]*sina[2]
    m[0,1] = -cosa[2]*sina[0] - cosa[0]*cosa[1]*sina[2]
    m[0,2] = cosa[0]*sina[1]
    m[1,0] = cosa[0]*sina[2] + cosa[1]*cosa[2]*sina[0]
    m[1,1] = cosa[0]*cosa[2] - cosa[1]*sina[0]*sina[2]
    m[1,2] = sina[0]*sina[1]
    m[2,0] = -cosa[2]*sina[1]
    m[2,1] = sina[1]*sina[2]
    m[2,2] = cosa[1]
    return m

def from_mrc(fn):
    fh = open(fn,"rb")
    d = fh.read()
    pdim = unpack('iii',d[:12])
    ptotal = pdim[0]*pdim[1]*pdim[2]
    cdim = unpack('fff',d[40:52])
    data = np.ndarray(shape=(ptotal,),dtype=np.float32)
    data[:] = unpack('f'*ptotal,d[1024:])
    data3d = data.reshape(pdim,order='F')
    return data3d,cdim

def to_mrc(fn,data,cdim):
    fh = open(fn,"wb")
    pdim = data.shape
    fh.write(pack('iii',*pdim))
    fh.write(pack('i',2))
    fh.write(pack('iii',0,0,0))
    fh.write(pack('iii',pdim[0],pdim[1],pdim[2]))
    fh.write(pack('fff',*cdim))
    fh.write(pack('fff',90,90,90))
    fh.write(pack('iii',1,2,3))
    fh.seek(1024)
    fh.write(pack('f'*pdim[0]*pdim[1]*pdim[2],*data.flatten('F')))
    fh.close()
    print("Written to file %s ..."%fn)

def tex3D_nocheck(T,x,y,z):
    a = frac(x-0.5)
    b = frac(y-0.5)
    g = frac(z-0.5)
    i = int(np.floor(x-0.5))
    j = int(np.floor(y-0.5))
    k = int(np.floor(z-0.5))
    return (1-a)*(1-b)*(1-g)*pad(T,i,j,k) + a*(1-b)*(1-g)*pad(T,i+1,j,k) + (1-a)*b*(1-g)*pad(T,i,j+1,k) + a*b*(1-g)*pad(T,i+1,j+1,k) + (1-a)*(1-b)*g*pad(T,i,j,k+1) + a*(1-b)*g*pad(T,i+1,j,k+1) + (1-a)*b*g*pad(T,i,j+1,k+1) + a*b*g*pad(T,i+1,j+1,k+1)

def tex3D(T,x,y,z):
    if T.shape[0] < x < 0 or T.shape[1] < y < 0 or T.shape[2] < z < 0:
        return 0.0
    a = frac(x-0.5)
    b = frac(y-0.5)
    g = frac(z-0.5)
    i = int(np.floor(x-0.5))
    j = int(np.floor(y-0.5))
    k = int(np.floor(z-0.5))
    return (1-a)*(1-b)*(1-g)*pad(T,i,j,k) + a*(1-b)*(1-g)*pad(T,i+1,j,k) + (1-a)*b*(1-g)*pad(T,i,j+1,k) + a*b*(1-g)*pad(T,i+1,j+1,k) + (1-a)*(1-b)*g*pad(T,i,j,k+1) + a*(1-b)*g*pad(T,i+1,j,k+1) + (1-a)*b*g*pad(T,i,j+1,k+1) + a*b*g*pad(T,i+1,j+1,k+1)

from scipy.spatial.transform import Rotation as R

def rot_from_quat(q):
    #different conventions in scipy and my code
    q = [q[1],q[2],q[3],q[0]]
    qf = 1/(sum([q[i]**2 for i in range(4)]))
    qn = [qf*q[i] for i in range(4)]
    r = R.from_quat(qn)
    return r.as_matrix()

def v_aveq(target,query,translation,q,scan_nodes,scan_weights):
    ave = 0.0
    rot_mat = rot_from_quat(q)
    translation = np.array(translation)
    for n,w in zip(scan_nodes,scan_weights):
        n_tr = rot_mat@n
        n_tr += translation
        ave += w*tex3D(target,*n_tr)
    return ave

def v_ave(target,query,translation,rot_eulers,scan_nodes,scan_weights):
    ave = 0.0
    rot_mat = mat_from_euler(rot_eulers)
    translation = np.array(translation)
    half_query = np.array([e/2 for e in query.shape])
    for n,w in zip(scan_nodes,scan_weights):
        n_tr = rot_mat@n
        n_tr += half_query + translation
        ave += w*tex3D(target,*n_tr)
    return ave

def u_t_v(target,query,translation,rot_eulers,scan_nodes,scan_weights,u_ave,v_ave):
    u_t_v = 0.0
    v_m_v_ave = 0.0
    rot_mat = mat_from_euler(rot_eulers)
    translation = np.array(translation)
    half_query = np.array([e/2 for e in query.shape])
    s = 0
    for i,(n,w) in enumerate(zip(scan_nodes,scan_weights)):
        n_tr = rot_mat@n
        n_tr += half_query + translation
        n_0 = n + half_query
        v_ = tex3D(target,*n_tr)-v_ave
        u_ = tex3D(query,*n_0)-u_ave
        if np.abs(tex3D(target,*n_tr)-tex3D(query,*n_0)>0.634):
            print(tex3D(target,*n_tr),tex3D(query,*n_0),n_0,n_tr)
        u_t_v += w*u_*v_
        if u_*v_ < 0:
            s += w*u_*v_
        v_m_v_ave += w*v_*v_
    print(s)
    return u_t_v,v_m_v_ave

def u_t_vq(target,query,translation,q,scan_nodes,scan_weights,u_ave,v_ave):
    u_t_v = 0.0
    v_m_v_ave = 0.0
    rot_mat = rot_from_quat(q)
    translation = np.array(translation)
    half_query = np.array([e/2 for e in query.shape])
    s = 0
    for i,(n,w) in enumerate(zip(scan_nodes,scan_weights)):
        n_tr = rot_mat@n
        n_tr += half_query + translation
        n_0 = n + half_query
        v_ = tex3D(target,*n_tr)-v_ave
        u_ = tex3D(query,*n_0)-u_ave
        if np.abs(tex3D(target,*n_tr)-tex3D(query,*n_0)>0.634):
            print(tex3D(target,*n_tr),tex3D(query,*n_0),n_0,n_tr)
        u_t_v += w*u_*v_
        if u_*v_ < 0:
            s += w*u_*v_
        v_m_v_ave += w*v_*v_
    return u_t_v,v_m_v_ave

def cam_score(u_t_v,u_m_ave_sq,v_m_ave_sq):
    if v_m_ave_sq > 0.0:
        return u_t_v/np.sqrt((v_m_ave_sq*u_m_ave_sq))
    return 0.0

def tex3D_normal(T,_x,_y,_z):
    if 1 < _x < 0 or 1 < _y < 0 or 1 < _z < 0:
        return 0.0
    x = _x*T.shape[0]
    y = _y*T.shape[1]
    z = _z*T.shape[2]
    a = frac(x-0.5)
    b = frac(y-0.5)
    g = frac(z-0.5)
    i = int(np.floor(x-0.5))
    j = int(np.floor(y-0.5))
    k = int(np.floor(z-0.5))
    return (1-a)*(1-b)*(1-g)*pad(T,i,j,k) + a*(1-b)*(1-g)*pad(T,i+1,j,k) + (1-a)*b*(1-g)*pad(T,i,j+1,k) + a*b*(1-g)*pad(T,i+1,j+1,k) + (1-a)*(1-b)*g*pad(T,i,j,k+1) + a*(1-b)*g*pad(T,i+1,j,k+1) + (1-a)*b*g*pad(T,i,j+1,k+1) + a*b*g*pad(T,i+1,j+1,k+1)

def cam(query,target,transformations):
    target_c = np.zeros_like(query)
    qm = query-np.average(query)
    qm_n = np.linalg.norm(qm)
    t_start = np.zeros((3,),dtype=np.int32)
    t_stop = np.zeros((3,),dtype=np.int32)
    tc_start = np.zeros((3,),dtype=np.int32)
    tc_stop = np.zeros((3,),dtype=np.int32)
    scores = []
    for transformation in transformations:
        do_score = True
        for i in range(3):
            t = target.shape[i]
            q = query.shape[i]
            trans = transformation[i]
            if trans >= t:
                do_score = False
            if trans <= -q:
                do_score = False
            if trans > 0:
                t_start[i] = trans
                t_stop[i] = min(t,trans+q)
                tc_start[i] = 0
                tc_stop[i] = min(q,t-trans)
            else:
                t_start[i] = 0
                t_stop[i] = q + trans
                tc_start[i] = -trans
                tc_stop[i] = q
        if not do_score:
            scores.append(0.0)
            continue
        target_c[:] = 0.0
        target_c[tc_start[0]:tc_stop[0],tc_start[1]:tc_stop[1],tc_start[2]:tc_stop[2]] = target[t_start[0]:t_stop[0],t_start[1]:t_stop[1],t_start[2]:t_stop[2]]
        target_c -= np.average(target_c)
        target_cn = np.linalg.norm(target_c)
        if target_cn > 0.0:
            scores.append(max(np.sum((target_c*qm).flatten())/(target_cn*qm_n),0.0))
        else:
            scores.append(0.0)
        print("%s -- > %s"%(str(transformation),str(scores[-1])))
    return scores

def scale_nodes(ns,scale):
    return [scale[0] + n*(scale[1]-scale[0]) for n in ns]

def get_cheby_nodes_weights_linear(n):
    theta = np.array([0.0]*n)
    for i in range(n):
        theta[i] = (n-1-i)*np.pi/(n-1)
    nodes = np.cos(theta)
    nodessc = [(1+no)/2 for no in nodes]
    weights = np.zeros_like(nodes)
    for i in range(n):
        weights[i] = 1.0
        jhi = (n-1)//2
        for j in range(jhi):
            if (2*(j+1) == n-1):
                b = 1.0
            else:
                b = 2.0
            weights[i] = weights[i]-b*np.cos(2*(j+1)*theta[i])/(4*j*(j+2)+3)
    weights[0] = 0.5*weights[0]/(n-1)
    weights[n-1] = 0.5*weights[n-1]/(n-1)
    for i in range(1,n-1):
        weights[i] = weights[i]/(n-1)
    return nodessc,weights

def get_cheby_nodes_weights_threed(n,scale):
    ns,ws = get_cheby_nodes_weights_linear(n)
    ns0 = scale_nodes(ns,scale[0])
    ns1 = scale_nodes(ns,scale[1])
    ns2 = scale_nodes(ns,scale[2])
    ns3d = [None]*(n**3)
    for i in range(n):
        for j in range(n):
            for k in range(n):
                ns3d[n*n*k+n*j+i] = np.array([ns0[i],ns1[j],ns2[k]])
    ws3d = [None]*(n**3)
    for i in range(n):
        for j in range(n):
            for k in range(n):
                ws3d[n*n*k+n*j+i] = ws[i]*ws[j]*ws[k]
    return ns3d,ws3d

def get_pixel_nodes_weights_threed(shape):
    ns = []
    ws = []
    w = 1/(shape[0]*shape[1]*shape[2])
    for i in range(shape[0]):
        for j in range(shape[1]):
            for k in range(shape[2]):
                ns.append(np.array([i+0.5,j+0.5,k+0.5]))
                ws.append(w)
    return ns,ws

def target_to_query(qdim_h,rot_trans,trans,coord):
    ret = rot_trans@(coord-trans-qdim_h)+qdim_h
    return ret

def query_to_target(qdim_h,rot,trans,coord):
   return rot@(coord-qdim_h)+qdim_h+trans

def transform_back(translation,rotation,data):
    inv_rotation = rotation.transpose()
    data2 = np.zeros_like(data)
    half_shape = np.array([e/2 for e in data.shape])
    for i,j,k in product(range(data.shape[0]),range(data.shape[1]),range(data.shape[2])):
        px_t = np.array([i+0.5,j+0.5,k+0.5])
        px_q = query_to_target(half_shape,inv_rotation,translation,px_t)
        t = tex3D(data,*px_q)  
        data2[i,j,k] = t
    return data2

def transform(translation,rotation,data,density):
    inv_rotation = rotation.transpose()
    half_shape = np.array([e/2 for e in data.shape])
    for i,j,k in product(range(density.shape[0]),range(density.shape[1]),range(density.shape[2])):
        px_t = np.array([i+0.5,j+0.5,k+0.5])
        px_q = target_to_query(half_shape,inv_rotation,translation,px_t)
        t = tex3D(data,*px_q)  
        density[i,j,k] = t
    return density

def embed_q(trans,q,data,density):
    rotation = rot_from_quat(q)
    inv_rotation = rotation.transpose()
    half_shape = np.array([e/2 for e in data.shape])
    for i,j,k in product(range(density.shape[0]),range(density.shape[1]),range(density.shape[2])):
        px_t = np.array([i+0.5,j+0.5,k+0.5])
        px_q = inv_rotation@(px_t-trans)+half_shape
        t = tex3D(data,*px_q)  
        density[i,j,k] = t
    return density

def qn(b,M,L,N):
    f = (2*np.pi)**2/(M*N)
    beta = (2*b+1)*np.pi/(2*L-1)
    if b == L-1:
        return np.real(f*vn(beta,L))
    else:
        return np.real(f*(vn(beta,L) + vn(2*np.pi - beta,L)))
def vn(t,L):
    ret = 1/(2*L-1)
    _s = 0
    for mp in range(-(L-1),L):
        _s += w(-mp)*np.exp(mp*t*1j)
    return ret*_s

def w(mp):
    if abs(mp) ==1:
        return mp*np.pi/2*1j
    elif (mp-1)%2 == 0:
        return 0
    else:
        return 2/(1-mp**2)

def pad(T,i,j,k):
    if 0 > i or i >= T.shape[0] or 0 > j or j >= T.shape[1] or 0 > k or k >= T.shape[2]:
        return 0.0
    else:
        return T[i,j,k]

plogp = lambda p: 0 if p==0 else p*np.log(p)

def rot_index(ref,r):
    L = ref[1]
    N = ref[2]
    x = r//(L*N)
    z = (r - x*L*N)//L
    y = r - x*L*N - z*L
    l = x*L*N + z*L + y 
    return x,y,z,l

def gen_rot_nodes_weights(s):
        ref = [2,1,2]
        for i in range(s):
            ref[0] *= 2;
            ref[2] *= 2;
            ref[1] = ((2*ref[1]-1)*3)//2 +1;
        M = ref[0]
        L = ref[1]
        N = ref[2]
        angles = []
        weights = []
        for r in range(0,M*L*N):
            a,b,g,r_lin = rot_index(ref, r)
            angles.append([2*np.pi*a/M,(2*b+1)*np.pi/(2*L-1),2*np.pi*g/N])
            weights.append(qn(b,M,L,N))
        return angles,weights
import random
c = 0.8
query,qdim = from_mrc("./query.mrc")
target,tdim = from_mrc("./query.mrc")
trans_target = np.zeros_like(target)

#tns = [np.array(e) for e in [[0,0,0],[0,0,5],[0,5,0],[5,0,0],[-5,0,0],[0,-5,0],[0,0,-5]]]
tns = [np.array(e) for e in [[0,0,0]]]
rns = [np.array(e) for e in [[np.pi/k,0,0] for k in range(1,10)]]
transformations = list(product(tns,rns))
#t_limit_down = (len(rns)*len(tns))//2 + len(rns)//2 - 3
#t_limit_up = (len(rns)*len(tns))//2 + len(rns)//2 + 3
#r_trans_index = random.randint(t_limit_down,t_limit_up) 
r_trans_index = random.randint(0,len(tns)*len(rns)-1)
t_limit_down = 0
t_limit_up = len(rns)*len(tns)

def test_transformation(transformations, t_limit_down,t_limit_up,u_ave_sq,rns,sns,u_ave):
    for i,(t,r) in enumerate(transformations[t_limit_down:t_limit_up]):
        i += t_limit_down 
        ti = i//len(rns)
        ri = i-len(rns)*ti    
        vave = v_ave(target_trans,query,t,r,sns,sws)
        utv,vavesq = u_t_v(target_trans,query,t,r,sns,sws,u_ave,vave)
        cs[i] = cam_score(utv,u_ave_sq,vavesq)
        vaves[i] = vave
        utvs[i] = utv
        vavesqs[i] = vavesq
        print(ti,ri,cs[i])
    #_pass = "True" if r_trans_index in np.where(np.max(cs) == cs)[0] else "False"
    #print("Test passed: %s,%s"%(_pass,str(transformations[r_trans_index])))

if False:
    print("Random transformation: %i"%r_trans_index)
    rand_transformation = transformations[r_trans_index]
    print(rand_transformation)
    target_trans = transform(np.array(rand_transformation[0]),mat_from_euler(rand_transformation[1]),target)
    test_transformation(transformations,t_limit_down,t_limit_up,u_ave_sq,rns,sns,u_ave)
    
    rns = [np.array(e) for e in [[0,np.pi/k,0] for k in range(1,10)]]
    transformations = list(product(tns,rns))
    r_trans_index = random.randint(0,len(tns)*len(rns)-1)
    print("Random transformation: %i"%r_trans_index)
    rand_transformation = transformations[r_trans_index]
    print(rand_transformation)
    target_trans = transform(np.array(rand_transformation[0]),mat_from_euler(rand_transformation[1]),target)
    test_transformation(transformations,t_limit_down,t_limit_up,u_ave_sq,rns,sns,u_ave)
    
    rns = [np.array(e) for e in [[0,0,np.pi/k] for k in range(1,10)]]
    transformations = list(product(tns,rns))
    r_trans_index = random.randint(0,len(tns)*len(rns)-1)
    print("Random transformation: %i"%r_trans_index)
    rand_transformation = transformations[r_trans_index]
    print(rand_transformation)
    target_trans = transform(np.array(rand_transformation[0]),mat_from_euler(rand_transformation[1]),target)
    test_transformation(transformations,t_limit_down,t_limit_up,u_ave_sq,rns,sns,u_ave)
    
    rns = [np.array(e) for e in [[0,np.pi/l,np.pi/k] for k,l in product(range(1,4),range(1,4))]]
    transformations = list(product(tns,rns))
    r_trans_index = random.randint(0,len(tns)*len(rns)-1)
    print("Random transformation: %i"%r_trans_index)
    rand_transformation = transformations[r_trans_index]
    print(rand_transformation)
    target_trans = transform(np.array(rand_transformation[0]),mat_from_euler(rand_transformation[1]),target)
    test_transformation(transformations,t_limit_down,t_limit_up,u_ave_sq,rns,sns,u_ave)
    
    rns = [np.array(e) for e in [[np.pi/l,0,np.pi/k] for k,l in product(range(1,4),range(1,4))]]
    transformations = list(product(tns,rns))
    r_trans_index = random.randint(0,len(tns)*len(rns)-1)
    print("Random transformation: %i"%r_trans_index)
    rand_transformation = transformations[r_trans_index]
    print(rand_transformation)
    target_trans = transform(np.array(rand_transformation[0]),mat_from_euler(rand_transformation[1]),target)
    test_transformation(transformations,t_limit_down,t_limit_up,u_ave_sq,rns,sns,u_ave)
    
    rns = [np.array(e) for e in [[np.pi/l,np.pi/k,0] for k,l in product(range(1,4),range(1,4))]]
    transformations = list(product(tns,rns))
    r_trans_index = random.randint(0,len(tns)*len(rns)-1)
    print("Random transformation: %i"%r_trans_index)
    rand_transformation = transformations[r_trans_index]
    print(rand_transformation)
    target_trans = transform(np.array(rand_transformation[0]),mat_from_euler(rand_transformation[1]),target)
    test_transformation(transformations,t_limit_down,t_limit_up,u_ave_sq,rns,sns,u_ave)
    
    rns = [np.array(e) for e in [[np.pi/l,np.pi/k,np.pi/m] for k,l,m in product(range(1,4),range(1,4),range(1,4))]]
    cs = np.zeros(len(tns)*len(rns))
    vaves = np.zeros(len(tns)*len(rns))
    utvs = np.zeros(len(tns)*len(rns))
    vavesqs = np.zeros(len(tns)*len(rns))
    transformations = list(product(tns,rns))
    r_trans_index = random.randint(0,len(tns)*len(rns)-1)
    print("Random transformation: %i"%r_trans_index)
    rand_transformation = transformations[r_trans_index]
    print(rand_transformation)
    target_trans = transform(np.array(rand_transformation[0]),mat_from_euler(rand_transformation[1]),target)
    t_limit_up = len(rns)*len(tns)
    test_transformation(transformations,t_limit_down,t_limit_up,u_ave_sq,rns,sns,u_ave)

import sys
sys.exit()
t_level = 4
r_level = 3
s_level = 5
rns,rws = gen_rot_nodes_weights(r_level)
sns,sws = get_cheby_nodes_weights_threed(2**s_level,list(zip(-0.5*np.array(query.shape),0.5*np.array(query.shape))))
tns,tws = get_cheby_nodes_weights_threed(2**t_level,list(zip((1-c)*np.array(target.shape),c*np.array(target.shape))))
u_ave = v_ave(query,query,[0,0,0],[0,0,0],sns,sws)
half_query = np.array([e/2 for e in query.shape])
u_ave_sq = sum([w*(tex3D(query,*n)-u_ave)**2 for n,w in zip(sns+half_query,sws)])
rot_vol = len(rns)
cs = np.zeros(len(tns)*len(rns))
vaves = np.zeros(len(tns)*len(rns))
utvs = np.zeros(len(tns)*len(rns))
vavesqs = np.zeros(len(tns)*len(rns))
cs = np.zeros(len(tns)*len(rns))
vaves = np.zeros(len(tns)*len(rns))
utvs = np.zeros(len(tns)*len(rns))
vavesqs = np.zeros(len(tns)*len(rns))
transformations = list(product(tns,rns))
T = 42
R = 162
test_transf = transformations[T*len(rns) + R]
target_trans = transform(np.array(test_transf[0]),mat_from_euler(test_transf[1]),target)
print(test_transf)
to_mrc("/data/eclipse-workspace/fitter/target.mrc",target_trans,tdim)
sys.exit(0)
#while True:
for r_trans_index in [4,58,162]:
    #r_trans_index = random.randint(0,len(tns)*len(rns)-1)
    print("Random transformation: %i"%r_trans_index)
    rand_transformation = transformations[r_trans_index]
    print(rand_transformation)
    target_trans = transform(np.array(rand_transformation[0]),mat_from_euler(rand_transformation[1]),target)
    t_limit_up = len(rns)*len(tns)
    test_transformation(transformations,t_limit_down,t_limit_up,u_ave_sq,rns,sns,u_ave)

class TestSuite(object):
    def __init__(self,trans_node_dim,scan_node_dim,refinements,target_mrc,query_mrc):
        self.tnd = trans_node_dim
        self.snd = scan_node_dim
        self.refs = refinements
        self.t,self.tdim = self.from_mrc(target_mrc)
        self.q,self.qdim = self.from_mrc(query_mrc)
        self.qa = self.q - np.average(self.q)
        self.tnodes,self.tweights = self.get_cheby_nodes_weights_threed_32(trans_node_dim)
        self.snodes,self.sweights = self.get_cheby_nodes_weights_threed_32(scan_node_dim)
        self.refs = refinements
        self.rot_quat = dict()
        self.scale = [self.q.shape[i]/self.t.shape[i] for i in range(3)]
        self.max_rot_vol = self.rot_vol(-1)
        self.tstride = 4*self.max_rot_vol + 3*len(self.refs)//3
        self.working_mem = np.zeros((self.tstride*trans_node_dim**3,))
        self.qa_sqrt = np.sqrt(self.ave_sq(self.qa))
        for s in range(len(refinements)//3):
            self.rot_quat[s] =  self.gen_rot_nodes_weights(s)

    def from_mrc(self,fn):
        fh = open(fn,"rb")
        d = fh.read()
        pdim = unpack('iii',d[:12])
        ptotal = pdim[0]*pdim[1]*pdim[2]
        cdim = unpack('fff',d[40:52])
        data = np.ndarray(shape=(ptotal,),dtype=np.float32)
        data[:] = unpack('f'*ptotal,d[1024:])
        data3d = data.reshape(pdim)
        return data3d,cdim

    def rot_vol(self,s):
        return self.refs[3*s]*self.refs[3*s+1]*self.refs[3*s+2]

    def trans_vol(self):
        return self.tnd**3

    def get_cheby_nodes_weights_linear_32(self,n):
        theta = np.array([0.0]*n,dtype=np.float32)
        for i in range(n):
            theta[i] = (n-1-i)*np.pi/(n-1)
        nodes = np.cos(theta)
        nodessc = nodes+1
        nodessc /= 2
        weights = np.zeros_like(nodes)
        for i in range(n):
            weights[i] = 1.0
            jhi = (n-1)//2
            for j in range(jhi):
                if (2*(j+1) == n-1):
                    b = 1.0
                else:
                    b = 2.0
                weights[i] = weights[i]-b*np.cos(2*(j+1)*theta[i])/(4*j*(j+2)+3)
        weights[0] = 0.5*weights[0]/(n-1)
        weights[n-1] = 0.5*weights[n-1]/(n-1)
        for i in range(1,n-1):
            weights[i] = weights[i]/(n-1)
        return nodessc,weights

    def get_cheby_nodes_weights_threed_32(self,n):
        nodessc,oned_weights = self.get_cheby_nodes_weights_linear_32(n)
        nodes3d = [None]*(n**3)
        for i in range(n):
            for j in range(n):
                for k in range(n):
                    nodes3d[n*n*k+n*j+i] = np.array([nodessc[i],nodessc[j],nodessc[k]])
        weights3d = np.array([None]*(n**3),dtype=np.float32)
        for i in range(n):
            for j in range(n):
                for k in range(n):
                    weights3d[n*n*k+n*j+i] = oned_weights[i]*oned_weights[j]*oned_weights[k]
        return nodes3d,weights3d

#    def tex3D(self,T,_x,_y,_z):
#        if not ((0 <= _x <= 1) and (0 <= _y <= 1) and (0 <= _z <= 1)):
#            return 0.0 
#        x = _x*T.shape[0]
#        y = _y*T.shape[1]
#        z = _z*T.shape[2]
#        a = frac(x-0.5)
#        b = frac(y-0.5)
#        g = frac(z-0.5)
#        i = int(np.floor(x-0.5))
#        j = int(np.floor(y-0.5))
#        k = int(np.floor(z-0.5))
#        return (1-a)*(1-b)*(1-g)*T[i,j,k] + a*(1-b)*(1-g)*T[i+1,j,k] + (1-a)*b*(1-g)*T[i,j+1,k] + a*b*(1-g)*T[i+1,j+1,k] + (1-a)*(1-b)*g*T[i,j,k+1] + a*(1-b)*g*T[i+1,j,k+1] + (1-a)*b*g*T[i,j+1,k+1] + a*b*g*T[i+1,j+1,k+1]

    def gen_rot_nodes_weights(self,s):
        M = self.refs[3*s]
        L = self.refs[3*s + 1]
        N = self.refs[3*s + 2]
        angles = []
        weights = []
        for r in range(0,M*L*N):
            a,b,g,r_lin = self.rot_index(s, r)
            angles.append([2*np.pi*a/M,(2*b+1)*np.pi/(2*L-1),2*np.pi*g/N])
            weights.append(qn(b,M,L,N))
        return angles,weights

    def mat_from_euler(self,angle):
        cosa = [np.cos(a) for a in angle]
        sina = [np.sin(a) for a in angle]
        m = np.ndarray((3,3))
        m[0,0] = cosa[0]*cosa[1]*cosa[2] - sina[0]*sina[2]
        m[0,1] = -cosa[2]*sina[0] - cosa[0]*cosa[1]*sina[2]
        m[0,2] = cosa[0]*sina[1]
        m[1,0] = cosa[0]*sina[2] + cosa[1]*cosa[2]*sina[0]
        m[1,1] = cosa[0]*cosa[2] - cosa[1]*sina[0]*sina[2]
        m[1,2] = sina[0]*sina[1]
        m[2,0] = -cosa[2]*sina[1]
        m[2,1] = sina[1]*sina[2]
        m[2,2] = cosa[1]
        return m

    def tex3D(self,T,_x,_y,_z):
        x = _x*T.shape[0]
        y = _y*T.shape[1]
        z = _z*T.shape[2]
        a = frac(x-0.5)
        b = frac(y-0.5)
        g = frac(z-0.5)
        i = int(np.floor(x-0.5))
        j = int(np.floor(y-0.5))
        k = int(np.floor(z-0.5))
        return (1-a)*(1-b)*(1-g)*pad(T,i,j,k) + a*(1-b)*(1-g)*pad(T,i+1,j,k) + (1-a)*b*(1-g)*pad(T,i,j+1,k) + a*b*(1-g)*pad(T,i+1,j+1,k) + (1-a)*(1-b)*g*pad(T,i,j,k+1) + a*(1-b)*g*pad(T,i+1,j,k+1) + (1-a)*b*g*pad(T,i,j+1,k+1) + a*b*g*pad(T,i+1,j+1,k+1)

    def rot_index(self,s,r):
        L_c = self.refs[3*s+1]
        N_c = self.refs[3*s+2]
        L_m = self.refs[-2]
        N_m = self.refs[-1]
        n = len(self.refs)//3 - s - 1
        x = r//(L_c*N_c)
        z = (r - x*L_c*N_c)//L_c
        y = r - x*L_c*N_c - z*L_c
        l = x*2**n*L_m*N_m + z*2**n*L_m + y*3**n - (1-3**n)//2
        return x,y,z,l

    def v_ave(self,s,t_offset,t_vol,debug=False):
        for t in range(t_offset,t_offset+t_vol):
            for r in range(self.rot_vol(s)):
                a,b,g,r_lin = self.rot_index(s, r)
                lin_index = t*self.tstride + r_lin
                if (a%2 == 0 and (2*b+1)%3==0 and g%2 == 0 and s>0):
                    continue
                
                _rot = self.rot_quat[s][0][r]
                # print("%i | %i %i %i --> %i --> %f %f %f"%(s,a,b,g,r_lin,_rot[0],_rot[1],_rot[2]))
                m_rot = self.mat_from_euler(_rot)
                # if r==0:
                #     m_rot = np.eye(3)
                tr = self.tnodes[t]
                _sum = 0.0
                for i,(n,w) in enumerate(zip(self.snodes,self.sweights)):
                    n_s = self.scale*n
                    nr = m_rot@n_s
                    nt = nr + tr
                    if debug:
                        print("%f %f %f --> %.10e\t%.10e"%(nt[0],nt[1],nt[2],w,self.tex3D(self.t,*nt)))
                    _sum += w*self.tex3D(self.t,*nt)
                self.working_mem[lin_index] = _sum
                # print("%i --> %.10e"%(r,_sum))
                # self.working_mem[lin_index] += s + 1 

    def v_t_u(self,s,t_offset,t_vol,debug=False):
        for t in range(t_offset,t_offset+t_vol):
            for r in range(self.rot_vol(s)):
                a,b,g,r_lin = self.rot_index(s, r)
                lin_index = t*self.tstride + r_lin
                if (a%2 == 0 and (2*b+1)%3==0 and g%2 == 0 and s>0):
                    continue
                _rot = self.rot_quat[s][0][r]
                # print("%i | %i %i %i --> %i --> %f %f %f"%(s,a,b,g,r_lin,_rot[0],_rot[1],_rot[2]))
                m_rot = self.mat_from_euler(_rot)
                # if r==0:
                #     m_rot = np.eye(3)
                tr = self.tnodes[t]
                _v_ave = self.working_mem[lin_index]
                _v_t_u = 0.0
                _v_m_v_ave = 0.0
                for i,(n,w) in enumerate(zip(self.snodes,self.sweights)):
                    n_s = self.scale*n
                    nr = m_rot@n_s
                    nt = nr + tr
                    if debug:
                        print("%f %f %f %.10e %.10e %.10e --> %.10e\t%.10e"%(nt[0],nt[1],nt[2],w,self.tex3D(self.t,*nt)))
                    _v_t_u += w*(self.tex3D(self.t,*nt) - _v_ave)*self.tex3D(self.qa,*n)
                    _v_m_v_ave += w*(self.tex3D(self.t,*nt) - _v_ave)**2
                self.working_mem[self.max_rot_vol + lin_index] = _v_t_u
                # print("%i --> %.10e"%(r,_v_t_u))
                # print("%i --> %.10e"%(r,_v_m_v_ave))
                self.working_mem[2*self.max_rot_vol + lin_index] = _v_m_v_ave

    def ave_sq(self,d):
        _sum = 0.0
        for i,(n,w) in enumerate(zip(self.snodes,self.sweights)):
            _sum += w*self.tex3D(d,*n)**2
        return _sum

    def calc_cam(self,s,t_offset,t_vol,debug=False):
        for t in range(t_offset,t_offset+t_vol):
            for r in range(self.rot_vol(s)):
                a,b,g,r_lin = self.rot_index(s, r)
                lin_index = t*self.tstride + r_lin
                if (a%2 == 0 and (2*b+1)%3==0 and g%2 == 0 and s>0):
                    continue
                _v_t_u = self.working_mem[self.max_rot_vol + lin_index]
                _v_m_v_ave_sq = np.sqrt(self.working_mem[2*self.max_rot_vol + lin_index])
                # print("%i %.10e %.10e %.10e -> %.10e"%(r,_v_t_u,self.qa_sqrt,_v_m_v_ave_sq,_v_t_u/(self.qa_sqrt*_v_m_v_ave_sq)))
                self.working_mem[lin_index] = _v_t_u/(self.qa_sqrt*_v_m_v_ave_sq)

    def min_sweep(self,s,t_offset,t_vol):
        for t in range(t_offset,t_offset+t_vol):
            _min = np.inf
            for r in range(self.rot_vol(s)):
                a,b,g,r_lin = self.rot_index(s, r)
                lin_index = t*self.tstride + r_lin
                _min = min(_min, self.working_mem[lin_index])
            self.working_mem[4*self.max_rot_vol+t*self.tstride+3*s] = _min

    def shift_by_min(self,s,t_offset,t_vol):
        for t in range(t_offset,t_offset+t_vol):
            _min = self.working_mem[4*self.max_rot_vol+t*self.tstride + 3*s]
            print("MIN:",_min)
            for r in range(self.rot_vol(s)):
                a,b,g,r_lin = self.rot_index(s, r)
                lin_index = t*self.tstride + r_lin
                self.working_mem[3*self.max_rot_vol + lin_index] = self.working_mem[lin_index] - _min
                # print("%i --> %f"%(r, self.working_mem[3*self.max_rot_vol + lin_index]))

    def div_by_ave(self,s,t_offset,t_vol):
        for t in range(t_offset,t_offset+t_vol):
            _ave = self.working_mem[4*self.max_rot_vol+ self.tstride +3*s+1]
            # print(_ave)
            for r in range(self.rot_vol(s)):
                a,b,g,r_lin = self.rot_index(s, r)
                lin_index = t*self.tstride + r_lin
                self.working_mem[3*self.max_rot_vol + lin_index] /= _ave
                # print("%i --> %f"%(lin_index,self.working_mem[3*self.max_rot_vol + lin_index]))

    def quad_sweep_av(self,s,t_offset,t_vol):
        for t in range(t_offset,t_offset+t_vol):
            _sum = 0.0
            for r in range(self.rot_vol(s)):
                a,b,g,r_lin = self.rot_index(s, r)
                lin_index = t*self.tstride + r_lin
                _sum += self.rot_quat[s][1][r]*self.working_mem[3*self.max_rot_vol + lin_index]
            #     print("%i %.10e %.10e --> %.10e"%(lin_index,self.rot_quat[s][1][r],self.working_mem[3*self.max_rot_vol + lin_index],self.rot_quat[s][1][r]*self.working_mem[3*self.max_rot_vol + lin_index]))
            # print(_sum)
            self.working_mem[4*self.max_rot_vol+ self.tstride +3*s+1] = _sum

    def quad_sweep_ent(self,s,t_offset,t_vol):
        for t in range(t_offset,t_offset+t_vol):
            _sum = 0.0
            for r in range(self.rot_vol(s)):
                a,b,g,r_lin = self.rot_index(s, r)
                lin_index = t*self.tstride + r_lin
                _sum += self.rot_quat[s][1][r]*plogp(self.working_mem[3*self.max_rot_vol + lin_index])
            #     print("%i %.10e %.10e --> %.10e"%(lin_index,self.rot_quat[s][1][r],self.working_mem[3*self.max_rot_vol + lin_index],self.rot_quat[s][1][r]*self.working_mem[3*self.max_rot_vol + lin_index]))
            self.working_mem[4*self.max_rot_vol+ self.tstride +3*s+1] = -_sum

    def test_quad_weights(self,s):
        return sum([self.rot_quat[s][1][r] for r in range(self.rot_vol(s))])/(8*np.pi**2)

    def test_quad_sweep_av(self,s):
        #int(So(3))beta d_rho = 4*pi**3
        _sum = 0.0
        for r in range(self.rot_vol(s)):
            _sum += self.rot_quat[s][0][r][1]*self.rot_quat[s][1][r]
        return _sum/(4*np.pi**3)

    def sweep(self,s,t_offset,t_vol):
        self.v_ave(s,t_offset,t_vol)
        self.v_t_u(s,t_offset,t_vol)
        self.calc_cam(s,t_offset,t_vol)
        self.min_sweep(s,t_offset,t_vol)
        self.shift_by_min(s,t_offset,t_vol)
        self.quad_sweep_av(s,t_offset,t_vol)
        self.div_by_ave(s,t_offset,t_vol)
        self.quad_sweep_ent(s,t_offset,t_vol)

if __name__ == "__main__":
    import sys
    sys.exit()
    # test_suite = TestSuite(8,8,[8,5,8,16,14,16],"/data/eclipse-workspace/fitter/base.mrc","/data/eclipse-workspace/fitter/part.mrc")
    test_suite = TestSuite(8,8,[4,2,4,8,5,8,16,14,16],"/data/eclipse-workspace/fitter/base.mrc","/data/eclipse-workspace/fitter/part.mrc")
    # print(test_suite.test_quad_weights(0))
    # print(test_suite.test_quad_weights(0))
    # sys.exit()
    # print(test_suite.test_quad_sweep_av(0))
    # print(test_suite.test_quad_sweep_av(1))
    # print(test_suite.test_quad_sweep_av(2))
    # print(test_suite.ave_sq(test_suite.qa))
    # sys.exit()
    # test_suite.v_ave(0,0,1)
    # test_suite.v_t_u(0,0,1)
    # test_suite.working_mem[0:test_suite.max_rot_vol] = 0.0
    # test_suite.calc_cam(0,0,1)
    # test_suite.min_sweep(0,0,1)
    # # print(test_suite.working_mem[4*test_suite.max_rot_vol])
    # # print(test_suite.working_mem[0:test_suite.max_rot_vol])
    # test_suite.shift_by_min(0,0,1)
    # # print(test_suite.working_mem[0*test_suite.max_rot_vol:1*test_suite.max_rot_vol])
    # # print(test_suite.working_mem[3*test_suite.max_rot_vol:4*test_suite.max_rot_vol])
    # test_suite.quad_sweep_av(0,0,1)
    # test_suite.div_by_ave(0,0,1)
    # test_suite.quad_sweep_ent(0,0,1)
    test_suite.sweep(0,0,1)
    test_suite.sweep(1,0,1)
    test_suite.sweep(2,0,1)
    # sys.exit()
    # print("SWEEP 2")
    # print(test_suite.working_mem[test_suite.max_rot_vol:2*test_suite.max_rot_vol])
    # print(test_suite.working_mem[2*test_suite.max_rot_vol:3*test_suite.max_rot_vol])
    # test_suite.v_ave(1,0,1)
    # test_suite.v_t_u(1,0,1)
    # test_suite.calc_cam(1,0,1)
    # test_suite.min_sweep(1,0,1)
    # test_suite.shift_by_min(1,0,1)
    # # print(test_suite.working_mem[3*test_suite.max_rot_vol:4*test_suite.max_rot_vol])
    # test_suite.quad_sweep_av(1,0,1)
    # test_suite.div_by_ave(1,0,1)
    # test_suite.quad_sweep_ent(1,0,1)
    # print(test_suite.working_mem[3*test_suite.max_rot_vol:4*test_suite.max_rot_vol])
    # print(test_suite.working_mem[0:test_suite.max_rot_vol])

    # print(test_suite.working_mem[test_suite.max_rot_vol:2*test_suite.max_rot_vol])
    # print(test_suite.working_mem[2*test_suite.max_rot_vol:3*test_suite.max_rot_vol])
