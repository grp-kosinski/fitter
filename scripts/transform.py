import chimera
import sys
from math import pi
import numpy as np
MRC = False
res = list(open(sys.argv[1]))
mrcp = res[0].strip()
pdbp = res[1].strip()
if pdbp[-3:] == "mrc":
	MRC = True
trans = []
def bbox(points):
    xs,ys,zs = zip(*points)
    return [min(xs),max(xs),min(ys),max(ys),min(zs),max(zs)]
chimera.openModels.open(mrcp)
for l in res[2:]:
	lf = [float(e) for e in l.strip().split(",")]
	t = lf[:3]
	r = lf[3:7]
	s = lf[7]
	m = chimera.openModels.open(pdbp)[0]
	#m = chimera.openModels.list()[-1]
	if MRC:
		center = [0.5*s*p for s,p in zip(m.data.original_step,m.data.size)]
		center = chimera.Vector(*center)
		xf = chimera.Xform_translation(*center)
		print(xf)
	else:
		#is this right?
		bbox_pdb = bbox(np.array([a.coord().toVector() for a in m.atoms])) 
		offset = chimera.Vector(bbox_pdb[0],bbox_pdb[2],bbox_pdb[4])
		center = chimera.Vector(0.5*(bbox_pdb[1] + bbox_pdb[0]),0.5*(bbox_pdb[3] + bbox_pdb[2]),0.5*(bbox_pdb[5] + bbox_pdb[4]))
		xf = chimera.Xform_translation(center)
		#center = sum([a.coord().toVector() for a in m.atoms],chimera.Vector(0,0,0))/len(m.atoms)
	if (r[0]**2 + r[1]**2 + r[2]**2 != 0):
		xf.rotate(r[0],r[1],r[2],r[3]/pi*180)
		print(xf)
	xf.translate(chimera.Vector(*t)-2*center)
	print(xf)
	print("------------------------")
	m.openState.xform = xf


