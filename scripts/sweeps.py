import numpy as np
from itertools import product

def gen_rot_nodes_index(i,_tuple):
    M = _tuple[0]
    N = _tuple[1]
    L = _tuple[2]
    a = i//(L*N)
    b = (i-L*N*a)//5
    g = i-a*L*N-b*N
    ret.append([2*np.pi*a/M,2*np.pi*g/N,(2*b+1)*np.pi/(2*L-1)])
    return ret


def gen_rot_nodes(_tuple):
    M = _tuple[0]
    N = _tuple[1]
    L = _tuple[2]
    ret = []
    for i,(a,b,g) in enumerate(product(range(0,M),range(0,L),range(0,N))):
        #print(i,a,g,b)
        ret.append([2*np.pi*a/M,(2*b+1)*np.pi/(2*L-1),2*np.pi*g/N])
    return ret

ars = [[2,2,2],[4,4,5],[8,8,14],[16,16,14],[32,32,14],[32,32,41]]

def refine_index(nodes,t,t_new):
    M = ars[t][0]
    N = ars[t][1]
    L = ars[t][2]
    ret = []
    if ars[t][2] == ars[t_new][2]:
        for m in range(0,M,2):
            for l in range(0,L):
                for n in range(0,N,2):
                    ret.append(m*N*L + l*N + n)
    elif (ars[t][0] == ars[t_new][0]) and (ars[t][2] != ars[t_new][2]):
        for m in range(0,M):
            for l in range(1,L,3):
                for n in range(0,N):
                    ret.append(m*N*L + l*N +n)
    else:
        for m in range(0,M,2):
            for l in range(1,L,3):
                for n in range(0,N,2):
                    ret.append(m*N*L+l*N + n)
    return ret

def get_index_sets(ars):
    #all_rots = [gen_rot_nodes(ars[-1])]
    M,N,L = ars[-1]
    all_rots = [list(range(M*N*L))]
    for i in range(1,len(ars)):
        all_rots.append(refine_index(all_rots[0],-i,-i-1))
    all_rots_c = [[e for e in all_rots[0]]]
    for i in range(1,len(all_rots)):
        all_rots_c.append([all_rots_c[i-1][j] for j in all_rots[i]])
    for t in range(-1,-len(all_rots_c),-1):
        for e in all_rots_c[t]:
            for c in range(t-1,-len(all_rots_c)-1,-1):
                all_rots_c[c].remove(e)    
    return all_rots_c

