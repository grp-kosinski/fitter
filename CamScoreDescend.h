/*
 * CamScoreDescend.h
 *
 *  Created on: Sep 14, 2021
 *      Author: kkarius
 */

#ifndef CAMSCOREDESCEND_H_
#define CAMSCOREDESCEND_H_


#include<map>
#include<util.h>
#include<cuda_util_include/cutil_math.h>
#include<derived_atomic_functions.h>

#include<ChebychevNodes.h>
#include<McEwenNodes.h>
#include<Density.h>
#include<fstream>
#include<string>

struct CamScoreDescend_Register {
	
	//transformations and descend
	float * h_transformations;
	float * d_transformations;
	float * h_scores;
	float * d_scores;
	int * d_converging;
	int * d_all_convergent;
	int * d_indeces;

	//scoring itself
	float * d_v_ave = nullptr;
	float * d_cam = nullptr;
	float * d_v_m_v_ave = nullptr;
	float * d_u_t_v = nullptr;
	float * h_u_ave = nullptr;
	float * d_u_ave = nullptr;
	float * h_u_norm = nullptr;
	float * d_u_norm = nullptr;
	float * h_cam = nullptr;
};


class CamScoreDescend {
public:
	CamScoreDescend(){};
	virtual ~CamScoreDescend(){};
	void createInstance(std::pair<float3,float3> search_volume, float d, float epsilon, int M, size_t *const& max_init_transform_vol_per_gpu, const int & trans_S, const int & rot_S, const int & gpu_index);
	void descend(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes, size_t *const& max_transformations_per_gpu, const int & gpu_num, const int & gpu_index);	
	static size_t initTransformationVolume(std::pair<float3,float3> search_volume, const float& d, const int& M);
	void destroyInstance(const int & gpu_index);

	std::map<int,CamScoreDescend_Register> _instance_registry;
	//supposed to be the same on all instances
	float d;
	float epsilon;
	int M;
	int trans_S;
	int rot_S;
	std::pair<float3,float3> search_volume;
	int init_transformations;
	int translation_search_grid_size;
	int rotation_search_grid_size;
};

#endif /* CAMSCOREDESCEND_H_ */
