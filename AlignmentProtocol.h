/*
 * AlignmentProtocol.h
 *
 *  Created on: Jan 14, 2021
 *      Author: kkarius
 */

#ifndef ALIGNMENTPROTOCOL_H_
#define ALIGNMENTPROTOCOL_H_

#include<vector>
#include<string>
#include<cuda_runtime.h>
#include<cuda_runtime_api.h>
#include<util.h>

//#include<Particles.h>

class AlignmentProtocol {
public:
	AlignmentProtocol();//{getMemoryInfo(0,global_mem,shared_mem);};
	AlignmentProtocol(std::string search_dir):_search_dir(search_dir){getMemoryInfo(0,global_mem,shared_mem);};
	void prepare(void);
	void prepareGPU(int gpu_index);
	void runOnGPU(int gpu_index);
	void create_random_test_frames(float4 *& particle_set,const int num_particles, const int num_frames, const float * bounding_box);
	void create_test_cube(float4 *& h_particle_set,float4 *& d_particle_set);
	bool rmsd(float4 *& d_particle_set,float *& h_rmsds, int & num_particles, int & num_frames,
			unsigned int gridDim, unsigned int blockDim);
	void rmsdRMF(std::string infile_name,float *& h_rmsds,int & num_frames);
	void create_rmf_files(void);
	bool align(float4 * h_particles_4,float4 * d_particles_4,int frames,int particle_num,
			const bool OFFDEVICE,float3 *& h_coms,double *& h_U,int blockDim,int gridDim);
	void alignRMF(std::string infile_name, std::string outfile_name);
	void testFileRMF(std::string infile_name,std::string testfile_name,int test_frame_num);
	void testCube(void);
	void pureTest(uint gridDim,uint blockDim);
	void rmfTest(void);
	void rmfTestAlignment(std::string infile_name,std::string testfile_name);
	void getKarneySet(const char * rot_file_name,float4 *& quats, int & rot_num);
	void benchmark(std::string output_file);
	size_t global_mem = 0;
	size_t shared_mem = 0;
	virtual ~AlignmentProtocol(){};
private:
	std::string _search_dir;
	//the first file will be the reference particle set
	std::vector<std::string> _rmf_file_paths;
	void _read_dir_entries(void);

//	Particles * _reference = new Particles;
};

#endif /* ALIGNMENTPROTOCOL_H_ */
