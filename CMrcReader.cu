/*
 * CMrcReader.cpp
 *
 *  Created on: Jun 6, 2019
 *      Author: kkarius
 */

#include <CMrcReader.h>
CMrcReader::CMrcReader(){}

float CMrcReader::getPixelSize(const char * file_name){
	float pixel_size;
	CMrcReader reader;
	reader.setFileName(file_name);
	reader.readHeader();
	pixel_size = reader.header.cella.x/((float) reader.header.nx);
	printf("Pixel size %f derived from x dimension of mrc file %s\n",pixel_size,file_name);
	return pixel_size;
}
void CMrcReader::writeTo(const char * file_name, float *const& data){
	CMrcReader reader;
	reader.setFileName(file_name);
	reader.readHeader();
	if (reader.header.mode == 2) {
		int volume = reader.header.nx * reader.header.ny * reader.header.nz;
		int bytes = sizeof(float) * volume;
		std::fstream x_FileHandle;
		x_FileHandle.open(file_name,
				std::fstream::in | std::fstream::binary);
		//jump header
		x_FileHandle.seekg(1024, std::ios::beg);
		x_FileHandle.read((char*) data, bytes);
	}
}

void CMrcReader::printHeader() {
	if (!x_HeaderRead) {
		readHeader();
	}
	std::cout << "nx:" << header.nx << std::endl;
	std::cout << "ny:" << header.ny << std::endl;
	std::cout << "nz:" << header.nz << std::endl;

	std::cout << "mode:" << header.mode << std::endl;

	std::cout << "nxstart:" << header.nxstart << std::endl;
	std::cout << "nystart:" << header.nystart << std::endl;
	std::cout << "nzstart:" << header.nzstart << std::endl;

	std::cout << "mx: " << header.mx << std::endl;
	std::cout << "my: " << header.my << std::endl;
	std::cout << "mz: " << header.mz << std::endl;

	std::cout << "cella.x: " << header.cella.x << std::endl;
	std::cout << "cella.y: " << header.cella.y << std::endl;
	std::cout << "cella.z: " << header.cella.z << std::endl;

	std::cout << "cellb.alpha: " << header.cellb.alpha << std::endl;
	std::cout << "cellb.beta: " << header.cellb.beta << std::endl;
	std::cout << "cellb.gamma: " << header.cellb.gamma << std::endl;

	std::cout << "mapc: " << header.mapc << std::endl;
	std::cout << "mapr: " << header.mapr << std::endl;
	std::cout << "maps: " << header.maps << std::endl;

	std::cout << "dmin: " << header.dmin << std::endl;
	std::cout << "dmax: " << header.dmax << std::endl;
	std::cout << "dmean: " << header.dmean << std::endl;

	std::cout << "ispg: " << header.ispg << std::endl;
	std::cout << "nsymbt: " << header.nsymbt << std::endl;
//	std::cout << "extra1: " << header.extra1 << std::endl;
//	std::cout << "exttype: " << header.exttype << std::endl;
//	std::cout << "nversion: " << header.nversion << std::endl;

	std::cout << "origin.x: " << header.origin.x << std::endl;
	std::cout << "origin.y: " << header.origin.y << std::endl;
	std::cout << "origin.z: " << header.origin.z << std::endl;

	std::cout << "map: " << header.map[0] << header.map[1] << header.map[2]
			<< header.map[3] << std::endl;
	std::cout << "machst: " << (int) header.machst[0] << ","
			<< (int) header.machst[1] << "," << (int) header.machst[2] << ","
			<< (int) header.machst[3] << std::endl;
	std::cout << "rms: " << header.rms << std::endl;
	std::cout << "nlabl: " << header.nlabl << std::endl;
	std::cout << "label: " << header.label << std::endl;

}

std::ostream& operator<<(std::ostream& out, const SCellA& cellA) {
	return out << cellA.x << cellA.y << cellA.z;
}

std::ostream& operator<<(std::ostream& out, const SCellB& cellB) {
	return out << cellB.alpha << cellB.beta << cellB.gamma;
}

void CMrcReader::setDimensions(float dim[3],uint pixel_dim[3]){
	header.cella.x = dim[0];
	header.cella.y = dim[1];
    header.cella.z = dim[2];
	header.nx = pixel_dim[0];
    header.ny = pixel_dim[1];
    header.nz = pixel_dim[2];
	header.mx = pixel_dim[0];
	header.my = pixel_dim[1];
	header.mz = pixel_dim[2];
	header.origin.x = 0.0;
	header.origin.y = 0.0;
    header.origin.z = 0.0;
    data = (float *)malloc(pixel_dim[0]*pixel_dim[1]*pixel_dim[2]*sizeof(float));
    for (int i = 0;i<pixel_vol();i++){
    	data[i] = 0.0;
    }
}

void CMrcReader::addCube(float dim[3],uint pixel_dim[3],float pos[3],float cube_dim[3],
		float cube_val){
    int x;
    int y;
    int z;
    float xpixeldim = dim[0]/pixel_dim[0];
    float ypixeldim = dim[1]/pixel_dim[1];
    float zpixeldim = dim[2]/pixel_dim[2];
    float xpos;
    float ypos;
    float zpos;
    for (int i = 0;i<pixel_dim[0]*pixel_dim[1]*pixel_dim[2];i++){
    	z = i/(pixel_dim[0]*pixel_dim[1]);
    	y = (i - z*pixel_dim[0]*pixel_dim[1])/pixel_dim[1];
    	x = i - y*pixel_dim[1] - z*pixel_dim[0]*pixel_dim[1];
    	xpos = x*xpixeldim;
    	ypos = y*ypixeldim;
    	zpos = z*zpixeldim;
    	if ((pos[0]-cube_dim[0]/2 <= xpos) && (xpos <= pos[0]+cube_dim[0]/2) &&
    		(pos[1]-cube_dim[1]/2 <= ypos) && (ypos <= pos[1]+cube_dim[1]/2) &&
			(pos[2]-cube_dim[2]/2 <= zpos) && (zpos <= pos[2]+cube_dim[2]/2)){
    		data[i] += cube_val;
    	}
     }
}

void CMrcReader::makeCube(float dim[3],uint pixel_dim[3],float pos[3],float cube_dim[3],
			float cube_val, float non_cube_val){

	setDimensions(dim,pixel_dim);
    int x;
    int y;
    int z;
    float xpixeldim = dim[0]/pixel_dim[0];
    float ypixeldim = dim[1]/pixel_dim[1];
    float zpixeldim = dim[2]/pixel_dim[2];
    float xpos;
    float ypos;
    float zpos;
    for (int i = 0;i<pixel_dim[0]*pixel_dim[1]*pixel_dim[2];i++){
    	z = i/(pixel_dim[0]*pixel_dim[1]);
    	y = (i - z*pixel_dim[0]*pixel_dim[1])/pixel_dim[1];
    	x = i - y*pixel_dim[1] - z*pixel_dim[0]*pixel_dim[1];
    	xpos = x*xpixeldim;
    	ypos = y*ypixeldim;
    	zpos = z*zpixeldim;
    	if ((pos[0]-cube_dim[0]/2 <= xpos) && (xpos <= pos[0]+cube_dim[0]/2) &&
    		(pos[1]-cube_dim[1]/2 <= ypos) && (ypos <= pos[1]+cube_dim[1]/2) &&
			(pos[2]-cube_dim[2]/2 <= zpos) && (zpos <= pos[2]+cube_dim[2]/2)){
    		data[i] = cube_val;
    	}
    	else {
    		data[i] = non_cube_val;
    	}
    }
    write();
}

float CMrcReader::average(){
	float ret = 0.0;
	for (int i=0;i<pixel_vol();i++){
		ret += data[i];
	}
	ret /= pixel_vol();
	return ret;
}

void CMrcReader::write(){
	std::fstream x_FileHandle;
	x_FileHandle.open(mrc_file_name, std::fstream::out | std::fstream::binary);
	x_FileHandle.seekg(0, std::ios::beg);

	x_FileHandle.write(reinterpret_cast<const char *>(&header.nx), sizeof(header.nx));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.ny), sizeof(header.ny));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.nz), sizeof(header.nz));

	x_FileHandle.write(reinterpret_cast<const char *>(&header.mode), sizeof(header.mode));

	x_FileHandle.write(reinterpret_cast<const char *>(&header.nxstart), sizeof(header.nxstart));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.nystart), sizeof(header.nystart));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.nzstart), sizeof(header.nzstart));

	x_FileHandle.write(reinterpret_cast<const char *>(&header.mx), sizeof(header.mx));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.my), sizeof(header.my));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.mz), sizeof(header.mz));

	x_FileHandle.write(reinterpret_cast<const char *>(&header.cella), sizeof(header.cella));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.cellb), sizeof(header.cellb));

	x_FileHandle.write(reinterpret_cast<const char *>(&header.mapc), sizeof(header.mapc));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.mapr), sizeof(header.mapr));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.maps), sizeof(header.maps));

	x_FileHandle.write(reinterpret_cast<const char *>(&header.dmin), sizeof(header.dmin));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.dmax), sizeof(header.dmax));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.dmean), sizeof(header.dmean));

	x_FileHandle.write(reinterpret_cast<const char *>(&header.ispg), sizeof(header.ispg));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.nsymbt), sizeof(header.nsymbt));

//	x_FileHandle.read((char*) &header.extra1, sizeof(header.extra1));
//	x_FileHandle.read((char*) &header.exttype, sizeof(header.exttype));
//	x_FileHandle.read((char*) &header.nversion, sizeof(header.nversion));
//	x_FileHandle.read((char*) &header.extra2, sizeof(header.extra2));
	x_FileHandle.seekg(196, std::ios::beg);

	x_FileHandle.write(reinterpret_cast<const char *>(&header.origin), sizeof(header.origin));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.map), sizeof(header.map));

	x_FileHandle.write(reinterpret_cast<const char *>(&header.machst), sizeof(header.machst));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.rms), sizeof(header.rms));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.nlabl), sizeof(header.nlabl));
	x_FileHandle.write(reinterpret_cast<const char *>(&header.label), sizeof(header.label));

	if (header.mode == 2) {
		int volume = header.nx * header.ny * header.nz;
		int bytes = sizeof(float) * volume;
		x_FileHandle.seekg(1024, std::ios::beg);
		x_FileHandle.write((char*) data, bytes);
	}
	x_FileHandle.close();
}


void CMrcReader::readHeader() {
	std::fstream x_FileHandle;
	x_FileHandle.open(mrc_file_name, std::fstream::in | std::fstream::binary);

	x_FileHandle.read((char*) &header.nx, sizeof(header.nx));
	x_FileHandle.read((char*) &header.ny, sizeof(header.ny));
	x_FileHandle.read((char*) &header.nz, sizeof(header.nz));

	x_FileHandle.read((char*) &header.mode, sizeof(header.mode));

	x_FileHandle.read((char*) &header.nxstart, sizeof(header.nxstart));
	x_FileHandle.read((char*) &header.nystart, sizeof(header.nystart));
	x_FileHandle.read((char*) &header.nzstart, sizeof(header.nzstart));

	x_FileHandle.read((char*) &header.mx, sizeof(header.mx));
	x_FileHandle.read((char*) &header.my, sizeof(header.my));
	x_FileHandle.read((char*) &header.mz, sizeof(header.mz));

	x_FileHandle.read((char*) &header.cella, sizeof(header.cella));
	x_FileHandle.read((char*) &header.cellb, sizeof(header.cellb));

	x_FileHandle.read((char*) &header.mapc, sizeof(header.mapc));
	x_FileHandle.read((char*) &header.mapr, sizeof(header.mapr));
	x_FileHandle.read((char*) &header.maps, sizeof(header.maps));

	x_FileHandle.read((char*) &header.dmin, sizeof(header.dmin));
	x_FileHandle.read((char*) &header.dmax, sizeof(header.dmax));
	x_FileHandle.read((char*) &header.dmean, sizeof(header.dmean));

	x_FileHandle.read((char*) &header.ispg, sizeof(header.ispg));
	x_FileHandle.read((char*) &header.nsymbt, sizeof(header.nsymbt));
//	x_FileHandle.read((char*) &header.extra1, sizeof(header.extra1));
//	x_FileHandle.read((char*) &header.exttype, sizeof(header.exttype));
//	x_FileHandle.read((char*) &header.nversion, sizeof(header.nversion));
//	x_FileHandle.read((char*) &header.extra2, sizeof(header.extra2));
	x_FileHandle.seekg(196, std::ios::beg);

	x_FileHandle.read((char*) &header.origin, sizeof(header.origin));
	x_FileHandle.read((char*) &header.map, sizeof(header.map));

	x_FileHandle.read((char*) &header.machst, sizeof(header.machst));
	x_FileHandle.read((char*) &header.rms, sizeof(header.rms));
	x_FileHandle.read((char*) &header.nlabl, sizeof(header.nlabl));
	x_FileHandle.read((char*) &header.label, sizeof(header.label));

	x_FileHandle.close();
}

int CMrcReader::readData() {
	//    * mode 0 -> int8
	//    * mode 1 -> int16
	//    * mode 2 -> float32
	//    * mode 4 -> complex64
	//    * mode 6 -> uint16

	// 1. read the data type by accessing the mode property
	// 2. read the shape from header -> for our purposes (nx,ny,nz)
	// 3. calculate number of bytes by sizeof(dtype)*nx*ny*nz

	if (header.mode == 2) {
		int volume = header.nx * header.ny * header.nz;
		int bytes = sizeof(float) * volume;
		data = (float*) malloc(bytes);
		std::fstream x_FileHandle;
		x_FileHandle.open(mrc_file_name,
				std::fstream::in | std::fstream::binary);
		//jump header
		x_FileHandle.seekg(1024, std::ios::beg);
		x_FileHandle.read((char*) data, bytes);
		return 0;
	}
	return -1;
}

CMrcReader::~CMrcReader() {
}

