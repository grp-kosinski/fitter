/*
 * CParticles.h
 *
 *  Created on: Feb 25, 2019
 *      Author: kkarius
 */

#ifndef UTIL_H_
#define UTIL_H_
#include <cuda_util_include/cutil_math.h>
#include <functional>
#include <thrust/functional.h>
#include <unistd.h>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <stdlib.h>
#include <assert.h>
#include <cmath>
#include <functional>
#include <numeric>
#include <vector>
#include <string>
#include <memory>

#include <thread>
#define M_PI 3.14159265358979323846

template <class T, unsigned int blockSize>
__device__
void warp_reduce(volatile T * sdata, int tid){
	if (blockSize >= 64) {sdata[tid] += sdata[tid + 32];__syncwarp();}
	if (blockSize >= 32) {sdata[tid] += sdata[tid + 16];__syncwarp();}
	if (blockSize >= 16) {sdata[tid] += sdata[tid +  8];__syncwarp();}
	if (blockSize >=  8) {sdata[tid] += sdata[tid +  4];__syncwarp();}
	if (blockSize >=  4) {sdata[tid] += sdata[tid +  2];__syncwarp();}
	if (blockSize >=  2) {sdata[tid] += sdata[tid +  1];__syncwarp();}
}

template <class T, unsigned int blockSize>
__device__
void min_warp_reduce(volatile T * sdata, int tid){
	if (blockSize >= 64) {sdata[tid] = min(sdata[tid],sdata[tid + 32]);__syncwarp();}
	if (blockSize >= 32) {sdata[tid] = min(sdata[tid],sdata[tid + 16]);__syncwarp();}
	if (blockSize >= 16) {sdata[tid] = min(sdata[tid],sdata[tid +  8]);__syncwarp();}
	if (blockSize >=  8) {sdata[tid] = min(sdata[tid],sdata[tid +  4]);__syncwarp();}
	if (blockSize >=  4) {sdata[tid] = min(sdata[tid],sdata[tid +  2]);__syncwarp();}
	if (blockSize >=  2) {sdata[tid] = min(sdata[tid],sdata[tid +  1]);__syncwarp();}
}

template<typename ... Args>
std::string string_format( const std::string& format, Args ... args )
{
    size_t size = snprintf( nullptr, 0, format.c_str(), args ... ) + 1; // Extra space for '\0'
    if( size <= 0 ){ throw std::runtime_error( "Error during formatting." ); }
    std::unique_ptr<char[]> buf( new char[ size ] );
    snprintf( buf.get(), size, format.c_str(), args ... );
    return std::string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
}

using namespace std;

template <typename T>
vector<size_t> sort_indexes(const vector<T> &v) {

  // initialize original index locations
  vector<size_t> idx(v.size());
  iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  // using std::stable_sort instead of std::sort
  // to avoid unnecessary index re-orderings
  // when v contains elements of equal values
  stable_sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) {return v[i1] > v[i2];});

  return idx;
}

template <typename T>
vector<size_t> sort_indexes_inverse(const vector<T> &v) {

  // initialize original index locations
  vector<size_t> idx(v.size());
  iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  // using std::stable_sort instead of std::sort
  // to avoid unnecessary index re-orderings
  // when v contains elements of equal values
  stable_sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) {return v[i1] < v[i2];});

  return idx;
}

typedef float TDensity;

__host__ __device__
inline size_t vol(uint3 u){
	return u.x*u.y*u.z;
}

__host__ __device__
inline size_t vol(int3 u){
	return u.x*u.y*u.z;
}

inline float vol(float3 u){
	return u.x*u.y*u.z;
}

//nasa discussion paper
inline void euler_to_axis_angle(float * in, float * out){
	float q[4] = {cos(in[1]/2)*cos(0.5f*(in[0]+in[2])),
			     -sin(in[1]/2)*sin(0.5f*(in[0]-in[2])),
				  sin(in[1]/2)*cos(0.5f*(in[0]-in[2])),
				  cos(in[1]/2)*sin(0.5f*(in[0]+in[2]))};
	float f = sqrtf(q[1]*q[1]+q[2]*q[2]+q[3]*q[3]);
//	f = 0.0f;
	if (f==0){
		out[0] = 0.0f;
		out[1] = 0.0f;
		out[2] = 1.0f;
		out[3] = 0.0f;
	}else{
		out[0] = q[1]/f;
		out[1] = q[2]/f;
		out[2] = q[3]/f;
		out[3] = 2*atan2(f,q[0]);
	}
}
//__global__
//void quaternions_to_rotations(double * d_U, float * d_rot_matrices,
//		size_t num_frames) {
//	int tid = threadIdx.x;
//	if (tid >= num_frames) {
//		return;
//	}
//
//	float4 q; //quaternion to be turned into a rotation matrix
//	q.x = (float) d_U[tid * 16 + 12];
//	q.y = (float) d_U[tid * 16 + 13];
//	q.z = (float) d_U[tid * 16 + 14];
//	q.w = (float) d_U[tid * 16 + 15];
//
////	if (tid<10){
////		printf("%i: [%f,%f,%f,%f]\n",tid,q.x,q.y,q.z,q.w);
////	}
//
//// 0 1 2
//// 3 4 5 --> 0 3 6 1 4 7 2 5 8
//// 6 7 8
//	d_rot_matrices[tid * 9 + 0] = 1 - 2 * q.z * q.z - 2 * q.w * q.w;
//	d_rot_matrices[tid * 9 + 3] = 2 * q.y * q.z - 2 * q.x * q.w;
//	d_rot_matrices[tid * 9 + 6] = 2 * q.y * q.w + 2 * q.x * q.z;
//	d_rot_matrices[tid * 9 + 1] = 2 * q.z * q.y + 2 * q.x * q.w;
//	d_rot_matrices[tid * 9 + 4] = 1 - 2 * q.w * q.w - 2 * q.y * q.y;
//	d_rot_matrices[tid * 9 + 7] = 2 * q.z * q.w - 2 * q.x * q.y;
//	d_rot_matrices[tid * 9 + 2] = 2 * q.w * q.y - 2 * q.x * q.z;
//	d_rot_matrices[tid * 9 + 5] = 2 * q.w * q.z + 2 * q.x * q.y;
//	d_rot_matrices[tid * 9 + 8] = 1 - 2 * q.y * q.y - 2 * q.z * q.z;
//}
//
//
//__global__
//void quaternions_to_angles_and_axis(double * d_U, float4 * d_angles_axis,
//		size_t num_frames) {
//	int tid = threadIdx.x;
//	if (tid >= num_frames) {
//		return;
//	}
//
//	float4 q; //quaternion to be turned into a rotation matrix
//	q.x = (float) d_U[tid * 16 + 12];
//	q.y = (float) d_U[tid * 16 + 13];
//	q.z = (float) d_U[tid * 16 + 14];
//	q.w = (float) d_U[tid * 16 + 15];
//
//	// q = q[cos(theta/2);sin(theta/2)*v]
//
//	float4 angle_axis;
//	float theta = 2*acos(q.x);
//	float sin_theta_half = 1 - q.x*q.x;
//	angle_axis.x = theta;
//	angle_axis.y = q.y/sin_theta_half;
//	angle_axis.z = q.z/sin_theta_half;
//	angle_axis.w = q.w/sin_theta_half;
//	d_angles_axis[tid] = angle_axis;
//}

//cuda helper function stolen from the interwebs
#define CUDA_ERROR_CHECK
#define CudaSafeCall( err ) __cudaSafeCall( err, __FILE__, __LINE__ )
#define CudaCheckError()    __cudaCheckError( __FILE__, __LINE__ )
inline void __cudaSafeCall(cudaError err, const char *file, const int line) {
#ifdef CUDA_ERROR_CHECK
	if (cudaSuccess != err) {
		fprintf( stderr, "cudaSafeCall() failed at %s:%i : %s\n", file, line,
				cudaGetErrorString(err));
		exit(-1);
	}
#endif
	return;
}
inline void __cudaCheckError(const char *file, const int line) {
#ifdef CUDA_ERROR_CHECK
	cudaError err = cudaGetLastError();
	if (cudaSuccess != err) {
		fprintf( stderr, "cudaCheckError() failed at %s:%i : %s\n", file, line,
				cudaGetErrorString(err));
		exit(-1);
	}

// More careful checking. However, this will affect performance.
// Comment away if needed.
	err = cudaDeviceSynchronize();
	if (cudaSuccess != err) {
		fprintf( stderr, "cudaCheckError() with sync failed at %s:%i : %s\n",
				file, line, cudaGetErrorString(err));
		exit(-1);
	}
#endif
	return;
}

#include <sys/stat.h>
#include <bits/stdc++.h>
#include <iostream>
#include <sys/types.h>



inline std::string getPathName(const std::string& s) {

   char sep = '/';

#ifdef _WIN32
   sep = '\\';
#endif

   size_t i = s.rfind(sep, s.length());
   if (i != std::string::npos) {
      return(s.substr(0, i));
   }

   return("");
}

inline bool hasEnding (std::string const &fullString, std::string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

inline bool dir_exists(const std::string &s)
{
  struct stat buffer;
  return (stat (s.c_str(), &buffer) == 0);
}

inline void make_dir(const char * name){
	if (!dir_exists(std::string(name))){
		if (mkdir(name, 0777) == -1)
			std::cerr << "Error :  " << strerror(errno) << std::endl;
	}
}

inline bool file_exists (const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
    	fprintf( stderr, "File %s is not found ...\n", name.c_str());
        return false;
    }
}

inline bool file_exists (const char * name) {
    if (FILE *file = fopen(name, "r")) {
        fclose(file);
        return true;
    } else {
    	fprintf( stderr, "File %s is not found ...\n", name);
        return false;
    }
}

const size_t one_gb = 1073741824;
static const int MAXTHREADS = 1024;
struct compare_float4_x
{
 	__host__ __device__
 	bool operator()(float4 lhs, float4 rhs)
 	{
 	      return lhs.x < rhs.x;
 	}
};

struct compare_float4_y
{
 	__host__ __device__
 	bool operator()(float4 lhs, float4 rhs)
 	{
 	      return lhs.y < rhs.y;
 	}
};

struct compare_float4_z
{
 	__host__ __device__
 	bool operator()(float4 lhs, float4 rhs)
 	{
 	      return lhs.z < rhs.z;
 	}
};

struct min_float4 : public thrust::binary_function<float4,float4,float4>
{
  __host__ __device__
  float4 operator()(float4 x, float4 y) { return fminf(x,y); }
};

struct max_float4 : public thrust::binary_function<float4,float4,float4>
{
  __host__ __device__
  float4 operator()(float4 x, float4 y) { return fmaxf(x,y); }
};

__device__ __host__
inline int ipow(int x, unsigned int p)
{
  if (p == 0) return 1;
  if (p == 1) return x;

  int ret = 1;
  for (int i=0;i<p;++i){
	  ret *= x;
  }
  return ret;
}


inline float3 parse_float3_string(const char * str){
    float3 ret;
    std::vector<float> vect;
    std::string input(str);
    std::istringstream ss(input);
    std::string token;
    while(std::getline(ss, token, ',')) {
        vect.push_back(std::stof(token.c_str()));
    }    
    ret.x = vect[0];
    ret.y = vect[1];
    ret.z = vect[2];
    return ret;
}

inline float4 parse_float4_string(const char * str){
    float4 ret;
    std::vector<float> vect;
    std::string input(str);
    std::istringstream ss(input);
    std::string token;
    while(std::getline(ss, token, ',')) {
        vect.push_back(std::stof(token.c_str()));
    }    
    ret.x = vect[0];
    ret.y = vect[1];
    ret.z = vect[2];
    ret.w = vect[3];
    return ret;
}

// trim from end
static inline std::string &ltrim(std::string &s) {
    s.erase(std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}
// trim from end
static inline std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}

inline float4 q_div(float4 q, float4 r){
	float f = 1/(r.x*r.x + r.y*r.y + r.z*r.z + r.w*r.w);
	return {f*(r.x*q.x + r.y*q.y + r.z*q.z + r.w*q.w),
		f*(r.x*q.y - r.y*q.x + r.z*q.w - r.w*q.z),	
		f*(r.x*q.z - r.y*q.w - r.z*q.x + r.w*q.y),	
		f*(r.x*q.w + r.y*q.z - r.z*q.y - r.w*q.x)};	
}

inline float4 q_mult(float4 q, float4 r){
    return { r.x*q.x - r.y*q.y - r.z*q.z - r.w*q.w,
             r.x*q.y + r.y*q.x - r.z*q.w + r.w*q.z,
             r.x*q.z + r.y*q.w + r.z*q.x - r.w*q.y,
             r.x*q.w - r.y*q.z + r.z*q.y + r.w*q.x};
}

inline void getMemoryInfo(int dev, size_t &global_mem, size_t &shared_mem){
	cudaSetDevice(dev);
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, dev);
    global_mem = deviceProp.totalGlobalMem;
    shared_mem = deviceProp.sharedMemPerBlock;
}

inline std::string baseName(std::string const & path)
{
  return path.substr(path.find_last_of("/\\") + 1);
}

inline int closestInt(float d)
{
  	return d < 0 ? (int) ceil(d - 0.5) : (int) floor(d + 0.5);
}

#endif /* UTIL_H_ */


