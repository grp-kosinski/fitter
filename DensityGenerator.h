/*
 * DensityGenerator.h
 *
 *  Created on: Mar 3, 2021
 *      Author: kkarius
 */

#ifndef DENSITYGENERATOR_H_
#define DENSITYGENERATOR_H_

#include<cuda_util_include/cutil_math.h>
#include<Particles.h>
#include<Density.h>
#include<Stamp.h>
#include<map>

struct DensityGenerator_Register {
	float * h_offset_shift;
	float * d_offset_shift;
	uint * d_offsets;
};

class DensityGenerator {
public:
	DensityGenerator(){};
	void createInstanceFromParticles(Particles * const& particles, const int &gpu_index);
	void generateMolecularDensity(Density *const& density, Particles *const& particles, Stamp * const& stamp, int gridDim, int blockDim, int gpu_index);
	void freeInstance(const int& gpu_index);	
	void freeAllInstances(void);
	virtual ~DensityGenerator(){};
private:
	friend class Particles;
	friend class Density;
	std::map<int,DensityGenerator_Register> _instance_registry;
};

#endif /* DENSITYGENERATOR_H_ */
