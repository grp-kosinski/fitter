/*
 * CamScoreDescend.cu
 *
 *  Created on: Sep 14, 2021
 *      Author: kkarius
 */

#include <CamScoreDescend.h>
#include <score_helper.h>
#include <thrust/distance.h>

__host__ __device__
void normalize_and_rotation(float *const& q, float *const& rotation){
	float4 qn = {q[0],q[1],q[2],q[3]};
	qn *= rsqrtf(qn.x*qn.x + qn.y*qn.y + qn.z*qn.z + qn.w*qn.w);
	rotation[0] = 1-2*(qn.z*qn.z + qn.w*qn.w);
	rotation[1] =   2*(qn.y*qn.z - qn.w*qn.x);
	rotation[2] =   2*(qn.y*qn.w + qn.z*qn.x);
	rotation[3] =   2*(qn.y*qn.z + qn.w*qn.x);
	rotation[4] = 1-2*(qn.y*qn.y + qn.w*qn.w);
	rotation[5] =   2*(qn.z*qn.w - qn.y*qn.x);
	rotation[6] =   2*(qn.y*qn.w - qn.z*qn.x);
	rotation[7] =   2*(qn.z*qn.w + qn.y*qn.x);
	rotation[8] = 1-2*(qn.y*qn.y + qn.z*qn.z);
}

__global__
void and_kernel(int * d_bools, size_t vol, int * d_result){
	__shared__ int concat[512];
	int tid = threadIdx.x;
	size_t grid_size = blockDim.x*gridDim.x;
	while (tid < 512){
		concat[tid] = 1;
		tid += blockDim.x;
	}
	__syncthreads();
	tid = threadIdx.x + blockIdx.x*blockDim.x;
	while (tid < vol){
		concat[threadIdx.x] *= d_bools[tid];
		tid += grid_size;
	}
	__syncthreads();
	if (threadIdx.x < 256) { concat[threadIdx.x] *= concat[threadIdx.x + 256];} __syncthreads();
	if (threadIdx.x < 128) { concat[threadIdx.x] *= concat[threadIdx.x + 128];} __syncthreads();
	if (threadIdx.x <  64) { concat[threadIdx.x] *= concat[threadIdx.x +  64];} __syncthreads();
	if (threadIdx.x <  32) { concat[threadIdx.x] *= concat[threadIdx.x +  32];} __syncthreads();
	if (threadIdx.x <  16) { concat[threadIdx.x] *= concat[threadIdx.x +  16];} __syncthreads();
	if (threadIdx.x <   8) { concat[threadIdx.x] *= concat[threadIdx.x +   8];} __syncthreads();
	if (threadIdx.x <   4) { concat[threadIdx.x] *= concat[threadIdx.x +   4];} __syncthreads();
	if (threadIdx.x <   2) { concat[threadIdx.x] *= concat[threadIdx.x +   2];} __syncthreads();
	if (threadIdx.x <   1) { concat[threadIdx.x] *= concat[threadIdx.x +   1];} __syncthreads();
	if (threadIdx.x==0){
		atomicAnd(&d_result[0], concat[0]);
	}
}

__global__
void v_ave(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, uint3 * d_query_pdim,uint3 * d_cheby_node_dim, float * d_cheby_nodes, float * d_cheby_weights, float * d_transformations, int translation_search_grid_size, int rotation_search_grid_size, int init_transformations, float * d_v_ave, int * d_converging){
	__shared__ float concat[512];
	__shared__ float3 translation;
	__shared__ float rotation[9];
	uint3 thrid = {blockDim.x*blockIdx.x + threadIdx.x,blockDim.y*blockIdx.y + threadIdx.y,blockDim.z*blockIdx.z + threadIdx.z};
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;
	if (thrid.x >= d_cheby_node_dim->x && thrid.y >= d_cheby_node_dim->y && thrid.z >= d_cheby_node_dim->z){
		concat[tid] = 0.0f;
		return;
	}
	float3 node_coords_target = {d_cheby_nodes[thrid.x],
				     d_cheby_nodes[thrid.y + d_cheby_node_dim->x],
			  	     d_cheby_nodes[thrid.z + d_cheby_node_dim->x + d_cheby_node_dim->y]};
	
	float3 node_coords_target_r;
	float3 node_coords_target_rt;
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
			    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     			    *d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];

	int init_trans_stride = 3*translation_search_grid_size + 4*rotation_search_grid_size;
	float * curr;
	int t; // search translations
	int r; // search rotations
	int k = 0; // initial transformations
	while (k < init_transformations){
		if (d_converging[k]){
			k++;
			continue;
		}
		curr = d_transformations + k*init_trans_stride;
		r = 0;
		while (r < rotation_search_grid_size){
			if (tid == 0){
					//void normalize_and_rotation(float *const& q, float *const rotation)
				normalize_and_rotation(d_transformations + k*init_trans_stride + 3*translation_search_grid_size + 4*r,&rotation[0]);
			}
			__syncthreads();
			rotate(node_coords_target_r,node_coords_target,&rotation[0]);
			t = 0;
			while(t < translation_search_grid_size){
				if (tid == 0){
					translation = {curr[3*t], curr[3*t + 1], curr[3*t + 2]};
				}
				__syncthreads();
				node_coords_target_rt = node_coords_target_r + translation;
				concat[tid] = node_weight*tex3D<float>(d_density_target, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z);
				__syncthreads();
				if (tid < 256) { concat[tid] += concat[tid + 256];} __syncthreads();
				if (tid < 128) { concat[tid] += concat[tid + 128];} __syncthreads();
				if (tid <  64) { concat[tid] += concat[tid +  64];} __syncthreads();
				if (tid <  32) { concat[tid] += concat[tid +  32];} __syncthreads();
				if (tid <  16) { concat[tid] += concat[tid +  16];} __syncthreads();
				if (tid <   8) { concat[tid] += concat[tid +   8];} __syncthreads();
				if (tid <   4) { concat[tid] += concat[tid +   4];} __syncthreads();
				if (tid <   2) { concat[tid] += concat[tid +   2];} __syncthreads();
				if (tid <   1) { concat[tid] += concat[tid +   1];} __syncthreads();
				if (tid==0){
					atomicAdd(&d_v_ave[k*translation_search_grid_size*rotation_search_grid_size + r*translation_search_grid_size +  t], concat[0]);
				}
				t++;
			}
			r++;
		}
		k++;
	}
}

__global__
void u_t_v(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, uint3 * d_query_pdim,uint3 * d_cheby_node_dim, float * d_cheby_nodes, float * d_cheby_weights, float * d_transformations, int translation_search_grid_size, int rotation_search_grid_size, int init_transformations, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v,float * d_u_ave, int * d_converging){
	__shared__ float concat1[512]; //v_m_v_ave
	__shared__ float concat2[512];// u_t_v
	__shared__ float3 translation;
	__shared__ float rotation[9];
	__shared__ float v_ave;
	float v_m_v_ave;
	uint3 thrid = {blockDim.x*blockIdx.x + threadIdx.x,blockDim.y*blockIdx.y + threadIdx.y,blockDim.z*blockIdx.z + threadIdx.z};
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;
	if (thrid.x >= d_cheby_node_dim->x && thrid.y >= d_cheby_node_dim->y && thrid.z >= d_cheby_node_dim->z){
		concat1[tid] = 0.0f;
		concat2[tid] = 0.0f;
		return;
	}
	float u_ave = d_u_ave[0];
	float3 half_query_dim = {(float) d_query_pdim->x,(float) d_query_pdim->y,(float) d_query_pdim->z};
	half_query_dim *= 0.5f;
	float3 node_coords_target = {d_cheby_nodes[thrid.x],
				     d_cheby_nodes[thrid.y + d_cheby_node_dim->x],
				     d_cheby_nodes[thrid.z + d_cheby_node_dim->x + d_cheby_node_dim->y]};
	float3 node_coords_query = node_coords_target + half_query_dim;
	float3 node_coords_target_r;
	float3 node_coords_target_rt;
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
			    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     			    *d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];

	int init_trans_stride = 3*translation_search_grid_size + 4*rotation_search_grid_size;
	float * curr;
	int t; // search translations
	int r; // search rotations
	int k = 0; // initial transformations
	while (k < init_transformations){
		if (d_converging[k]){
			k++;
			continue;
		}
		curr = d_transformations + k*init_trans_stride;
		r = 0;
		while (r < rotation_search_grid_size){
			if (tid == 0){
				//void normalize_and_rotation(float *const& q, float *const rotation)
				normalize_and_rotation(d_transformations + k*init_trans_stride + 3*translation_search_grid_size + 4*r,&rotation[0]);
			}
			__syncthreads();
			rotate(node_coords_target_r,node_coords_target,&rotation[0]);
			t = 0;
			while(t < translation_search_grid_size){
				if (tid == 0){
					translation = {curr[3*t], curr[3*t + 1], curr[3*t + 2]};
					v_ave = d_v_ave[k*translation_search_grid_size*rotation_search_grid_size + r*translation_search_grid_size*r +  t];
				}
				__syncthreads();
				node_coords_target_rt = node_coords_target_r + translation;
				v_m_v_ave = tex3D<float>(d_density_target, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z)-v_ave;
				concat1[tid] = node_weight*v_m_v_ave*v_m_v_ave;
				concat2[tid] = node_weight*v_m_v_ave*(tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z) - u_ave);
				__syncthreads();
				float f = tex3D<float>(d_density_target, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z); 
				//if (r == 0 && t==0 && tid == 1){
				//	printf("%f\n",v_ave);
				//}
				if (tid < 256)  { concat1[tid] += concat1[tid + 256];  concat2[tid] += concat2[tid + 256];} __syncthreads();
				if (tid < 128)  { concat1[tid] += concat1[tid + 128];  concat2[tid] += concat2[tid + 128];} __syncthreads();
				if (tid < 64)   { concat1[tid] += concat1[tid + 64];   concat2[tid] += concat2[tid + 64];} __syncthreads();
				if (tid < 32)   { concat1[tid] += concat1[tid + 32];   concat2[tid] += concat2[tid + 32];} __syncthreads();
				if (tid < 16)   { concat1[tid] += concat1[tid + 16];   concat2[tid] += concat2[tid + 16];} __syncthreads();
				if (tid < 8)    { concat1[tid] += concat1[tid + 8];    concat2[tid] += concat2[tid + 8];} __syncthreads();
				if (tid < 4)    { concat1[tid] += concat1[tid + 4];    concat2[tid] += concat2[tid + 4];} __syncthreads();
				if (tid < 2)    { concat1[tid] += concat1[tid + 2];    concat2[tid] += concat2[tid + 2];} __syncthreads();
				if (tid < 1)    { concat1[tid] += concat1[tid + 1];    concat2[tid] += concat2[tid + 1];} __syncthreads();
				if (tid==0){
					atomicAdd(&d_v_m_v_ave[k*translation_search_grid_size*rotation_search_grid_size + r*translation_search_grid_size +  t], concat1[0]);
					atomicAdd(&d_u_t_v[k*translation_search_grid_size*rotation_search_grid_size + r*translation_search_grid_size +  t], concat2[0]);
				}
				t++;
			}
			r++;
		}
		k++;
	}
}

__global__
void cam_score(int translation_search_grid_size, int rotation_search_grid_size, int init_transformations, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v, float * d_u_norm, float * d_cam){
	float sqrt_u_norm = sqrtf(d_u_norm[0]);
	int tid = blockDim.x*blockIdx.x + threadIdx.x;
	int grid_size = gridDim.x*blockDim.x;
	while (tid < rotation_search_grid_size*translation_search_grid_size*init_transformations){
        	if (d_v_m_v_ave[tid] != 0.0f){
			d_cam[tid] = d_u_t_v[tid]/(sqrt_u_norm*sqrtf(d_v_m_v_ave[tid]));
		} else {
			d_cam[tid] = 0.0f;
		}
		tid += grid_size;
	}
}


__global__
void u_ave_scan(cudaTextureObject_t d_density_query, uint3 * d_query_pdim, uint3 * d_cheby_node_dim, float * d_cheby_nodes, float * d_cheby_weights,
		float * d_u_ave){
	__shared__ float concat[512]; //u_ave
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;
	float3 node_coords_query = {d_cheby_nodes[blockDim.x*blockIdx.x + threadIdx.x] + 0.5f*((float) d_query_pdim->x),
				    d_cheby_nodes[blockDim.y*blockIdx.y + threadIdx.y + d_cheby_node_dim->x] + 0.5f*((float) d_query_pdim->y),
				    d_cheby_nodes[blockDim.z*blockIdx.z + threadIdx.z + d_cheby_node_dim->x + d_cheby_node_dim->y] + 0.5f*((float) d_query_pdim->z)};
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
			    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     			    *d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];
	float u = tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z);
        //printf("%i,%i,%i %03d %.10e %.10e %.10e | %.10e | %.10e\n",blockDim.x*blockIdx.x + threadIdx.x,blockDim.y*blockIdx.y + threadIdx.y + d_cheby_node_dim->x,blockDim.z*blockIdx.z + threadIdx.z + d_cheby_node_dim->x + d_cheby_node_dim->y,tid,node_coords_query.x,node_coords_query.y,node_coords_query.z,node_weight,u);
	concat[tid] = node_weight*tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z);
	__syncthreads();
	//if (concat[tid] > 0.0f && blockIdx.x == 2 && blockIdx.y == 2 && blockIdx.z == 2 && threadIdx.x == 0)
	//	printf("%f %f %f: %.10e\n",node_coords_query.x,node_coords_query.y,node_coords_query.z,concat[tid]);
	if (tid < 256) {concat[tid] += concat[tid + 256]; } __syncthreads();
	if (tid < 128) {concat[tid] += concat[tid + 128]; } __syncthreads();
	if (tid <  64) {concat[tid] += concat[tid +  64]; } __syncthreads();
	if (tid <  32) {concat[tid] += concat[tid +  32]; } __syncthreads();
	if (tid <  16) {concat[tid] += concat[tid +  16]; } __syncthreads();
	if (tid <   8) {concat[tid] += concat[tid +  8];  } __syncthreads();
	if (tid <   4) {concat[tid] += concat[tid +  4];  } __syncthreads();
	if (tid <   2) {concat[tid] += concat[tid +  2];  } __syncthreads();
	if (tid <   1) {concat[tid] += concat[tid +  1];  } __syncthreads();
	if (tid==0){
		 atomicAdd(&d_u_ave[0], concat[0]);
	}
}

__global__
void generate_transformations(float * d_transformations, float d, int k, int M, int n_total, int trans_S, int rot_S, int * d_converging){
	extern __shared__ float3 shifts[];
	float3 * trans_shift_directions = &shifts[0];
	int translation_search_grid_size = ipow(2*trans_S,3) + 1;
	int rotation_search_grid_size = ipow(2*rot_S,3) + 1;
	float3 * rot_shift_directions = &trans_shift_directions[0] + translation_search_grid_size;	
	int bid = blockIdx.x;
	int tid;
	float3 shift;
	int3 grid_point;
	float3 init_transl;
	float4 init_rot;
	float * current;
	float * current_rot;
	float3 unit_transl = {d,d,d};
	int init_trans_stride = 3*translation_search_grid_size + 4*rotation_search_grid_size;
	unit_transl *= powf(0.5,(float)(k+trans_S));
	float3 unit_rot = {2.0f,2.0f,2.0f};
	unit_rot *= powf(0.5,(float)(M + k + rot_S - 1));
	int trans_D = 2*trans_S;
	int rot_D = 2*rot_S;	
	//too hard coded. should be configured from the outside for different search grids
	//float3 shift_directions[9] = {{0.0f,0.0f,0.0f},{-1.0f,-1.0f,-1.0f},{1.0f,-1.0f,-1.0f},{-1.0f,1.0f,-1.0f},{-1.0f,-1.0f,1.0f},
	//			      {-1.0f,1.0f,1.0f},{1.0f,-1.0f,1.0f},{1.0f,1.0f,-1.0f},{1.0f,1.0f,1.0f}};
	
	tid = threadIdx.x;
	while (tid < translation_search_grid_size){
		grid_point.z = tid/(trans_D*trans_D);
		grid_point.y = (tid - grid_point.z*trans_D*trans_D)/trans_D;
		grid_point.x = tid - grid_point.z*trans_D*trans_D - grid_point.y*trans_D;
		trans_shift_directions[tid+1] = {(float) -trans_S + 2*grid_point.x, (float) -trans_S + 2*grid_point.y, (float) -trans_S + 2*grid_point.z};
		tid += blockDim.x;
	}
	if (threadIdx.x == 0)
		trans_shift_directions[0] = {0.0f,0.0f,0.0f};
	__syncthreads();
	tid = threadIdx.x;
	while (tid < rotation_search_grid_size){
		grid_point.z = tid/(rot_D*rot_D);
		grid_point.y = (tid - grid_point.z*rot_D*rot_D)/rot_D;
		grid_point.x = tid - grid_point.z*rot_D*rot_D - grid_point.y*rot_D;
		rot_shift_directions[tid+1] = {(float) -rot_S + 2*grid_point.x, (float) -rot_S + 2*grid_point.y, (float) -rot_S + 2*grid_point.z};
		tid += blockDim.x;
	}
	if (threadIdx.x == 0)
		trans_shift_directions[0] = {0.0f,0.0f,0.0f};
	__syncthreads();
	while (bid < n_total){
		if (d_converging[bid]) {
			bid += gridDim.x;
			continue;
		}
		current = d_transformations + bid*init_trans_stride;
		current_rot = current + 3*translation_search_grid_size;
		init_transl = {current[0],current[1],current[2]};
		init_rot = {current_rot[0],current_rot[1],current_rot[2],current_rot[3]};
		tid = threadIdx.x;
		int rid;
		while (tid < translation_search_grid_size + rotation_search_grid_size){
			//translations
			if (tid < translation_search_grid_size){
				shift = unit_transl*trans_shift_directions[tid];
				current[3*tid    ] = init_transl.x + shift.x;
				current[3*tid + 1] = init_transl.y + shift.y;
				current[3*tid + 2] = init_transl.z + shift.z;
			//rotations	
			} else {
				rid = tid - translation_search_grid_size;
				shift = unit_rot*rot_shift_directions[rid];
				// p0 = 1	
				if (init_rot.x == 1.0f){
					current_rot[4*rid    ] = 1.0f;
					current_rot[4*rid + 1] = init_rot.y + shift.x;
					current_rot[4*rid + 2] = init_rot.z + shift.y;
					current_rot[4*rid + 3] = init_rot.w + shift.z;
				}
				// p1 = 1  
				else if (init_rot.y == 1.0f){
					current_rot[4*rid    ] = init_rot.x + shift.x;
					current_rot[4*rid + 1] = 1.0f;
					current_rot[4*rid + 2] = init_rot.z + shift.y;
					current_rot[4*rid + 3] = init_rot.w + shift.z;
					
				}
				// p2 = 1
				else if (init_rot.z == 1.0f){
					current_rot[4*rid    ] = init_rot.x + shift.x;
					current_rot[4*rid + 1] = init_rot.y + shift.y;
					current_rot[4*rid + 2] = 1.0f;
					current_rot[4*rid + 3] = init_rot.w + shift.z;
				}
				// p3 = 1
				 else if (init_rot.w == 1.0f){
					current_rot[4*rid    ] = init_rot.x + shift.x;
					current_rot[4*rid + 1] = init_rot.y + shift.y;
					current_rot[4*rid + 2] = init_rot.z + shift.z;
					current_rot[4*rid + 3] = 1.0f;
				}
			}
			tid += blockDim.x;
		}
		bid += gridDim.x;
	}	
}

template <uint BLOCKSIZE>
__global__
void max_score_index(float * d_scores, float * d_max_scores, int * d_indeces, int translation_search_grid_size, int rotation_search_grid_size, int * d_converging){
	__shared__ float s_scores[BLOCKSIZE];
	__shared__ int s_indeces[BLOCKSIZE];
	if (d_converging[blockIdx.x])
		return;
	int tid = threadIdx.x;
	//initialize
	while (tid < BLOCKSIZE){
		s_scores[tid] = -FLT_MAX;
		tid += blockDim.x;
	}
	__syncthreads();
	tid = threadIdx.x;
	float * d_curr_scores = d_scores + blockIdx.x*translation_search_grid_size*rotation_search_grid_size; 
	float current;
	while(tid < translation_search_grid_size*rotation_search_grid_size){
		current = d_curr_scores[tid];	
		if (current > s_scores[threadIdx.x]){
			s_scores[threadIdx.x] = current;
			s_indeces[threadIdx.x] = tid;		
		}
		tid += blockDim.x;
	}
	__syncthreads();
	//reduction
        if (BLOCKSIZE == 1024){
		if (threadIdx.x < 512){ 
			current = s_scores[threadIdx.x + 512];	
			if (current > s_scores[threadIdx.x]){
				s_scores[threadIdx.x] = current;
				s_indeces[threadIdx.x] = s_indeces[threadIdx.x + 512];		
			}
		} __syncthreads();
	}
        if (BLOCKSIZE >= 512){
		if (threadIdx.x < 256){ 
			current = s_scores[threadIdx.x + 256];	
			if (current > s_scores[threadIdx.x]){
				s_scores[threadIdx.x] = current;
				s_indeces[threadIdx.x] = s_indeces[threadIdx.x + 256];		
			}
		} __syncthreads();
	}
        if (BLOCKSIZE >= 256){
		if (threadIdx.x < 128){ 
			current = s_scores[threadIdx.x + 128];	
			if (current > s_scores[threadIdx.x]){
				s_scores[threadIdx.x] = current;
				s_indeces[threadIdx.x] = s_indeces[threadIdx.x + 128];		
			}
		} __syncthreads();
	}
        if (BLOCKSIZE >= 128){
		if (threadIdx.x < 64){ 
			current = s_scores[threadIdx.x + 64];	
			if (current > s_scores[threadIdx.x]){
				s_scores[threadIdx.x] = current;
				s_indeces[threadIdx.x] = s_indeces[threadIdx.x + 64];		
			}
		} __syncthreads();
	}
        if (BLOCKSIZE >= 64){
		if (threadIdx.x < 32){ 
			current = s_scores[threadIdx.x + 32];	
			if (current > s_scores[threadIdx.x]){
				s_scores[threadIdx.x] = current;
				s_indeces[threadIdx.x] = s_indeces[threadIdx.x + 32];		
			}
		} __syncthreads();
	}
        if (BLOCKSIZE >= 32){
		if (threadIdx.x < 16){ 
			current = s_scores[threadIdx.x + 16];	
			if (current > s_scores[threadIdx.x]){
				s_scores[threadIdx.x] = current;
				s_indeces[threadIdx.x] = s_indeces[threadIdx.x + 16];		
			}
		} __syncthreads();
	}
        if (BLOCKSIZE >= 16){
		if (threadIdx.x < 8){ 
			current = s_scores[threadIdx.x + 8];	
			if (current > s_scores[threadIdx.x]){
				s_scores[threadIdx.x] = current;
				s_indeces[threadIdx.x] = s_indeces[threadIdx.x + 8];		
			}
		} __syncthreads();
	}
        if (BLOCKSIZE >= 8){
		if (threadIdx.x < 4){ 
			current = s_scores[threadIdx.x + 4];	
			if (current > s_scores[threadIdx.x]){
				s_scores[threadIdx.x] = current;
				s_indeces[threadIdx.x] = s_indeces[threadIdx.x + 4];		
			}
		} __syncthreads();
	}
        if (BLOCKSIZE >= 4){
		if (threadIdx.x < 2){ 
			current = s_scores[threadIdx.x + 2];	
			if (current > s_scores[threadIdx.x]){
				s_scores[threadIdx.x] = current;
				s_indeces[threadIdx.x] = s_indeces[threadIdx.x + 2];		
			}
		} __syncthreads();
	}
	current = s_scores[threadIdx.x + 1];	
	if (current > s_scores[threadIdx.x]){
		s_scores[threadIdx.x] = current;
		s_indeces[threadIdx.x] = s_indeces[threadIdx.x + 1];		
	} __syncthreads();

        if (threadIdx.x==0){
                d_max_scores[blockIdx.x] = s_scores[0];
		d_indeces[blockIdx.x] = s_indeces[0];
		if (blockIdx.x == 822){
			printf("");
		}
        }

}

__global__
void transformations_and_convergence(float * d_transformations, int * d_indeces, float * d_new_scores, float * d_old_scores, float eps, int * d_converging, int init_transformations, int translation_search_grid_size, int rotation_search_grid_size){
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	int grid_size = blockDim.x*gridDim.x;
	int t;
	int r;
	float * curr;
	float * curr_rot;
	int search_grid_stride = 3*translation_search_grid_size + 4*rotation_search_grid_size;
	while (tid < init_transformations){
		//copy best transformations to T0
		if (!d_converging[tid]){
			r = d_indeces[tid]/translation_search_grid_size;
			t = d_indeces[tid] - r*translation_search_grid_size; 
			curr = d_transformations + tid*search_grid_stride;
			curr_rot = curr + 3*translation_search_grid_size;
			curr[0] = curr[3*t];
			curr[1] = curr[3*t + 1];
			curr[2] = curr[3*t + 2];
			curr_rot[0] = curr_rot[4*r];
			curr_rot[1] = curr_rot[4*r + 1];
			curr_rot[2] = curr_rot[4*r + 2];
			curr_rot[3] = curr_rot[4*r + 3];
		
		}
		//assess convergence
		if (fabsf(d_old_scores[tid] - d_new_scores[tid]) < eps){
			d_converging[tid] = 1;
		}
		else {
			d_old_scores[tid] = d_new_scores[tid];
		}
		tid += grid_size;
	}	
}

__global__ 
void descend_kernel(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, uint3 * d_query_pdim, uint3 * d_cheby_node_dim, float * d_cheby_nodes, float * d_cheby_weights, float * d_transformations, int trans_S, int rot_S, int init_transformations, float * d_cam, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v, float * d_u_ave, float * d_u_norm, float * d_scores, int * d_indeces, int * d_converging, int * d_all_convergent, float d, float eps, int M)
{
	int translation_search_grid_size = ipow(2*trans_S,3) + 1;
	int rotation_search_grid_size = ipow(2*rot_S,3) + 1;
	size_t gen_trans_shared_mem = sizeof(float3)*(translation_search_grid_size + rotation_search_grid_size);
	int tid = threadIdx.x;
	d_all_convergent[0] = 0;
	int k = 1;
	float * d_new_scores = d_scores;
	float * d_old_scores = d_scores + init_transformations;
 	if(tid == 0){
		initValue<float><<<128,512>>>(d_old_scores,init_transformations,-FLT_MAX);
		initValue<int><<<128,512>>>(d_converging,init_transformations,0);
 	}
 	__syncthreads();
 	if(tid == 0)
 		cudaDeviceSynchronize();
 	__syncthreads();
	//calc kernel sizes for scoring kernels
	uint3 block_dim = {8,8,8};
	uint3 grid_dim = {(d_cheby_node_dim->x%block_dim.x == 0) ? d_cheby_node_dim->x/block_dim.x : d_cheby_node_dim->x/block_dim.x, 
			(d_cheby_node_dim->y%block_dim.y == 0) ? d_cheby_node_dim->y/block_dim.y : d_cheby_node_dim->y/block_dim.y, 
			(d_cheby_node_dim->z%block_dim.z == 0) ? d_cheby_node_dim->z/block_dim.z : d_cheby_node_dim->z/block_dim.z};
	while (!d_all_convergent[0]){
		//init working mem, no synchronization required, d_cam is set, not added to.
		if(tid == 0){
			initValue<float><<<128,512>>>(d_v_ave,init_transformations*translation_search_grid_size*rotation_search_grid_size,0.0f);
			initValue<float><<<128,512>>>(d_v_m_v_ave,init_transformations*translation_search_grid_size*rotation_search_grid_size,0.0f);
			initValue<float><<<128,512>>>(d_u_t_v,init_transformations*translation_search_grid_size*rotation_search_grid_size,0.0f);
		}
		__syncthreads();
  		if(tid == 0)
    			cudaDeviceSynchronize();
  		__syncthreads();
		
		//update transforms
		if(tid == 0)
			//void generate_transformations(float * d_transformations, float d, int k, int M, int n_total, int translation_search_grid_size, int rotation_search_grid_size, int * d_converging){
			generate_transformations<<<init_transformations/512 + 1,512,gen_trans_shared_mem>>>(d_transformations, d, k, M, init_transformations, trans_S, rot_S, d_converging);
		__syncthreads();
  		if(tid == 0)
    			cudaDeviceSynchronize();
  		__syncthreads();
		//calculate score according to transforms
		//v_ave
		//u_t_v
		//cam
		//void v_ave(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, uint3 * d_query_pdim,uint3 * d_cheby_node_dim, float * d_cheby_nodes, float * d_cheby_weights, float * d_transformations, int translation_search_grid_size, int rotation_search_grid_size, int init_transformations, float * d_v_ave, int * d_converging){
  		if(tid == 0)
			v_ave<<<grid_dim,block_dim>>>(d_density_target, d_density_query, d_query_pdim, d_cheby_node_dim, d_cheby_nodes, d_cheby_weights, d_transformations, translation_search_grid_size, rotation_search_grid_size, init_transformations, d_v_ave, d_converging);
		__syncthreads();
  		if(tid == 0)
    			cudaDeviceSynchronize();
  		__syncthreads();
		//void u_t_v(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, uint3 * d_query_pdim,uint3 * d_cheby_node_dim, float * d_cheby_nodes, float * d_cheby_weights, float3 * d_transformations, int translation_search_grid_size, int rotation_search_grid_size, int init_transformations, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v,float * d_u_ave){
  		if(tid == 0)
			u_t_v<<<grid_dim,block_dim>>>(d_density_target,d_density_query,d_query_pdim,d_cheby_node_dim,d_cheby_nodes,d_cheby_weights,d_transformations,translation_search_grid_size,rotation_search_grid_size,init_transformations,d_v_ave,d_v_m_v_ave,d_u_t_v,d_u_ave,d_converging);
		__syncthreads();
  		if(tid == 0)
    			cudaDeviceSynchronize();
  		__syncthreads();
		//void cam_score(int rotation_search_grid_size, int translation_search_grid_size, int init_transformations, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v, float * d_u_norm, float * d_cam){
  		if(tid == 0)
			cam_score<<<(rotation_search_grid_size*translation_search_grid_size*init_transformations)/512 + 1,512>>>(rotation_search_grid_size,translation_search_grid_size, init_transformations, d_v_ave, d_v_m_v_ave, d_u_t_v, d_u_norm, d_cam);
		__syncthreads();
  		if(tid == 0)
    			cudaDeviceSynchronize();
  		__syncthreads();
		//template <uint BLOCKSIZE>
		//__global__
		//void max_score_index(float * d_scores, float * d_max_scores, int * d_indeces, int translation_search_grid_size, int rotation_search_grid_size, int * d_converging){
  		if(tid == 0)
			max_score_index<512><<<init_transformations,256>>>(d_cam, d_new_scores, d_indeces, translation_search_grid_size, rotation_search_grid_size, d_converging);
		__syncthreads();
  		if(tid == 0)
    			cudaDeviceSynchronize();
  		__syncthreads();
		//__global__
		//void transformations_and_convergence(float3 * d_transformations, int * d_indeces, float * d_new_scores, float * d_old_scores, float eps, bool * d_converging, int init_transformations, int translation_search_grid_size){
  		if(tid == 0)
			transformations_and_convergence<<<init_transformations/512 + 1,512>>>(d_transformations, d_indeces, d_new_scores, d_old_scores, eps, d_converging, init_transformations, translation_search_grid_size, rotation_search_grid_size);		
		__syncthreads();
  		if(tid == 0)
    			cudaDeviceSynchronize();
  		__syncthreads();
		//__global__
		//void and_kernel(int * d_bools, size_t vol, int * d_result){
  		if(tid == 0){
			d_all_convergent[0] = 1;
			and_kernel<<<init_transformations/256 + 1,256>>>(d_converging,init_transformations,d_all_convergent);
		}
		__syncthreads();
  		if(tid == 0)
    			cudaDeviceSynchronize();
  		__syncthreads();
		if(tid == 0) printf("Completed %i\n",k);
		k++;
	}
	
}
__global__
void u_norm_scan(cudaTextureObject_t d_density_query, uint3 * d_query_pdim, float * d_cheby_nodes, float * d_cheby_weights,
		uint3 * d_cheby_node_dim, float * d_u_ave, float * d_u_norm){
	__shared__ float concat[512]; //u_ave
	__shared__ float u_ave;
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;
	if (tid == 0)
		u_ave = d_u_ave[0];
	__syncthreads();
	float3 node_coords_query = {d_cheby_nodes[blockDim.x*blockIdx.x + threadIdx.x] + 0.5f*((float) d_query_pdim->x),
				    d_cheby_nodes[blockDim.y*blockIdx.y + threadIdx.y + d_cheby_node_dim->x] + 0.5f*((float) d_query_pdim->y),
				    d_cheby_nodes[blockDim.z*blockIdx.z + threadIdx.z + d_cheby_node_dim->x + d_cheby_node_dim->y] + 0.5f*((float) d_query_pdim->z)};
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
			    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     			    *d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];
	float u = tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z);
	concat[tid] = node_weight*(u-u_ave)*(u-u_ave);
	__syncthreads();
	if (tid < 256) {concat[tid] += concat[tid + 256];} __syncthreads();
	if (tid < 128) {concat[tid] += concat[tid + 128];} __syncthreads();
	if (tid <  64) {concat[tid] += concat[tid + 64]; } __syncthreads();
	if (tid <  32) {concat[tid] += concat[tid + 32]; } __syncthreads();
	if (tid <  16) {concat[tid] += concat[tid + 16]; } __syncthreads();
	if (tid <   8) {concat[tid] += concat[tid + 8];  } __syncthreads();
	if (tid <   4) {concat[tid] += concat[tid + 4];  } __syncthreads();
	if (tid <   2) {concat[tid] += concat[tid + 2];  } __syncthreads();
	if (tid <   1) {concat[tid] += concat[tid + 1];  } __syncthreads();
	if (tid==0){
		atomicAdd(&d_u_norm[0], concat[0]);
	}
}


void CamScoreDescend::descend(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes, size_t *const& max_transformations_per_gpu, const int & gpu_num, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	CamScoreDescend_Register * cam_score_descend_instance = &_instance_registry[gpu_index];
	Density_Register * density_instance_target = &target->_instance_registry[gpu_index];
	Density_Register * density_instance_query = &query->_instance_registry[gpu_index];
	ChebychevNodes_Register * scan_nodes_instance = &scan_nodes->_instance_registry[gpu_index];

	//prepare kernel parameters, calculate once-per-search parameters
	cam_score_descend_instance->h_u_ave[0] = 0.0f;
	CudaSafeCall(cudaMemcpy(cam_score_descend_instance->d_u_ave,cam_score_descend_instance->h_u_ave,sizeof(*cam_score_descend_instance->d_u_ave),cudaMemcpyHostToDevice));
	dim3 cheby_dim = scan_nodes_instance->h_node_dim[0];
	const dim3 block_dim = {8,8,8};
	dim3 grid_dim = {cheby_dim.x/block_dim.x,cheby_dim.y/block_dim.y,cheby_dim.z/block_dim.z}; //number of blocks
	
	//the normalized coordinates of texture memory refer to the pixel dims, therefore the relative size of
	//two volumes is characterized by the ratio of pixel dims.
	assert(abs(density_instance_query->h_pixel_size[0] - density_instance_target->h_pixel_size[0]) < 0.0001f);
	float h_translation_vol = (float) vol((density_instance_target->h_pixel_dim[0] - density_instance_query->h_pixel_dim[0]));
	//	__global__
	//	void initZero(float * d_target, size_t vol){

	if (density_instance_target->textureObj == nullptr)
		target->createTexture(gpu_index);
	if (density_instance_query->textureObj == nullptr )
		query->createTexture(gpu_index);

	//independent of sweeps, need for cam score calculation. do beforehand.
	u_ave_scan<<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],density_instance_query->d_pixel_dim,scan_nodes_instance->d_node_dim,scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,
	cam_score_descend_instance->d_u_ave);
	CudaCheckError();
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(cam_score_descend_instance->h_u_ave,cam_score_descend_instance->d_u_ave,sizeof(*cam_score_descend_instance->d_u_ave),cudaMemcpyDeviceToHost));
	cam_score_descend_instance->h_u_norm[0] = 0.0f;
	CudaSafeCall(cudaMalloc((void**)&cam_score_descend_instance->d_u_norm,sizeof(*cam_score_descend_instance->d_u_norm)));
	CudaSafeCall(cudaMemcpy(cam_score_descend_instance->d_u_norm,cam_score_descend_instance->h_u_norm,sizeof(*cam_score_descend_instance->d_u_norm),cudaMemcpyHostToDevice));

	//__global__
	//void u_norm_scan(cudaTextureObject_t d_density_query, uint3 * d_query_pdim, float * d_cheby_nodes, float * d_cheby_weights,
        //uint3 * d_cheby_node_dim, float * d_u_ave, float * d_u_norm)
	u_norm_scan<<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],density_instance_query->d_pixel_dim,scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,scan_nodes_instance->d_node_dim,
			cam_score_descend_instance->d_u_ave,cam_score_descend_instance->d_u_norm);
	CudaCheckError();
	cudaDeviceSynchronize();

	size_t total_transformations = CamScoreDescend::initTransformationVolume(search_volume, d, M);	
	std::vector<size_t> transformations_per_gpu(gpu_num);
	for (int g=0; g < gpu_num; g++)transformations_per_gpu[g] = std::min<size_t>(max_transformations_per_gpu[gpu_index],total_transformations/gpu_num);	
	size_t global_stride = 0;
	size_t initial_offset = 0;
	size_t local_transformation_volume = transformations_per_gpu[gpu_index];
	for (int g = 0; g < gpu_num; g++) global_stride += transformations_per_gpu[g];
        for (int s = 0; s < gpu_index; s++) initial_offset += transformations_per_gpu[s];
                
	size_t current_offset = initial_offset;
	//size_t current_end;
	int3 n = {(int) (search_volume.second.x/d),(int) (search_volume.second.y/d),(int) (search_volume.second.z/d)};
	float3 offset = search_volume.first;
	//4 cubes, half a tesseract
	int N = ipow(2,M-1);
	float d_R = 1.0f/N;
	int total_R = 4*N*N*N;	
	int total_T = n.x*n.y*n.z;
	int r_start;
	int r_end;
	int t_start;
	int t_end;
	float3 translation;
	int3 t_index;
	int4 r_index;
	float4 rotation;
	int init_stride = 3*translation_search_grid_size + 4*rotation_search_grid_size;
	int init_count;
	float * curr;
	int actual_transformation_volume = std::min<int>(local_transformation_volume,total_T*total_R);
	while (current_offset < total_T*total_R){
		r_start = current_offset/total_T;
		r_end = (current_offset + actual_transformation_volume)/total_T;
		t_start = current_offset - r_start*total_T;
		t_end = current_offset + actual_transformation_volume - (r_end-1)*total_T;
		init_count = 0;
		for (int r = r_start; r < r_end; r++){
			r_index.x = r/(N*N*N);
			r_index.y = (r - r_index.x*N*N*N)/(N*N);
			r_index.z = (r - r_index.x*N*N*N - r_index.y*N*N)/N;
			r_index.w = r - r_index.x*N*N*N - r_index.y*N*N - r_index.z*N;
			if  (r_index.x == 0){
				//cube P0
				rotation.x = 1.0f;
				rotation.y = -1.0f + (2*r_index.y + 1)*d_R;
				rotation.z = -1.0f + (2*r_index.z + 1)*d_R;
				rotation.w = -1.0f + (2*r_index.w + 1)*d_R;
			}
			else if (r_index.x == 1){
				//cube P1
				rotation.x = -1.0f + (2*r_index.y + 1)*d_R;
				rotation.y = 1.0f;
				rotation.z = -1.0f + (2*r_index.z + 1)*d_R;
				rotation.w = -1.0f + (2*r_index.w + 1)*d_R;
			}
			else if (r_index.x == 2){
				//cube P2
				rotation.x = -1.0f + (2*r_index.y + 1)*d_R;
				rotation.y = -1.0f + (2*r_index.z + 1)*d_R;
				rotation.z = 1.0f;
				rotation.w = -1.0f + (2*r_index.w + 1)*d_R;
			}
			else if (r_index.x == 3){
				//cube P3
				rotation.x = -1.0f + (2*r_index.y + 1)*d_R;
				rotation.y = -1.0f + (2*r_index.z + 1)*d_R;
				rotation.z = -1.0f + (2*r_index.w + 1)*d_R;
				rotation.w = 1.0f;
			}
			for (int t = t_start; t < t_end; t++){
				t_index.z = t/(n.x*n.y);
				t_index.y = (t - t_index.z*n.x*n.y)/n.x;
				t_index.x = t - t_index.y*n.x - t_index.z*n.x*n.y;
				translation.x = offset.x + (t_index.x + 0.5f)*d;
				translation.y = offset.y + (t_index.y + 0.5f)*d;
				translation.z = offset.z + (t_index.z + 0.5f)*d;
				curr = cam_score_descend_instance->h_transformations + init_stride*init_count;
				curr[0] = translation.x;
				curr[1] = translation.y;
				curr[2] = translation.z;
				curr[3*translation_search_grid_size] = rotation.x;
				curr[3*translation_search_grid_size + 1] = rotation.y;
				curr[3*translation_search_grid_size + 2] = rotation.z;
				curr[3*translation_search_grid_size + 3] = rotation.w;
				init_count++;
			}
		}
		//test
		//cam_score_descend_instance->h_transformations[0] = 30.19018166;
		//cam_score_descend_instance->h_transformations[1] = 63.57256446;
		//cam_score_descend_instance->h_transformations[2] = 58.82614784;
		//cam_score_descend_instance->h_transformations[3] = 0.24624988;
		//cam_score_descend_instance->h_transformations[4] = 0.3617316;
		//cam_score_descend_instance->h_transformations[5] = 0.83874931;
		//cam_score_descend_instance->h_transformations[6] = -0.32405376;
		CudaSafeCall(cudaMemcpy(cam_score_descend_instance->d_transformations,cam_score_descend_instance->h_transformations,local_transformation_volume*rotation_search_grid_size*translation_search_grid_size*sizeof(*cam_score_descend_instance->d_transformations),cudaMemcpyHostToDevice));
		//best scores for from each search grid for each init transformation, 2* for one old and one new
		///__global__
		//void descend(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, uint3 * d_query_pdim, uint3 * d_cheby_node_dim, float * d_cheby_nodes, float * d_cheby_weights, float3 * d_transformations, int translation_search_grid_size, int init_transformations, float * d_cam, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v, float * d_u_ave, float * d_u_norm, float * d_scores, int * d_indeces, bool * d_converging, float d, float eps)
		descend_kernel<<<1,1>>>(density_instance_target->textureObj[0], density_instance_query->textureObj[0], density_instance_query->d_pixel_dim, scan_nodes_instance->d_node_dim, scan_nodes_instance->d_node_list, scan_nodes_instance->d_weights,
 cam_score_descend_instance->d_transformations, trans_S, rot_S, local_transformation_volume, cam_score_descend_instance->d_cam, cam_score_descend_instance->d_v_ave, cam_score_descend_instance->d_v_m_v_ave, cam_score_descend_instance->d_u_t_v, cam_score_descend_instance->d_u_ave, cam_score_descend_instance->d_u_norm, cam_score_descend_instance->d_scores, cam_score_descend_instance->d_indeces, cam_score_descend_instance->d_converging, cam_score_descend_instance->d_all_convergent, d, epsilon,M);
		cudaDeviceSynchronize();
		current_offset += global_stride;
	}
	CudaSafeCall(cudaMemcpy(cam_score_descend_instance->h_transformations,cam_score_descend_instance->d_transformations,local_transformation_volume*rotation_search_grid_size*translation_search_grid_size*sizeof(*cam_score_descend_instance->d_transformations),cudaMemcpyDeviceToHost));
	CudaSafeCall(cudaMemcpy(cam_score_descend_instance->h_scores,cam_score_descend_instance->d_scores,2*local_transformation_volume*sizeof(*cam_score_descend_instance->d_scores),cudaMemcpyDeviceToHost));
	std::ofstream out;
	std::string fname = std::string("descend_score_gpu_") + std::to_string(gpu_index) + std::string(".csv"); 
	out.open(fname.c_str());
	float * current_trans;
	for (int i = 0; i < local_transformation_volume; i++){
		current_trans = cam_score_descend_instance->h_transformations + i*(3*translation_search_grid_size + 4*rotation_search_grid_size);
		out << current_trans[0] << "," << current_trans[1] << "," << current_trans[2] << "," << current_trans[3*translation_search_grid_size] << "," << current_trans[3*translation_search_grid_size + 1] << "," << current_trans[3*translation_search_grid_size +2] << "," << current_trans[3*translation_search_grid_size +3] << "," << cam_score_descend_instance->h_scores[i] << std::endl;
	}
	out.close();   
}

size_t CamScoreDescend::initTransformationVolume(std::pair<float3,float3> search_volume, const float& d, const int& M){
	int3 n = {(int) (search_volume.second.x/d),(int) (search_volume.second.y/d),(int) (search_volume.second.z/d)};
	int N = ipow(2,M-1);
	int total_R = 4*N*N*N;	
	return (size_t) n.x*n.y*n.z*total_R;
}


void CamScoreDescend::createInstance(std::pair<float3,float3> search_volume, float d, float epsilon, int M, size_t *const& max_init_transform_vol_per_gpu, int const& trans_S, int const& rot_S, const int & gpu_index){
	//search nodes between {0.0f,0.0f,0.0f} - {search_volume.x,search_volume.y,search_volume.z}
	this->d = d;
	this->M = M;
	this->epsilon = epsilon;
	this->search_volume = search_volume;
	this->trans_S = trans_S;	
	this->rot_S = rot_S;	

	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		CamScoreDescend_Register instance;
		//body centric grid of a cuboid, rather specific ... might be subject to change
		translation_search_grid_size = ipow(2*trans_S,3) + 1;
		//corresponding to 4 cells of a tesseract, see CFF Karney 
		//"Quaternions in Molecular Modeling"
		rotation_search_grid_size = ipow(2*rot_S,3) + 1;
		
		size_t free_mem;
		size_t total_mem;
		cudaMemGetInfo( &free_mem, &total_mem );
		//FREE_MEM/(transformations + max_indeces + 2*score + converging + v_ave + v_m_v_ave + u_t_v)
		//Translations: one init, search_grid_trans + search_grid_rot, one quaterion and one R3 vector
		//Max_indeces: one per init
		//Score: one per init
		//Conveging: one per init
		//v_ave, v_m_v_ave, u_t_v: (1 + search_grid_trans*search_grid_rot)
		float float_vol = (0.8f*free_mem)/((translation_search_grid_size+rotation_search_grid_size)*(sizeof(float3) + sizeof(float4)) + sizeof(int) + 2*sizeof(float) + sizeof(int) + 3*translation_search_grid_size*rotation_search_grid_size*sizeof(float));
		max_init_transform_vol_per_gpu[gpu_index] = (size_t) float_vol;
		size_t init_vol = max_init_transform_vol_per_gpu[gpu_index];
		instance.h_transformations = (float *) malloc(init_vol*translation_search_grid_size*sizeof(*instance.h_transformations));
		CudaSafeCall(cudaMalloc((void **) &instance.d_transformations,init_vol*translation_search_grid_size*sizeof(*instance.d_transformations)));
		instance.h_scores = (float *) malloc(2*init_vol*sizeof(*instance.h_scores));
		CudaSafeCall(cudaMalloc((void **) &instance.d_scores,2*init_vol*sizeof(*instance.d_scores)));
		//converging, bool array for assessment of convergence 
		CudaSafeCall(cudaMalloc((void **) &instance.d_converging,init_vol*sizeof(*instance.d_converging)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_all_convergent,sizeof(*instance.d_all_convergent)));
		//indeces for remembering the max score and transform
		CudaSafeCall(cudaMalloc((void **) &instance.d_indeces,init_vol*sizeof(*instance.d_indeces)));

		//cam score related memory segments
		//u_norn and u_ave are two constants in cam score
		CudaSafeCall(cudaMalloc((void **) &instance.d_u_ave,sizeof(*instance.d_u_ave)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_u_norm,sizeof(*instance.d_u_norm)));
		instance.h_u_ave = (float *) malloc(sizeof(*instance.h_u_ave));
		instance.h_u_norm = (float *) malloc(sizeof(*instance.h_u_norm));
							

		CudaSafeCall(cudaMalloc((void **) &instance.d_v_ave,init_vol*translation_search_grid_size*sizeof(*instance.d_v_ave)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_v_m_v_ave,init_vol*translation_search_grid_size*sizeof(*instance.d_v_m_v_ave)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_u_t_v,init_vol*translation_search_grid_size*sizeof(*instance.d_u_t_v)));
		instance.d_cam = instance.d_v_ave;

		_instance_registry[gpu_index] = instance;
	}
}
