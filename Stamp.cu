/*
 * Stamp.cu
 *
 *  Created on: Mar 3, 2021
 *      Author: kkarius
 */

#include <Stamp.h>

void gaussian_topf_extension(float R, float p, const float & pixel_size,
	uint *& d_stamp_pixel_dim, uint *& h_stamp_pixel_dim){
	float d;
	float sigma = GAUSSIAN_KERNEL_RESOLUTION_CONVERSION_FACTOR * R;
	//0.3989422804014327 == 1/sqrt(2*pi)
	d = 2*sqrtf(-2*log(p)/log(2))*sigma;
	h_stamp_pixel_dim = (uint *) malloc(3*sizeof(*h_stamp_pixel_dim));
	cudaMalloc(&d_stamp_pixel_dim,3*sizeof(*h_stamp_pixel_dim));
	uint dim = (uint) (2*d/pixel_size);
	if (dim%2 == 0) dim++;
	h_stamp_pixel_dim[0] = dim;
	h_stamp_pixel_dim[1] = dim;
	h_stamp_pixel_dim[2] = dim;
	cudaMemcpy(d_stamp_pixel_dim,h_stamp_pixel_dim,3*sizeof(*h_stamp_pixel_dim),cudaMemcpyHostToDevice);
}

float gaussian_topf(float d,float R){
	float sigma = GAUSSIAN_KERNEL_RESOLUTION_CONVERSION_FACTOR * R;
	//0.3989422804014327 == 1/sqrt(2*pi)
	return 0.3989422804014327 / sigma * expf((-0.5f*d*d) / (sigma * sigma));
}

__host__
void create_centric_stamp(float *& d_stamp, int *& d_stamp_offsets, const float & pixel_size,
//	uint3 * const & h_pixel_dim,uint *& h_stamp_pixel_dim, size_t & stamp_vol,std::function <float (float)> kernel){
	uint *& h_stamp_pixel_dim, size_t *& stamp_vol,std::function <float (float)> kernel){
	uint3 mid_pixel = {(uint) h_stamp_pixel_dim[0]/2,(uint) h_stamp_pixel_dim[1]/2,(uint) h_stamp_pixel_dim[2]/2};
	float3 mid_coord = {((float) mid_pixel.x + 0.5f)*pixel_size,((float) mid_pixel.y + 0.5f)*pixel_size,
			    ((float) mid_pixel.z + 0.5f)*pixel_size};
	stamp_vol[0] = h_stamp_pixel_dim[0]*h_stamp_pixel_dim[1]*h_stamp_pixel_dim[2];
	float * h_stamp = (float *) malloc(stamp_vol[0]*sizeof(*h_stamp));
	int * h_stamp_offsets = (int *) malloc(stamp_vol[0]*sizeof(*h_stamp_offsets));
	cudaMalloc((void **) &d_stamp, stamp_vol[0]*sizeof(*d_stamp));
	cudaMalloc((void **) &d_stamp_offsets, stamp_vol[0]*sizeof(*d_stamp_offsets));
	float d;
	float3 pixel_coord;
	for (int i = 0; i < h_stamp_pixel_dim[0]; i++){
		for (int j = 0; j < h_stamp_pixel_dim[1]; j++){
			for (int k = 0; k < h_stamp_pixel_dim[2]; k++){
				pixel_coord = {((float) i + 0.5f)*pixel_size,((float) j + 0.5f)*pixel_size,((float) k + 0.5f)*pixel_size};
				d = length(mid_coord - pixel_coord);
				h_stamp[k*h_stamp_pixel_dim[0]*h_stamp_pixel_dim[1] + j*h_stamp_pixel_dim[0] + i] = kernel(d);
//				h_stamp_offsets[k*h_stamp_pixel_dim[0]*h_stamp_pixel_dim[1] + j*h_stamp_pixel_dim[0] + i] =(k-mid_pixel.z)*h_pixel_dim->x*h_pixel_dim->y + (j - mid_pixel.y)*h_pixel_dim->x + (i - mid_pixel.x);
			}
		}
	}
	cudaMemcpy(d_stamp,h_stamp,sizeof(*h_stamp)*stamp_vol[0],cudaMemcpyHostToDevice);
//	cudaMemcpy(d_stamp_offsets,h_stamp_offsets,sizeof(*h_stamp_offsets)*stamp_vol,cudaMemcpyHostToDevice);
}

void Stamp::populateOffsets(Density *const& density, int gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Stamp_Register * instance = &_instance_registry[gpu_index];
	Density_Register * density_instance = &density->_instance_registry[gpu_index];
	uint3 mid_pixel = {(uint) instance->h_stamp_pixel_dim[0]/2,(uint) instance->h_stamp_pixel_dim[1]/2,(uint) instance->h_stamp_pixel_dim[2]/2};
//	float3 pixel_coord;
	for (int i = 0; i < instance->h_stamp_pixel_dim[0]; i++){
		for (int j = 0; j < instance->h_stamp_pixel_dim[1]; j++){
			for (int k = 0; k < instance->h_stamp_pixel_dim[2]; k++){
				instance->h_stamp_offsets[k*instance->h_stamp_pixel_dim[0]*instance->h_stamp_pixel_dim[1] + j*instance->h_stamp_pixel_dim[0] + i] =(k-mid_pixel.z)*density_instance->h_pixel_dim->x*density_instance->h_pixel_dim->y + (j - mid_pixel.y)*density_instance->h_pixel_dim->x + (i - mid_pixel.x);
			}
		}
	}
	cudaMemcpy(instance->d_stamp_offsets,instance->h_stamp_offsets,sizeof(*instance->h_stamp_offsets)*instance->h_stamp_vol[0],cudaMemcpyHostToDevice);
}


void Stamp::freeInstance(const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Stamp_Register * instance = &_instance_registry[gpu_index];
	cudaFree(instance->d_stamp);
	cudaFree(instance->d_stamp_offsets);
	cudaFree(instance->d_stamp_pixel_dim);
	free(instance->h_stamp);
	free(instance->h_stamp_offsets);
	free(instance->h_stamp_pixel_dim);
	free(instance->h_stamp_vol);
	free(instance->h_pixel_size);
}

void Stamp::freeAllInstances(){
	for (auto const &instance: _instance_registry){
		cudaFree(instance.second.d_stamp);
		cudaFree(instance.second.d_stamp_offsets);
		cudaFree(instance.second.d_stamp_pixel_dim);
		free(instance.second.h_stamp);
		free(instance.second.h_stamp_offsets);
		free(instance.second.h_stamp_pixel_dim);
		free(instance.second.h_stamp_vol);
		free(instance.second.h_pixel_size);
	}
	_instance_registry.clear();
}

void Stamp::createInstance(const float & resolution, const float & pixel_size, const int &gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	//signifies cutoff value
	const float p = 0.1;
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		Stamp_Register instance;
		instance.h_stamp_pixel_dim = (uint * ) malloc(3*sizeof(*instance.h_stamp_pixel_dim));
		instance.h_stamp_vol = (size_t *) malloc(sizeof(*instance.h_stamp_vol));
		instance.h_pixel_size = (float *) malloc(sizeof(*instance.h_pixel_size));
		instance.h_pixel_size[0] = pixel_size;
		CudaSafeCall(cudaMalloc((void **)&instance.d_stamp_pixel_dim,3*sizeof(instance.d_stamp_pixel_dim)));
		gaussian_topf_extension(resolution, p, pixel_size, instance.d_stamp_pixel_dim, instance.h_stamp_pixel_dim);
		_instance_registry[gpu_index] = instance;
		std::function <float (float)> kernel = std::bind(gaussian_topf,std::placeholders::_1,resolution);

		//needs separate function
//		create_centric_stamp(instance.d_stamp,instance.d_stamp_offsets,pixel_size,instance.h_stamp_pixel_dim,instance.h_stamp_vol,kernel);
		uint3 mid_pixel = {instance.h_stamp_pixel_dim[0]/2,instance.h_stamp_pixel_dim[1]/2,instance.h_stamp_pixel_dim[2]/2};
		float3 mid_coord = {((float) mid_pixel.x + 0.5f)*pixel_size,((float) mid_pixel.y + 0.5f)*pixel_size,
				    ((float) mid_pixel.z + 0.5f)*pixel_size};
		instance.h_stamp_vol[0] = instance.h_stamp_pixel_dim[0]*instance.h_stamp_pixel_dim[1]*instance.h_stamp_pixel_dim[2];
		instance.h_stamp = (float *) malloc(instance.h_stamp_vol[0]*sizeof(*instance.h_stamp));
		instance.h_stamp_offsets = (int *) malloc(instance.h_stamp_vol[0]*sizeof(*instance.h_stamp_offsets));
		cudaMalloc((void **) &instance.d_stamp, instance.h_stamp_vol[0]*sizeof(*instance.d_stamp));
		cudaMalloc((void **) &instance.d_stamp_offsets, instance.h_stamp_vol[0]*sizeof(*instance.d_stamp_offsets));
		float d;
		float3 pixel_coord;
		for (int i = 0; i < instance.h_stamp_pixel_dim[0]; i++){
			for (int j = 0; j < instance.h_stamp_pixel_dim[1]; j++){
				for (int k = 0; k < instance.h_stamp_pixel_dim[2]; k++){
					pixel_coord = {((float) i + 0.5f)*pixel_size,((float) j + 0.5f)*pixel_size,((float) k + 0.5f)*pixel_size};
					d = length(mid_coord - pixel_coord);
					instance.h_stamp[k*instance.h_stamp_pixel_dim[0]*instance.h_stamp_pixel_dim[1] + j*instance.h_stamp_pixel_dim[0] + i] = kernel(d);
				}
			}
		}
		cudaMemcpy(instance.d_stamp,instance.h_stamp,sizeof(*instance.h_stamp)*instance.h_stamp_vol[0],cudaMemcpyHostToDevice);
		_instance_registry[gpu_index] = instance;
	}
}
