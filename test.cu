//#define BOOST_TEST_MODULE general_engine_test
//#include <boost/test/included/unit_test.hpp>
//#include <Density.h>
//#include <Labeler.h>
//#include <Kernels.h>
////#include <Sampler.h>
//#include <chrono>
//
//#include <thrust/device_vector.h>
//#include <thrust/replace.h>
//#include <thrust/iterator/counting_iterator.h>
//#include <thrust/iterator/transform_iterator.h>
//#include <thrust/sort.h>
//#include <thrust/device_ptr.h>
//#include <thrust/adjacent_difference.h>
//#include <thrust/transform_reduce.h>
//#include <thrust/functional.h>
//#include <cuda.h>
//
//#include <cub/device/device_radix_sort.cuh>
//
//#include <functional>
//#include <derived_atomic_functions.h>
//#include <cuda_util_include/cutil_math.h>
//#include <TransformationGrid.h>
//
//#include <fstream>
//#include <Engine.h>
//#include <thread>
#include <chrono>
//#include <NodeLoadPdb.h>
//#include <NodeMolecularDensity.h>
//#include <NodeDensityThreshold.h>
//#include <NodeLoadMrc.h>
//#include <ScoreCam.h>

//#include<AlignmentProtocol.h>
//#include<Engine.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <util.h>

#include <cstdio>



//texture<float, cudaTextureType3D, cudaReadModeElementType> tex; 

__global__ void testKernel(float * output,cudaTextureObject_t texobj,float width,float height,float depth)
{
    int tidx = threadIdx.x + blockIdx.x * blockDim.x;
    int tidy = threadIdx.y + blockIdx.y * blockDim.y;
    int tidz = threadIdx.z + blockIdx.z * blockDim.z;

    float x = float(tidx)+0.5f;
    float y = float(tidy)+0.5f;
    float z = float(tidz)+0.5f;
//    if (tidx < 5 && tidy < 5 && tidz < 5)
    if (tidx == 0 && tidy == 0 && tidz == 0){
		printf("%i %i %i --> %+.5e\n", tidx,tidy,tidz,tex3D<float>(texobj, 0.005, 0.005, 0.005));
		printf("%i %i %i --> %+.5e\n", tidx,tidy,tidz,tex3D<float>(texobj, 0.005, 0.015, 0.005));
		printf("%i %i %i --> %+.5e\n", tidx,tidy,tidz,tex3D<float>(texobj, 0.015, 0.005, 0.005));
		printf("%i %i %i --> %+.5e\n", tidx,tidy,tidz,tex3D<float>(texobj, 0.015, 0.015, 0.005));
		printf("%i %i %i --> %+.5e\n", tidx,tidy,tidz,tex3D<float>(texobj, 0.01, 0.01, 0.01));
		printf("\n");
    }
}

inline void gpuAssert(cudaError_t code, char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
int main(void)
{
    float * d_output, *h_output;
        
    uint3 pixel_dim = {100, 100, 100};
    uint width = sizeof(float) * pixel_dim.x;

    float * h_volumeMem;
    float * h_mrc_volume = (float *) malloc(vol(pixel_dim)*sizeof(float));

    float * h_volumeMem_r;
    float * h_mrc_volume_r = (float *) malloc(vol(pixel_dim)*sizeof(float));


    int volume = pixel_dim.x * pixel_dim.y * pixel_dim.z;
	int bytes = sizeof(float) * volume;
	std::fstream x_FileHandle;
	x_FileHandle.open("./base.mrc",	std::fstream::in | std::fstream::binary);
	//jump header
	x_FileHandle.seekg(1024, std::ios::beg);
	x_FileHandle.read((char*) h_mrc_volume, bytes);

	//reorder axis (x,y,z) --> (x,z,y)
	for (int z = 0; z < pixel_dim.z;++z){
		for (int y = 0; y < pixel_dim.y;++y){
			for (int x = 0; x < pixel_dim.x; ++x){
				h_mrc_volume_r[x + z*pixel_dim.x + y*pixel_dim.x*pixel_dim.z] = h_mrc_volume[x + y*pixel_dim.x + z*pixel_dim.x*pixel_dim.y];
			}
		}
	}


    cudaExtent volumeSizeBytes = make_cudaExtent(width, pixel_dim.y,pixel_dim.z);
    cudaPitchedPtr d_volumeMem; 
    gpuErrchk(cudaMalloc3D(&d_volumeMem, volumeSizeBytes));

    size_t size = d_volumeMem.pitch * pixel_dim.y * pixel_dim.z;
    h_volumeMem = (float *)malloc(size);
    for (int i=0; i < vol(pixel_dim); ++i)h_volumeMem[i] = 0.0;

    //init<float>((char *)h_volumeMem, d_volumeMem.pitch, SIZE_X, SIZE_Y, SIZE_Z);
    size_t pitch = d_volumeMem.pitch;
    size_t slicePitch = pitch * pixel_dim.y;
    int v = 0;
    for (int z = 0; z < pixel_dim.z; ++z) {
        char * slice = (char *) h_volumeMem + z * slicePitch;
        for (int y = 0; y < pixel_dim.y; ++y) {
            float * row = (float *)(slice + y * pitch);
            for (int x = 0; x < pixel_dim.x; ++x) {
                //row[x] = float(v++);
                row[x] = h_mrc_volume_r[ x + y*pixel_dim.x + z*pixel_dim.x*pixel_dim.z];
            }
        }
    }
    gpuErrchk(cudaMemcpy(d_volumeMem.ptr, h_volumeMem, size, cudaMemcpyHostToDevice));

    cudaArray * d_volumeArray;
    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();
    cudaExtent volumeSize = make_cudaExtent(pixel_dim.x, pixel_dim.z, pixel_dim.y);
    gpuErrchk( cudaMalloc3DArray(&d_volumeArray, &channelDesc, volumeSize) ); 

    cudaMemcpy3DParms copyParams = {0};
    copyParams.srcPtr = d_volumeMem;
    copyParams.dstArray = d_volumeArray;
    copyParams.extent = volumeSize;
    copyParams.kind = cudaMemcpyDeviceToDevice;
    gpuErrchk( cudaMemcpy3D(&copyParams) ); 

    cudaTextureObject_t textureObject = 0;
    cudaResourceDesc    texRes;
    memset(&texRes, 0, sizeof(cudaResourceDesc));
    texRes.resType = cudaResourceTypeArray;
    texRes.res.array.array  = d_volumeArray;
    cudaTextureDesc     texDescr;
    memset(&texDescr, 0, sizeof(cudaTextureDesc));
    texDescr.normalizedCoords = true;
    texDescr.filterMode = cudaFilterModeLinear;
    texDescr.addressMode[0] = cudaAddressModeBorder;   // clamp
    texDescr.addressMode[1] = cudaAddressModeBorder;
    texDescr.addressMode[2] = cudaAddressModeBorder;
    texDescr.readMode = cudaReadModeElementType;
	
    gpuErrchk(cudaCreateTextureObject(&textureObject, &texRes, &texDescr, NULL));
    cudaDeviceSynchronize();
//    cudaFree(d_volumeMem.ptr);
//    free(h_volumeMem);

    testKernel<<<4,dim3(8,8,8)>>>(d_output,textureObject,(float) pixel_dim.x, (float) pixel_dim.z,(float) pixel_dim.y);
    gpuErrchk(cudaPeekAtLastError());
    cudaDeviceSynchronize();

    return 0;
}


//typedef float  float;
//
//const size_t SIZE_X = 2;
//const size_t SIZE_Y = 3;
//const size_t SIZE_Z = 4;
//const size_t width = sizeof(float) * SIZE_X;
//
//__global__ void testKernel(float * output, cudaTextureObject_t textureObject, int dimx, int dimy, int dimz, int width, int height, int depth)
//{
//    int tidx = threadIdx.x + blockIdx.x * blockDim.x;
//    int tidy = threadIdx.y + blockIdx.y * blockDim.y;
//    int tidz = threadIdx.z + blockIdx.z * blockDim.z;
//
//    float x = float(tidx)+0.1f;
//    float y = float(tidy)+0.1f;
//    float z = float(tidz)+0.1f;
//
//    size_t oidx = tidx + tidy*dimx + tidz*dimx*dimy;
//    output[oidx] = tex3D<float>(textureObject, x/width, y/height, z/depth);
//    //output[oidx] = tex3D(textureObject, x/width, y/height, z/depth);
//}
//
//inline void gpuAssert(cudaError_t code, char *file, int line, bool abort=true)
//{
//   if (code != cudaSuccess)
//   {
//      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
//      if (abort) exit(code);
//   }
//}
//
//#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
//
//template<typename T>
//void init(char * devPtr, size_t pitch, int width, int height, int depth)
//{
//    size_t slicePitch = pitch * height;
//    int v = 0;
//    for (int z = 0; z < depth; ++z) {
//        char * slice = devPtr + z * slicePitch;
//        for (int y = 0; y < height; ++y) {
//            T * row = (T *)(slice + y * pitch);
//            for (int x = 0; x < width; ++x) {
//                row[x] = T(v++);
//            }
//        }
//    }
//}
//
//#include<algorithm>
//
//int main(void)
//{
//    cudaPitchedPtr d_volumeMem;
//    cudaTextureObject_t textureObject;
//    cudaExtent volumeSizeBytes = make_cudaExtent(width, SIZE_Y, SIZE_Z);
//    gpuErrchk(cudaMalloc3D(&d_volumeMem, volumeSizeBytes));
//    
//    //mrc_like_memory allocation
//    float * h_mrc_volume = (float *) malloc(SIZE_X*SIZE_Y*SIZE_Z*sizeof(float));
//    //pitch_memory allocation
//    size_t size = d_volumeMem.pitch * SIZE_Y * SIZE_Z;
//    char * h_pitch_volume = (char *) malloc(size);
//    std::fill(h_pitch_volume,h_pitch_volume+size,0);
//    //initialize
//    for (int i=0;i<SIZE_X*SIZE_Y*SIZE_Z;++i) h_mrc_volume[i] = i;
//    uint3 mrc_index;
//    uint3 pitched_index;
//    size_t mrc_linear;
//    size_t pitched_linear;
//    size_t pitch = d_volumeMem.pitch;
//    size_t slice_pitch = pitch*SIZE_Z;
//    char * slice = nullptr;
//    float * row = nullptr;
//    //role of z and y switched in pitched memory location calculation!
//    for (int k=0; k < SIZE_Z; ++k){
//        for (int j=0; j < SIZE_Y; ++j){
//	    slice = h_pitch_volume + j*slice_pitch;
//            row = (float *)(slice + k*pitch);
//	    for (int i = 0; i < SIZE_X; ++i){
//	    	//mrc_index = {i,j,k};
//		//pitched_index = {i,k,j};d
//		mrc_linear = i+j*SIZE_X+k*SIZE_X*SIZE_Y;
//		//printf("%u --> %u\n",mrc_linear,j*slice_pitch+k*pitch+i*sizeof(float));
//           	row[i] = h_mrc_volume[mrc_linear];
//	    }
//	}
//    }
//    
//    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();
//    //cuda Array
//    cudaArray *d_cuArr;
//    cudaMalloc3DArray(&d_cuArr, &channelDesc, volumeSizeBytes, 0);
//    cudaMemcpy3DParms copyParams = {0};
//    
//    //Array creation
//    copyParams.srcPtr   = make_cudaPitchedPtr(h_pitch_volume, SIZE_X*sizeof(float), SIZE_Y, SIZE_Z);
//    copyParams.dstArray = d_cuArr;
//    copyParams.extent   = make_cudaExtent(SIZE_X,SIZE_Y,SIZE_Z);
//    copyParams.kind     = cudaMemcpyHostToDevice;
//    //Array creation End
//
//    cudaResourceDesc texRes;
//    memset(&texRes, 0, sizeof(cudaResourceDesc));
//    texRes.resType = cudaResourceTypeArray;
//    texRes.res.array.array  = d_cuArr;
//    cudaTextureDesc texDescr;
//    memset(&texDescr, 0, sizeof(cudaTextureDesc));
//    texDescr.normalizedCoords = true;
//    texDescr.filterMode = cudaFilterModeLinear;
//    texDescr.addressMode[0] = cudaAddressModeBorder;   // clamp
//    texDescr.addressMode[1] = cudaAddressModeBorder;
//    texDescr.addressMode[2] = cudaAddressModeBorder;
//    texDescr.readMode = cudaReadModeElementType;
//    gpuErrchk(cudaCreateTextureObject(&textureObject, &texRes, &texDescr, NULL));
//    
//    float *h_volumeMem, *d_output, *h_output;
//
//    //cudaExtent volumeSizeBytes = make_cudaExtent(width, SIZE_Y, SIZE_Z);
//    gpuErrchk(cudaMalloc3D(&d_volumeMem, volumeSizeBytes));
//
//    h_volumeMem = (float *)malloc(size);
//    init<float>((char *)h_volumeMem, d_volumeMem.pitch, SIZE_X, SIZE_Y, SIZE_Z);
//    gpuErrchk(cudaMemcpy(d_volumeMem.ptr, h_volumeMem, size, cudaMemcpyHostToDevice));
//    gpuErrchk(cudaMemcpy3D(&copyParams));
//    printf("Memory pitch: %u\n",d_volumeMem.pitch);
//    //cudaArray * d_volumeArray;
//    //cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();
//    //cudaExtent volumeSize = make_cudaExtent(SIZE_X, SIZE_Y, SIZE_Z);
//    //gpuErrchk( cudaMalloc3DArray(&d_volumeArray, &channelDesc, volumeSize) );
//
//    //cudaMemcpy3DParms copyParams = {0};
//    //copyParams.srcPtr = d_volumeMem;
//    //copyParams.dstArray = d_volumeArray;
//    //copyParams.extent = volumeSize;
//    //copyParams.kind = cudaMemcpyDeviceToDevice;
//    //gpuErrchk( cudaMemcpy3D(&copyParams) );
//
//    //tex.normalized = true;
//    //tex.filterMode = cudaFilterModeLinear;
//    //tex.addressMode[0] = cudaAddressModeWrap;
//    //tex.addressMode[1] = cudaAddressModeWrap;
//    //tex.addressMode[2] = cudaAddressModeWrap;
//    //gpuErrchk(cudaBindTextureToArray(tex, d_volumeArray, channelDesc));
//
//    size_t osize = 64 * sizeof(float);
//    gpuErrchk(cudaMalloc((void**)&d_output, osize));
//
//    testKernel<<<1,dim3(4,4,4)>>>(d_output,textureObject,4,4,4,SIZE_X,SIZE_Y,SIZE_Z);
//    gpuErrchk(cudaPeekAtLastError());
//
//    h_output = (float *)malloc(osize);
//    gpuErrchk(cudaMemcpy(h_output, d_output, osize, cudaMemcpyDeviceToHost));
//
//    for(int i=0; i<64; i++)
//        fprintf(stdout, "%d %f\n", i, h_output[i]);
//
//    return 0;
//}

//
//using namespace std::chrono_literals;
////VALID TEST
//BOOST_AUTO_TEST_CASE(alignment_rsmd_test)
//{
//	printf(" ================================= Alignment and RMSD Test ====================================\n");
//	printf(" ============================= ++++++++++++++++++++++++++++++++ ===========================\n");
//	AlignmentProtocol align_protocol("");
//	align_protocol.testCube();
//	align_protocol.rmfTest();
//	align_protocol.pureTest(16,32);
//	align_protocol.rmfTestAlignment("./test/concat_small.rmf3","./test/concat_small_aligned.rmf3");
//}
