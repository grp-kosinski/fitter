/*
 * Test.cpp
 *
 *  Created on: Feb 19, 2019
 *      Author: kkarius
 */

#include <assert.h>
#include"/usr/include/complex.h"
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "so3/so3.h"

#include <cmocka.h>


so3_parameters_t parameters = {};

complex double * f_back = calloc((2 * parameters->N - 1) * parameters->L * parameters->L, sizeof(complex double));
