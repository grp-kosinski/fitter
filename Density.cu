/*
 * Density.cu
 *
 *  Created on: Sep 17, 2019
 *      Author: kkarius
 */

#include<Density.h>
#include<thrust/device_vector.h>
#include<thrust/device_ptr.h>
#include<thrust/sequence.h>
#include<thrust/remove.h>
#include<thrust/fill.h>
#include<util.h>
#include<score_helper.h>

struct is_between {
	//rho0 < rho1
	float rho0;
	float rho1;
	__device__
	bool operator()(const float f){
		return f >= rho0 && f <= rho1;
	}
};

struct is_outside {
	//rho0 < rho1
	float rho0;
	float rho1;
	__device__
	bool operator()(const float f){
		return f < rho0 || f > rho1;
	}
};

struct binarize : public thrust::unary_function<float,float>
{
	float threshold;
	float replace_above;
	float replace_below;
    __device__
	float operator()(const float& u) const {
    	if (u < threshold){
    		return replace_below;
    	} else {
		 return replace_above;
    	}
	}
};

__host__ __device__
uint3 Density::linear_to_index(const uint &linear_index, uint3 * const& pixel_dim){
	uint3 ret;
	ret.z = linear_index/(pixel_dim->x*pixel_dim->y);
	ret.y = (linear_index - ret.z*pixel_dim->x*pixel_dim->y)/pixel_dim->x;
	ret.x = linear_index - ret.y*pixel_dim->x - ret.z*pixel_dim->x*pixel_dim->y;
	return ret;
}

__host__ __device__
uint Density::index_to_linear(const uint3 &pixel_index,uint3 * const& pixel_dim){
	return pixel_index.x + pixel_index.y*pixel_dim->x + pixel_index.z*pixel_dim->y*pixel_dim->x;
}

void Density::writeToMrcSurface(const char * mrc_path, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
		CMrcReader reader;
		reader.setFileName(mrc_path);
		Density_Register * density_instance = &_instance_registry[gpu_index];

		CudaSafeCall(cudaMemcpy(density_instance->h_coord_dim,density_instance->d_coord_dim,sizeof(*density_instance->h_coord_dim),cudaMemcpyDeviceToHost));
		CudaSafeCall(cudaMemcpy(density_instance->h_coord_offset,density_instance->d_coord_offset,sizeof(*density_instance->h_coord_offset),cudaMemcpyDeviceToHost));
		CudaSafeCall(cudaMemcpy(density_instance->h_pixel_size,density_instance->d_pixel_size,sizeof(*density_instance->h_pixel_size),cudaMemcpyDeviceToHost));
		CudaSafeCall(cudaMemcpy(density_instance->h_pixel_dim,density_instance->d_pixel_dim,sizeof(*density_instance->h_pixel_dim),cudaMemcpyDeviceToHost));
		reader.header.cella.x = density_instance->h_coord_dim[0].x;
		reader.header.cella.y = density_instance->h_coord_dim[0].y;
		reader.header.cella.z = density_instance->h_coord_dim[0].z;
		CudaSafeCall(cudaMemcpy(density_instance->h_data,density_instance->d_data,vol(density_instance->h_pixel_dim[0])*sizeof(*density_instance->h_data),cudaMemcpyDeviceToHost));
		reader.header.origin.x = density_instance->h_coord_offset[0].x;
		reader.header.origin.y = density_instance->h_coord_offset[0].y;
		reader.header.origin.z = density_instance->h_coord_offset[0].z;
		reader.header.nx = density_instance->h_pixel_dim[0].x;
		reader.header.ny = density_instance->h_pixel_dim[0].y;
		reader.header.nz = density_instance->h_pixel_dim[0].z;
		reader.header.mx = density_instance->h_pixel_dim[0].x;
		reader.header.my = density_instance->h_pixel_dim[0].y;
		reader.header.mz = density_instance->h_pixel_dim[0].z;
		if (density_instance->h_data != nullptr)
			free(density_instance->h_data);
		density_instance->h_data = (TDensity *) malloc(vol(density_instance->h_pixel_dim[0])*sizeof(*density_instance->h_data));
		CudaSafeCall(cudaMemcpy(density_instance->h_data,density_instance->d_data,vol(density_instance->h_pixel_dim[0])*sizeof(*density_instance->h_data),cudaMemcpyDeviceToHost));
		uint * h_indeces = (uint *) malloc(density_instance->h_on_surface_vol[0]*sizeof(*h_indeces));
		CudaSafeCall(cudaMemcpy(h_indeces,density_instance->d_on_surface,density_instance->h_on_surface_vol[0]*sizeof(*h_indeces),cudaMemcpyDeviceToHost));
		TDensity * hm_data = (TDensity *) malloc(vol(density_instance->h_pixel_dim[0])*sizeof(*hm_data));
		for (int i=0;i<vol(density_instance->h_pixel_dim[0]);++i) hm_data[i] = (TDensity) 0.0f;
		for (int i=0;i<density_instance->h_on_surface_vol[0];++i) hm_data[h_indeces[i]] = density_instance->h_data[h_indeces[i]];
		reader.data = hm_data;
		reader.write();
		printf("Wrote to file %s ...\n coord dim %f %f %f\n pixel dim %u %u %u ...\n",mrc_path,
						reader.header.cella.x,reader.header.cella.y,reader.header.cella.z,
				density_instance->h_pixel_dim[0].x,density_instance->h_pixel_dim[0].y,density_instance->h_pixel_dim[0].z);
		free(h_indeces);
}

void Density::writeToMrcOnly(const char * mrc_path, thrust::device_vector<uint> & td_indeces, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	CMrcReader reader;
	reader.setFileName(mrc_path);
	Density_Register * density_instance = &_instance_registry[gpu_index];

	CudaSafeCall(cudaMemcpy(density_instance->h_coord_dim,density_instance->d_coord_dim,sizeof(*density_instance->h_coord_dim),cudaMemcpyDeviceToHost));
	CudaSafeCall(cudaMemcpy(density_instance->h_coord_offset,density_instance->d_coord_offset,sizeof(*density_instance->h_coord_offset),cudaMemcpyDeviceToHost));
	CudaSafeCall(cudaMemcpy(density_instance->h_pixel_size,density_instance->d_pixel_size,sizeof(*density_instance->h_pixel_size),cudaMemcpyDeviceToHost));
	CudaSafeCall(cudaMemcpy(density_instance->h_pixel_dim,density_instance->d_pixel_dim,sizeof(*density_instance->h_pixel_dim),cudaMemcpyDeviceToHost));
	reader.header.cella.x = density_instance->h_coord_dim[0].x;
	reader.header.cella.y = density_instance->h_coord_dim[0].y;
	reader.header.cella.z = density_instance->h_coord_dim[0].z;
	CudaSafeCall(cudaMemcpy(density_instance->h_data,density_instance->d_data,vol(density_instance->h_pixel_dim[0])*sizeof(*density_instance->h_data),cudaMemcpyDeviceToHost));
	reader.header.origin.x = density_instance->h_coord_offset[0].x;
	reader.header.origin.y = density_instance->h_coord_offset[0].y;
	reader.header.origin.z = density_instance->h_coord_offset[0].z;
	reader.header.nx = density_instance->h_pixel_dim[0].x;
	reader.header.ny = density_instance->h_pixel_dim[0].y;
	reader.header.nz = density_instance->h_pixel_dim[0].z;
	reader.header.mx = density_instance->h_pixel_dim[0].x;
	reader.header.my = density_instance->h_pixel_dim[0].y;
	reader.header.mz = density_instance->h_pixel_dim[0].z;
	if (density_instance->h_data != nullptr)
		free(density_instance->h_data);
	density_instance->h_data = (TDensity *) malloc(vol(density_instance->h_pixel_dim[0])*sizeof(*density_instance->h_data));
	CudaSafeCall(cudaMemcpy(density_instance->h_data,density_instance->d_data,vol(density_instance->h_pixel_dim[0])*sizeof(*density_instance->h_data),cudaMemcpyDeviceToHost));
	thrust::device_vector<uint> th_indeces(td_indeces.size());
	th_indeces = td_indeces;
	TDensity * hm_data = (TDensity *) malloc(vol(density_instance->h_pixel_dim[0])*sizeof(*hm_data));
	for (int i=0;i<vol(density_instance->h_pixel_dim[0]);++i) hm_data[i] = (TDensity) 0.0f;
	for (int i=0;i<th_indeces.size();++i) hm_data[th_indeces[i]] = density_instance->h_data[th_indeces[i]];
	reader.data = hm_data;
	reader.write();
	printf("Wrote to file %s ...\n coord dim %f %f %f\n pixel dim %u %u %u ...\n",mrc_path,
					reader.header.cella.x,reader.header.cella.y,reader.header.cella.z,
			density_instance->h_pixel_dim[0].x,density_instance->h_pixel_dim[0].y,density_instance->h_pixel_dim[0].z);
}

void Density::writeToMrcOnly(const char * mrc_path, uint * d_indeces, uint volume, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	CMrcReader reader;
	reader.setFileName(mrc_path);
	Density_Register * density_instance = &_instance_registry[gpu_index];

	CudaSafeCall(cudaMemcpy(density_instance->h_coord_dim,density_instance->d_coord_dim,sizeof(*density_instance->h_coord_dim),cudaMemcpyDeviceToHost));
	CudaSafeCall(cudaMemcpy(density_instance->h_coord_offset,density_instance->d_coord_offset,sizeof(*density_instance->h_coord_offset),cudaMemcpyDeviceToHost));
	CudaSafeCall(cudaMemcpy(density_instance->h_pixel_size,density_instance->d_pixel_size,sizeof(*density_instance->h_pixel_size),cudaMemcpyDeviceToHost));
	CudaSafeCall(cudaMemcpy(density_instance->h_pixel_dim,density_instance->d_pixel_dim,sizeof(*density_instance->h_pixel_dim),cudaMemcpyDeviceToHost));
	reader.header.cella.x = density_instance->h_coord_dim[0].x;
	reader.header.cella.y = density_instance->h_coord_dim[0].y;
	reader.header.cella.z = density_instance->h_coord_dim[0].z;
	CudaSafeCall(cudaMemcpy(density_instance->h_data,density_instance->d_data,vol(density_instance->h_pixel_dim[0])*sizeof(*density_instance->h_data),cudaMemcpyDeviceToHost));
	reader.header.origin.x = density_instance->h_coord_offset[0].x;
	reader.header.origin.y = density_instance->h_coord_offset[0].y;
	reader.header.origin.z = density_instance->h_coord_offset[0].z;
	reader.header.nx = density_instance->h_pixel_dim[0].x;
	reader.header.ny = density_instance->h_pixel_dim[0].y;
	reader.header.nz = density_instance->h_pixel_dim[0].z;
	reader.header.mx = density_instance->h_pixel_dim[0].x;
	reader.header.my = density_instance->h_pixel_dim[0].y;
	reader.header.mz = density_instance->h_pixel_dim[0].z;
	if (density_instance->h_data != nullptr)
		free(density_instance->h_data);
	density_instance->h_data = (TDensity *) malloc(vol(density_instance->h_pixel_dim[0])*sizeof(*density_instance->h_data));
	CudaSafeCall(cudaMemcpy(density_instance->h_data,density_instance->d_data,vol(density_instance->h_pixel_dim[0])*sizeof(*density_instance->h_data),cudaMemcpyDeviceToHost));
	uint * h_indeces = (uint *) malloc(volume*sizeof(*h_indeces));
	CudaSafeCall(cudaMemcpy(h_indeces,d_indeces,volume*sizeof(*h_indeces),cudaMemcpyDeviceToHost));
	TDensity * hm_data = (TDensity *) malloc(vol(density_instance->h_pixel_dim[0])*sizeof(*hm_data));
	for (int i=0;i<vol(density_instance->h_pixel_dim[0]);++i) hm_data[i] = (TDensity) 0.0f;
	for (int i=0;i<volume;++i) hm_data[h_indeces[i]] = density_instance->h_data[h_indeces[i]];
	reader.data = hm_data;
	reader.write();
	printf("Wrote to file %s ...\n coord dim %f %f %f\n pixel dim %u %u %u ...\n",mrc_path,
					reader.header.cella.x,reader.header.cella.y,reader.header.cella.z,
			density_instance->h_pixel_dim[0].x,density_instance->h_pixel_dim[0].y,density_instance->h_pixel_dim[0].z);
}


//void Density::cut_and_binarize(Density & sub_density, float threshold,float above_value,float below_value,uint3 tolerance){
//	typedef thrust::device_vector<uint>::iterator   UIntIterator;
//	typedef thrust::device_vector<float>::iterator FloatIterator;
//	// typedef a tuple of these iterators
//	typedef thrust::tuple<UIntIterator, FloatIterator> IteratorTuple;
//	// typedef the zip_iterator of this tuple
//	typedef thrust::zip_iterator<IteratorTuple> ZipIterator;
//	// finally, create the zip_iterator
//	thrust::device_vector<uint> linear_mem_offsets(h_pixel_vol());
//	thrust::sequence(linear_mem_offsets.begin(),linear_mem_offsets.end());
//	ZipIterator iter_begin(thrust::make_tuple(linear_mem_offsets.begin(), td_data.begin()));
//	ZipIterator iter_end(thrust::make_tuple(linear_mem_offsets.end(), td_data.end()));
//	thrust::pair<uint3,uint3> init;
//	//default for min
//	init.first.x = UINT32_MAX;
//	init.first.y = UINT32_MAX;
//	init.first.z = UINT32_MAX;
//	//default for max
//	init.second.x = 0;
//	init.second.y = 0;
//	init.second.z = 0;
//	conditional_to_index to_index;
//	to_index.threshold = threshold;
//	to_index.d_pixel_dim = d_pixel_dim;
//	thrust::pair<uint3,uint3> bounding_box = thrust::transform_reduce(iter_begin, iter_end, to_index, init, uint_bounding_box_pair());
//	uint3 cut_out_dim = bounding_box.second - bounding_box.first + 1;
//	uint3 pixel_dim = cut_out_dim + 2*tolerance;
//	uint3 index_offset = bounding_box.first;
//	uint * h_sub_pixel_dim = (uint *) malloc(3*sizeof(*h_sub_pixel_dim));
//	float * h_sub_coord_dim = (float *) malloc(3*sizeof(*h_sub_coord_dim));
//	h_sub_pixel_dim[0] = pixel_dim.x;
//	h_sub_pixel_dim[1] = pixel_dim.y;
//	h_sub_pixel_dim[2] = pixel_dim.z;
//	for (int i=0;i<3;i++) h_sub_coord_dim[i] = (((float) h_sub_pixel_dim[i])/((float) h_pixel_dim[i]))*h_coord_dim[i];
//
//	sub_density.h_coord_dim = h_sub_coord_dim;
//	sub_density.h_pixel_dim = h_sub_pixel_dim;
//	sub_density.h_pixel_size[0] = h_coord_dim[0]/h_pixel_dim[0];
//	sub_density.th_data.resize(sub_density.h_pixel_vol());
//	sub_density.h_data = thrust::raw_pointer_cast(&sub_density.th_data[0]);
//	sub_density.host_initiated = true;
//	for (int i = 0; i<3; i++) sub_density.h_mid_point[i] = h_sub_coord_dim[i]/2;
//
//	if (pixel_dim.x*pixel_dim.y*pixel_dim.z>0){
//		uint3 pixel_index_sub;
//		uint3 pixel_index;
//		for (uint x = 0; x <  cut_out_dim.x; x++){
//			for (uint y = 0; y < cut_out_dim.y; y++){
//				for (uint z = 0; z < cut_out_dim.z; z++){
//					pixel_index = {x + index_offset.x,y + index_offset.y,z + index_offset.z};
//					pixel_index_sub = {x + tolerance.x,y + tolerance.y,z + tolerance.z};
//					if(h_data[index_to_linear_space(pixel_index,h_pixel_dim)] >= threshold){
//						sub_density.h_data[index_to_linear_space(pixel_index_sub,h_sub_pixel_dim)] = above_value;
//					} else {
//						sub_density.h_data[index_to_linear_space(pixel_index_sub,h_sub_pixel_dim)] = below_value;
//					}
//				}
//			}
//		}
//		sub_density.init_device();
//		sub_density.to_device();
//		cudaDeviceSynchronize();
//	} else {
//		throw std::length_error("Resulting density seems invalid");
//	}
//}

//__device__
//float4 Density::d_pixel_linear_index_to_coord(const uint & pixel_linear_index){
//	float4 ret;
//	uint3 pixel = Density::linear_to_index(pixel_linear_index,d_pixel_dim);
//	ret.x = (static_cast<float>(pixel.x)/static_cast<float>(d_pixel_dim->x))*d_coord_dim->x;
//	ret.y = (static_cast<float>(pixel.y)/static_cast<float>(d_pixel_dim->y))*d_coord_dim->y;
//	ret.z = (static_cast<float>(pixel.z)/static_cast<float>(d_pixel_dim->z))*d_coord_dim->z;
//	ret.w = 0.0;
//	return ret;
//}

//__device__
//float4 Density::d_pixel_index_to_coord(const uint3 & pixel_index){
//	float4 ret;
//	ret.x = (static_cast<float>(pixel_index.x)/static_cast<float>(d_pixel_dim[0]))*d_coord_dim[0];
//	ret.y = (static_cast<float>(pixel_index.y)/static_cast<float>(d_pixel_dim[1]))*d_coord_dim[1];
//	ret.z = (static_cast<float>(pixel_index.z)/static_cast<float>(d_pixel_dim[2]))*d_coord_dim[2];
//	ret.w = 0.0;
//	return ret;
//}
//
//__host__
//float4 Density::h_pixel_linear_index_to_coord(const uint & pixel_index){
//	float4 ret;
//	uint4 pixel = h_pixel_index_to_pixel(pixel_index);
//	ret.x = (static_cast<float>(pixel.x)/static_cast<float>(h_pixel_dim[0]))*h_coord_dim[0];
//	ret.y = (static_cast<float>(pixel.y)/static_cast<float>(h_pixel_dim[1]))*h_coord_dim[1];
//	ret.z = (static_cast<float>(pixel.z)/static_cast<float>(h_pixel_dim[2]))*h_coord_dim[2];
//	ret.w = 0.0;
//	return ret;
//}
//

//TODO: test if forced inline makes sense
__device__ __forceinline__ void pixel_index_to_shared(float3 * const &shared_mem,uint * const &global_linear_address, uint3 * const &d_pixel_dim,const float &pixel_size){
	uint3 global_pixel_index;
	global_pixel_index.z = *global_linear_address/(d_pixel_dim->x*d_pixel_dim->y);
	global_pixel_index.y = (*global_linear_address - global_pixel_index.z*d_pixel_dim->x*d_pixel_dim->y)/d_pixel_dim->x;
	global_pixel_index.x =  *global_linear_address - global_pixel_index.z*d_pixel_dim->x*d_pixel_dim->y - global_pixel_index.y*d_pixel_dim->x;
	shared_mem->z = (__uint2float_rn(global_pixel_index.z) + 0.5)*pixel_size;
	shared_mem->y = (__uint2float_rn(global_pixel_index.y) + 0.5)*pixel_size;
	shared_mem->x = (__uint2float_rn(global_pixel_index.x) + 0.5)*pixel_size;
//	printf("%u %f %f %f\n",*global_linear_address,shared_mem->x,shared_mem->y,shared_mem->z);
}

//TODO: test if forced inline makes sense
__device__ __forceinline__ float calc_distance_from_shared(uint * const &global_linear_address, float3 * const &shared_index, uint3 * d_pixel_dim, float pixel_size){
	uint3 global_pixel_index;
	global_pixel_index.z = *global_linear_address/(d_pixel_dim->x*d_pixel_dim->y);
	global_pixel_index.y = (*global_linear_address - global_pixel_index.z*d_pixel_dim->x*d_pixel_dim->y)/d_pixel_dim->x;
	global_pixel_index.x =  *global_linear_address - global_pixel_index.z*d_pixel_dim->x*d_pixel_dim->y - global_pixel_index.y*d_pixel_dim->x;
	float3 diff_vector = {(__uint2float_rn(global_pixel_index.x) + 0.5f)*pixel_size - shared_index->x,
						  (__uint2float_rn(global_pixel_index.y) + 0.5f)*pixel_size - shared_index->y,
						  (__uint2float_rn(global_pixel_index.z) + 0.5f)*pixel_size - shared_index->z};
	return length(diff_vector);
}

struct copy_only_indices : public thrust::unary_function<uint,void>
{
	float * d_from;
	float * d_to;
    __device__
	void operator()(const uint& u) const {
		d_to[u] = d_from[u];
	}
};

template <uint BLOCKSIZE>
__global__
void difference_map_kernel(uint * d_from, uint * d_to,uint total_to_size, uint total_from_size, float * d_difference, uint3 pixel_dim, float pixel_size){
	__shared__ float3 dm2shared[BLOCKSIZE];
	int to_offset = BLOCKSIZE*blockIdx.x;
	int t = threadIdx.x;
	while (t < BLOCKSIZE){
		if (to_offset + t < total_to_size)
			pixel_index_to_shared(&dm2shared[t],d_to + to_offset + t, &pixel_dim,pixel_size);
		else
			dm2shared[t] = {FLT_MAX,FLT_MAX,FLT_MAX};
		t += blockDim.x;
	}
	__syncthreads();
	t = threadIdx.x;
	float _min;
	float3 _fmin;
	int m;
	while (t < total_from_size){
		pixel_index_to_shared(&_fmin,d_from+t,&pixel_dim,pixel_size);
		m = 0;
		_min = FLT_MAX;
		while (m < BLOCKSIZE){
			_min = fminf(_min,length(_fmin - dm2shared[m]));
			m += 1;
		}
		atomicMin(&d_difference[d_from[t]], _min);
//		d_difference[d_from[t]] = __uint2float_rn(d_from[t]);
		t += blockDim.x;
	}
}

struct is_fltmax
{
  __host__ __device__
  bool operator()(float x)
  {
	return x == FLT_MAX;
  }
};

__host__ __device__
void normalize_and_rotation_inverse(float4 *const& q, float *const& rotation){
        float4 qn = {q->x,q->y,q->z,q->w};
        qn *= rsqrtf(qn.x*qn.x + qn.y*qn.y + qn.z*qn.z + qn.w*qn.w);
        rotation[0] = 1-2*(qn.z*qn.z + qn.w*qn.w);
        rotation[3] =   2*(qn.y*qn.z - qn.w*qn.x);
        rotation[6] =   2*(qn.y*qn.w + qn.z*qn.x);
        rotation[1] =   2*(qn.y*qn.z + qn.w*qn.x);
        rotation[4] = 1-2*(qn.y*qn.y + qn.w*qn.w);
        rotation[7] =   2*(qn.z*qn.w - qn.y*qn.x);
        rotation[2] =   2*(qn.y*qn.w - qn.z*qn.x);
        rotation[5] =   2*(qn.z*qn.w + qn.y*qn.x);
        rotation[8] = 1-2*(qn.y*qn.y + qn.z*qn.z);
}


__global__
void transform_to(cudaTextureObject_t texobj, float * d_data, uint3 query_pixel_dim, uint3 target_pixel_dim , float3 trans_shift, float3 eulers){
        __shared__ float translation[3];
        __shared__ float rotation[9];
	
	int tidx = threadIdx.x + blockIdx.x * blockDim.x;
    	int tidy = threadIdx.y + blockIdx.y * blockDim.y;
    	int tidz = threadIdx.z + blockIdx.z * blockDim.z;

	if ( tidx >= target_pixel_dim.x || tidy >= target_pixel_dim.y || tidz >= target_pixel_dim.z )
		return;
	float3 half_volume = {0.5f*__uint2float_rd(query_pixel_dim.x),0.5f*__uint2float_rd(query_pixel_dim.y),0.5f*__uint2float_rd(query_pixel_dim.z)};	
    	float3 coords_pixel = {__uint2float_rd(tidx)+0.5f,
    			       __uint2float_rd(tidy)+0.5f,
    			       __uint2float_rd(tidz)+0.5f};
	coords_pixel -= half_volume;
    	size_t oidx = tidx + tidy*target_pixel_dim.x + tidz*target_pixel_dim.x*target_pixel_dim.y;
        float3 coords_pixel_t;
        float3 coords_pixel_tr;
	if (threadIdx.x == 0 && threadIdx.y == 0 && threadIdx.z == 0){
                //write transformations to memory
		eulers_to_memory_inverse(&eulers,&rotation[0]);
		translation[0] = -trans_shift.x;
		translation[1] = -trans_shift.y;
		translation[2] = -trans_shift.z;
	}
        __syncthreads();
	translate(coords_pixel_t, coords_pixel, &translation[0]);
        rotate(coords_pixel_tr,coords_pixel_t,&rotation[0]);
	coords_pixel_tr += half_volume;
	//printf("%f %f %f -> %f %f %f -> %f %f %f\n",coords_pixel.x,coords_pixel.y,coords_pixel.z,coords_pixel_t.x,coords_pixel_t.y,coords_pixel_t.z,coords_pixel_tr.x,coords_pixel_tr.y,coords_pixel_tr.z);
    	d_data[oidx] = tex3D<float>(texobj, coords_pixel_tr.x, coords_pixel_tr.y, coords_pixel_tr.z);
}

__global__
void transform_to(cudaTextureObject_t texobj, float * d_data, uint3 query_pixel_dim, uint3 target_pixel_dim , float3 trans_shift, float4 q){
        __shared__ float translation[3];
        __shared__ float rotation[9];
	
	int tidx = threadIdx.x + blockIdx.x * blockDim.x;
    	int tidy = threadIdx.y + blockIdx.y * blockDim.y;
    	int tidz = threadIdx.z + blockIdx.z * blockDim.z;

	if ( tidx >= target_pixel_dim.x || tidy >= target_pixel_dim.y || tidz >= target_pixel_dim.z )
		return;
	float3 half_volume = {0.5f*__uint2float_rd(query_pixel_dim.x),0.5f*__uint2float_rd(query_pixel_dim.y),0.5f*__uint2float_rd(query_pixel_dim.z)};	
    	float3 coords_pixel = {__uint2float_rd(tidx)+0.5f,
    			       __uint2float_rd(tidy)+0.5f,
    			       __uint2float_rd(tidz)+0.5f};
	//coords_pixel -= half_volume;
    	size_t oidx = tidx + tidy*target_pixel_dim.x + tidz*target_pixel_dim.x*target_pixel_dim.y;
        float3 coords_pixel_t;
        float3 coords_pixel_tr;
	if (threadIdx.x == 0 && threadIdx.y == 0 && threadIdx.z == 0){
                //write transformations to memory
		normalize_and_rotation_inverse(&q,&rotation[0]);
		translation[0] = -trans_shift.x;
		translation[1] = -trans_shift.y;
		translation[2] = -trans_shift.z;
	}
        __syncthreads();
	translate(coords_pixel_t, coords_pixel, &translation[0]);
        rotate(coords_pixel_tr,coords_pixel_t,&rotation[0]);
	coords_pixel_tr += half_volume;
	//printf("%f %f %f -> %f %f %f -> %f %f %f\n",coords_pixel.x,coords_pixel.y,coords_pixel.z,coords_pixel_t.x,coords_pixel_t.y,coords_pixel_t.z,coords_pixel_tr.x,coords_pixel_tr.y,coords_pixel_tr.z);
    	d_data[oidx] = tex3D<float>(texobj, coords_pixel_tr.x, coords_pixel_tr.y, coords_pixel_tr.z);
}


void Density::embed(Density *const& target, const float3& translation, const float3& eulers, const int& gpu_index){
	//translational shifts are counted from the origin of the target density, offset being -query_pixel_dim/2
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * target_density = &target->_instance_registry[gpu_index];
	Density_Register * source_density = &_instance_registry[gpu_index];
	uint3 block_dim = {8,8,8};
  	uint3 grid_dim = {target_density->h_pixel_dim->x/block_dim.x + 1, target_density->h_pixel_dim->y/block_dim.y + 1, target_density->h_pixel_dim->z/block_dim.z + 1}; 
//__global__
//void transform_to(cudaTextureObject_t texobj, float * d_data, uint3 pixel_dim, float3 trans_shift, float3 eulers){
	//printf("Embedding density of shape %u %u %u in density of shape %u %u %u using translation %f %f %f and eulers %f %f %f ...\n",source_density->h_pixel_dim->x,source_density->h_pixel_dim->y,source_density->h_pixel_dim->z,target_density->h_pixel_dim->x,target_density->h_pixel_dim->y,target_density->h_pixel_dim->z,translation.x,translation.y,translation.z,eulers.x,eulers.y,eulers.z);
	transform_to<<<grid_dim,block_dim>>>(source_density->textureObj[0],target_density->d_data,source_density->h_pixel_dim[0],target_density->h_pixel_dim[0],translation,eulers);
        CudaCheckError();
        cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(target_density->h_data,target_density->d_data,vol(target_density->h_pixel_dim[0]),cudaMemcpyDeviceToHost));
        cudaDeviceSynchronize();
	createTexture(gpu_index);
}

void Density::embed(Density *const& target, const float3& translation, const float4& q, const int& gpu_index){
	//translational shifts are counted from the origin of the target density, offset being -query_pixel_dim/2
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * target_density = &target->_instance_registry[gpu_index];
	Density_Register * source_density = &_instance_registry[gpu_index];
	uint3 block_dim = {8,8,8};
  	uint3 grid_dim = {target_density->h_pixel_dim->x/block_dim.x + 1, target_density->h_pixel_dim->y/block_dim.y + 1, target_density->h_pixel_dim->z/block_dim.z + 1}; 
//__global__
//void transform_to(cudaTextureObject_t texobj, float * d_data, uint3 pixel_dim, float3 trans_shift, float3 eulers){
	//printf("Embedding density of shape %u %u %u in density of shape %u %u %u using translation %f %f %f and quaternons %f %f %f %f ...\n",source_density->h_pixel_dim->x,source_density->h_pixel_dim->y,source_density->h_pixel_dim->z,target_density->h_pixel_dim->x,target_density->h_pixel_dim->y,target_density->h_pixel_dim->z,translation.x,translation.y,translation.z,q.x,q.y,q.z,q.w);
	transform_to<<<grid_dim,block_dim>>>(source_density->textureObj[0],target_density->d_data,source_density->h_pixel_dim[0],target_density->h_pixel_dim[0],translation,q);
        CudaCheckError();
        cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(target_density->h_data,target_density->d_data,vol(target_density->h_pixel_dim[0]),cudaMemcpyDeviceToHost));
        cudaDeviceSynchronize();
	createTexture(gpu_index);
}

void Density::transformTo(Density *const& target, const float3& trans_shift, const float3& eulers, const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	target->freeInstance(gpu_index);
	target->createInstanceFromDensity(this,gpu_index);
	Density_Register * target_density = &target->_instance_registry[gpu_index];	
	Density_Register * source_density = &_instance_registry[gpu_index];

	uint3 block_dim = {8,8,8};
  	uint3 grid_dim = {source_density->h_pixel_dim->x/block_dim.x + 1, source_density->h_pixel_dim->y/block_dim.y + 1, source_density->h_pixel_dim->z/block_dim.z + 1}; 
//__global__
//void transform_to(cudaTextureObject_t texobj, float * d_data, uint3 pixel_dim, float3 trans_shift, float3 eulers){
	transform_to<<<grid_dim,block_dim>>>(source_density->textureObj[0],target_density->d_data,source_density->h_pixel_dim[0],target_density->h_pixel_dim[0],trans_shift,eulers);
        CudaCheckError();
        cudaDeviceSynchronize();
	createTexture(gpu_index);
}

__global__
void transform_to(cudaTextureObject_t texobj, float * d_data, uint3 pixel_dim, uint3 * d_trans_cheby_node_dim,
     float * d_trans_cheby_nodes, uint * d_refinements, uint r, uint t){
        __shared__ float translation[3];
        __shared__ float rotation[9];
	
	int tidx = threadIdx.x + blockIdx.x * blockDim.x;
    	int tidy = threadIdx.y + blockIdx.y * blockDim.y;
    	int tidz = threadIdx.z + blockIdx.z * blockDim.z;

	if ( tidx >= pixel_dim.x || tidy >= pixel_dim.y || tidz >= pixel_dim.z )
		return;
	float3 half_volume = {0.5f*__uint2float_rd(pixel_dim.x),0.5f*__uint2float_rd(pixel_dim.y),0.5f*__uint2float_rd(pixel_dim.z)};	
    	float3 coords_pixel = {__uint2float_rd(tidx)+0.5f - half_volume.x,
    			       __uint2float_rd(tidy)+0.5f - half_volume.y,
    			       __uint2float_rd(tidz)+0.5f - half_volume.z};

    	size_t oidx = tidx + tidy*pixel_dim.x + tidz*pixel_dim.x*pixel_dim.y;
        float3 coords_pixel_t;
        float3 coords_pixel_tr;
	uint3 abg;
	if (threadIdx.x == 0 && threadIdx.y == 0 && threadIdx.z == 0){
                //write transformations to memory
        	rot_index(d_refinements, abg, r);
                rotation_to_memory_inverse(&rotation[0],abg,d_refinements);
                #ifdef DEBUG_BASE_SCORE_TEST
        	rotation[0] = 1.0; rotation[1] = 0.0; rotation[2] = 0.0; rotation[3] = 0.0; rotation[4] = 1.0; rotation[5] = 0.0; rotation[6] = 0.0; rotation[7] = 0.0;rotation[8] = 1.0;
		#endif

		translation_to_memory_inverse(&translation[0], d_trans_cheby_nodes, d_trans_cheby_node_dim, t);
	}
        __syncthreads();
	translate(coords_pixel_t, coords_pixel, &translation[0]);
        rotate(coords_pixel_tr,coords_pixel_t,&rotation[0]);
	coords_pixel_tr += half_volume;
    	d_data[oidx] = tex3D<float>(texobj, coords_pixel_tr.x, coords_pixel_tr.y, coords_pixel_tr.z);
}

void Density::transformTo(Density *const& target, ChebychevNodes *const& trans_nodes, ChebychevNodes *const& scan_nodes,McEwenNodes *const& rot_nodes, const uint& trans_index, const uint& rot_index, const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	target->freeInstance(gpu_index);
	target->createInstanceFromDensity(this,gpu_index);
	Density_Register * target_density = &target->_instance_registry[gpu_index];	
	Density_Register * source_density = &_instance_registry[gpu_index];
        ChebychevNodes_Register * trans_nodes_instance = &trans_nodes->_instance_registry[gpu_index];
        ChebychevNodes_Register * scan_nodes_instance = &scan_nodes->_instance_registry[gpu_index];
        McEwenNodes_Register * rot_nodes_instance = &rot_nodes->_instance_registry[gpu_index]; 		

	uint3 block_dim = {8,8,8};
  	uint3 grid_dim = {source_density->h_pixel_dim->x/block_dim.x + 1, source_density->h_pixel_dim->y/block_dim.y + 1, source_density->h_pixel_dim->z/block_dim.z + 1}; 
//	__global__
//void transform_to(cudaTextureObject_t d_density, float * d_data, uint3 * d_pixel_dim, uint3 * d_trans_cheby_node_dim,
//     float * d_trans_cheby_nodes, uint * d_refinements, uint r, uint t)
	transform_to<<<block_dim,grid_dim>>>(source_density->textureObj[0],target_density->d_data,target_density->h_pixel_dim[0], trans_nodes_instance->d_node_dim, trans_nodes_instance->d_node_list, rot_nodes_instance->d_refinements, rot_index, trans_index);
        CudaCheckError();
        cudaDeviceSynchronize();
	createTexture(gpu_index);
}

void Density::createDifferences(float * const& d_differences, const int& offset, const int& volume, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * density = &_instance_registry[gpu_index];
	uint3 pixel_dim;
	getPixelDim(pixel_dim,gpu_index);
	thrust::fill(thrust::device_pointer_cast(&d_differences[0]),thrust::device_pointer_cast(&d_differences[vol(pixel_dim)]),FLT_MAX);
	const uint to_chunk_size = 128;
	uint total_to_chunks = density->h_on_surface_vol[0]/to_chunk_size + 1;
//	template <uint BLOCKSIZE>
//	__global__
//	void difference_map_kernel2(uint * d_from, uint * d_to,uint total_to_size,uint total_from_size, float * d_difference, uint3 * d_pixel_dim, float pixel_size){
	difference_map_kernel<to_chunk_size><<<total_to_chunks,1024>>>(density->d_off_surface + offset, density->d_on_surface,
			density->h_on_surface_vol[0], volume, d_differences, density->d_pixel_dim[0],	density->h_pixel_size[0]);
	CudaCheckError();
	thrust::replace_if(thrust::device,thrust::device_pointer_cast(&d_differences[0]),thrust::device_pointer_cast(&d_differences[vol(pixel_dim)]),is_fltmax{},0.0);
	CudaCheckError();
}

void Density::createDifferences(float * const& d_differences, float * const& h_differences, uint3 *const& h_padding_pixel_dim,
		float3 *const& h_padding_coord_dim, const uint& padding, uint *& h_padding_off_surface_indexes,
		const int& offset, const int& volume, const int & gpu_index)
{
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * density = &_instance_registry[gpu_index];
	uint on_surface_pixel_vol = density->h_on_surface_vol[0];
	uint padding_pixel_vol = vol(h_padding_pixel_dim[0]);
	uint * h_on_surface_padding = (uint *) malloc(sizeof(*h_on_surface_padding)*on_surface_pixel_vol);
	uint * d_on_surface_padding;
	CudaSafeCall(cudaMalloc((void **)&d_on_surface_padding,sizeof(*d_on_surface_padding)*on_surface_pixel_vol));
	uint * h_off_surface_padding = (uint *) malloc(sizeof(*h_off_surface_padding)*padding_pixel_vol);
	uint * d_off_surface_padding;

	CudaSafeCall(cudaMemcpy(h_on_surface_padding,density->d_on_surface,sizeof(*h_on_surface_padding)*on_surface_pixel_vol,cudaMemcpyDeviceToHost));
	cudaDeviceSynchronize();
	uint3 pixel_index;
	for (int i=0;i < on_surface_pixel_vol; ++i){
		pixel_index = Density::linear_to_index(h_on_surface_padding[i],density->h_pixel_dim);
		pixel_index += {padding,padding,padding};
		h_on_surface_padding[i] = Density::index_to_linear(pixel_index,h_padding_pixel_dim);
	}
	uint start;
	uint stop;
	uint c=0;
	for (int i = 0; i< on_surface_pixel_vol + 1; ++i){
		if (i==0){
			start = 0;
			stop = h_on_surface_padding[i];
			if (stop != 0){
				h_off_surface_padding[c] = 0;
				c++;
			}
		} else if (i == on_surface_pixel_vol) {
			start = h_on_surface_padding[i-1];
			stop = padding_pixel_vol;
		} else {
			start = h_on_surface_padding[i-1];
			stop = h_on_surface_padding[i];
		}
		for (uint u = start + 1;u < stop; u++){
			h_off_surface_padding[c] = u;
			c++;
		}
	}
	CudaSafeCall(cudaMemcpy(d_on_surface_padding,h_on_surface_padding,sizeof(*d_on_surface_padding)*on_surface_pixel_vol,cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMalloc((void **)&d_off_surface_padding,sizeof(*d_off_surface_padding)*c));
	CudaSafeCall(cudaMemcpy(d_off_surface_padding,h_off_surface_padding,sizeof(*d_off_surface_padding)*c,cudaMemcpyHostToDevice));
	cudaDeviceSynchronize();
	thrust::fill(thrust::device_pointer_cast(&d_differences[0]),thrust::device_pointer_cast(&d_differences[padding_pixel_vol]),FLT_MAX);
	//translate the thing to diffmap terminology
	const uint to_chunk_size = 128;
	uint total_to_chunks = density->h_on_surface_vol[0]/to_chunk_size + 1;
//	template <uint BLOCKSIZE>
//	__global__
//	void difference_map_kernel2(uint * d_from, uint * d_to,uint total_to_size,uint total_from_size, float * d_difference, uint3 * d_pixel_dim, float pixel_size){
	difference_map_kernel<to_chunk_size><<<total_to_chunks,1024>>>(d_off_surface_padding + offset, d_on_surface_padding,
			on_surface_pixel_vol , volume, d_differences, h_padding_pixel_dim[0], density->h_pixel_size[0]);
	cudaDeviceSynchronize();
	CudaCheckError();
	thrust::replace_if(thrust::device,thrust::device_pointer_cast(&d_differences[0]),thrust::device_pointer_cast(&d_differences[padding_pixel_vol]),is_fltmax{},0.0);
	CudaCheckError();
//	uint _m = 0;
//	for (uint u=0; u<volume;++u){
//		_m = std::max(_m,h_off_surface_padding[u]);
//	}
	for (uint u=offset; u<volume+offset;++u){
		CudaSafeCall(cudaMemcpy(h_differences + h_off_surface_padding[u],d_differences + h_off_surface_padding[u],sizeof(*h_differences),cudaMemcpyDeviceToHost));
	}
	cudaDeviceSynchronize();
	CudaCheckError();
	h_padding_off_surface_indexes = (uint *) malloc(volume*sizeof(*h_padding_off_surface_indexes));
	for (int i = 0;i <volume;++i) h_padding_off_surface_indexes[i] = h_off_surface_padding[offset+i];
	cudaFree(d_on_surface_padding);
	free(h_off_surface_padding);
	cudaFree(d_off_surface_padding);
	free(h_on_surface_padding);
}

void Density::defineSurface(const float& threshold0, const float& threshold1, const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * density = &_instance_registry[gpu_index];
	uint3 pixel_dim;
	getPixelDim(pixel_dim,gpu_index);
	uint pixel_vol = vol(pixel_dim);
	size_t on_surface_size;
	size_t off_surface_size;
	thrust::device_vector<uint> td_on_surface_tmp(pixel_vol);
	thrust::device_vector<uint> td_off_surface_tmp(pixel_vol);
	is_between predicate_between;
	is_outside predicate_outside;
	predicate_between.rho0 = threshold0;
	predicate_between.rho1 = threshold1;
	predicate_outside.rho0 = threshold0;
	predicate_outside.rho1 = threshold1;
	thrust::device_vector<uint> linear_mem_offsets(pixel_vol);
	thrust::sequence(linear_mem_offsets.begin(),linear_mem_offsets.end());
	CudaCheckError();
	thrust::device_vector<uint>::iterator out_between = thrust::copy_if(linear_mem_offsets.begin(),linear_mem_offsets.end(),
			thrust::device_pointer_cast(&density->d_data[0]), td_on_surface_tmp.begin(),predicate_between);
	CudaCheckError();
	thrust::device_vector<uint>::iterator out_outside = thrust::copy_if(linear_mem_offsets.begin(),linear_mem_offsets.end(),
			thrust::device_pointer_cast(&density->d_data[0]), td_off_surface_tmp.begin(),predicate_outside);
	CudaCheckError();
	on_surface_size = thrust::distance(td_on_surface_tmp.begin(),out_between);
	off_surface_size = thrust::distance(td_off_surface_tmp.begin(),out_outside);

	if (density->d_on_surface != nullptr)
		cudaFree(density->d_on_surface);
	if (density->d_off_surface != nullptr)
		cudaFree(density->d_off_surface);
	CudaSafeCall(cudaMalloc((void **)&density->d_on_surface,on_surface_size*sizeof(*density->d_on_surface)));
	CudaSafeCall(cudaMalloc((void **)&density->d_off_surface,off_surface_size*sizeof(*density->d_off_surface)));
	CudaSafeCall(cudaMemcpy(density->d_on_surface,thrust::raw_pointer_cast(&td_on_surface_tmp[0]),on_surface_size*sizeof(*density->d_on_surface),cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(density->d_off_surface,thrust::raw_pointer_cast(&td_off_surface_tmp[0]),off_surface_size*sizeof(*density->d_off_surface),cudaMemcpyDeviceToDevice));
	density->h_on_surface_vol[0] = on_surface_size;
	CudaSafeCall(cudaMemcpy(density->d_on_surface_vol,density->h_on_surface_vol,sizeof(*density->d_on_surface_vol),cudaMemcpyHostToDevice));
	CudaCheckError();
	printf("Defined surface on density with %f%% of pixels\n",100.0f*((float)on_surface_size)/((float) pixel_vol));
}

void Density::getSurfaceVolume(int &volume,const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * density = &_instance_registry[gpu_index];
	volume = density->h_on_surface_vol[0];
}

//
//__device__
//void rotate_f3(float3 const& position_0, float3 &position_r, float4* const& rotation){
//	// rot_quat = [a,b,c,d]
//	// pos_quat = [0,x,y,z] - in memory as [x,y,z,0]
//	//  (a^2*x + 2*a*c*z - 2*a*d*y + b^2*x + 2*b*c*y + 2*b*d*z - c^2*x - d^2*x)*i
//	//+ (a^2*y - 2*a*b*z + 2*a*d*x - b^2*y + 2*b*c*x + 2*c*d*z + c^2*y - d^2*y)*j
//	//+ (a^2*z + 2*a*b*y - 2*a*c*x - b^2*z + 2*b*d*x - c^2*z + 2*c*d*y + d^2*z)*k
//
//	//x (a^2*x + 2*a*c*z - 2*a*d*y + b^2*x + 2*b*c*y + 2*b*d*z - c^2*x - d^2*x)
//	position_r.x = rotation->x*rotation->x*position_0.x
//	    + 2*rotation->x*rotation->z*position_0.z
//	    - 2*rotation->x*rotation->w*position_0.y
//	    + rotation->y*rotation->y*position_0.x
//            + 2*rotation->y*rotation->z*position_0.y
//  	    + 2*rotation->y*rotation->w*position_0.z
//	    - rotation->z*rotation->z*position_0.x
//	    - rotation->w*rotation->w*position_0.x;
//	//y (a^2*y - 2*a*b*z + 2*a*d*x - b^2*y + 2*b*c*x + 2*c*d*z + c^2*y - d^2*y)
//	position_r.y = rotation->x*rotation->x*position_0.y
//	    - 2*rotation->x*rotation->y*position_0.z
//	    + 2*rotation->x*rotation->w*position_0.x
//	    - rotation->y*rotation->y*position_0.y
//	    + 2*rotation->y*rotation->z*position_0.x
//	    + 2*rotation->z*rotation->w*position_0.z
//            + rotation->z*rotation->z*position_0.y
//	    - rotation->w*rotation->w*position_0.y;
//	//z (a^2*z + 2*a*b*y - 2*a*c*x - b^2*z + 2*b*d*x + 2*c*d*y - c^2*z + d^2*z)
//	position_r.z = rotation->x*rotation->x*position_0.z
//	    + 2*rotation->x*rotation->y*position_0.y
//	    - 2*rotation->x*rotation->z*position_0.x
//	    - rotation->y*rotation->y*position_0.z
//	    + 2*rotation->y*rotation->z*position_0.x
//	    + 2*rotation->z*rotation->w*position_0.y
//	    - rotation->z*rotation->z*position_0.z
//	    + rotation->w*rotation->w*position_0.z;
//}


__device__
float3 d_pixel_index_to_coord(const uint3 & pixel_index, uint* const& d_pixel_dim, float* const& d_coord_dim){
	float3 ret;
	//"+ 0.5" pixel-center == pixel_coord
	ret.x = (__uint2float_rn(pixel_index.x))/__uint2float_rn(d_pixel_dim[0])*d_coord_dim[0];
	ret.y = (__uint2float_rn(pixel_index.y))/__uint2float_rn(d_pixel_dim[1])*d_coord_dim[1];
	ret.z = (__uint2float_rn(pixel_index.z))/__uint2float_rn(d_pixel_dim[2])*d_coord_dim[2];
//	ret.w = 0.0;
	return ret;
}

void Density::createInstanceFromMrc(const char * mrc_path,float crop, int gpu_index){
	if (!file_exists(mrc_path))
		std::exit(-1);
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) != _instance_registry.end()){
		Density_Register * instance = &_instance_registry[gpu_index];
		free(instance->h_pixel_size);
		free(instance->h_coord_dim);
		free(instance->h_coord_offset);
		free(instance->h_pixel_dim);
		free(instance->h_molmap_padding);
		free(instance->h_on_surface_vol);
		CudaSafeCall(cudaFree(instance->d_coord_dim));
		CudaSafeCall(cudaFree(instance->d_coord_offset));
		CudaSafeCall(cudaFree(instance->d_pixel_dim));
		CudaSafeCall(cudaFree(instance->d_pixel_size));
		CudaSafeCall(cudaFree(instance->d_on_surface_vol));
		CudaSafeCall(cudaFree(instance->d_data));
	}
	Density_Register instance;
	CMrcReader reader;
	reader.mrc_file_name = mrc_path;
	reader.readHeader();
	reader.readData();

	instance.h_pixel_size = (float *) malloc(sizeof(*instance.h_pixel_size));
	instance.h_coord_dim = (float3 *) malloc(sizeof(*instance.h_coord_dim));
	instance.h_coord_offset = (float3 *) malloc(sizeof(*instance.h_coord_offset));
	instance.h_pixel_dim = (uint3 *) malloc(sizeof(*instance.h_pixel_dim));
	instance.h_molmap_padding = (float3 *) malloc(sizeof(*instance.h_molmap_padding));
	instance.h_on_surface_vol = (uint*) malloc(sizeof(*instance.h_on_surface_vol));

	instance.h_coord_dim[0].x = reader.header.cella.x;
	instance.h_coord_dim[0].y = reader.header.cella.y;
	instance.h_coord_dim[0].z = reader.header.cella.z;
	instance.h_coord_offset[0].x = reader.header.origin.x;
	instance.h_coord_offset[0].y = reader.header.origin.y;
	instance.h_coord_offset[0].z = reader.header.origin.z;
	instance.h_pixel_dim[0].x = reader.header.nx;
	instance.h_pixel_dim[0].y = reader.header.ny;
	instance.h_pixel_dim[0].z = reader.header.nz;
	//assumes isotropy
	instance.h_pixel_size[0] = instance.h_coord_dim[0].x/((float) instance.h_pixel_dim[0].x);
	instance.h_data = (TDensity *) malloc(instance.h_pixel_dim[0].x*instance.h_pixel_dim[0].y*instance.h_pixel_dim[0].z*sizeof(*instance.h_data));
	instance.h_crop_pixel_offset = (uint3 *)malloc(sizeof(*instance.h_crop_pixel_offset));
	for (int i = 0;i<instance.h_pixel_dim[0].x*instance.h_pixel_dim[0].y*instance.h_pixel_dim[0].z;i++) instance.h_data[i] = reader.data[i];

	CudaSafeCall(cudaMalloc((void **) &instance.d_coord_dim,sizeof(*instance.d_coord_dim)));
	CudaSafeCall(cudaMalloc((void **) &instance.d_coord_offset,sizeof(*instance.d_coord_offset)));
	CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_dim,sizeof(*instance.d_pixel_dim)));
	CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_size,sizeof(*instance.d_pixel_size)));
	CudaSafeCall(cudaMalloc((void **) &instance.d_on_surface_vol,sizeof(*instance.d_on_surface)));
	CudaSafeCall(cudaMalloc((void **) &instance.d_data,instance.h_pixel_dim[0].x*instance.h_pixel_dim[0].y*instance.h_pixel_dim[0].z*sizeof(*instance.d_data)));
	CudaSafeCall(cudaMemcpy(instance.d_coord_dim,instance.h_coord_dim,sizeof(*instance.d_coord_dim),cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMemcpy(instance.d_pixel_dim,instance.h_pixel_dim,sizeof(*instance.d_pixel_dim),cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMemcpy(instance.d_coord_offset,instance.h_coord_offset,sizeof(*instance.d_coord_offset),cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMemcpy(instance.d_pixel_size,instance.h_pixel_size,sizeof(*instance.h_pixel_size),cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMemcpy(instance.d_data,instance.h_data,instance.h_pixel_dim[0].x*instance.h_pixel_dim[0].y*instance.h_pixel_dim[0].z*sizeof(*instance.d_data),cudaMemcpyHostToDevice));
	file_path = std::string(mrc_path);
	_instance_registry[gpu_index] = instance;
	cropToThreshold(crop,48,256,gpu_index);
}

//unary function that is used as a first step for a thrust::reduce_transform
//operation, representing the transform part
//turns linear indexes into triple pixel indexes for later min/max reduction
//values above the threshold are assigned MAX and 0 so that they are not
//considered in the reductions
struct conditional_to_index : public thrust::unary_function<thrust::tuple<uint,TDensity>,thrust::pair<uint3,uint3>>
{
	uint3 * d_pixel_dim;
	TDensity threshold;
    __device__
	thrust::pair<uint3,uint3> operator()(const thrust::tuple<uint,TDensity>& u) const {
    	thrust::pair<uint3,uint3> ret;
    	if (thrust::get<1>(u) < threshold){
    		//default for min
    		ret.first = {UINT32_MAX,UINT32_MAX,UINT32_MAX};
    		//default for max
    		ret.second = {0,0,0};
    	} else {
			ret.first.z = thrust::get<0>(u)/(d_pixel_dim[0].x*d_pixel_dim[0].y);
			ret.first.y = (thrust::get<0>(u) - ret.first.z*d_pixel_dim[0].x*d_pixel_dim[0].y)/d_pixel_dim[0].x;
			ret.first.x = thrust::get<0>(u) - ret.first.y*d_pixel_dim[0].x - ret.first.z*d_pixel_dim[0].x*d_pixel_dim[0].y;
			ret.second.x = ret.first.x;
			ret.second.y = ret.first.y;
			ret.second.z = ret.first.z;
    	}
		return ret;
	}
};

//binary function that is used as a first step for a thrust::reduce_transform
//operation, representing the reduce part
//simultaneously calculates min and max
struct uint_bounding_box_pair : public thrust::binary_function<thrust::pair<uint3,uint3>,thrust::pair<uint3,uint3>,thrust::pair<uint3,uint3>>
{
    __device__
	thrust::pair<uint3,uint3> operator()(const thrust::pair<uint3,uint3>& u, const thrust::pair<uint3,uint3>& v) const {
    	thrust::pair<uint3,uint3> ret;
    	ret.first = min(u.first,v.first);
    	ret.second = max(u.second,v.second);
//		printf("%u %u %u -- %u %u %u\n",ret.first.x,ret.first.y,ret.first.z,ret.second.x,ret.second.y,ret.second.z);
		return ret;
	}
};

__global__
void cut_out_density(TDensity * d_old, TDensity * d_new, size_t num_pixels_new, uint3 offset,
		uint3 * d_pixel_dim_old, uint3 * d_pixel_dim_new){
	int grid_size = blockDim.x*gridDim.x;
	int p = blockDim.x*blockIdx.x + threadIdx.x;
	uint3 pixel_new;
	uint3 pixel_old;
	uint linear_old;
	while (p<num_pixels_new){
		pixel_new = Density::linear_to_index(p, d_pixel_dim_new);
		pixel_old = pixel_new + offset;
		linear_old = Density::index_to_linear(pixel_old,d_pixel_dim_old);
		d_new[p] = d_old[linear_old];
		p+=grid_size;
	}
}

#include<thrust/device_vector.h>
#include<thrust/tuple.h>
#include<thrust/iterator/zip_iterator.h>
#include<thrust/sequence.h>
#include<thrust/transform_reduce.h>

__global__
void set_zero_if_above(float * d_data, size_t pixel_vol, float threshold){
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	size_t grid_size = blockDim.x*gridDim.x;
	while (tid < pixel_vol){
		if (d_data[tid] <= threshold)
			d_data[tid] = 0.0f;
		tid += grid_size;
	}
}

void Density::setZeroIfAbove(const float & threshold,int gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	size_t pixel_vol = vol(instance->h_pixel_dim[0]);
	int block_dim = 256;
	set_zero_if_above<<<pixel_vol/block_dim + 1, block_dim>>>(instance->d_data,pixel_vol,threshold);
	cudaDeviceSynchronize();
}

void Density::cropToThreshold(const float & threshold,int gridDim,int blockDim, int gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	typedef thrust::device_vector<uint>::iterator   UIntIterator;
	typedef thrust::device_vector<float>::iterator FloatIterator;
	//	// typedef a tuple of these iterators
	typedef thrust::tuple<UIntIterator, FloatIterator> IteratorTuple;
	//	// typedef the zip_iterator of this tuple
	typedef thrust::zip_iterator<IteratorTuple> ZipIterator;
	size_t pixel_vol = instance->h_pixel_dim[0].x*instance->h_pixel_dim[0].y*instance->h_pixel_dim[0].z;
	//	// finally, create the zip_iterator
	thrust::device_vector<uint> linear_mem_offsets(pixel_vol);
	thrust::sequence(linear_mem_offsets.begin(),linear_mem_offsets.end());
	thrust::device_ptr<TDensity> d_data_begin = thrust::device_pointer_cast(&instance->d_data[0]);
	thrust::device_ptr<TDensity> d_data_end = thrust::device_pointer_cast(&instance->d_data[pixel_vol]);
	ZipIterator iter_begin(thrust::make_tuple(linear_mem_offsets.begin(),d_data_begin ));
	ZipIterator iter_end(thrust::make_tuple(linear_mem_offsets.end(), d_data_end));
	thrust::pair<uint3,uint3> init;
	//default for min
	init.first.x = UINT32_MAX;
	init.first.y = UINT32_MAX;
	init.first.z = UINT32_MAX;
	//default for max
	init.second.x = 0;
	init.second.y = 0;
	init.second.z = 0;
	conditional_to_index to_index;
	to_index.threshold = threshold;
	to_index.d_pixel_dim = instance->d_pixel_dim;
	thrust::pair<uint3,uint3> bounding_box = thrust::transform_reduce(iter_begin, iter_end, to_index, init, uint_bounding_box_pair());
//	bounding_box = {{10,10,10},{120,84,120}};
	uint3 pixel_dim = bounding_box.second - bounding_box.first;
	instance->h_crop_pixel_offset[0] = bounding_box.first;
	if (file_path.size() > 0)
	{
		
		printf("Cropped density of file %s with threshold %f to %u,%u,%u - %u,%u,%u\n",threshold,file_path.c_str(),bounding_box.first.x,bounding_box.first.y,bounding_box.first.z,bounding_box.second.x,bounding_box.second.y,bounding_box.second.z);
	} else {
		printf("Cropped density with threshold %f to %u,%u,%u - %u,%u,%u\n",threshold,bounding_box.first.x,bounding_box.first.y,bounding_box.first.z,bounding_box.second.x,bounding_box.second.y,bounding_box.second.z);
	}
	pixel_dim.x +=1; pixel_dim.y +=1;pixel_dim.z +=1;
	uint new_pixel_vol = pixel_dim.x*pixel_dim.y*pixel_dim.z;
	//nothing to be done
	if (pixel_dim.x*pixel_dim.y*pixel_dim.z == pixel_vol)
		return;
	uint3 * h_pixel_dim_new = (uint3 *) malloc(sizeof(*h_pixel_dim_new));
	uint3 * d_pixel_dim_new;
	CudaSafeCall(cudaMalloc((void **) &d_pixel_dim_new,sizeof(*d_pixel_dim_new)));
	float3 * h_coord_dim_new = (float3 *) malloc(sizeof(*h_coord_dim_new));
	TDensity * h_data_new = (TDensity *) malloc(new_pixel_vol*sizeof(*h_data_new));
	TDensity * d_data_new;
	CudaSafeCall(cudaMalloc((void **)&d_data_new,new_pixel_vol*sizeof(*d_data_new)));
	h_pixel_dim_new[0] = pixel_dim;
	h_coord_dim_new[0].x = instance->h_pixel_size[0]*((float) h_pixel_dim_new[0].x);
	h_coord_dim_new[0].y = instance->h_pixel_size[0]*((float) h_pixel_dim_new[0].y);
	h_coord_dim_new[0].z = instance->h_pixel_size[0]*((float) h_pixel_dim_new[0].z);
	CudaSafeCall(cudaMemcpy(instance->d_coord_dim,h_coord_dim_new,sizeof(*instance->d_coord_dim),cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMemcpy(d_pixel_dim_new,h_pixel_dim_new,sizeof(*d_pixel_dim_new),cudaMemcpyHostToDevice));
	cut_out_density<<<gridDim,blockDim>>>(instance->d_data, d_data_new, new_pixel_vol, bounding_box.first,
			instance->d_pixel_dim, d_pixel_dim_new);
	CudaCheckError();
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(h_data_new,d_data_new,new_pixel_vol*sizeof(*h_data_new),cudaMemcpyDeviceToHost));
	//free pointers that are to be replaced due to the new volume
	CudaSafeCall(cudaFree(instance->d_data));
	CudaSafeCall(cudaFree(instance->d_pixel_dim));
	instance->d_data = d_data_new;
	instance->h_data = h_data_new;
	instance->d_pixel_dim = d_pixel_dim_new;
	instance->h_pixel_dim[0] = h_pixel_dim_new[0];
	instance->h_coord_dim[0] = h_coord_dim_new[0];
}

void Density::setPixelCube(const uint3 &offset, const uint3 &dimension, const TDensity& value, const int &gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	uint total_pixels = vol(instance->h_pixel_dim[0]);
	uint3 current_pixel;
	for (int i=0;i<total_pixels;i++){
		current_pixel = Density::linear_to_index(i,instance->h_pixel_dim);
		if ((offset.x <= current_pixel.x) && (current_pixel.x < offset.x+dimension.x)
		  &&(offset.y <= current_pixel.y) && (current_pixel.y < offset.y+dimension.y)
		  &&(offset.z <= current_pixel.z) && (current_pixel.z < offset.z+dimension.z)){
			instance->h_data[i] = value;
		}
	}
	CudaSafeCall(cudaMemcpy(instance->d_data,instance->h_data,total_pixels*sizeof(*instance->d_data),cudaMemcpyHostToDevice));
	cudaDeviceSynchronize();
}

void Density::createInstanceFromDensity(Density * const & density, const int &gpu_index){
	//doesn't allocate data on gpu
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		Density_Register instance_copy;
		Density_Register * instance = &density->_instance_registry[gpu_index];
		instance_copy.h_pixel_size = (float *) malloc(sizeof(*instance_copy.h_pixel_size));
		instance_copy.h_coord_dim = (float3 *)malloc(sizeof(*instance_copy.h_coord_dim));
		instance_copy.h_coord_offset = (float3 *)malloc(sizeof(*instance_copy.h_coord_offset));
		instance_copy.h_pixel_dim = (uint3 *)malloc(sizeof(*instance_copy.h_pixel_dim));
		instance_copy.h_external_memory = (size_t *) malloc(sizeof(*instance_copy.h_external_memory));
		instance_copy.h_external_memory[0] = 0;
		instance_copy.h_data = (TDensity *) malloc(vol(instance->h_pixel_dim[0])*sizeof(*instance_copy.h_data));
		instance_copy.h_crop_pixel_offset = (uint3 *)malloc(sizeof(*instance_copy.h_crop_pixel_offset));
		instance_copy.h_molmap_padding = (float3 *) malloc(sizeof(*instance_copy.h_molmap_padding));
		instance_copy.h_on_surface_vol = (uint *) malloc(sizeof(*instance_copy.h_on_surface_vol));

		instance_copy.h_pixel_size[0] = instance->h_pixel_size[0];
		instance_copy.h_coord_dim[0] = instance->h_coord_dim[0];
		instance_copy.h_coord_offset[0] = instance->h_coord_offset[0];
		instance_copy.h_pixel_dim[0] = instance->h_pixel_dim[0];

		CudaSafeCall(cudaMalloc((void **) &instance_copy.d_coord_dim,sizeof(*instance_copy.d_coord_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance_copy.d_coord_offset,sizeof(*instance_copy.d_coord_offset)));
		CudaSafeCall(cudaMalloc((void **) &instance_copy.d_pixel_dim,sizeof(*instance_copy.d_pixel_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance_copy.d_pixel_size,sizeof(*instance_copy.d_pixel_size)));
		CudaSafeCall(cudaMalloc((void **) &instance_copy.d_on_surface_vol,sizeof(*instance_copy.d_on_surface_vol)));
		CudaSafeCall(cudaMalloc((void **) &instance_copy.d_data,vol(instance->h_pixel_dim[0])*sizeof(*instance_copy.d_data)));

		CudaSafeCall(cudaMemcpy(instance_copy.d_coord_dim,instance_copy.h_coord_dim,sizeof(*instance_copy.d_coord_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance_copy.d_pixel_dim,instance_copy.h_pixel_dim,sizeof(*instance_copy.d_pixel_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance_copy.d_coord_offset,instance_copy.h_coord_offset,sizeof(*instance_copy.d_coord_offset),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance_copy.d_pixel_size,instance_copy.h_pixel_size,sizeof(*instance_copy.h_pixel_size),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance_copy.d_on_surface_vol,instance_copy.h_on_surface_vol,sizeof(*instance_copy.d_on_surface_vol),cudaMemcpyHostToDevice));
		_instance_registry[gpu_index] = instance_copy;
		printf("Created gpu instance %i ...\n",gpu_index);
	}
}

void Density::createMultiInstance(const uint3 &pix_dim, const float &pixel_size, const int& N, const int &gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		Density_Register instance;
		instance.h_pixel_size = (float *) malloc(sizeof(*instance.h_pixel_size));
		instance.h_on_surface_vol = (uint *) malloc(sizeof(*instance.h_on_surface_vol));
		instance.h_coord_dim = (float3 *)malloc(sizeof(*instance.h_coord_dim));
		instance.h_coord_offset = (float3 *)malloc(sizeof(*instance.h_coord_offset));
		instance.h_pixel_dim = (uint3 *)malloc(sizeof(*instance.h_pixel_dim));
		instance.h_data = (TDensity *) malloc(vol(pix_dim)*sizeof(*instance.h_data));
		instance.h_crop_pixel_offset = (uint3 *)malloc(sizeof(*instance.h_crop_pixel_offset));
		instance.h_molmap_padding = (float3 *) malloc(sizeof(*instance.h_molmap_padding));
		//no allocation of CPU for data
		instance.h_pixel_size[0] = pixel_size;
		instance.h_coord_dim[0] = {pixel_size*pix_dim.x, pixel_size*pix_dim.y, pixel_size*pix_dim.z};
		instance.h_coord_offset[0] = {0,0,0};
		instance.h_pixel_dim[0] = pix_dim;

		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_dim,sizeof(*instance.d_coord_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_offset,sizeof(*instance.d_coord_offset)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_dim,sizeof(*instance.d_pixel_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_size,sizeof(*instance.d_pixel_size)));
		//only reflected GPU
		CudaSafeCall(cudaMalloc((void **) &instance.d_cdata,N*vol(pix_dim)*sizeof(*instance.d_cdata)));
		CudaSafeCall(cudaMemcpy(instance.d_coord_dim,instance.h_coord_dim,sizeof(*instance.d_coord_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_dim,instance.h_pixel_dim,sizeof(*instance.d_pixel_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_coord_offset,instance.h_coord_offset,sizeof(*instance.d_coord_offset),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_size,instance.h_pixel_size,sizeof(*instance.h_pixel_size),cudaMemcpyHostToDevice));
		_instance_registry[gpu_index] = instance;
		printf("Created gpu multi instance %i ...\n",gpu_index);
	}

}

void Density::createInstanceFromBounds(const float3 &coord_dim, const float &pixel_size,const size_t &extern_memory, const int &gpu_index){
	//doesn't allocate data on gpu
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		Density_Register instance;
		assert(coord_dim.x > 0 && coord_dim.y > 0 && coord_dim.z > 0);
		assert(pixel_size > 0);
		uint3 pixel_dim;
		//not all pixel sizes fit
		float3 corrected_coord_dim;
		pixel_dim.x = (uint) ceil(coord_dim.x/pixel_size);
		pixel_dim.y = (uint) ceil(coord_dim.y/pixel_size);
		pixel_dim.z = (uint) ceil(coord_dim.z/pixel_size);
		uint pixel_vol = pixel_dim.x*pixel_dim.y*pixel_dim.z;
		//in case pixel_size is bad, closest value that will result coord_dim will be chosen
		corrected_coord_dim.x = pixel_dim.x*pixel_size;
		corrected_coord_dim.y = pixel_dim.y*pixel_size;
		corrected_coord_dim.z = pixel_dim.z*pixel_size;
		instance.h_pixel_size = (float *) malloc(sizeof(*instance.h_pixel_size));
		instance.h_on_surface_vol = (uint *) malloc(sizeof(*instance.h_on_surface_vol));
		instance.h_coord_dim = (float3 *)malloc(sizeof(*instance.h_coord_dim));
		instance.h_coord_offset = (float3 *)malloc(sizeof(*instance.h_coord_offset));
		instance.h_pixel_dim = (uint3 *)malloc(sizeof(*instance.h_pixel_dim));
		instance.h_external_memory = (size_t *) malloc(sizeof(*instance.h_external_memory));
		instance.h_external_memory[0] = 0;
		instance.h_data = (TDensity *) malloc(pixel_vol*sizeof(*instance.h_data));
		instance.h_crop_pixel_offset = (uint3 *)malloc(sizeof(*instance.h_crop_pixel_offset));
		instance.h_molmap_padding = (float3 *) malloc(sizeof(*instance.h_molmap_padding));

		if (extern_memory > 0){
			instance.h_external_memory[0] = extern_memory;
		}
		//no allocation of CPU for data
		instance.h_pixel_size[0] = pixel_size;
		instance.h_coord_dim[0] = corrected_coord_dim;
		instance.h_coord_offset[0] = {0,0,0};
		instance.h_pixel_dim[0] = pixel_dim;

		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_dim,sizeof(*instance.d_coord_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_offset,sizeof(*instance.d_coord_offset)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_dim,sizeof(*instance.d_pixel_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_size,sizeof(*instance.d_pixel_size)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_on_surface_vol,sizeof(*instance.d_on_surface_vol)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_data,pixel_vol*sizeof(*instance.d_data)));
		CudaSafeCall(cudaMemcpy(instance.d_coord_dim,instance.h_coord_dim,sizeof(*instance.d_coord_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_dim,instance.h_pixel_dim,sizeof(*instance.d_pixel_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_coord_offset,instance.h_coord_offset,sizeof(*instance.d_coord_offset),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_size,instance.h_pixel_size,sizeof(*instance.h_pixel_size),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_on_surface_vol,instance.h_on_surface_vol,sizeof(*instance.h_on_surface_vol),cudaMemcpyHostToDevice));
		_instance_registry[gpu_index] = instance;
		printf("Created gpu instance %i ...\n",gpu_index);
	}
}

template <uint blockSize>
__global__
void cheby_quadrature(cudaTextureObject_t d_density, float * d_node_list, float * d_weights,uint3 * d_node_dim,float * d_result){
	//COULD be optimized, for now just test
	__shared__ float concat[2*blockSize];
	size_t total_nodes = d_node_dim[0].x*d_node_dim[0].y*d_node_dim[0].z;
	size_t grid_size = blockDim.x*gridDim.x;
	int tid = threadIdx.x;
	int p = blockIdx.x*blockDim.x + threadIdx.x;
	int i,j,k;
	int layer_dim = d_node_dim[0].x*d_node_dim[0].y;
	float * d_nodes_x = d_node_list;
	float * d_nodes_y = d_node_list + d_node_dim[0].x;
	float * d_nodes_z = d_node_list + d_node_dim[0].x + d_node_dim[0].y;
	float * d_weights_x = d_weights;
	float * d_weights_y = d_weights + d_node_dim[0].x;
	float * d_weights_z = d_weights + d_node_dim[0].x + d_node_dim[0].y;
	concat[tid] = 0.0f;
	while (p < total_nodes){
		k = p/layer_dim;
		j = (p - k*layer_dim)/d_node_dim[0].x;
		i = p - j*d_node_dim[0].x - k*layer_dim;
		concat[tid] += d_weights_x[i]*d_weights_y[j]*d_weights_z[k]*tex3D<float>(d_density, d_nodes_x[i], d_nodes_y[j],d_nodes_z[k]);
		p += grid_size;
	}
	__syncthreads();
	if (blockSize >= 512){if (tid < 256) { concat[tid] += concat[tid + 256];} __syncthreads();}
	if (blockSize >= 256){if (tid < 128) { concat[tid] += concat[tid + 128];} __syncthreads();}
	if (blockSize >= 128){if (tid <  64) { concat[tid] += concat[tid + 64];} __syncthreads();}
	warp_reduce<float,blockSize>(concat,tid);
	__syncthreads();

	if (tid ==0)
		 atomicAdd(d_result, concat[0]);
}

struct mask_unary : public thrust::unary_function<int,TDensity>
{
	TDensity h_threshold;
    __device__
	int operator()(const TDensity& value) const {
    	if (value > h_threshold)
    		return 1;
    	return 0;
	}
};

void Density::createDensityMask(const TDensity & threshold, int *const& d_mask,const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	uint pixel_vol = vol(instance->h_pixel_dim[0]);
	//sizeof(int) == sizeof(float),otherwise this shit hits the fan
//	CudaSafeCall(cudaMemcpy(d_mask,instance->d_data,pixel_vol*sizeof(*instance->d_data),cudaMemcpyDeviceToDevice));
	mask_unary _mask;
	_mask.h_threshold = threshold;
	thrust::transform(thrust::device_pointer_cast(&instance->d_data[0]),thrust::device_pointer_cast(&instance->d_data[pixel_vol]),
			thrust::device_pointer_cast(&d_mask[0]),_mask);
	cudaDeviceSynchronize();
}

void Density::getPixelDim(uint3 & pixel_dim, const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	pixel_dim = *instance->h_pixel_dim;
}

void Density::getMaxPixelDim(uint & _max, const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	uint3 pixel_dim = *instance->h_pixel_dim;
	_max = std::max(std::max(pixel_dim.x,pixel_dim.y),pixel_dim.z);
}

void Density::average(const float& threshold, float &average, float &norm, float&sum, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	float _sum = 0.0f;
	float _sum_mask = 0.0f;
	float _norm = 0.0f;
	for (int i = 0; i < vol(instance->h_pixel_dim[0]);i++){
		if (instance->h_data[i] >= threshold){
			_sum += instance->h_data[i];
			_sum_mask += 1.0f;
		}
	}
	average = _sum/_sum_mask;
	for (int i = 0; i < vol(instance->h_pixel_dim[0]);i++){
		if (instance->h_data[i] >= threshold){
			_norm += (instance->h_data[i] - average)*(instance->h_data[i] - average);
		}
	}
	sum = _sum_mask;
	norm = sqrtf(_norm);
}

float Density::average(Density * const& mask, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	Density_Register * mask_instance = &mask->_instance_registry[gpu_index];
	float _sum = 0.0f;
	float _sum_mask = 0.0f;
	for (int i = 0; i < vol(instance->h_pixel_dim[0]);i++){
		_sum += instance->h_data[i]*mask_instance->h_data[i];
		_sum_mask += mask_instance->h_data[i];
	}
	return _sum/_sum_mask;	
}

void Density::approximateVolume(ChebychevNodes * const& chebychev_nodes, float * h_result, float * d_result, int gridDim,int blockDim, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	ChebychevNodes_Register * nodes_instance = &chebychev_nodes->_instance_registry[gpu_index];
	if (instance->textureObj != nullptr){
		cheby_quadrature<512><<<gridDim,blockDim>>>(instance->textureObj[0], nodes_instance->d_node_list, nodes_instance->d_weights,nodes_instance->d_node_dim,d_result);
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(h_result,d_result,sizeof(*h_result),cudaMemcpyDeviceToHost));
		//reverse effect of weights
		h_result[0] *= (instance->h_coord_dim[0].x*instance->h_coord_dim[0].y*instance->h_coord_dim[0].z);
	}
}

void Density::getCoordDim(float3 & coord_dim, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	coord_dim = {instance->h_coord_dim[0].x,instance->h_coord_dim[0].y,instance->h_coord_dim[0].z};
}

void Density::getPixelSize(float & pixel_size, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	pixel_size = instance->h_pixel_size[0];
}

void Density::approximateAverage(ChebychevNodes * const& chebychev_nodes, float * h_result, float * d_result, int gridDim,int blockDim, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	ChebychevNodes_Register * nodes_instance = &chebychev_nodes->_instance_registry[gpu_index];
	if (instance->textureObj != nullptr){
		cheby_quadrature<512><<<gridDim,blockDim>>>(instance->textureObj[0], nodes_instance->d_node_list, nodes_instance->d_weights,nodes_instance->d_node_dim,d_result);
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(h_result,d_result,sizeof(*h_result),cudaMemcpyDeviceToHost));
	}
}

void Density::createInstanceGaussian(const uint3 & pixel_dim, const float & pixel_size, const int &gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		Density_Register instance;
		instance.h_pixel_size = (float *) malloc(sizeof(*instance.h_pixel_size));
		instance.h_coord_dim = (float3 *)malloc(sizeof(*instance.h_coord_dim));
		instance.h_coord_offset = (float3 *)malloc(sizeof(*instance.h_coord_offset));
		instance.h_pixel_dim = (uint3 *)malloc(sizeof(*instance.h_pixel_dim));
		instance.h_external_memory = (size_t *) malloc(sizeof(*instance.h_external_memory));
		instance.h_data = (TDensity *) malloc(vol(pixel_dim)*sizeof(*instance.h_data));
		instance.h_crop_pixel_offset = (uint3 *)malloc(sizeof(*instance.h_crop_pixel_offset));
		//no allocation of CPU for data
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_dim,sizeof(*instance.d_coord_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_offset,sizeof(*instance.d_coord_offset)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_dim,sizeof(*instance.d_pixel_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_size,sizeof(*instance.d_pixel_size)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_data,vol(pixel_dim)*sizeof(*instance.d_data)));
		instance.h_pixel_size[0] = pixel_size;
		instance.h_pixel_dim[0] = pixel_dim;
		instance.h_coord_dim[0] = {pixel_size*pixel_dim.x,pixel_size*pixel_dim.y,pixel_size*pixel_dim.z};
		instance.h_coord_offset[0] = {0,0,0};
		float3 pixel_middle;
		float3 mid_point = {instance.h_coord_dim[0].x/2,instance.h_coord_dim[0].y/2,instance.h_coord_dim[0].z/2};
		float d;
		float sum =0;
		for (int i = 0;i<pixel_dim.x;i++){
			for (int j=0;j<pixel_dim.y;j++){
				for (int k=0;k<pixel_dim.z;k++){
					pixel_middle = {((float)i + 0.5f)*pixel_size,((float)j + 0.5f)*pixel_size,((float)k + 0.5f)*pixel_size};
					d = length(mid_point-pixel_middle);
					instance.h_data[i + j*pixel_dim.x + k*pixel_dim.x*pixel_dim.y] = 1/sqrtf(powf((2*M_PI),3.0f))*expf(-0.5f*d*d);
					sum += instance.h_data[i + j*pixel_dim.x + k*pixel_dim.x*pixel_dim.y];
				}
			}
		}
		CudaSafeCall(cudaMemcpy(instance.d_coord_dim,instance.h_coord_dim,sizeof(*instance.d_coord_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_coord_offset,instance.h_coord_offset,sizeof(*instance.d_coord_offset),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_dim,instance.h_pixel_dim,sizeof(*instance.d_pixel_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_size,instance.h_pixel_size,sizeof(*instance.d_pixel_size),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_data,instance.h_data,vol(pixel_dim)*sizeof(*instance.d_data),cudaMemcpyHostToDevice));

		_instance_registry[gpu_index] = instance;
	}
}

float p(const float& x, const float& a){
	return 1.0f/powf(a,8.0f)*powf((powf(x,2.0f)-powf(a,2.0f)),2.0f)*powf((powf(x,2.0f)-powf(0.5*a,2.0f)),2.0f);
}

float p_xyz(const float3 &xyz, const float& a){
	return p(xyz.x,a)*p(xyz.y,a)*p(xyz.z,a);
}

void Density::createInstanceTestFunction(const float & a, const uint & pixels, const int &gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		Density_Register instance;
		float pixel_size =  (2*a)/pixels;
		instance.h_pixel_size = (float *) malloc(sizeof(*instance.h_pixel_size));
		instance.h_coord_dim = (float3 *)malloc(sizeof(*instance.h_coord_dim));
		instance.h_coord_offset = (float3 *)malloc(sizeof(*instance.h_coord_offset));
		instance.h_pixel_dim = (uint3 *)malloc(sizeof(*instance.h_pixel_dim));
		instance.h_external_memory = (size_t *) malloc(sizeof(*instance.h_external_memory));
		instance.h_coord_offset[0] = {0,0,0};
		instance.h_coord_dim[0] = {2*a,2*a,2*a};
		instance.h_pixel_dim[0] = {pixels,pixels,pixels};
		instance.h_pixel_size[0] = pixel_size;
		instance.h_data = (TDensity *) malloc(vol(instance.h_pixel_dim[0])*sizeof(*instance.h_data));
		instance.h_crop_pixel_offset = (uint3 *)malloc(sizeof(*instance.h_crop_pixel_offset));
		//no allocation of CPU for data
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_dim,sizeof(*instance.d_coord_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_offset,sizeof(*instance.d_coord_offset)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_dim,sizeof(*instance.d_pixel_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_size,sizeof(*instance.d_pixel_size)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_data,vol(instance.h_pixel_dim[0])*sizeof(*instance.d_data)));
		float3 pixel_middle;
		float3 origin = {a,a,a};
		for (int i = 0;i<pixels;i++){
			for (int j=0;j<pixels;j++){
				for (int k=0;k<pixels;k++){
					pixel_middle = {((float)i + 0.5f)*pixel_size,((float)j + 0.5f)*pixel_size,((float)k + 0.5f)*pixel_size};
					instance.h_data[i + j*pixels + k*pixels*pixels] = p_xyz(pixel_middle-origin,a);
				}
			}
		}
		CudaSafeCall(cudaMemcpy(instance.d_coord_dim,instance.h_coord_dim,sizeof(*instance.d_coord_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_coord_offset,instance.h_coord_offset,sizeof(*instance.d_coord_offset),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_dim,instance.h_pixel_dim,sizeof(*instance.d_pixel_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_size,instance.h_pixel_size,sizeof(*instance.d_pixel_size),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_data,instance.h_data,vol(instance.h_pixel_dim[0])*sizeof(*instance.d_data),cudaMemcpyHostToDevice));
		_instance_registry[gpu_index] = instance;
	}
}

void Density::createInstanceMask(Density *const& source, const float& threshold, const int &gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		Density_Register * source_instance = &source->_instance_registry[gpu_index];
		Density_Register instance;
		instance.h_pixel_size = (float *) malloc(sizeof(*instance.h_pixel_size));
		instance.h_coord_dim = (float3 *)malloc(sizeof(*instance.h_coord_dim));
		instance.h_coord_offset = (float3 *)malloc(sizeof(*instance.h_coord_offset));
		instance.h_pixel_dim = (uint3 *)malloc(sizeof(*instance.h_pixel_dim));
		instance.h_external_memory = (size_t *) malloc(sizeof(*instance.h_external_memory));
		
		instance.h_coord_offset[0] = {0,0,0};
		instance.h_pixel_dim[0] = source_instance->h_pixel_dim[0];
		instance.h_pixel_size[0] = source_instance->h_pixel_size[0];
		instance.h_coord_dim[0] = {instance.h_pixel_dim->x*source_instance->h_pixel_size[0],instance.h_pixel_dim->y*source_instance->h_pixel_size[0],instance.h_pixel_dim->z*source_instance->h_pixel_size[0]};
		instance.h_data = (TDensity *) malloc(vol(instance.h_pixel_dim[0])*sizeof(*instance.h_data));
		instance.h_crop_pixel_offset = (uint3 *)malloc(sizeof(*instance.h_crop_pixel_offset));
		//no allocation of CPU for data
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_dim,sizeof(*instance.d_coord_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_offset,sizeof(*instance.d_coord_offset)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_dim,sizeof(*instance.d_pixel_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_size,sizeof(*instance.d_pixel_size)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_data,vol(instance.h_pixel_dim[0])*sizeof(*instance.d_data)));
		
		for (int i = 0; i < vol(instance.h_pixel_dim[0]) ; i++) instance.h_data[i] = (TDensity) 0.0;
		for (uint k = 0;k<instance.h_pixel_dim[0].z;k++){
			for (uint j=0;j<instance.h_pixel_dim[0].y;j++){
				for (uint i=0;i<instance.h_pixel_dim[0].x;i++){
					if (source_instance->h_data[Density::index_to_linear({i,j,k},instance.h_pixel_dim)] >= threshold){
						instance.h_data[Density::index_to_linear({i,j,k},instance.h_pixel_dim)] = 1.0f;
					}
				}
			}
		}
		CudaSafeCall(cudaMemcpy(instance.d_coord_dim,instance.h_coord_dim,sizeof(*instance.d_coord_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_coord_offset,instance.h_coord_offset,sizeof(*instance.d_coord_offset),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_dim,instance.h_pixel_dim,sizeof(*instance.d_pixel_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_size,instance.h_pixel_size,sizeof(*instance.d_pixel_size),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_data,instance.h_data,vol(instance.h_pixel_dim[0])*sizeof(*instance.d_data),cudaMemcpyHostToDevice));
		_instance_registry[gpu_index] = instance;
	}
	
}

void Density::createInstanceCutAround(Density *const& source, const float3 & position, const uint3 & pixdim, const int &gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		Density_Register * source_instance = &source->_instance_registry[gpu_index];
		Density_Register instance;
		instance.h_pixel_size = (float *) malloc(sizeof(*instance.h_pixel_size));
		instance.h_coord_dim = (float3 *)malloc(sizeof(*instance.h_coord_dim));
		instance.h_coord_offset = (float3 *)malloc(sizeof(*instance.h_coord_offset));
		instance.h_pixel_dim = (uint3 *)malloc(sizeof(*instance.h_pixel_dim));
		instance.h_external_memory = (size_t *) malloc(sizeof(*instance.h_external_memory));
		//in float pixels!	
		uint3 cut_at = {(uint) std::round(position.x),(uint) std::round(position.y),(uint) std::round(position.z)};
		uint3 mid = { pixdim.x/2, pixdim.y/2, pixdim.z/2}; 
		instance.h_coord_offset[0] = {0,0,0};
		instance.h_pixel_dim[0] = pixdim;
		instance.h_pixel_size[0] = source_instance->h_pixel_size[0];
		instance.h_coord_dim[0] = {instance.h_pixel_dim->x*source_instance->h_pixel_size[0],instance.h_pixel_dim->y*source_instance->h_pixel_size[0],instance.h_pixel_dim->z*source_instance->h_pixel_size[0]};
		instance.h_data = (TDensity *) malloc(vol(instance.h_pixel_dim[0])*sizeof(*instance.h_data));
		instance.h_crop_pixel_offset = (uint3 *)malloc(sizeof(*instance.h_crop_pixel_offset));
		//no allocation of CPU for data
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_dim,sizeof(*instance.d_coord_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_offset,sizeof(*instance.d_coord_offset)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_dim,sizeof(*instance.d_pixel_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_size,sizeof(*instance.d_pixel_size)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_data,vol(instance.h_pixel_dim[0])*sizeof(*instance.d_data)));
		uint3 pix;
		for (uint k = 0;k<instance.h_pixel_dim[0].z;k++){
			for (uint j=0;j<instance.h_pixel_dim[0].y;j++){
				for (uint i=0;i<instance.h_pixel_dim[0].x;i++){
					pix = {i-mid.x+cut_at.x,j-mid.y+cut_at.y,k-mid.z+cut_at.z};
					if (pix.x < source_instance->h_pixel_dim[0].x && pix.y < source_instance->h_pixel_dim[0].y && pix.z < source_instance->h_pixel_dim[0].z)	  {
						instance.h_data[Density::index_to_linear({i,j,k},instance.h_pixel_dim)] =
						source_instance->h_data[Density::index_to_linear({i-mid.x+cut_at.x,j-mid.y+cut_at.y,k-mid.z+cut_at.z},source_instance->h_pixel_dim)];
			        	} else {
						instance.h_data[Density::index_to_linear({i,j,k},instance.h_pixel_dim)] = 0.0f;
					}
				}
			}
		}
		CudaSafeCall(cudaMemcpy(instance.d_coord_dim,instance.h_coord_dim,sizeof(*instance.d_coord_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_coord_offset,instance.h_coord_offset,sizeof(*instance.d_coord_offset),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_dim,instance.h_pixel_dim,sizeof(*instance.d_pixel_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_size,instance.h_pixel_size,sizeof(*instance.d_pixel_size),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_data,instance.h_data,vol(instance.h_pixel_dim[0])*sizeof(*instance.d_data),cudaMemcpyHostToDevice));
		_instance_registry[gpu_index] = instance;
	}
}

void Density::createInstanceCut(Density *const& source, const float3 & offset, const float3 & part, const int &gpu_index){
	//offset < part <1.0, offset+part < 1.0
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		Density_Register * source_instance = &source->_instance_registry[gpu_index];
		Density_Register instance;
		instance.h_pixel_size = (float *) malloc(sizeof(*instance.h_pixel_size));
		instance.h_coord_dim = (float3 *)malloc(sizeof(*instance.h_coord_dim));
		instance.h_coord_offset = (float3 *)malloc(sizeof(*instance.h_coord_offset));
		instance.h_pixel_dim = (uint3 *)malloc(sizeof(*instance.h_pixel_dim));
		instance.h_external_memory = (size_t *) malloc(sizeof(*instance.h_external_memory));

		float source_pixel_size = source_instance->h_pixel_size[0];
		float3 source_dim = source_instance->h_coord_dim[0];
		float3 source_offset = {offset.x*source_dim.x,offset.y*source_dim.y,offset.z*source_dim.z};
		float3 source_part = {(offset.x + part.x)*source_dim.x,(offset.y + part.y)*source_dim.y,(offset.z + part.z)*source_dim.z};
		uint3 source_from_pixel = {static_cast<uint>(source_offset.x/source_pixel_size),
								   static_cast<uint>(source_offset.y/source_pixel_size),
							       static_cast<uint>(source_offset.z/source_pixel_size)};
		uint3 source_to_pixel =   {static_cast<uint>(source_part.x/source_pixel_size),
								   static_cast<uint>(source_part.y/source_pixel_size),
								   static_cast<uint>(source_part.z/source_pixel_size)};

		instance.h_coord_offset[0] = {0,0,0};
		instance.h_pixel_dim[0] = source_to_pixel - source_from_pixel;
		instance.h_pixel_size[0] = source_pixel_size;
		instance.h_coord_dim[0] = {instance.h_pixel_dim->x*source_pixel_size,instance.h_pixel_dim->y*source_pixel_size,instance.h_pixel_dim->z*source_pixel_size};
		instance.h_data = (TDensity *) malloc(vol(instance.h_pixel_dim[0])*sizeof(*instance.h_data));
		instance.h_crop_pixel_offset = (uint3 *)malloc(sizeof(*instance.h_crop_pixel_offset));
		//no allocation of CPU for data
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_dim,sizeof(*instance.d_coord_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_offset,sizeof(*instance.d_coord_offset)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_dim,sizeof(*instance.d_pixel_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_size,sizeof(*instance.d_pixel_size)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_data,vol(instance.h_pixel_dim[0])*sizeof(*instance.d_data)));
		for (uint k = 0;k<instance.h_pixel_dim[0].z;k++){
			for (uint j=0;j<instance.h_pixel_dim[0].y;j++){
				for (uint i=0;i<instance.h_pixel_dim[0].x;i++){
					instance.h_data[Density::index_to_linear({i,j,k},instance.h_pixel_dim)] =
							source_instance->h_data[Density::index_to_linear({i+source_from_pixel.x,j+source_from_pixel.y,k+source_from_pixel.z},source_instance->h_pixel_dim)];
				}
			}
		}
		CudaSafeCall(cudaMemcpy(instance.d_coord_dim,instance.h_coord_dim,sizeof(*instance.d_coord_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_coord_offset,instance.h_coord_offset,sizeof(*instance.d_coord_offset),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_dim,instance.h_pixel_dim,sizeof(*instance.d_pixel_dim),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_pixel_size,instance.h_pixel_size,sizeof(*instance.d_pixel_size),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_data,instance.h_data,vol(instance.h_pixel_dim[0])*sizeof(*instance.d_data),cudaMemcpyHostToDevice));
		_instance_registry[gpu_index] = instance;
	}
}

void Density::createInstance(const int &gpu_index,const size_t &external_memory){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		Density_Register instance;
		instance.h_pixel_size = (float *) malloc(sizeof(*instance.h_pixel_size));
		instance.h_coord_dim = (float3 *)malloc(sizeof(*instance.h_coord_dim));
		instance.h_coord_offset = (float3 *)malloc(sizeof(*instance.h_coord_offset));
		instance.h_pixel_dim = (uint3 *)malloc(sizeof(*instance.h_pixel_dim));
		instance.h_external_memory = (size_t *) malloc(sizeof(*instance.h_external_memory));
		instance.h_external_memory[0] = external_memory;
		instance.h_data = (TDensity *) malloc(external_memory*sizeof(*instance.h_data));
		instance.h_crop_pixel_offset = (uint3 *)malloc(sizeof(*instance.h_crop_pixel_offset));
		//no allocation of CPU for data
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_dim,sizeof(*instance.d_coord_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_coord_offset,sizeof(*instance.d_coord_offset)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_dim,sizeof(*instance.d_pixel_dim)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_pixel_size,sizeof(*instance.d_pixel_size)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_data,external_memory*sizeof(*instance.d_data)));
		_instance_registry[gpu_index] = instance;
	}
}

__global__ 
void test_texture_kernel(float * output,cudaTextureObject_t texobj, uint3 pixel_dim)
{	
	int tidx = threadIdx.x + blockIdx.x * blockDim.x;
    	int tidy = threadIdx.y + blockIdx.y * blockDim.y;
    	int tidz = threadIdx.z + blockIdx.z * blockDim.z;
	
	if ( tidx >= pixel_dim.x || tidy >= pixel_dim.y || tidz >= pixel_dim.z )
		return;
    	float x = float(tidx)+0.5f;
    	float y = float(tidy)+0.5f;
    	float z = float(tidz)+0.5f;

    	size_t oidx = tidx + tidy*pixel_dim.x + tidz*pixel_dim.x*pixel_dim.y;
    	output[oidx] = tex3D<float>(texobj, x/__uint2float_rd(pixel_dim.x), y/__uint2float_rd(pixel_dim.y), z/__uint2float_rd(pixel_dim.z));
	//if (output[oidx] > 45.5f)
	//	printf("%i %f %f %f --> %f\n",oidx,x,y,z,output[oidx]);
}

void Density::testTexture(int gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	if (instance->textureObj == nullptr){
		createTexture(gpu_index);
	}
	uint3 pixel_dim = instance->h_pixel_dim[0];
	float * h_output = (float *) malloc(vol(pixel_dim)*sizeof(float));
	float * d_output;
	uint3 block_dim = {8,8,8};
	uint3 grid_dim = {pixel_dim.x/block_dim.x + 1, pixel_dim.y/block_dim.y + 1, pixel_dim.z/block_dim.z + 1};
	CudaSafeCall(cudaMalloc((void **)&d_output,sizeof(float)*vol(pixel_dim)));	
	test_texture_kernel<<<grid_dim,block_dim>>>(d_output,instance->textureObj[0],pixel_dim);
	CudaCheckError();
	CudaSafeCall(cudaMemcpy(h_output,d_output,vol(pixel_dim)*sizeof(float),cudaMemcpyDeviceToHost));
	cudaDeviceSynchronize();
	printf("List of failures start.\n");	
	for (int i=0;i<vol(pixel_dim);++i){
		if (std::abs(h_output[i]-instance->h_data[i])>1e-5){
			printf("FAILURE: %i %.10e\n",i,h_output[i]-instance->h_data[i]);
		}
	}
	printf("List of failures end.\n");	
}


void Density::createTexture(int gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	uint3 pixel_dim = instance->h_pixel_dim[0];
	CudaSafeCall(cudaMemcpy(instance->h_data,instance->d_data,vol(instance->h_pixel_dim[0])*sizeof(*instance->h_data),cudaMemcpyDeviceToHost));
	uint width = sizeof(float) * pixel_dim.x;

	float * h_volumeMem;
	cudaExtent volumeSizeBytes = make_cudaExtent(width, pixel_dim.y,pixel_dim.z);
	cudaPitchedPtr d_volumeMem;
	CudaSafeCall(cudaMalloc3D(&d_volumeMem, volumeSizeBytes));

	size_t byte_size = d_volumeMem.pitch * pixel_dim.y * pixel_dim.z;
	h_volumeMem = (float *)malloc(byte_size);
	for (int i = 0; i < byte_size/sizeof(*h_volumeMem); ++i) h_volumeMem[i] = 0.0f;
	size_t pitch = d_volumeMem.pitch;
	size_t slicePitch = pitch * pixel_dim.y;
	for (int z = 0; z < pixel_dim.z; ++z) {
		char * slice = (char *) h_volumeMem + z * slicePitch;
		for (int y = 0; y < pixel_dim.y; ++y) {
				float * row = (float *)(slice + y * pitch);
				for (int x = 0; x < pixel_dim.x; ++x) {
					row[x] = instance->h_data[x + y*pixel_dim.x + z*pixel_dim.x*pixel_dim.y];
				}
		}
	}
	CudaSafeCall(cudaMemcpy(d_volumeMem.ptr, h_volumeMem, byte_size, cudaMemcpyHostToDevice));

	//cudaArray * d_volumeArray;
	cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();
	cudaExtent volumeSize = make_cudaExtent(pixel_dim.x, pixel_dim.y, pixel_dim.z);
	//CudaSafeCall( cudaMalloc3DArray(&d_volumeArray, &channelDesc, volumeSize) );
	CudaSafeCall( cudaMalloc3DArray(&instance->d_texture, &channelDesc, volumeSize) );

	cudaMemcpy3DParms copyParams = {0};
	copyParams.srcPtr = d_volumeMem;
	//copyParams.dstArray = instance->d_volumeArray;
	copyParams.dstArray = instance->d_texture;
	copyParams.extent = volumeSize;
	copyParams.kind = cudaMemcpyDeviceToDevice;
	CudaSafeCall( cudaMemcpy3D(&copyParams) );

	//cudaTextureObject_t textureObject = 0;
	cudaTextureObject_t texObj = 0;
	cudaResourceDesc    texRes;
	memset(&texRes, 0, sizeof(cudaResourceDesc));
	texRes.resType = cudaResourceTypeArray;
	texRes.res.array.array  = instance->d_texture;
	cudaTextureDesc texDescr;
	memset(&texDescr, 0, sizeof(cudaTextureDesc));
	texDescr.normalizedCoords = false;
	texDescr.filterMode = cudaFilterModeLinear;
	texDescr.addressMode[0] = cudaAddressModeBorder;
	texDescr.addressMode[1] = cudaAddressModeBorder;
	texDescr.addressMode[2] = cudaAddressModeBorder;
	texDescr.readMode = cudaReadModeElementType;

	if (instance->textureObj == nullptr){
		instance->textureObj = (cudaTextureObject_t *) malloc(sizeof(*instance->textureObj));
		cudaCreateTextureObject(&texObj, &texRes, &texDescr, NULL);
	} else {
		cudaDestroyTextureObject(instance->textureObj [0]);
		cudaCreateTextureObject(&texObj, &texRes, &texDescr, NULL);
	}
	instance->textureObj[0] = texObj;

	cudaDeviceSynchronize();
	cudaFree(d_volumeMem.ptr);
	free(h_volumeMem);
}

void Density::freeInstance(const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	if (instance->d_coord_dim != nullptr) cudaFree(instance->d_coord_dim);
	if (instance->d_coord_offset != nullptr) cudaFree(instance->d_coord_offset);
	if (instance->d_pixel_dim != nullptr) cudaFree(instance->d_pixel_dim);
	if (instance->d_data != nullptr) cudaFree(instance->d_data);
	if (instance->d_pixel_size != nullptr) cudaFree(instance->d_pixel_size);
	
	if (instance->h_crop_pixel_offset != nullptr) free(instance->h_crop_pixel_offset);
	if (instance->h_coord_dim != nullptr) free(instance->h_coord_dim);
	if (instance->h_pixel_dim != nullptr) free(instance->h_pixel_dim);
	if (instance->h_coord_offset != nullptr) free(instance->h_coord_offset);
	if (instance->h_data != nullptr) free(instance->h_data);
	if (instance->h_pixel_size != nullptr) free(instance->h_pixel_size);
	if (instance->h_external_memory != nullptr) free(instance->h_external_memory);
	
	auto it = _instance_registry.find(gpu_index);
	_instance_registry.erase(it);
}

void Density::freeAllInstances(){
	for (auto const &instance: _instance_registry){
		cudaFree(instance.second.d_coord_dim);
		cudaFree(instance.second.d_coord_offset);
		cudaFree(instance.second.d_pixel_dim);
		cudaFree(instance.second.d_data);
		cudaFree(instance.second.d_pixel_size);
		free(instance.second.h_crop_pixel_offset);
		free(instance.second.h_coord_dim);
		free(instance.second.h_pixel_dim);
		free(instance.second.h_coord_offset);
		free(instance.second.h_data);
		free(instance.second.h_pixel_size);
		free(instance.second.h_external_memory);
	}
	_instance_registry.clear();
}

struct sum_threshold : public thrust::binary_function<TDensity,TDensity,TDensity>
{
	TDensity threshold;
    __device__
	TDensity operator()(const TDensity& u, const TDensity& v) const {
    	if (u < threshold && v < threshold)
    		return (TDensity) 0.0;
    	if (u < threshold)
    		return v;
    	if (v < threshold)
    		return u;
		return u+v;
	}
};

void Density::pixelVolume(const TDensity & threshold, float *const& h_result, float * const& d_result, const int &gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	thrust::device_ptr<TDensity> d_pixel_begin = thrust::device_pointer_cast(&instance->d_data[0]);
	thrust::device_ptr<TDensity> d_pixel_end = thrust::device_pointer_cast(&instance->d_data[vol(*instance->h_pixel_dim)]);
	sum_threshold binary_op;
	binary_op.threshold = threshold;
	h_result[0] = thrust::reduce(d_pixel_begin,d_pixel_end,0.0f,binary_op)*powf(instance->h_pixel_size[0],3.0f);
	CudaSafeCall(cudaMemcpy(d_result,h_result,sizeof(*d_result),cudaMemcpyHostToDevice));
}

void Density::writeLabelToMrc(const char * prefix, uint *const& d_labels, const uint& label,const uint &log_2_dim, const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	uint padding_vol = ipow(log_2_dim,3);
	uint * h_labels = (uint *) malloc(padding_vol*sizeof(*d_labels));
	CudaSafeCall(cudaMemcpy(h_labels,d_labels,padding_vol*sizeof(*d_labels),cudaMemcpyDeviceToHost));
	cudaDeviceSynchronize();
	CMrcReader reader;
	std::string name = std::string(prefix) + std::string("_") + std::to_string(label) + std::string(".mrc");
	reader.setFileName(name.c_str());
	Density_Register * density_instance = &_instance_registry[gpu_index];
	CudaSafeCall(cudaMemcpy(density_instance->h_coord_dim,density_instance->d_coord_dim,sizeof(*density_instance->h_coord_dim),cudaMemcpyDeviceToHost));
	CudaSafeCall(cudaMemcpy(density_instance->h_coord_offset,density_instance->d_coord_offset,sizeof(*density_instance->h_coord_offset),cudaMemcpyDeviceToHost));
	CudaSafeCall(cudaMemcpy(density_instance->h_pixel_size,density_instance->d_pixel_size,sizeof(*density_instance->h_pixel_size),cudaMemcpyDeviceToHost));
	CudaSafeCall(cudaMemcpy(density_instance->h_pixel_dim,density_instance->d_pixel_dim,sizeof(*density_instance->h_pixel_dim),cudaMemcpyDeviceToHost));
	reader.header.cella.x = density_instance->h_coord_dim[0].x;
	reader.header.cella.y = density_instance->h_coord_dim[0].y;
	reader.header.cella.z = density_instance->h_coord_dim[0].z;
	CudaSafeCall(cudaMemcpy(density_instance->h_data,density_instance->d_data,vol(density_instance->h_pixel_dim[0])*sizeof(*density_instance->h_data),cudaMemcpyDeviceToHost));
	reader.header.origin.x = density_instance->h_coord_offset[0].x;
	reader.header.origin.y = density_instance->h_coord_offset[0].y;
	reader.header.origin.z = density_instance->h_coord_offset[0].z;
	reader.header.nx = density_instance->h_pixel_dim[0].x;
	reader.header.ny = density_instance->h_pixel_dim[0].y;
	reader.header.nz = density_instance->h_pixel_dim[0].z;
	reader.header.mx = density_instance->h_pixel_dim[0].x;
	reader.header.my = density_instance->h_pixel_dim[0].y;
	reader.header.mz = density_instance->h_pixel_dim[0].z;
	TDensity * h_label_data = (TDensity *) malloc(sizeof(*h_label_data)*vol(density_instance->h_pixel_dim[0]));

	uint3 pixel_index;
	uint density_linear_index;
	uint label_linear_index;
	uint3 label_pixel_dim = {log_2_dim,log_2_dim,log_2_dim};
	for (uint x=0; x < reader.header.nx;++x){
		for (uint y=0; y < reader.header.ny;++y){
			for (uint z=0; z < reader.header.nz;++z){
				pixel_index = {x,y,z};
				density_linear_index = Density::index_to_linear(pixel_index,density_instance->h_pixel_dim);
				label_linear_index = Density::index_to_linear(pixel_index,&label_pixel_dim);
				if (h_labels[label_linear_index] == label){
					h_label_data[density_linear_index] = density_instance->h_data[density_linear_index];
				} else {
					h_label_data[density_linear_index] = 0.0f;
				}
			}
		}
	}
	reader.data = h_label_data;
	reader.write();
	printf("Wrote to file %s ...\n coord dim %f %f %f\n pixel dim %u %u %u ...\n",name.c_str(),
					reader.header.cella.x,reader.header.cella.y,reader.header.cella.z,
			density_instance->h_pixel_dim[0].x,density_instance->h_pixel_dim[0].y,density_instance->h_pixel_dim[0].z);
	free(h_labels);
	free(h_label_data);
}

struct is_greater
{
	float threshold;
	__device__
	bool operator()(const float &x)
	{
		return x > threshold;
	}
};

#include<thrust/count.h>

void Density::pixelsAbove(const TDensity & threshold, int *const& h_result, int *const& d_result, const int &gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	thrust::device_ptr<TDensity> d_pixel_begin = thrust::device_pointer_cast(&instance->d_data[0]);
	thrust::device_ptr<TDensity> d_pixel_end = thrust::device_pointer_cast(&instance->d_data[vol(*instance->h_pixel_dim)]);
	h_result[0] = thrust::reduce(d_pixel_begin,d_pixel_end,0.0f)*powf(instance->h_pixel_size[0],3.0f);
	is_greater predicate;
	predicate.threshold = threshold;
	h_result[0] = thrust::count_if(d_pixel_begin, d_pixel_end, predicate);
	CudaSafeCall(cudaMemcpy(d_result,h_result,sizeof(*d_result),cudaMemcpyHostToDevice));
	cudaDeviceSynchronize();
}

struct translate_float_neg : public thrust::unary_function<TDensity,TDensity>
{
	TDensity * d_shift;
	TDensity h_threshold;
    __device__
	TDensity operator()(const TDensity& value) const {
    	if (value > h_threshold)
    		return value - d_shift[0];
    	else
			return (TDensity) 0.0f;
	}
};

void Density::shiftBy(TDensity * const & d_shift_value, const TDensity & threshold, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	translate_float_neg unary_function_shift;
	unary_function_shift.d_shift = d_shift_value;
	unary_function_shift.h_threshold = threshold;
	thrust::device_ptr<TDensity> d_pixel_begin = thrust::device_pointer_cast(&instance->d_data[0]);
	thrust::device_ptr<TDensity> d_pixel_end = thrust::device_pointer_cast(&instance->d_data[vol(*instance->h_pixel_dim)]);
	thrust::transform(d_pixel_begin,d_pixel_end,d_pixel_begin,unary_function_shift);
	cudaDeviceSynchronize();
}
void Density::mask(const float & threshold, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	for (int i = 0; i < vol(instance->h_pixel_dim[0]) ; i++){
		if (instance->h_data[i] < threshold)
			instance->h_data[i] = (TDensity) 0.0;
	}
	CudaSafeCall(cudaMemcpy(instance->d_data,instance->h_data,vol(instance->h_pixel_dim[0])*sizeof(*instance->h_data),cudaMemcpyHostToDevice));
}

void Density::flip(const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	TDensity * h_flip = (TDensity *) malloc(vol(instance->h_pixel_dim[0])*sizeof(*h_flip));
	for (int i = 0; i < vol(instance->h_pixel_dim[0]) ; i++) h_flip[i] = (TDensity) 0.0;
	for (uint k = 0; k < instance->h_pixel_dim->z; k++){
		for (uint j=0; j < instance->h_pixel_dim->y; j++){
			for (uint i=0; i < instance->h_pixel_dim->x; i++){
			    h_flip[Density::index_to_linear({i,j,k}, instance->h_pixel_dim)] =
					instance->h_data[Density::index_to_linear({instance->h_pixel_dim->x - i - 1, instance->h_pixel_dim->y - j - 1, instance->h_pixel_dim->z - k - 1 }, instance->h_pixel_dim)];
			}
		}
	}
	CudaSafeCall(cudaMemcpy(instance->d_data,h_flip,vol(instance->h_pixel_dim[0])*sizeof(*h_flip),cudaMemcpyHostToDevice));
	free(instance->h_data);
	instance->h_data = h_flip;
}

void Density::prepareQueryForCam(const TDensity & threshold, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	uint pixel_vol_total = vol(*instance->h_pixel_dim);
	float pixel_size = *instance->h_pixel_size;

	float * h_vol = (float *) malloc(sizeof(*h_vol));
	float * d_vol;
	CudaSafeCall(cudaMalloc((void **)&d_vol,sizeof(*d_vol)));
	int * h_num = (int *) malloc(sizeof(*h_num));
	int * d_num;
	CudaSafeCall(cudaMalloc((void **)&d_num,sizeof(*d_num)));
	float * h_shift = (float *) malloc(sizeof(*h_shift));
	float * d_shift;
	CudaSafeCall(cudaMalloc((void **)&d_shift,sizeof(*d_shift)));

	h_vol[0] = 0.0f;
	CudaSafeCall(cudaMemcpy(d_vol,h_vol,sizeof(*h_vol),cudaMemcpyHostToDevice));
	pixelVolume(threshold,h_vol,d_vol,gpu_index);
	pixelsAbove(threshold,h_num,d_num,gpu_index);
	//shift so that the relevant parts add up to 0
	h_shift[0] = (h_vol[0])/(powf(pixel_size,3.0f)*(float)h_num[0]);
	CudaSafeCall(cudaMemcpy(d_shift,h_shift,sizeof(*d_shift),cudaMemcpyHostToDevice));
	cudaDeviceSynchronize();
	shiftBy(d_shift, threshold, gpu_index);
	cudaDeviceSynchronize();

	cudaFree(d_vol);
	cudaFree(d_num);
	cudaFree(d_shift);
	free(h_shift);
	free(h_num);
	free(h_vol);
}

void Density::setBounds(const float3 &coord_dim, const float &pixel_size, const int &gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Density_Register * instance = &_instance_registry[gpu_index];
	assert(coord_dim.x > 0 && coord_dim.y > 0 && coord_dim.z > 0);
	assert(pixel_size > 0);
	uint3 pixel_dim;
	//not all pixel sizes fit
	float3 corrected_coord_dim;
	pixel_dim.x = (uint) ceil(coord_dim.x/pixel_size);
	pixel_dim.y = (uint) ceil(coord_dim.y/pixel_size);
	pixel_dim.z = (uint) ceil(coord_dim.z/pixel_size);
	//in case pixel_size is bad, closest value that will result coord_dim will be chosen
	corrected_coord_dim.x = pixel_dim.x*pixel_size;
	corrected_coord_dim.y = pixel_dim.y*pixel_size;
	corrected_coord_dim.z = pixel_dim.z*pixel_size;
	instance->h_pixel_size[0] = pixel_size;
	instance->h_coord_dim[0] = corrected_coord_dim;
	instance->h_coord_offset[0] = {0,0,0};
	instance->h_pixel_dim[0] = pixel_dim;
	CudaSafeCall(cudaMemcpy(instance->d_coord_dim,instance->h_coord_dim,sizeof(*instance->d_coord_dim),cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMemcpy(instance->d_pixel_dim,instance->h_pixel_dim,sizeof(*instance->d_pixel_dim),cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMemcpy(instance->d_coord_offset,instance->h_coord_offset,sizeof(*instance->d_coord_offset),cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMemcpy(instance->d_pixel_size,instance->h_pixel_size,sizeof(*instance->h_pixel_size),cudaMemcpyHostToDevice));
}

struct min_functor: public thrust::binary_function<thrust::tuple<uint,float>,thrust::tuple<uint,float>,thrust::tuple<uint,float>>
{
	float threshold;
    __device__
	thrust::tuple<uint,float> operator()(const thrust::tuple<uint,float>& u, const thrust::tuple<uint,float>& v) const
        {
    	if (thrust::get<1>(u) < threshold){
    		return v;
    	} else {
    		if (thrust::get<1>(v) < threshold) return u;
    		return thrust::get<0>(u) < thrust::get<0>(v) ? u : v;     // z components
    	}
   }
};

void Density::toMrc(const char * mrc_path, const uint &padding, int gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	CMrcReader reader;
	reader.setFileName(mrc_path);
	Density_Register * density_instance = &_instance_registry[gpu_index];
	CudaSafeCall(cudaMemcpy(density_instance->h_coord_dim,density_instance->d_coord_dim,sizeof(*density_instance->h_coord_dim),cudaMemcpyDeviceToHost));
	CudaSafeCall(cudaMemcpy(density_instance->h_coord_offset,density_instance->d_coord_offset,sizeof(*density_instance->h_coord_offset),cudaMemcpyDeviceToHost));
	CudaSafeCall(cudaMemcpy(density_instance->h_pixel_size,density_instance->d_pixel_size,sizeof(*density_instance->h_pixel_size),cudaMemcpyDeviceToHost));
	CudaSafeCall(cudaMemcpy(density_instance->h_pixel_dim,density_instance->d_pixel_dim,sizeof(*density_instance->h_pixel_dim),cudaMemcpyDeviceToHost));
	reader.header.cella.x = density_instance->h_coord_dim[0].x + 2*padding*density_instance->h_pixel_size[0];
	reader.header.cella.y = density_instance->h_coord_dim[0].y + 2*padding*density_instance->h_pixel_size[0];
	reader.header.cella.z = density_instance->h_coord_dim[0].z + 2*padding*density_instance->h_pixel_size[0];
	CudaSafeCall(cudaMemcpy(density_instance->h_data,density_instance->d_data,vol(density_instance->h_pixel_dim[0])*sizeof(*density_instance->h_data),cudaMemcpyDeviceToHost));
	size_t pixel_vol_p = (density_instance->h_pixel_dim[0].x + 2*padding)*(density_instance->h_pixel_dim[0].y + 2*padding)*(density_instance->h_pixel_dim[0].z + 2*padding);
	reader.header.origin.x = density_instance->h_coord_offset[0].x;
	reader.header.origin.y = density_instance->h_coord_offset[0].y;
	reader.header.origin.z = density_instance->h_coord_offset[0].z;
	reader.header.nx = density_instance->h_pixel_dim[0].x + 2*padding;
	reader.header.ny = density_instance->h_pixel_dim[0].y + 2*padding;
	reader.header.nz = density_instance->h_pixel_dim[0].z + 2*padding;
	reader.header.mx = density_instance->h_pixel_dim[0].x + 2*padding;
	reader.header.my = density_instance->h_pixel_dim[0].y + 2*padding;
	reader.header.mz = density_instance->h_pixel_dim[0].z + 2*padding;
	if (padding ==0){
		if (density_instance->h_data != nullptr)
			free(density_instance->h_data);
		density_instance->h_data = (TDensity *) malloc(vol(density_instance->h_pixel_dim[0])*sizeof(*density_instance->h_data));
		CudaSafeCall(cudaMemcpy(density_instance->h_data,density_instance->d_data,vol(density_instance->h_pixel_dim[0])*sizeof(*density_instance->h_data),cudaMemcpyDeviceToHost));
		reader.data = density_instance->h_data;
		reader.write();
	} else {
		reader.data = (TDensity *) malloc(pixel_vol_p*sizeof(*density_instance->h_data));
		uint3 pixel_dim_padding = {density_instance->h_pixel_dim[0].x + 2*padding,
								   density_instance->h_pixel_dim[0].y + 2*padding,
								   density_instance->h_pixel_dim[0].z + 2*padding};
		for (int i = 0; i< pixel_vol_p; i++) reader.data[i] = (TDensity) 0.0;
		for (uint k = 0;k<density_instance->h_pixel_dim[0].z;k++){
			for (uint j=0;j<density_instance->h_pixel_dim[0].y;j++){
				for (uint i=0;i<density_instance->h_pixel_dim[0].x;i++){
					reader.data[Density::index_to_linear({i+padding,j+padding,k+padding},&pixel_dim_padding)] =
					density_instance->h_data[Density::index_to_linear({i,j,k},density_instance->h_pixel_dim)];
				}
			}
		}
		reader.write();
	}
	printf("Wrote to file %s ...\n coord dim %f %f %f\n pixel dim %u %u %u ...\n",mrc_path,
					reader.header.cella.x,reader.header.cella.y,reader.header.cella.z,
			density_instance->h_pixel_dim[0].x,density_instance->h_pixel_dim[0].y,density_instance->h_pixel_dim[0].z);
}

void Density::toMrcRaw(const char * mrc_path, uint3 pixel_dim, float3 coord_dim, float *const& h_data){
	CMrcReader reader;
	reader.setFileName(mrc_path);
	reader.header.cella.x = coord_dim.x;
	reader.header.cella.y = coord_dim.y;
	reader.header.cella.z = coord_dim.z;
	size_t pixel_vol_p = vol(pixel_dim);
	reader.header.origin.x = 0.0f;
	reader.header.origin.y = 0.0f;
	reader.header.origin.z = 0.0f;
	reader.header.nx = pixel_dim.x;
	reader.header.ny = pixel_dim.y;
	reader.header.nz = pixel_dim.z;
	reader.header.mx = pixel_dim.x;
	reader.header.my = pixel_dim.y;
	reader.header.mz = pixel_dim.z;
	reader.data = h_data;
	reader.write();
	printf("Wrote to file %s ...\n coord dim %f %f %f\n pixel dim %u %u %u ...\n",mrc_path,
			reader.header.cella.x,reader.header.cella.y,reader.header.cella.z,
			pixel_dim.x,pixel_dim.y,pixel_dim.z);
}

#ifdef DIFFMAP
int main(int argc, char ** argv){
	Density density;
	Density diffmap;
			finish = std::chrono::high_resolution_clock::now();
			elapsed = finish-start;
			std::cout << "Time:" << std::endl << elapsed.count() << std::endl;
			std::string diffmap_s(diffmap_filepath);
			std::string surface_path = diffmap_s.substr(0,diffmap_s.length()-4) + std::string("_surface.mrc");
			density.writeToMrcOnly(surface_path.c_str(),density._instance_registry[0].d_on_surface,density._instance_registry[0].h_on_surface_vol[0],0);
			diffmap.toMrc(diffmap_filepath,0,0);
		}
	}

}
#endif
#include <getopt.h>
#ifdef EMBED 
int main(int argc, char ** argv){
	int opt;
	Density target;
	Density query;
	char target_shape_filepath[PATH_MAX + 1];
	char target_filepath[PATH_MAX + 1];
	char query_filepath[PATH_MAX + 1];
	bool quat = false;
	float3 eulers;
	float4 q;

	if (argc == 7){
		realpath(argv[1],query_filepath);
		realpath(argv[2],target_shape_filepath);
		realpath(argv[3],target_filepath);
		float3 trans_shift = parse_float3_string(argv[4]);
		while(1)
        	{
        	        opt = getopt(argc, argv, "e:q:");
        	        if (opt == -1) break; /* end of list */
        	        switch(opt)
        	        {
        	        case 'e':
				eulers = parse_float3_string(optarg);
        	                break;
        	        case 'q':
        	                q = parse_float4_string(optarg);
				quat = true;
				break;
			}
		}
		if (file_exists(target_shape_filepath) && file_exists(query_filepath) ){
			target.createInstanceFromMrc(target_shape_filepath,0.0f,0);
			query.createInstanceFromMrc(query_filepath,0.0f,0);
			query.createTexture(0);
			if (!quat){
				query.embed(&target,trans_shift,eulers,0);
			} else {
				
				query.embed(&target,trans_shift,q,0);
			}
			target.toMrc(target_filepath,0,0);
		}
	}

}
#endif
#ifdef TRANSFORM
int main(int argc, char ** argv){
	Density density;
	Density target;
	if (argc == 5){
		char filepath[PATH_MAX + 1];
		char target_filepath[PATH_MAX + 1];
		realpath(argv[1],filepath);
		realpath(argv[2],target_filepath);
		float3 trans_shift = parse_float3_string(argv[3]);
		float3 eulers = parse_float3_string(argv[4]);
		if (file_exists(filepath)){
			density.createInstanceFromMrc(filepath,0.0f, 0);
			density.createTexture(0);
			target.createInstanceFromDensity(&density, 0);//a bit hacky but it works
			density.transformTo(&target,trans_shift,eulers,0);
			target.toMrc(target_filepath,0,0);
		}
	}

}
#endif

