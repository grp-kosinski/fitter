/*
 * CamScoreDescend.h
 *
 *  Created on: Sep 14, 2021
 *      Author: kkarius
 */

#ifndef CAMSCOREIMPROVED_H_
#define CAMSCOREIMPROVED_H_


#include<map>
#include<util.h>
#include<cuda_util_include/cutil_math.h>
#include<derived_atomic_functions.h>

#include<ChebychevNodes.h>
#include<McEwenNodes.h>
#include<Density.h>
#include<fstream>
#include<string>
#include<cufft.h>

struct CamFFTScan_Register {
	int * R;
	cufftComplex *d_f1, *d_f1sq;
};


class CamFFTScan {
public:
	void createInstance(const char * target_fn,const char * query_fn, const char * output_dir, float3 _tmax, float4 _qmax, uint3 rot_grid, float _query_threshold, const int& gpu_index);
	void genDistsAndAvgs(int * Rs, int num_gpus, const int& gpu_index);
	void genDistsAndAvgsOV(int * Rs, int num_gpus, const int& gpu_index);
	void initFFT(int *const& Rs, const int& gpu_index);
	void initFFTOV(int *const& Rs, const int& gpu_index);
	void calculateEntropy(float *const& h_entropies, uint3 rot_dim, const char * working_dir, const int& num_gpus    , const int& gpu_index);
	Density target;
	Density target_cut;
	Density mask;
	Density query;
	Density query_cut;
	Density f2_multi;
    Density m_multi;
	McEwenNodes rot_nodes;
	CamFFTScan(){};
	std::string output_dir;
        std::string ncc_basename;
        std::string ov_basename;
        std::string avg_basename;
	int R_total;
	float4 q_max;
	float3 t_max;
        uint3 cut_size;
        uint3 fft_size;
        uint3 ncc_offset;
        uint3 ncc_vol;
	float query_threshold;
	std::map<int, CamFFTScan_Register> _instance_registry;
        float f2avg;  
        float msum;
        float f2normsq;
	float rotavg;
	virtual ~CamFFTScan(){};
//	void createInstance(std::pair<float3,float3> search_volume, float d, float epsilon, int M, size_t *const& max_init_transform_vol_per_gpu, const int & trans_S, const int & rot_S, const int & gpu_index);
//	void descend(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes, size_t *const& max_transformations_per_gpu, const int & gpu_num, const int & gpu_index);	
//	static size_t initTransformationVolume(std::pair<float3,float3> search_volume, const float& d, const int& M);
//	void destroyInstance(const int & gpu_index);
//
//	std::map<int,CamScoreDescend_Register> _instance_registry;
//	//supposed to be the same on all instances
//	float d;
//	float epsilon;
//	int M;
//	int trans_S;
//	int rot_S;
//	std::pair<float3,float3> search_volume;
//	int init_transformations;
//	int translation_search_grid_size;
//	int rotation_search_grid_size;
};

#endif /* CAMSCOREIMPROVED_H_ */
