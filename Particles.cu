/*
 * CParticles.cpp
 *
 *  Created on: Feb 25, 2019
 *      Author: kkarius
 */

#include<Particles.h>
#include<float.h>
#include<thrust/device_ptr.h>

float3 Particles::volume(int gpu_index){
	float3 ret = {0,0,0};
	CudaSafeCall(cudaSetDevice(gpu_index));
	Particles_Register * instance = &_instance_registry[gpu_index];
	//always assume GPU is current
	CudaSafeCall(cudaMemcpy(instance->h_bounding_box,instance->d_bounding_box,sizeof(*instance->h_bounding_box),cudaMemcpyDeviceToHost));
	ret.x = instance->h_bounding_box[3] - instance->h_bounding_box[0];
	ret.y = instance->h_bounding_box[4] - instance->h_bounding_box[1];
	ret.z = instance->h_bounding_box[5] - instance->h_bounding_box[2];
	return ret;
}

void Particles::createInstance(int gpu_index,const size_t & external_memory){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		Particles_Register instance;
		instance.h_particle_count = (size_t *)malloc(sizeof(*instance.h_particle_count));
		instance.h_external_memory = (size_t *)malloc(sizeof(*instance.h_external_memory));
		instance.h_bounding_box = (float *)malloc(6*sizeof(*instance.h_bounding_box));
		CudaSafeCall(cudaMalloc((void **)&instance.d_bounding_box,6*sizeof(*instance.d_bounding_box)));
		CudaSafeCall(cudaMalloc((void **)&instance.d_particle_count, sizeof(*instance.d_particle_count)));
		instance.h_external_memory[0] = external_memory;
//		assert(instance.h_particle_count[0] != 0);
		CudaSafeCall(cudaMalloc((void **)&instance.d_data, external_memory*sizeof(*instance.d_data)));
		_instance_registry[gpu_index] = instance;
	}
}

void Particles::readFromPdb(PdbReader * const & reader, int gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Particles_Register * instance = &_instance_registry[gpu_index];
	reader->readCoords();
	instance->h_data = reader->_h_coordinates;
	instance->h_particle_count[0] = reader->atom_count;
	CudaSafeCall(cudaMemcpy(instance->d_data,instance->h_data,instance->h_particle_count[0]*sizeof(*instance->d_data),cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMemcpy(instance->d_particle_count,instance->h_particle_count,sizeof(*instance->h_particle_count),cudaMemcpyHostToDevice));
}

void Particles::freeInstance(const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Particles_Register * instance = &_instance_registry[gpu_index];
	cudaFree(instance->d_data);
	cudaFree(instance->d_bounding_box);
	cudaFree(instance->d_particle_count);
	free(instance->h_data);
	free(instance->h_bounding_box);
	free(instance->h_particle_count);
	free(instance->h_external_memory);
	_instance_registry.erase(gpu_index);
}

void Particles::freeAllInstances(){
	for (auto const &instance: _instance_registry){
		cudaFree(instance.second.d_data);
		cudaFree(instance.second.d_bounding_box);
		cudaFree(instance.second.d_particle_count);
		free(instance.second.h_data);
		free(instance.second.h_bounding_box);
		free(instance.second.h_particle_count);
		free(instance.second.h_external_memory);
	}
	_instance_registry.clear();
}

void Particles::createInstanceFromReader(int gpu_index,int reader_index, const size_t & external_memory){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		Particles_Register instance;
		PdbReader& reader = pdb_readers[reader_index];
		// //allocate
		instance.h_data = reader._h_coordinates;
		instance.h_particle_count = (size_t *)malloc(sizeof(*instance.h_particle_count));
		instance.h_external_memory = (size_t *)malloc(sizeof(*instance.h_external_memory));
		instance.h_particle_count[0] = reader.atom_count;
		instance.h_bounding_box = (float *)malloc(6*sizeof(*instance.h_bounding_box));
		CudaSafeCall(cudaMalloc((void **)&instance.d_bounding_box,6*sizeof(*instance.d_bounding_box)));
		CudaSafeCall(cudaMalloc((void **)&instance.d_particle_count, sizeof(*instance.d_particle_count)));
		if (external_memory == 0){
			CudaSafeCall(cudaMalloc((void **)&instance.d_data, instance.h_particle_count[0]*sizeof(*instance.d_data)));
		}
		else {
			CudaSafeCall(cudaMalloc((void **)&instance.d_data, external_memory*sizeof(*instance.d_data)));
		}
		instance.h_external_memory[0] = external_memory;
		//initialize
		CudaSafeCall(cudaMemcpy(instance.d_data,instance.h_data,instance.h_particle_count[0]*sizeof(*instance.d_data),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_particle_count,instance.h_particle_count,sizeof(*instance.h_particle_count),cudaMemcpyHostToDevice));
		_instance_registry[gpu_index] = instance;
	} 
	else {
		Particles_Register instance = _instance_registry[gpu_index];
		PdbReader& reader = pdb_readers[reader_index];
		instance.h_data = reader._h_coordinates;
		instance.h_particle_count[0] = reader.atom_count;
		assert(instance.h_particle_count[0] != 0);
		if (external_memory == 0){
			CudaSafeCall(cudaFree(instance.d_data));
			CudaSafeCall(cudaMalloc((void **)&instance.d_data, instance.h_particle_count[0]*sizeof(*instance.d_data)));
		}
		CudaSafeCall(cudaMemcpy(instance.d_data,instance.h_data,instance.h_particle_count[0]*sizeof(*instance.d_data),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMemcpy(instance.d_particle_count,instance.h_particle_count,sizeof(*instance.h_particle_count),cudaMemcpyHostToDevice));
	}
}

void Particles::getBounds(float *const& bounds,int gpu_index){
	for (int i=0;i<6;i++){bounds[i] = _instance_registry[gpu_index].h_bounding_box[i];}
}

void Particles::getParticleCount(size_t *const& count, int gpu_index){
	count[0] = _instance_registry[gpu_index].h_particle_count[0];
}

void Particles::find_bounds(int gpu_index){
	float eps = 0.05;
	float4 max = {FLT_MAX,FLT_MAX,FLT_MAX,0};
	float4 min = {0,0,0,0};
	Particles_Register  * instance = &_instance_registry[gpu_index];
	CudaSafeCall(cudaSetDevice(gpu_index));
	thrust::device_ptr<float4> d_coord_begin = thrust::device_pointer_cast(&instance->d_data[0]);
	thrust::device_ptr<float4> d_coord_end = thrust::device_pointer_cast(&instance->d_data[instance->h_particle_count[0]]);
	float4 min_coords = thrust::reduce(d_coord_begin,d_coord_end,max,min_float4());
	float4 max_coords = thrust::reduce(d_coord_begin,d_coord_end,min,max_float4());
	instance->h_bounding_box[0] = min_coords.x - eps;
	instance->h_bounding_box[1] = min_coords.y - eps;
	instance->h_bounding_box[2] = min_coords.z - eps;
	instance->h_bounding_box[3] = max_coords.x + eps;
	instance->h_bounding_box[4] = max_coords.y + eps;
	instance->h_bounding_box[5] = max_coords.z + eps;
	printf("Pdb bounds: %f %f %f - %f %f %f\n",min_coords.x,min_coords.y,min_coords.z,max_coords.x,max_coords.y,max_coords.z);
	CudaSafeCall(cudaMemcpy(instance->d_bounding_box,instance->h_bounding_box,6*sizeof(*instance->d_bounding_box),cudaMemcpyHostToDevice));
}

void Particles::writeTransformedPdb(const char * fname, const float3& rotation, const float3& translation, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	Particles_Register  * instance = &_instance_registry[gpu_index];
	//this doesn't really play with the gpu framework. careful
	if (pdb_readers.size() > 0){
		pdb_readers[0].writeTransformed(fname,rotation,translation);
	}
}

struct translate : public thrust::unary_function<float4,float4>
{
	float4 shift;
    __device__
	float4 operator()(const float4& coord) const {
    	return coord + shift;
	}
};

size_t Particles::particleCount(int gpu_index){
	Particles_Register * instance = &_instance_registry[gpu_index];
	return instance->h_particle_count[0];
}

void Particles::boundingBoxDimensions(float3 *& dimensions, int gpu_index){
	Particles_Register * instance = &_instance_registry[gpu_index];
	dimensions->x = instance->h_bounding_box[3] - instance->h_bounding_box[0];
	dimensions->y = instance->h_bounding_box[4] - instance->h_bounding_box[1];
	dimensions->z = instance->h_bounding_box[5] - instance->h_bounding_box[2];
}

int Particles::fromPdb(const char * file_path){
	//purely CPU
	PdbReader reader(file_path);
	reader.readCoords();
	pdb_readers.push_back(reader);
	return pdb_readers.size()-1;
}
