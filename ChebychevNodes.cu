/*
 * ChebychevNodes.cu
 *
 *  Created on: Mar 9, 2021
 *      Author: kkarius
 */

#include <ChebychevNodes.h>
#include <fstream>
#include <map>

int ChebychevNodes::get_node_dim(const float& diff,const uint& dim){
	double _max;
	double threshold = diff/dim;
	double * nodes; 
	int n = 2;
	_max = DBL_MAX;
	while (threshold < _max){
		nodes = (double *) malloc(n*sizeof(double));
		for (int i = 0;i < n; i++){
		 	nodes[i] = 0.5*(cos(double(n - 1 - i) * M_PI / double(n-1)) + 1);
		}
		_max = DBL_MIN;
		for (int i = 0;i < n-1; i++){
		 	_max = std::max(_max,nodes[i+1]-nodes[i]);
		}
		free(nodes);
		n++;	
	} 
	return n;
}

void calculate_nodes_and_weights(const uint3 & node_dim, float * const& h_node_list, float * const & h_weights){
	int n;
	int offset = 0;
	uint max_node_dim = std::max(node_dim.x,node_dim.y);
	max_node_dim = std::max(max_node_dim,node_dim.z);
	float * theta = (float *) malloc(max_node_dim*sizeof(float));
	int jhi;
	float b;
	uint node_dim_arr[3] = {node_dim.x,node_dim.y,node_dim.z};
	for (int i=0;i<3;i++){
		n = node_dim_arr[i];
		if (n == 1){
			h_node_list[offset] = 0.0;
			h_weights[offset] = 1.0;
		} else {
			for (int i = 0;i < n; i++){
				theta[i] = float(n - 1 - i) * M_PI / float(n-1);
				h_node_list[offset + i] = cos(theta[i]);
//				h_node_list[offset + i] = 0.5f;
			}
			for (int i = 0; i < n;i++){
				h_weights[offset+i] = 1.0;
				jhi = (n-1)/2;
				for (int j=0; j < jhi;j++){
					if (2*(j+1) == n-1){
						b = 1.0;
					} else {
						b = 2.0;
					}
					h_weights[offset + i] = h_weights[offset + i]-b*cos(2.0*float(j+1)*theta[i])/float(4*j*(j+2)+3);
				}
			}
			h_weights[offset] = 0.5*h_weights[offset]/float(n-1);
			for (int i=1;i<n-1;i++){
				h_weights[offset + i] = h_weights[offset + i]/float(n-1);
			}
			h_weights[offset + n - 1] = 0.5*h_weights[offset + n - 1]/float(n-1);
		}
		offset += n;
	}
	free(theta);
}

void scale_nodes(const float3 & offset, const float3 & coord_dim,const uint3 & node_dim,float * const & h_nodes){
	int n = 0;
	int m = node_dim.x;
	for (int i = n; i < m;i++){
		h_nodes[i] = offset.x + (1+h_nodes[i])*coord_dim.x/2;
	}
	//lazy, I know. Sue me.
	n += node_dim.x;
	m += node_dim.y;
	for (int i = n; i < m;i++){
		h_nodes[i] = offset.y + (1+h_nodes[i])*coord_dim.y/2;
	}
	n += node_dim.y;
	m += node_dim.z;
	for (int i = n; i < m;i++){
		h_nodes[i] = offset.z + (1+h_nodes[i])*coord_dim.z/2;
	}
}

size_t ChebychevNodes::getVolume(const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) != _instance_registry.end()){
		ChebychevNodes_Register * instance = &_instance_registry[gpu_index];
		return instance->h_node_vol[0];
	}
	return 0;
}

void ChebychevNodes::scale(std::pair<float3,float3>& scale,int gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	ChebychevNodes_Register * instance = &_instance_registry[gpu_index];
	scale_nodes(scale.first,scale.second,instance->h_node_dim[0],instance->h_node_list);
	CudaSafeCall(cudaMemcpy(instance->d_node_list,instance->h_node_list,(instance->h_node_dim[0].x + instance->h_node_dim[0].y + instance->h_node_dim[0].z)*sizeof(*instance->d_node_list),cudaMemcpyHostToDevice));
}

void ChebychevNodes::getTranslationIndex(uint3 & trans_index,const uint& t,const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	ChebychevNodes_Register * instance = &_instance_registry[gpu_index];
	trans_index.z = t/(instance->h_node_dim->x*instance->h_node_dim->y);
        trans_index.y = (t - trans_index.z*instance->h_node_dim->x*instance->h_node_dim->y)/instance->h_node_dim->x;
        trans_index.x = t - trans_index.y*instance->h_node_dim->x - trans_index.z*instance->h_node_dim->x*instance->h_node_dim->y;	
}

void ChebychevNodes::getTranslation(float3 & transformation,const uint& t,const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	ChebychevNodes_Register * instance = &_instance_registry[gpu_index];
	uint3 index;
	getTranslationIndex(index,t,gpu_index);
	transformation.x = instance->h_node_list[index.x];
        transformation.y = instance->h_node_list[index.y + instance->h_node_dim->x];
        transformation.z = instance->h_node_list[index.z + instance->h_node_dim->x + instance->h_node_dim->y];
}


void ChebychevNodes::createInstance(const int & trans_ref_level,int gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	uint node_dim = ipow(2,trans_ref_level);
	ChebychevNodes_Register instance;
	instance.h_node_dim = (uint3 *) malloc(sizeof(*instance.h_node_dim));
	instance.h_node_list = (float *) malloc((3*node_dim)*sizeof(*instance.h_node_list));
	instance.h_weights = (float *) malloc((3*node_dim)*sizeof(*instance.h_weights));
	instance.h_node_vol = (size_t *) malloc(sizeof(*instance.h_node_vol));
	CudaSafeCall(cudaMalloc((void **) &instance.d_node_dim,sizeof(*instance.d_node_dim)));
	CudaSafeCall(cudaMalloc((void **) &instance.d_node_list,(3*node_dim)*sizeof(*instance.d_node_list)));
	CudaSafeCall(cudaMalloc((void **) &instance.d_weights,(3*node_dim)*sizeof(*instance.d_weights)));
	if (trans_ref_level > 0){
		calculate_nodes_and_weights({node_dim,node_dim,node_dim}, instance.h_node_list, instance.h_weights);
	} else
	{
		instance.h_node_list[0] = 0.5f;
		instance.h_weights[0] = 1.0f;
	}
	instance.h_node_dim[0] = {node_dim,node_dim,node_dim};
	instance.h_node_vol[0] = node_dim*node_dim*node_dim;
	CudaSafeCall(cudaMemcpy(instance.d_node_dim,instance.h_node_dim,sizeof(*instance.d_node_dim),cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMemcpy(instance.d_node_list,instance.h_node_list,(3*node_dim)*sizeof(*instance.d_node_list),cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMemcpy(instance.d_weights,instance.h_weights,(3*node_dim)*sizeof(*instance.d_weights),cudaMemcpyHostToDevice));
	_instance_registry[gpu_index] = instance;
	printf("Created ChebyshevNodes with max refinement: %u %u %u, %u\n",node_dim,node_dim,node_dim,ipow(node_dim,3));
}

#ifdef MIN_TABLE
	int main(int argc, char ** argv){
		int n =  ChebychevNodes::get_node_dim(1.5,61);
		printf("%i\n",n);
		return 0;
	}
#endif
