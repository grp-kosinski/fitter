/*
 * CamScore.cu
 *
 *  Created on: Mar 13, 2021
 *      Author: kkarius
 */

#include<CamScore.h>
#include<cuda_util_include/cutil_math.h>
#include<McEwenNodes.h>
#include <score_helper.h>

template <uint BLOCKSIZE>
__global__
void v_ave_filenodes(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, float3 * d_scale,
		float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
		float * d_rotations, size_t h_rot_vol, const int translation_offset,const int translation_volume,
		size_t translation_stride, float * d_v_ave){
	__shared__ float concat[BLOCKSIZE];
	__shared__ float translation[3];
	__shared__ float rotation[9];
	float3 node_coords_query = {d_cheby_nodes[blockDim.x*blockIdx.x + threadIdx.x],
						  	    d_cheby_nodes[blockDim.y*blockIdx.y + threadIdx.y],
							    d_cheby_nodes[blockDim.z*blockIdx.z + threadIdx.z]};
	float3 node_coords_target = {node_coords_query.x*d_scale[0].x,
							     node_coords_query.y*d_scale[0].y,
								 node_coords_query.z*d_scale[0].z};
	float3 node_coords_target_r;
	float3 node_coords_target_rt;
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
					    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     					*d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;

	//translational scale - in terms of target
//	float3 t_scale = {1-d_scale->x,1-d_scale->y,1-d_scale->z};
	float3 scale = d_scale[0];
	//get it into the register for fast access - worth it? I don't know

	//masking
	if (tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z) == 0.0f)
		node_weight = 0.0f;
	int t; // translations
	int r = 0; // rotations
//	uint3 abg;
	float3 euler;
	while (r < h_rot_vol){
		//write rotation to memory
		if (tid == 0){
			printf("Handling rotation v_ave %i\n",r);
			//due to intrinsic order of angles in the file (for blender rendering)
			euler.x = d_rotations[3*r + 1];
			euler.y = d_rotations[3*r + 2];
			euler.z = d_rotations[3*r];
			eulers_to_memory(&euler,&rotation[0]);
		}
		__syncthreads();
		//apply rotate around coordinate origin, then shift from [-0.5,-0.5,-0.5] - [0.5,0.5,0.5] to [0,0,0] - [1,1,1]
		node_coords_target.x = node_coords_query.x - 0.5f;
		node_coords_target.y = node_coords_query.y - 0.5f;
		node_coords_target.z = node_coords_query.z - 0.5f;
		rotate(node_coords_target_r,node_coords_target,&rotation[0]);
		t= 0;
		while(t < translation_volume){
			if (tid == 0){
//				translation_to_memory(&translation[0], d_trans_cheby_nodes, &t_scale, d_trans_cheby_node_dim, translation_offset+t);
				translation_to_memory(&translation[0], d_trans_cheby_nodes,d_trans_cheby_node_dim, translation_offset+t);
			}
			__syncthreads();
			translate(node_coords_target_rt,node_coords_target_r,&scale, &translation[0]);
//			printf("%f %f %f --> %.10e\t%.10e\n",node_coords_target_rt.x,node_coords_target_rt.y,node_coords_target_rt.z,node_weight,tex3D<float>(d_density_target, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z));
			concat[tid] = node_weight*tex3D<float>(d_density_target, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z);
			__syncthreads();
			if (BLOCKSIZE >= 1024){if (tid < 512) { concat[tid] += concat[tid + 512];} __syncthreads();}
			if (BLOCKSIZE >= 512){if (tid < 256) { concat[tid] += concat[tid + 256];} __syncthreads();}
			if (BLOCKSIZE >= 256){if (tid < 128) { concat[tid] += concat[tid + 128];} __syncthreads();}
			if (BLOCKSIZE >= 128){if (tid <  64) { concat[tid] += concat[tid + 64];} __syncthreads();}
			if (BLOCKSIZE >= 64){if (tid < 32) { concat[tid] += concat[tid + 32];} __syncthreads();}
			if (BLOCKSIZE >= 32){if (tid < 16) { concat[tid] += concat[tid + 16];} __syncthreads();}
			if (BLOCKSIZE >= 16){if (tid < 8) { concat[tid] += concat[tid + 8];} __syncthreads();}
			if (BLOCKSIZE >= 8){if (tid <  4) { concat[tid] += concat[tid + 4];} __syncthreads();}
			if (BLOCKSIZE >= 4){if (tid < 2) { concat[tid] += concat[tid + 2];} __syncthreads();}
			if (BLOCKSIZE >= 2){if (tid <  1) { concat[tid] += concat[tid + 1];} __syncthreads();}
			if (tid==0){
			 	atomicAdd(&d_v_ave[t*translation_stride+r], concat[0]);
			}
			t += 1;
		}
		r += 1;
	}
}

__global__
void v_ave_sweep(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, float3 * d_scale,
		float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
		int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
		size_t translation_stride, float * d_v_ave){
	__shared__ float concat[512];
	__shared__ float translation[3];
	__shared__ float rotation[9];
	__shared__ uint rot_linear_index;
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;
	float3 node_coords_query = {d_cheby_nodes[blockDim.x*blockIdx.x + threadIdx.x],
						  	    d_cheby_nodes[blockDim.y*blockIdx.y + threadIdx.y],
							    d_cheby_nodes[blockDim.z*blockIdx.z + threadIdx.z]};
	float3 node_coords_target;
	float3 node_coords_target_r;
	float3 node_coords_target_rt;
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
					    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     					*d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];

	//translational scale - in terms of target
	float3 scale = d_scale[0];
	uint total_rotations = d_refinements[3*s]*d_refinements[3*s+1]*d_refinements[3*s+2];

	int t; // translations
	int r = 0; // rotations
//	uint3 abg;
	while (r < total_rotations){
		//write rotation to memory
		if (tid == 0){
			uint3 abg;
			rot_index(d_refinements, h_num_refinements, abg, &rot_linear_index, r, s);
			if (s>0){
				if (abg.x%2 == 0 && (2*abg.y+1)%3 == 0 && abg.z%2 == 0){
					r+=1;
					continue;
				}
			}
			rotation_to_memory(&rotation[0],abg,d_refinements,s);
//			rotation[0] = 1.0; rotation[1] = 0.0; rotation[2] = 0.0; rotation[3] = 0.0; rotation[4] = 1.0; rotation[5] = 0.0; rotation[6] = 0.0; rotation[7] = 0.0;rotation[8] = 1.0;
		}
		__syncthreads();
		//given that both query(q) and target(t) scan nodes are at [0,1], the scaling(s) and translating (tr) formula is
		//t = s*(q-0.5f)+tr
		//apply rotate around coordinate origin, then shift from [-0.5,-0.5,-0.5] - [0.5,0.5,0.5] to [0,0,0] - [1,1,1]
		node_coords_target.x = node_coords_query.x - 0.5f;
		node_coords_target.y = node_coords_query.y - 0.5f;
		node_coords_target.z = node_coords_query.z - 0.5f;
		rotate(node_coords_target_r,node_coords_target,&rotation[0]);
		t= 0;
		while(t < translation_volume){
			if (tid == 0){
				translation_to_memory(&translation[0], d_trans_cheby_nodes, d_trans_cheby_node_dim, translation_offset+t);
//				printf("%f %f %f\n",translation[0],translation[1],translation[2]);
				//				translation_to_memory(&translation[0], d_trans_cheby_nodes, &t_scale, d_trans_cheby_node_dim, translation_offset+t);
			}
			__syncthreads();
			translate(node_coords_target_rt,node_coords_target_r, &scale, &translation[0]);
//			printf("%f %f %f --> %f %f %f\n",node_coords_target.x,node_coords_target.y,node_coords_target.z,node_coords_target_rt.x,node_coords_target_rt.y,node_coords_target_rt.z);
			concat[tid] = node_weight*tex3D<float>(d_density_target, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z);
			__syncthreads();
			if (tid < 256) { concat[tid] += concat[tid + 256];} __syncthreads();
			if (tid < 128) { concat[tid] += concat[tid + 128];} __syncthreads();
			if (tid <  64) { concat[tid] += concat[tid + 64];} __syncthreads();
			if (tid <  32) { concat[tid] += concat[tid + 32];} __syncthreads();
			if (tid <  16) { concat[tid] += concat[tid + 16];} __syncthreads();
			if (tid <   8) { concat[tid] += concat[tid + 8];} __syncthreads();
			if (tid <   4) { concat[tid] += concat[tid + 4];} __syncthreads();
			if (tid <   2) { concat[tid] += concat[tid + 2];} __syncthreads();
			if (tid <   1) { concat[tid] += concat[tid + 1];} __syncthreads();
			if (tid==0){
//				if (concat[0]>0)
//					printf("%i, %f\n",t*translation_stride+rot_linear_index,concat[0]);
//				if (t*translation_stride+rot_linear_index == 80834)
//				printf("%010d | %.10e\n",t*translation_stride+rot_linear_index,concat[0]);
				atomicAdd(&d_v_ave[t*translation_stride+rot_linear_index], concat[0]);
			}
			t += 1;
		}
		r += 1;
	}
}

template <uint BLOCKSIZE>
__global__
void min_sweep(int s, uint * d_refinements, size_t h_num_refinements, size_t translation_stride,
		size_t sweep_size, float * d_scores){
	//blockIdx.x == translation index
	__shared__ float concat[2*BLOCKSIZE];
	uint rot_linear_index;
	uint total_rotations = d_refinements[3*s]*d_refinements[3*s+1]*d_refinements[3*s+2];
	//score offset for translations
	float * score_offset = d_scores + blockIdx.x*translation_stride;
	float * min_offset = d_scores + blockIdx.x*translation_stride + sweep_size;
	int tid = threadIdx.x;
	int r = tid;
	while (r < 2*BLOCKSIZE){
		concat[r] = FLT_MAX;
		r += blockDim.x;
	}
	r = tid;
	uint3 abg;
	while (r < total_rotations){
		rot_index(d_refinements, h_num_refinements, abg,&rot_linear_index, r,s);
		concat[threadIdx.x] = min(concat[threadIdx.x],score_offset[rot_linear_index]);
		r += blockDim.x;
	}
	__syncthreads();
	if (BLOCKSIZE >= 1024){if (tid < 512) { concat[tid] = min(concat[tid],concat[tid + 512]);} __syncthreads();}
	if (BLOCKSIZE >= 512){if (tid < 256) { concat[tid] = min(concat[tid],concat[tid + 256]);} __syncthreads();}
	if (BLOCKSIZE >= 256){if (tid < 128) { concat[tid] = min(concat[tid],concat[tid + 128]);} __syncthreads();}
	if (BLOCKSIZE >= 128){if (tid < 64) { concat[tid] = min(concat[tid],concat[tid + 64]);} __syncthreads();}
	if (BLOCKSIZE >= 64){if (tid < 32) { concat[tid] = min(concat[tid],concat[tid + 32]);} __syncthreads();}
	if (BLOCKSIZE >= 32){if (tid < 16) { concat[tid] = min(concat[tid],concat[tid + 16]);} __syncthreads();}
	if (BLOCKSIZE >= 16){if (tid < 8) { concat[tid] = min(concat[tid],concat[tid + 8]);} __syncthreads();}
	if (BLOCKSIZE >= 8){if (tid < 4) { concat[tid] = min(concat[tid],concat[tid + 4]);} __syncthreads();}
	if (BLOCKSIZE >= 4){if (tid < 2) { concat[tid] = min(concat[tid],concat[tid + 2]);} __syncthreads();}
	if (BLOCKSIZE >= 2){if (tid < 1) { concat[tid] = min(concat[tid],concat[tid + 1]);} __syncthreads();}
//	if (tid < 32) min_warp_reduce<float,BLOCKSIZE>(concat,tid); __syncthreads();
	//broken, ask
	if (tid==0){
		min_offset[0] = concat[0];
//		printf("%i : %f\n",blockIdx.x,min_offset[0]);
	}
}

template <uint BLOCKSIZE>
__global__
void min_sweep_strided(size_t translation_vol, size_t translation_stride, size_t sweep_size,
		float * d_scores, float * d_device_min)
{
	__shared__ float concat[2*BLOCKSIZE];
	float * min_offset = d_scores + sweep_size;
	int tid = threadIdx.x;
	int grid_size = blockDim.x*gridDim.x;
	int t = blockIdx.x*blockDim.x + threadIdx.x;
	while (t < 2*BLOCKSIZE){
		concat[t] = FLT_MAX;
		t += blockDim.x;
	}
	t = blockIdx.x*blockDim.x +threadIdx.x;
	while (t < translation_vol){
		concat[threadIdx.x] = min(concat[threadIdx.x],min_offset[t*translation_stride]);
		t += grid_size;
	}
	__syncthreads();
	if (BLOCKSIZE >= 1024){if (tid < 512) { concat[tid] = min(concat[tid],concat[tid + 512]);} __syncthreads();}
	if (BLOCKSIZE >= 512){if (tid < 256) { concat[tid] = min(concat[tid],concat[tid + 256]);} __syncthreads();}
	if (BLOCKSIZE >= 256){if (tid < 128) { concat[tid] = min(concat[tid],concat[tid + 128]);} __syncthreads();}
	if (BLOCKSIZE >= 128){if (tid < 64) { concat[tid] = min(concat[tid],concat[tid + 64]);} __syncthreads();}
	if (BLOCKSIZE >= 64){if (tid <  32) { concat[tid] = min(concat[tid],concat[tid + 32]);} __syncthreads();}
	if (BLOCKSIZE >= 32){if (tid < 16) { concat[tid] = min(concat[tid],concat[tid + 16]);} __syncthreads();}
	if (BLOCKSIZE >= 16){if (tid <  8) { concat[tid] = min(concat[tid],concat[tid + 8]);} __syncthreads();}
	if (BLOCKSIZE >= 8){if (tid < 4) { concat[tid] = min(concat[tid],concat[tid + 4]);} __syncthreads();}
	if (BLOCKSIZE >= 4){if (tid < 2) { concat[tid] = min(concat[tid],concat[tid + 2]);} __syncthreads();}
	if (BLOCKSIZE >= 2){if (tid < 1) { concat[tid] = min(concat[tid],concat[tid + 1]);} __syncthreads();}
//	if (tid < 32) min_warp_reduce<float,BLOCKSIZE>(concat,tid); __syncthreads();
	if (tid==0){
		atomicMin(&d_device_min[0],concat[0]);
	}
}

template <float (*F)(float &),uint BLOCKSIZE>
__global__
void quad_sweep(int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,size_t translation_stride,
		double * d_weights, float * d_dist, const int res_offset){
	__shared__ float concat[2*BLOCKSIZE];
	uint rot_linear_index;
	//get it into the register for fast access - worth it? I don't know
	uint total_rotations = d_refinements[3*s]*d_refinements[3*s+1]*d_refinements[3*s+2];
	//score offset for translations
	float * dist_offset = d_dist + (blockIdx.x + translation_offset)*translation_stride;
	float * quad_offset = d_dist + (blockIdx.x + translation_offset)*translation_stride +
			d_refinements[3*(h_num_refinements-1)]*d_refinements[3*(h_num_refinements-1)+1]*d_refinements[3*(h_num_refinements-1)+2] + res_offset;
	int tid = threadIdx.x;
	int r = tid;
	while (r < 2*BLOCKSIZE){
		concat[r] = 0.0f;
		r += blockDim.x;
	}
	r = tid;
	uint3 abg;
	while (r < total_rotations){
		rot_index(d_refinements, h_num_refinements, abg,&rot_linear_index, r,s);
		concat[threadIdx.x] += d_weights[abg.y]*F(dist_offset[rot_linear_index]);
//		printf("%i %u --> %.10e %.10e --> %.10e\n",r,abg.y,d_weights[abg.y],F(dist_offset[rot_linear_index]),d_weights[abg.y]*F(dist_offset[rot_linear_index]));
		r += blockDim.x;
	}
	__syncthreads();
	if (BLOCKSIZE >= 1024){if (tid < 512) { concat[tid] += concat[tid + 512];} __syncthreads();}
	if (BLOCKSIZE >= 512){if (tid < 256) { concat[tid] += concat[tid + 256];} __syncthreads();}
	if (BLOCKSIZE >= 256){if (tid < 128) { concat[tid] += concat[tid + 128];} __syncthreads();}
	if (BLOCKSIZE >= 128){if (tid <  64) { concat[tid] += concat[tid + 64];} __syncthreads();}
	if (BLOCKSIZE >= 64){if (tid < 32) { concat[tid] += concat[tid + 32];} __syncthreads();}
	if (BLOCKSIZE >= 32){if (tid < 16) { concat[tid] += concat[tid + 16];} __syncthreads();}
	if (BLOCKSIZE >= 16){if (tid < 8) { concat[tid] += concat[tid + 8];} __syncthreads();}
	if (BLOCKSIZE >= 8){if (tid <  4) { concat[tid] += concat[tid + 4];} __syncthreads();}
	if (BLOCKSIZE >= 4){if (tid <  2) { concat[tid] += concat[tid + 2];} __syncthreads();}
	if (BLOCKSIZE >= 2){if (tid <  1) { concat[tid] += concat[tid + 1];} __syncthreads();}
//	if (tid < 32) warp_reduce<float,BLOCKSIZE>(concat,tid); __syncthreads();
	if (tid==0){
		quad_offset[0] = concat[0];
		printf("ENT: %i --> %f\n",translation_offset+blockIdx.x,concat[0]);
	}
}

template <float (*F)(float &),uint BLOCKSIZE>
__global__
void quad_simple(uint * d_refinements, int translation_offset, size_t translation_stride, uint trans_node_dim,
		float * d_trans_weights, double * d_weights, float * d_dist, float * d_result){
	//translation offset must be real == connected to the geometric structure
	__shared__ float concat[2*BLOCKSIZE];
	uint rot_linear_index;
	//get it into the register for fast access - worth it? I don't know
	uint total_rotations = d_refinements[0]*d_refinements[1]*d_refinements[2];
	//score offset for translations
	uint3 trans_index;
	int t = translation_offset + blockIdx.x;
	float * dist_offset = d_dist + blockIdx.x*translation_stride;
	int tid = threadIdx.x;
	int r = tid;
	while (r < 2*BLOCKSIZE){
		concat[r] = 0.0f;
		r += blockDim.x;
	}
	r = tid;
	uint3 abg;
	while (r < total_rotations){
		rot_index(d_refinements, 1, abg,&rot_linear_index, r,0);
		concat[threadIdx.x] += d_weights[abg.y]*F(dist_offset[rot_linear_index]);
		r += blockDim.x;
	}
	__syncthreads();
	if (BLOCKSIZE >= 1024){if (tid < 512) { concat[tid] += concat[tid + 512];} __syncthreads();}
	if (BLOCKSIZE >= 512){if (tid < 256) { concat[tid] += concat[tid + 256];} __syncthreads();}
	if (BLOCKSIZE >= 256){if (tid < 128) { concat[tid] += concat[tid + 128];} __syncthreads();}
	if (BLOCKSIZE >= 128){if (tid < 64) { concat[tid] += concat[tid + 64];} __syncthreads();}
	if (BLOCKSIZE >= 64){if (tid < 32) { concat[tid] += concat[tid + 32];} __syncthreads();}
	if (BLOCKSIZE >= 32){if (tid < 16) { concat[tid] += concat[tid + 16];} __syncthreads();}
	if (BLOCKSIZE >= 16){if (tid < 8) { concat[tid] += concat[tid + 8];} __syncthreads();}
	if (BLOCKSIZE >= 8){if (tid <  4) { concat[tid] += concat[tid + 4];} __syncthreads();}
	if (BLOCKSIZE >= 4){if (tid <  2) { concat[tid] += concat[tid + 2];} __syncthreads();}
	if (BLOCKSIZE >= 2){if (tid <  1) { concat[tid] += concat[tid + 1];} __syncthreads();}
//	if (tid < 32) warp_reduce<float,BLOCKSIZE>(concat,tid); __syncthreads();
	if (tid==0){
//		if (concat[0] > 0.0f)
//			printf("%i: %.10e\n",t,concat[0]);
		trans_index.z = t/(trans_node_dim*trans_node_dim);
		trans_index.y = (t - trans_index.z*trans_node_dim*trans_node_dim)/trans_node_dim;
		trans_index.x = t - trans_index.y*trans_node_dim - trans_index.z*trans_node_dim*trans_node_dim;
		atomicAdd(&d_result[0],d_trans_weights[trans_index.x]*d_trans_weights[trans_index.y]*d_trans_weights[trans_index.z]*concat[0]);
	}
}

template <uint BLOCKSIZE>
__global__
void u_t_v_filenodes(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, float3 * d_scale,
		float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
		float * d_rotations, size_t h_rot_vol, const int translation_offset,const int translation_volume,
		size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v){
	__shared__ float concat1[BLOCKSIZE]; //v_m_v_ave
	__shared__ float concat2[BLOCKSIZE];// u_t_v
	__shared__ float translation[3];
	__shared__ float rotation[9];
	__shared__ float v_ave;
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;
	float3 node_coords_query = {d_cheby_nodes[blockDim.x*blockIdx.x + threadIdx.x],
						  	    d_cheby_nodes[blockDim.y*blockIdx.y + threadIdx.y],
							    d_cheby_nodes[blockDim.z*blockIdx.z + threadIdx.z]};
	float3 node_coords_target = {node_coords_query.x*d_scale[0].x,
							     node_coords_query.y*d_scale[0].y,
								 node_coords_query.z*d_scale[0].z};
	float3 node_coords_target_r;
	float3 node_coords_target_rt;
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
					    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     					*d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];

	float v_m_v_ave;

	//translational scale - in terms of target
//	float3 t_scale = {1-d_scale->x,1-d_scale->y,1-d_scale->z};
	float3 scale = d_scale[0];
	//get it into the register for fast access - worth it? I don't know

	int t; // translations
	int r = 0; // rotations
	float3 euler;
	while (r < h_rot_vol){
			//write rotation to memory
			if (tid == 0){
				printf("Handling rotation u_t_v %i\n",r);
				//due to intrinsic order of angles in the file (for blender rendering)
				euler.x = d_rotations[3*r + 1];
				euler.y = d_rotations[3*r + 2];
				euler.z = d_rotations[3*r];
				eulers_to_memory(&euler,&rotation[0]);
			}
		__syncthreads();
		//apply rotate around coordinate origin, then shift from [-0.5,-0.5,-0.5] - [0.5,0.5,0.5] to [0,0,0] - [1,1,1]
		node_coords_target.x = node_coords_query.x - 0.5f;
		node_coords_target.y = node_coords_query.y - 0.5f;
		node_coords_target.z = node_coords_query.z - 0.5f;
		rotate(node_coords_target_r,node_coords_target,&rotation[0]);
		t= 0;
		while(t < translation_volume){
			if (tid == 0){
				translation_to_memory(&translation[0], d_trans_cheby_nodes,d_trans_cheby_node_dim, translation_offset+t);
				v_ave = d_v_ave[t*translation_stride+r];
			}
			__syncthreads();
			translate(node_coords_target_rt,node_coords_target_r,&scale, &translation[0]);
			v_m_v_ave = tex3D<float>(d_density_target, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z)-v_ave;
			concat1[tid] = node_weight*v_m_v_ave*v_m_v_ave;
			concat2[tid] = node_weight*v_m_v_ave*tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z);
//			if (tid ==0){
//				printf("%i %f %f %f %.10e %.10e %.10e\n",tid,node_coords_query.x,node_coords_query.y,node_coords_query.z,node_weight,v_m_v_ave,tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z));
//			}
			__syncthreads();
//			if (BLOCKSIZE >= 1024){if (tid < 512)  { concat1[tid] += concat1[tid + 512];  concat2[tid] += concat2[tid + 512];} __syncthreads();}
			if (BLOCKSIZE >= 512) {if (tid < 256)  { concat1[tid] += concat1[tid + 256];  concat2[tid] += concat2[tid + 256];} __syncthreads();}
			if (BLOCKSIZE >= 256) {if (tid < 128)  { concat1[tid] += concat1[tid + 128];  concat2[tid] += concat2[tid + 128];} __syncthreads();}
			if (BLOCKSIZE >= 128) {if (tid < 64)   { concat1[tid] += concat1[tid + 64];   concat2[tid] += concat2[tid + 64];} __syncthreads();}
			if (BLOCKSIZE >= 64)  {if (tid < 32)   { concat1[tid] += concat1[tid + 32];   concat2[tid] += concat2[tid + 32];} __syncthreads();}
			if (BLOCKSIZE >= 32)  {if (tid < 16)   { concat1[tid] += concat1[tid + 16];   concat2[tid] += concat2[tid + 16];} __syncthreads();}
			if (BLOCKSIZE >= 16)  {if (tid < 8)    { concat1[tid] += concat1[tid + 8];    concat2[tid] += concat2[tid + 8];} __syncthreads();}
			if (BLOCKSIZE >= 8)   {if (tid < 4)    { concat1[tid] += concat1[tid + 4];    concat2[tid] += concat2[tid + 4];} __syncthreads();}
			if (BLOCKSIZE >= 4)   {if (tid < 2)    { concat1[tid] += concat1[tid + 2];    concat2[tid] += concat2[tid + 2];} __syncthreads();}
			if (BLOCKSIZE >= 2)   {if (tid < 1)    { concat1[tid] += concat1[tid + 1];    concat2[tid] += concat2[tid + 1];} __syncthreads();}
			if (tid==0){
				atomicAdd(&d_v_m_v_ave[t*translation_stride+r], concat1[0]);
				atomicAdd(&d_u_t_v[t*translation_stride+r], concat2[0]);
			}
			t += 1;
		}
		r += 1;
	}
}

__global__
void u_t_v_sweep(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, float3 * d_scale,
		float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
		int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
		size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v,float * d_u_ave){
	__shared__ float concat1[512]; //v_m_v_ave
	__shared__ float concat2[512];// u_t_v
	__shared__ float translation[3];
	__shared__ float rotation[9];
	__shared__ uint rot_linear_index;
	__shared__ float v_ave;
	float u_ave = d_u_ave[0];
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;
	float3 node_coords_query = {d_cheby_nodes[blockDim.x*blockIdx.x + threadIdx.x],
						  	    d_cheby_nodes[blockDim.y*blockIdx.y + threadIdx.y],
							    d_cheby_nodes[blockDim.z*blockIdx.z + threadIdx.z]};
	float3 node_coords_target = {node_coords_query.x*d_scale[0].x,
							     node_coords_query.y*d_scale[0].y,
								 node_coords_query.z*d_scale[0].z};
	float3 node_coords_target_r;
	float3 node_coords_target_rt;
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
					    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     					*d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];
	float v_m_v_ave;

	//translational scale - in terms of target
	float3 scale = d_scale[0];
	uint total_rotations = d_refinements[3*s]*d_refinements[3*s+1]*d_refinements[3*s+2];

	int t; // translations
	int r = 0; // rotations
	while (r < total_rotations){
		//write rotation to memory
		if (tid == 0){
			uint3 abg;
			rot_index(d_refinements, h_num_refinements, abg,&rot_linear_index, r,s);
			if (s>0){
				if (abg.x%2 == 0 && (2*abg.y+1)%3 == 0 && abg.z%2 == 0){
					r+=1;
					continue;
				}
			}
			rotation_to_memory(&rotation[0],abg,d_refinements,s);
		}
		__syncthreads();
		//apply rotate around coordinate origin, then shift from [-0.5,-0.5,-0.5] - [0.5,0.5,0.5] to [0,0,0] - [1,1,1]
		node_coords_target.x = node_coords_query.x - 0.5f;
		node_coords_target.y = node_coords_query.y - 0.5f;
		node_coords_target.z = node_coords_query.z - 0.5f;
		rotate(node_coords_target_r,node_coords_target,&rotation[0]);
		t= 0;
		while(t < translation_volume){
			if (tid == 0){
				translation_to_memory(&translation[0], d_trans_cheby_nodes, d_trans_cheby_node_dim, translation_offset+t);
				v_ave = d_v_ave[t*translation_stride+rot_linear_index];
			}
			__syncthreads();
			translate(node_coords_target_rt,node_coords_target_r,&scale,&translation[0]);
			v_m_v_ave = tex3D<float>(d_density_target, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z)-v_ave;
			concat1[tid] = node_weight*v_m_v_ave*v_m_v_ave;
//			printf("%.10e,\n",concat1[tid]);
			concat2[tid] = node_weight*v_m_v_ave*(tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z) - u_ave);
			__syncthreads();
			if (tid < 256)  { concat1[tid] += concat1[tid + 256];  concat2[tid] += concat2[tid + 256];} __syncthreads();
			if (tid < 128)  { concat1[tid] += concat1[tid + 128];  concat2[tid] += concat2[tid + 128];} __syncthreads();
			if (tid < 64)   { concat1[tid] += concat1[tid + 64];   concat2[tid] += concat2[tid + 64];} __syncthreads();
			if (tid < 32)   { concat1[tid] += concat1[tid + 32];   concat2[tid] += concat2[tid + 32];} __syncthreads();
			if (tid < 16)   { concat1[tid] += concat1[tid + 16];   concat2[tid] += concat2[tid + 16];} __syncthreads();
			if (tid < 8)    { concat1[tid] += concat1[tid + 8];    concat2[tid] += concat2[tid + 8];} __syncthreads();
			if (tid < 4)    { concat1[tid] += concat1[tid + 4];    concat2[tid] += concat2[tid + 4];} __syncthreads();
			if (tid < 2)    { concat1[tid] += concat1[tid + 2];    concat2[tid] += concat2[tid + 2];} __syncthreads();
			if (tid < 1)    { concat1[tid] += concat1[tid + 1];    concat2[tid] += concat2[tid + 1];} __syncthreads();
			if (tid==0){
//				if (t==0 )
//					printf("VNORMF: %.10e\n",concat1[0]);
				atomicAdd(&d_v_m_v_ave[t*translation_stride+rot_linear_index], concat1[0]);
//				printf("%07d | %.10e\n",t*translation_stride+rot_linear_index,concat1[0]);
				atomicAdd(&d_u_t_v[t*translation_stride+rot_linear_index], concat2[0]);
			}
			t += 1;
		}
		r += 1;
	}
}

__global__
void cam_score_filenodes(float * d_rotations, size_t h_rot_vol, const int translation_offset,const int translation_volume,
		size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v, float * d_u_ave){
	uint rot_linear_index;
	float sqrt_u_ave = sqrtf(d_u_ave[0]);
	//get it into the register for fast access - worth it? I don't know
	int t; // translations
	int r = blockIdx.x; // rotations
	while (r < h_rot_vol){
		if (threadIdx.x ==0)
			printf("Handling rotation %i cam_score \n",r);
		t= threadIdx.x;
		size_t tr_off;
		while(t < translation_volume){
			tr_off = t*translation_stride+rot_linear_index;
			//d_v_ave doubles as memory space for the cam score, since it's not needed anymore
			d_v_ave[tr_off] = d_u_t_v[tr_off]/(sqrt_u_ave*sqrtf(d_v_m_v_ave[tr_off]));
			t += blockDim.x;
		}
		r += gridDim.x;
	}
}

__global__
void cam_score(	int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
		size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v, float * d_u_norm){
	uint rot_linear_index;
	float sqrt_u_norm = sqrtf(d_u_norm[0]);
	//get it into the register for fast access - worth it? I don't know
	uint total_rotations = d_refinements[3*s]*d_refinements[3*s+1]*d_refinements[3*s+2];
	int t; // translations
	int r = blockIdx.x; // rotations
	uint3 abg;
	while (r < total_rotations){
		rot_index(d_refinements, h_num_refinements, abg,&rot_linear_index, r,s);
		//if s!=0, the rotation might have been handled in a preceding sweep. check for this.
		if (s>0){
			if ( abg.x%2 == 0 && (2*abg.y+1)%3 == 0 && abg.z%2 == 0){
				r+=gridDim.x;
				continue;
			}
		}
		t= threadIdx.x;
		size_t tr_off;
		while(t < translation_volume){
			tr_off = t*translation_stride+rot_linear_index;
			//d_v_ave doubles as memory space for the cam score, since it's not needed anymore
			if (d_v_m_v_ave[tr_off] != 0.0f){
//				printf("%07d %.10e/(%.10e*%.10e) = %.10e\n",tr_off,d_u_t_v[tr_off],sqrtf(d_v_m_v_ave[tr_off]),sqrt_u_norm,d_u_t_v[tr_off]/(sqrt_u_norm*sqrtf(d_v_m_v_ave[tr_off])));
				d_v_ave[tr_off] = d_u_t_v[tr_off]/(sqrt_u_norm*sqrtf(d_v_m_v_ave[tr_off]));
			} else {
				d_v_ave[tr_off] = 0.0f;
			}
//			if (threadIdx.x == 0)
//				printf("%u %u %u | %.5e\n",abg.x,abg.y,abg.z,d_v_ave[tr_off]);
			t += blockDim.x;
		}
		r += gridDim.x;
	}
}

//__global__
//void check_mapping(int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
//		size_t translation_stride, float * d_v_ave){
//	uint rot_linear_index;
//	__syncthreads();
//	//get it into the register for fast access - worth it? I don't know
//	uint total_rotations = d_refinements[3*s]*d_refinements[3*s+1]*d_refinements[3*s+2];
//	float score;
//	int t; // translations
//	int r = blockIdx.x; // rotations
//	uint3 abg;
//	if (blockIdx.x == 0 && threadIdx.x ==0)
//		printf("%f\n",(float)s);
//
//	float fs = (float)s +1.0f;
//	while (r < total_rotations){
//		rot_index(d_refinements, h_num_refinements, abg,&rot_linear_index, r,s);
//		//if s!=0, the rotation might have been handled in a preceding sweep. check for this.
//		if (s>0){
//			if ( abg.x%2 == 0 && (2*abg.y+1)%3 == 0 && abg.z%2 == 0){
//				r += gridDim.x;
//				continue;
//			}
//		}
//		t= threadIdx.x;
//		size_t tr_off;
//		while(t < translation_volume){
//			tr_off = (translation_offset+t)*translation_stride+rot_linear_index;
//			//d_v_ave doubles as memory space for the cam score, since it's not needed anymore
//			d_v_ave[tr_off] += fs;
////			printf("%i | %i %i %i --> %u \n",s,abg.x,abg.y,abg.z,rot_linear_index);
//			t += blockDim.x;
//		}
//		r += gridDim.x;
//	}
//}

__global__
void shift_by_min_simple(const int translation_offset,const int translation_volume, const size_t translation_stride, const size_t rot_vol,
		float * d_scores, float * d_dist, float h_min){
	//blocks - translations
	//threads - rotations
	float * rot_offset_scores;
	float * rot_offset_dist;
	int bid = blockIdx.x;
	int tid;
	while (bid < translation_volume){
		rot_offset_scores = d_scores + translation_stride*bid;
		rot_offset_dist = d_dist + translation_stride*bid;
		tid = threadIdx.x;
		while (tid < rot_vol){
//			if (tid == 0 && bid <10 )
//				printf("%i: %f - %f = %f\n",bid,rot_offset_scores[tid],min,rot_offset_scores[tid] - min);
			rot_offset_dist[tid] = rot_offset_scores[tid] - h_min;
			tid += blockDim.x;
		}
		bid += gridDim.x;
	}
}

__global__
void div_by_quad_simple(const int translation_volume, const size_t translation_stride, const size_t rot_vol,
		float * d_dist, float h_quad){
	//blocks - translations
	//threads - rotations
	float * rot_offset_dist;
	int bid = blockIdx.x;
	int tid;
//	if (tid == 0 && bid == 0)
//		printf("Dividing by %f\n",h_quad);
	while (bid < translation_volume){
		rot_offset_dist = d_dist + translation_stride*bid;
		tid = threadIdx.x;
		while (tid < rot_vol){
			rot_offset_dist[tid] /= h_quad;
			tid += blockDim.x;
		}
		bid += gridDim.x;
	}
}

__global__
void shift_by_min(	int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset, size_t translation_stride,
		size_t rot_vol, float * d_scores, float * d_dist){
	//get it into the register for fast access - worth it? I don't know
	uint total_rotations = d_refinements[3*s]*d_refinements[3*s+1]*d_refinements[3*s+2];
	//score offset for translations
	float * score_offset = d_scores + (blockIdx.x + translation_offset)*translation_stride;
	float * min_offset = d_scores + (blockIdx.x + translation_offset)*translation_stride + 4*rot_vol;
	float * dist_offset = d_dist + (blockIdx.x + translation_offset)*translation_stride;
	int r = threadIdx.x;
	uint rot_linear_index;
	// rotations
	uint3 abg;
	rot_index(d_refinements, h_num_refinements, abg,&rot_linear_index, r,s);

	while (r < total_rotations){
		rot_index(d_refinements, h_num_refinements, abg,&rot_linear_index, r,s);
		dist_offset[rot_linear_index] = score_offset[rot_linear_index] - min_offset[0];
		r += blockDim.x;
	}
}

__global__
void div_by_ave(int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset, size_t translation_stride,
		float * d_dist){
	//get it into the register for fast access - worth it? I don't know
	uint total_rotations = d_refinements[3*s]*d_refinements[3*s+1]*d_refinements[3*s+2];
	float * dist_offset = d_dist + (blockIdx.x + translation_offset)*translation_stride;
	float * ave_offset = d_dist + (blockIdx.x + translation_offset)*translation_stride +
			d_refinements[3*(h_num_refinements-1)]*d_refinements[3*(h_num_refinements-1)+1]*d_refinements[3*(h_num_refinements-1)+2] + 1;
	int r = threadIdx.x;
	uint rot_linear_index;
	uint3 abg;
	// rotations
	while (r < total_rotations){
		rot_index(d_refinements, h_num_refinements, abg,&rot_linear_index, r,s);
		dist_offset[rot_linear_index] /= ave_offset[0];
		r += blockDim.x;
	}
}

//template <void (*T)(int &)>
//template <float (*F)(float &),uint BLOCKSIZE>
__global__
void u_ave_scan(cudaTextureObject_t d_density_query, float * d_cheby_nodes, float * d_cheby_weights,
		float * d_u_ave){
	__shared__ float concat[512]; //u_ave
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;
	float3 node_coords_query = {d_cheby_nodes[blockDim.x*blockIdx.x + threadIdx.x],
						  	    d_cheby_nodes[blockDim.y*blockIdx.y + threadIdx.y],
							    d_cheby_nodes[blockDim.z*blockIdx.z + threadIdx.z]};
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
					    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     					*d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];
//	float u = tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z);
	concat[tid] = node_weight*tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z);;
//	printf("%03d %.10e %.10e %.10e | %.10e | %.10e\n",tid,node_coords_query.x,node_coords_query.y,node_coords_query.z,node_weight,u_ave);
	__syncthreads();
//	if (concat[tid] > 0.0f)
//		printf("%i:%e.10 %.10e,\n",tid,node_weight,concat[tid]);
	if (tid < 256) {concat[tid] += concat[tid + 256];} __syncthreads();
	if (tid < 128) {concat[tid] += concat[tid + 128];} __syncthreads();
	if (tid <  64) {concat[tid] += concat[tid + 64]; } __syncthreads();
	if (tid <  32) {concat[tid] += concat[tid + 32]; } __syncthreads();
	if (tid <  16) {concat[tid] += concat[tid + 16]; } __syncthreads();
	if (tid <   8) {concat[tid] += concat[tid + 8];  } __syncthreads();
	if (tid <   4) {concat[tid] += concat[tid + 4];  } __syncthreads();
	if (tid <   2) {concat[tid] += concat[tid + 2];  } __syncthreads();
	if (tid <   1) {concat[tid] += concat[tid + 1];  } __syncthreads();
	if (tid==0){
		 atomicAdd(&d_u_ave[0], concat[0]);
	}
}

__global__
void u_norm_scan(cudaTextureObject_t d_density_query, float * d_cheby_nodes, float * d_cheby_weights,
		float * d_u_ave, float * d_u_norm){
	__shared__ float concat[512]; //u_ave
	__shared__ float u_ave;
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;
	if (tid == 0)
		u_ave = d_u_ave[0];
	__syncthreads();
	float3 node_coords_query = {d_cheby_nodes[blockDim.x*blockIdx.x + threadIdx.x],
						  	    d_cheby_nodes[blockDim.y*blockIdx.y + threadIdx.y],
							    d_cheby_nodes[blockDim.z*blockIdx.z + threadIdx.z]};
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
					    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     					*d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];
	float u = tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z);
	concat[tid] = node_weight*(u-u_ave)*(u-u_ave);
	__syncthreads();
	if (tid < 256) {concat[tid] += concat[tid + 256];} __syncthreads();
	if (tid < 128) {concat[tid] += concat[tid + 128];} __syncthreads();
	if (tid <  64) {concat[tid] += concat[tid + 64]; } __syncthreads();
	if (tid <  32) {concat[tid] += concat[tid + 32]; } __syncthreads();
	if (tid <  16) {concat[tid] += concat[tid + 16]; } __syncthreads();
	if (tid <   8) {concat[tid] += concat[tid + 8];  } __syncthreads();
	if (tid <   4) {concat[tid] += concat[tid + 4];  } __syncthreads();
	if (tid <   2) {concat[tid] += concat[tid + 2];  } __syncthreads();
	if (tid <   1) {concat[tid] += concat[tid + 1];  } __syncthreads();
	if (tid==0){
		atomicAdd(&d_u_norm[0], concat[0]);
	}
}

void CamScore::entropy_test_two(const int & gpu_index){
	//one tranlation, half rotations 1, rest 0
	srand (time(NULL));
	float entropy_uniform = std::log(8*(float) M_PI * (float) M_PI);
	CudaSafeCall(cudaSetDevice(gpu_index));
	printf("Started entropy test two ...\n");
	CamScore_Register * cam_score_instance = &_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = cam_score_instance->trans_node;
	McEwenNodes_Register * rot_nodes_instance = cam_score_instance->rot_node;
	uint rotation_vol = rot_nodes_instance->h_max_nodes[0];
	uint3 * t_node_dim = trans_nodes_instance->h_node_dim;
	//assume isotropy
	uint trans_node_dim = t_node_dim->x;
	uint translation_volume = t_node_dim->x*t_node_dim->y*t_node_dim->z;
	float * d_trans_weights = trans_nodes_instance->d_weights;
	int gridDim = 128;
	int blockDim = 256;
	uint translation_vol = t_node_dim->x*t_node_dim->y*t_node_dim->z;
	uint transformation_vol = rotation_vol*translation_vol;
	size_t translation_stride = 4*rotation_vol + 3*rot_nodes_instance->h_num_refinements[0];
	int test_num = 100;
	float avg_gain = 0.0f;
//	float * h_scores = cam_score_instance->h_cam;
//	float * d_scores = cam_score_instance->d_v_ave;
	int r;
	int t;
	float * d_result;
	float h_result;
	CudaSafeCall(cudaMalloc((void **)&d_result,sizeof(*d_result)));
	float zero = 0.0f;
	for (int i = 0; i<test_num;i++){
		t = rand() % translation_vol;
		for (int j = 0; j<transformation_vol;++j)
			cam_score_instance->h_cam[j] = 0.0f;
		for (int j = 0; j<rotation_vol;++j){
			r = rand();
			cam_score_instance->h_cam[t*rotation_vol + j] = 0.0f;
			if (r <= RAND_MAX/2)
				cam_score_instance->h_cam[t*rotation_vol + j] = 1.0f;
		}
		for (int t = 0; t < translation_vol; ++t){
			CudaSafeCall(cudaMemcpy(cam_score_instance->d_dist + t*translation_stride,cam_score_instance->h_cam + t*rotation_vol,rotation_vol*sizeof(*cam_score_instance->d_dist),cudaMemcpyHostToDevice));
			cudaDeviceSynchronize();
		}
		CudaSafeCall(cudaMemcpy(d_result,&zero,sizeof(*d_result),cudaMemcpyHostToDevice));
		cudaDeviceSynchronize();
	//	template <float (*F)(float &),uint BLOCKSIZE>
	//	__global__
	//	void quad_simple(uint * d_refinements, size_t translation_offset, size_t translation_stride, uint trans_node_dim,
	//			float * d_trans_weights, double * d_weights, float * d_dist, float * d_result)
		quad_simple<idf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride, trans_node_dim,
				d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
		CudaCheckError();
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
		cudaDeviceSynchronize();
		printf("Result of first quadrature: %.e10\n",h_result);
		div_by_quad_simple<<<gridDim,blockDim>>>(translation_volume,translation_stride,rotation_vol,cam_score_instance->d_dist,
		h_result);
		CudaCheckError();
		cudaDeviceSynchronize();
		quad_simple<idf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride, trans_node_dim,
				d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
		printf("Quadrature of distribution: %f\n",h_result);

		h_result = 0.0f;
		CudaSafeCall(cudaMalloc((void **)&d_result,sizeof(*d_result)));
		CudaCheckError();
		cudaDeviceSynchronize();

//	template <float (*F)(float &),uint BLOCKSIZE>
//	__global__
//	void quad_simple(uint * d_refinements, size_t translation_offset, size_t translation_stride, uint trans_node_dim,
//			float * d_trans_weights, double * d_weights, float * d_dist, float * d_result)
		quad_simple<entf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride,
				trans_node_dim,d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
		CudaCheckError();
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
		printf("Information gain of distribution: %f\n",entropy_uniform-h_result);
		avg_gain += entropy_uniform-h_result;
		printf("Completed test %i ...\n",i);
	}
	printf("Avg gain: %f\n",avg_gain/test_num);
	printf("==================================== END OF TEST =========================================\n");
}

void CamScore::entropy_test_three(const int & gpu_index){
	//one tranlation, all rotations 1, rest 0
	float entropy_uniform = std::log(8*(float) M_PI * (float) M_PI);
	srand (time(NULL));
	CudaSafeCall(cudaSetDevice(gpu_index));
	printf("Started entropy test three ...\n");
	CamScore_Register * cam_score_instance = &_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = cam_score_instance->trans_node;
	McEwenNodes_Register * rot_nodes_instance = cam_score_instance->rot_node;
	uint rotation_vol = rot_nodes_instance->h_max_nodes[0];
	uint3 * t_node_dim = trans_nodes_instance->h_node_dim;
	//assume isotropy
	uint trans_node_dim = t_node_dim->x;
	uint translation_volume = t_node_dim->x*t_node_dim->y*t_node_dim->z;
	float * d_trans_weights = trans_nodes_instance->d_weights;
	int gridDim = 128;
	int blockDim = 256;
	uint translation_vol = t_node_dim->x*t_node_dim->y*t_node_dim->z;
	uint transformation_vol = rotation_vol*translation_vol;
	size_t translation_stride = 4*rotation_vol + 3*rot_nodes_instance->h_num_refinements[0];
	int test_num = 100;
	float avg_gain = 0.0f;
//	float * h_scores = cam_score_instance->h_cam;
//	float * d_scores = cam_score_instance->d_v_ave;
//	int r;
	int t;
	float * d_result;
	float h_result;
	CudaSafeCall(cudaMalloc((void **)&d_result,sizeof(*d_result)));
	float zero = 0.0f;
	for (int i = 0; i<test_num;i++){
		t = rand()%translation_vol;
		for (int j = 0; j<transformation_vol;++j)
			cam_score_instance->h_cam[j] = 0.0f;
		for (int j = 0; j<rotation_vol;++j)
			cam_score_instance->h_cam[t*rotation_vol + j] = 1.0f;
		for (int t = 0; t < translation_vol; ++t){
			CudaSafeCall(cudaMemcpy(cam_score_instance->d_dist + t*translation_stride,cam_score_instance->h_cam + t*rotation_vol,rotation_vol*sizeof(*cam_score_instance->d_dist),cudaMemcpyHostToDevice));
			cudaDeviceSynchronize();
		}
		CudaSafeCall(cudaMemcpy(d_result,&zero,sizeof(*d_result),cudaMemcpyHostToDevice));
		cudaDeviceSynchronize();
	//	template <float (*F)(float &),uint BLOCKSIZE>
	//	__global__
	//	void quad_simple(uint * d_refinements, size_t translation_offset, size_t translation_stride, uint trans_node_dim,
	//			float * d_trans_weights, double * d_weights, float * d_dist, float * d_result)
		quad_simple<idf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride, trans_node_dim,
				d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
		CudaCheckError();
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
		cudaDeviceSynchronize();
		printf("Result of first quadrature: %.e10\n",h_result);
		div_by_quad_simple<<<gridDim,blockDim>>>(translation_volume,translation_stride,rotation_vol,cam_score_instance->d_dist,
		h_result);
		CudaCheckError();
		cudaDeviceSynchronize();
		quad_simple<idf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride, trans_node_dim,
				d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
		printf("Quadrature of distribution: %f\n",h_result);

		h_result = 0.0f;
		CudaSafeCall(cudaMalloc((void **)&d_result,sizeof(*d_result)));
		CudaCheckError();
		cudaDeviceSynchronize();

//	template <float (*F)(float &),uint BLOCKSIZE>
//	__global__
//	void quad_simple(uint * d_refinements, size_t translation_offset, size_t translation_stride, uint trans_node_dim,
//			float * d_trans_weights, double * d_weights, float * d_dist, float * d_result)
		quad_simple<entf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride,
				trans_node_dim,d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
		CudaCheckError();
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
		printf("Information gain of distribution: %f\n",entropy_uniform-h_result);
		avg_gain += entropy_uniform-h_result;
		printf("Completed test %i ...\n",i);
	}
	printf("Avg gain: %f\n",avg_gain/test_num);
	printf("==================================== END OF TEST =========================================\n");
}

#include<list>

void CamScore::entropy_test_four(const int & gpu_index){
	//half tranlations 1, rest 0
	float entropy_uniform = std::log(8*(float) M_PI * (float) M_PI);
	CudaSafeCall(cudaSetDevice(gpu_index));
	srand (time(NULL));
	printf("Started entropy test four ...\n");
	CamScore_Register * cam_score_instance = &_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = cam_score_instance->trans_node;
	McEwenNodes_Register * rot_nodes_instance = cam_score_instance->rot_node;
	uint rotation_vol = rot_nodes_instance->h_max_nodes[0];
	uint3 * t_node_dim = trans_nodes_instance->h_node_dim;
	//assume isotropy
	uint trans_node_dim = t_node_dim->x;
	uint translation_volume = t_node_dim->x*t_node_dim->y*t_node_dim->z;
	float * d_trans_weights = trans_nodes_instance->d_weights;
	int gridDim = 128;
	int blockDim = 256;
	uint translation_vol = t_node_dim->x*t_node_dim->y*t_node_dim->z;
	uint transformation_vol = rotation_vol*translation_vol;
	size_t translation_stride = 4*rotation_vol + 3*rot_nodes_instance->h_num_refinements[0];
	int test_num = 10;
//	float * h_scores = cam_score_instance->h_cam;
//	float * d_scores = cam_score_instance->d_v_ave;
//	int r;
//	int t;
	int tc;
	float * d_result;
	float h_result;
	CudaSafeCall(cudaMalloc((void **)&d_result,sizeof(*d_result)));
	float zero = 0.0f;
	std::list<int> translations;
	for (int i = 0; i<test_num;i++){
		translations.clear();
		for (int j = 0; j<transformation_vol;++j)
			cam_score_instance->h_cam[j] = 0.0f;
		for (int t = 0; t < translation_vol/2;++t){
			tc = rand()%translation_vol;
			while(std::find(translations.begin(),translations.end(),tc) != translations.end())
				tc = rand()%translation_vol;
			translations.push_back(tc);
			for (int j = 0; j< rotation_vol;++j)
				cam_score_instance->h_cam[tc*rotation_vol + j] = 1.0;
		}
		for (int t = 0; t < translation_vol; ++t){
			CudaSafeCall(cudaMemcpy(cam_score_instance->d_dist + t*translation_stride,cam_score_instance->h_cam + t*rotation_vol,rotation_vol*sizeof(*cam_score_instance->d_dist),cudaMemcpyHostToDevice));
			cudaDeviceSynchronize();
		}
		CudaSafeCall(cudaMemcpy(d_result,&zero,sizeof(*d_result),cudaMemcpyHostToDevice));
		cudaDeviceSynchronize();
	//	template <float (*F)(float &),uint BLOCKSIZE>
	//	__global__
	//	void quad_simple(uint * d_refinements, size_t translation_offset, size_t translation_stride, uint trans_node_dim,
	//			float * d_trans_weights, double * d_weights, float * d_dist, float * d_result)
		quad_simple<idf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride, trans_node_dim,
				d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
		CudaCheckError();
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
		cudaDeviceSynchronize();
		printf("Result of first quadrature: %.e10\n",h_result);
		div_by_quad_simple<<<gridDim,blockDim>>>(translation_volume,translation_stride,rotation_vol,cam_score_instance->d_dist,
		h_result);
		CudaCheckError();
		cudaDeviceSynchronize();
		quad_simple<idf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride, trans_node_dim,
				d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
		printf("Quadrature of distribution: %f\n",h_result);

		h_result = 0.0f;
		CudaSafeCall(cudaMalloc((void **)&d_result,sizeof(*d_result)));
		CudaCheckError();
		cudaDeviceSynchronize();

//	template <float (*F)(float &),uint BLOCKSIZE>
//	__global__
//	void quad_simple(uint * d_refinements, size_t translation_offset, size_t translation_stride, uint trans_node_dim,
//			float * d_trans_weights, double * d_weights, float * d_dist, float * d_result)
		quad_simple<entf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride,
				trans_node_dim,d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
		CudaCheckError();
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
		printf("Information gain of distribution: %f\n",entropy_uniform-h_result);
		printf("Completed test %i ...\n",i);
	}
	printf("==================================== END OF TEST =========================================\n");
}


void CamScore::entropy_test_one(const int & gpu_index){
	//one tranlation, one rotation 1, rest 0
	float entropy_uniform = std::log(8*(float) M_PI * (float) M_PI);
	srand (time(NULL));
	CudaSafeCall(cudaSetDevice(gpu_index));
	printf("Started entropy test one ...\n");
	CamScore_Register * cam_score_instance = &_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = cam_score_instance->trans_node;
	McEwenNodes_Register * rot_nodes_instance = cam_score_instance->rot_node;
	uint rotation_vol = rot_nodes_instance->h_max_nodes[0];
	uint3 * t_node_dim = trans_nodes_instance->h_node_dim;
	//assume isotropy
	uint trans_node_dim = t_node_dim->x;
	uint translation_volume = t_node_dim->x*t_node_dim->y*t_node_dim->z;
	float * d_trans_weights = trans_nodes_instance->d_weights;
	int gridDim = 128;
	int blockDim = 256;
	uint translation_vol = t_node_dim->x*t_node_dim->y*t_node_dim->z;
	uint transformation_vol = rotation_vol*translation_vol;
	size_t translation_stride = 4*rotation_vol + 3*rot_nodes_instance->h_num_refinements[0];
	int test_num = 10;
//	float * h_scores = cam_score_instance->h_cam;
//	float * d_scores = cam_score_instance->d_v_ave;
	int t;
	int r;
	float * d_result;
	float h_result;
	CudaSafeCall(cudaMalloc((void **)&d_result,sizeof(*d_result)));
	float zero = 0.0f;

	for (int i = 0; i<test_num;i++){
		t = rand() % translation_vol;
		r = rand() % rotation_vol;
		for (int j = 0; j<transformation_vol;++j){
			cam_score_instance->h_cam[j] = 0.0f;
			if (t*rotation_vol + r == j)
				cam_score_instance->h_cam[j] = 1.0f;
		}
//		printf("Random spike at: %i\n",t*rotation_vol + r);
		for (int t = 0; t < translation_vol; ++t){
			CudaSafeCall(cudaMemcpy(cam_score_instance->d_dist + t*translation_stride,cam_score_instance->h_cam + t*rotation_vol,rotation_vol*sizeof(*cam_score_instance->d_dist),cudaMemcpyHostToDevice));
			cudaDeviceSynchronize();
		}
		CudaSafeCall(cudaMemcpy(d_result,&zero,sizeof(*d_result),cudaMemcpyHostToDevice));
		cudaDeviceSynchronize();
	//	template <float (*F)(float &),uint BLOCKSIZE>
	//	__global__
	//	void quad_simple(uint * d_refinements, size_t translation_offset, size_t translation_stride, uint trans_node_dim,
	//			float * d_trans_weights, double * d_weights, float * d_dist, float * d_result)
		quad_simple<idf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride, trans_node_dim,
				d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
		CudaCheckError();
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
		cudaDeviceSynchronize();
		printf("Result of first quadrature: %.e10\n",h_result);
		div_by_quad_simple<<<gridDim,blockDim>>>(translation_volume,translation_stride,rotation_vol,cam_score_instance->d_dist,
		h_result);
		CudaCheckError();
		cudaDeviceSynchronize();
		quad_simple<idf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride, trans_node_dim,
				d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
		printf("Quadrature of distribution: %f\n",h_result);

		h_result = 0.0f;
		CudaSafeCall(cudaMalloc((void **)&d_result,sizeof(*d_result)));
		CudaCheckError();
		cudaDeviceSynchronize();

//	template <float (*F)(float &),uint BLOCKSIZE>
//	__global__
//	void quad_simple(uint * d_refinements, size_t translation_offset, size_t translation_stride, uint trans_node_dim,
//			float * d_trans_weights, double * d_weights, float * d_dist, float * d_result)
		quad_simple<entf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride,
				trans_node_dim,d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
		CudaCheckError();
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
		printf("Information gain of distribution: %f\n",entropy_uniform-h_result);
		printf("Completed test %i ...\n",i);
	}
	printf("==================================== END OF TEST =========================================\n");
}

void CamScore::entropy_test_zero(const int & gpu_index){
	//one tranlation, one rotation 1, rest 0
	float entropy_uniform = std::log(8*(float) M_PI * (float) M_PI);
	CudaSafeCall(cudaSetDevice(gpu_index));
	printf("Started entropy test zero: Information gain of a uniform distribution ...\n");
	CamScore_Register * cam_score_instance = &_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = cam_score_instance->trans_node;
	McEwenNodes_Register * rot_nodes_instance = cam_score_instance->rot_node;
	uint rotation_vol = rot_nodes_instance->h_max_nodes[0];
	uint3 * t_node_dim = trans_nodes_instance->h_node_dim;
	//assume isotropy
	uint trans_node_dim = t_node_dim->x;
	uint translation_volume = t_node_dim->x*t_node_dim->y*t_node_dim->z;
	float * d_trans_weights = trans_nodes_instance->d_weights;
	int gridDim = 128;
	int blockDim = 256;
	uint translation_vol = t_node_dim->x*t_node_dim->y*t_node_dim->z;
	uint transformation_vol = rotation_vol*translation_vol;
	size_t translation_stride = 4*rotation_vol + 3*rot_nodes_instance->h_num_refinements[0];
//	float * h_scores = cam_score_instance->h_cam;
//	float * d_scores = cam_score_instance->d_v_ave;
	float * d_result;
	float h_result;
	CudaSafeCall(cudaMalloc((void **)&d_result,sizeof(*d_result)));
	float zero = 0.0f;
	for (int j = 0; j<transformation_vol;++j){
		cam_score_instance->h_cam[j] = 1.0f;
	}
	for (int t = 0; t < translation_vol; ++t){
		CudaSafeCall(cudaMemcpy(cam_score_instance->d_dist + t*translation_stride,cam_score_instance->h_cam + t*rotation_vol,rotation_vol*sizeof(*cam_score_instance->d_dist),cudaMemcpyHostToDevice));
		cudaDeviceSynchronize();
	}
	CudaSafeCall(cudaMemcpy(d_result,&zero,sizeof(*d_result),cudaMemcpyHostToDevice));
	cudaDeviceSynchronize();
//	template <float (*F)(float &),uint BLOCKSIZE>
//	__global__
//	void quad_simple(uint * d_refinements, size_t translation_offset, size_t translation_stride, uint trans_node_dim,
//			float * d_trans_weights, double * d_weights, float * d_dist, float * d_result)
	quad_simple<idf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride, trans_node_dim,
			d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
	CudaCheckError();
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
	cudaDeviceSynchronize();
	printf("Result of first quadrature: %.e10\n",h_result);
	div_by_quad_simple<<<gridDim,blockDim>>>(translation_volume,translation_stride,rotation_vol,cam_score_instance->d_dist,
	h_result);
	CudaCheckError();
	cudaDeviceSynchronize();
	quad_simple<idf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride, trans_node_dim,
			d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
	printf("Quadrature of distribution: %f\n",h_result);

	h_result = 0.0f;
	CudaSafeCall(cudaMalloc((void **)&d_result,sizeof(*d_result)));
	CudaCheckError();
	cudaDeviceSynchronize();

//	template <float (*F)(float &),uint BLOCKSIZE>
//	__global__
//	void quad_simple(uint * d_refinements, size_t translation_offset, size_t translation_stride, uint trans_node_dim,
//			float * d_trans_weights, double * d_weights, float * d_dist, float * d_result)
	quad_simple<entf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, 0,translation_stride,
			trans_node_dim,d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);
	CudaCheckError();
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(&h_result,d_result,sizeof(h_result),cudaMemcpyDeviceToHost));
	printf("Information gain of distribution: %f\n",entropy_uniform-h_result);
	printf("==================================== END OF TEST =========================================\n");
}

size_t CamScore::getMaxTranslations(McEwenNodes *const& rot_nodes,const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	uint rotation_vol = rot_nodes->_instance_registry[gpu_index].h_max_nodes[0];
	size_t sweep_space_vol = 4*rotation_vol + 3*rot_nodes->_instance_registry[gpu_index].h_num_refinements[0];
	size_t free_memory;
	size_t total_memory;
	CudaSafeCall(cudaMemGetInfo(&free_memory,&total_memory));
	size_t effective_free_memory = (size_t)(0.9f*(float)free_memory);
	//assumes floating point precision of working mem. possibly okay
	return effective_free_memory/(sweep_space_vol*sizeof(float));
}

void CamScore::createInstance(ChebychevNodes *const& trans_nodes,McEwenNodes *const& rot_nodes, const size_t & translation_vol, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		CamScore_Register instance;
		instance.trans_node = &trans_nodes->_instance_registry[gpu_index];
		instance.rot_node = &rot_nodes->_instance_registry[gpu_index];
		uint rotation_vol = instance.rot_node->h_max_nodes[0];
		uint transformation_vol = rotation_vol*translation_vol;
		instance.h_cam = (float *) malloc(sizeof(*instance.h_cam)*transformation_vol);
		instance.h_translation_vol = (size_t *) malloc(sizeof(*instance.h_translation_vol));
		CudaSafeCall(cudaMalloc((void **) &instance.d_translation_vol,sizeof(*instance.d_translation_vol)));
		//(d_v_ave[rot_vol] -- d_v_m_v_ave[rot_vol] -- d_u_t_v[rot_vol] -- dist[rot_vol] -- (mins + quads + Hs)[sweepnum])*transl_vol
		size_t sweep_space_vol = 4*rotation_vol + 3*instance.rot_node->h_num_refinements[0];
		CudaSafeCall(cudaMalloc((void **) &instance.d_working_mem,sweep_space_vol*translation_vol*sizeof(float)));
		instance.d_v_ave = instance.d_working_mem;
		instance.d_v_m_v_ave = instance.d_v_ave + rotation_vol;
		instance.d_u_t_v = instance.d_v_m_v_ave + rotation_vol;
		instance.d_dist = instance.d_u_t_v + rotation_vol;
		instance.d_entropies = instance.d_dist + rotation_vol;
		instance.h_translation_vol[0] = translation_vol;
		CudaSafeCall(cudaMemcpy(instance.d_translation_vol,instance.h_translation_vol,sizeof(*instance.h_translation_vol),cudaMemcpyHostToDevice));

		_instance_registry[gpu_index] = instance;
	}
}
#include<string>
void CamScore::createInstance(ChebychevNodes *const& trans_nodes, const char * file_rot_nodes, const size_t & translation_vol, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		CamScore_Register instance;
		instance.trans_node = &trans_nodes->_instance_registry[gpu_index];
		//parse file
		std::ifstream ifs;
		std::string line;
		std::string cell;
		ifs.open(file_rot_nodes);
		std::vector<float> angles;
		while (std::getline(ifs,line)){
			std::stringstream lineStream(line);
			while(std::getline(lineStream,cell, ','))
		    {
				angles.push_back(std::stof(cell.c_str()));
			}
		}
		float * h_rot_nodes = (float *) malloc(angles.size()*sizeof(*h_rot_nodes));
		for (int i=0;i<angles.size();++i)
			h_rot_nodes[i] = angles[i];
		instance.h_rot_vol = (size_t *) malloc(sizeof(*instance.h_rot_vol));
		instance.h_rot_vol[0] = angles.size()/3;
		CudaSafeCall(cudaMalloc((void **) &instance.d_rot_vol,sizeof(*instance.d_rot_vol)));
		CudaSafeCall(cudaMemcpy(instance.d_rot_vol,instance.h_rot_vol,sizeof(*instance.d_rot_vol),cudaMemcpyHostToDevice));
		CudaSafeCall(cudaMalloc((void **)&instance.d_rot_nodes,3*instance.h_rot_vol[0]*sizeof(instance.d_rot_nodes)));
		CudaSafeCall(cudaMemcpy(instance.d_rot_nodes,h_rot_nodes,3*instance.h_rot_vol[0]*sizeof(instance.d_rot_nodes),cudaMemcpyHostToDevice));
		uint transformation_vol = instance.h_rot_vol[0]*translation_vol;
		instance.h_cam = (float *) malloc(sizeof(*instance.h_cam)*transformation_vol);
		instance.h_translation_vol = (size_t *) malloc(sizeof(*instance.h_translation_vol));
		CudaSafeCall(cudaMalloc((void **) &instance.d_translation_vol,sizeof(*instance.d_translation_vol)));
		//(d_v_ave[rot_vol] -- d_v_m_v_ave[rot_vol] -- d_u_t_v[rot_vol] -- dist[rot_vol] -- (mins + quads + Hs))*transl_vol
		size_t sweep_space_vol = 4*instance.h_rot_vol[0] + 3;
		CudaSafeCall(cudaMalloc((void **) &instance.d_working_mem,sweep_space_vol*translation_vol*sizeof(float)));
		instance.d_v_ave = instance.d_working_mem;
		instance.d_v_m_v_ave = instance.d_v_ave + instance.h_rot_vol[0];
		instance.d_u_t_v = instance.d_v_m_v_ave + instance.h_rot_vol[0];
		instance.d_dist = instance.d_u_t_v + instance.h_rot_vol[0];
		instance.d_entropies = instance.d_dist + instance.h_rot_vol[0];
		instance.h_translation_vol[0] = translation_vol;
		CudaSafeCall(cudaMemcpy(instance.d_translation_vol,instance.h_translation_vol,sizeof(*instance.h_translation_vol),cudaMemcpyHostToDevice));
		_instance_registry[gpu_index] = instance;
		free(h_rot_nodes);
	}
}

void CamScore::destroyInstance(const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	CamScore_Register * instance = &_instance_registry[gpu_index];
	free(instance->h_rot_vol);
	cudaFree(instance->d_rot_vol);
    cudaFree(instance->d_rot_nodes);
    free(instance->h_cam);
    free(instance->h_translation_vol);
    cudaFree(instance->d_translation_vol);
    cudaFree(instance->d_working_mem);
    _instance_registry.erase(gpu_index);
}

void CamScore::score_filenodes(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes,const char * out_file,
		const int& translation_offset, const int& translation_volume, const int& gridDim, const int& blockDim, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	CamScore_Register * cam_score_instance = &_instance_registry[gpu_index];
	Density_Register * density_instance_target = &target->_instance_registry[gpu_index];
	Density_Register * density_instance_query = &query->_instance_registry[gpu_index];
	ChebychevNodes_Register * scan_nodes_instance = &scan_nodes->_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = cam_score_instance->trans_node;

	//init concat fields
	uint rotation_vol = cam_score_instance->h_rot_vol[0];

	size_t translation_stride = 4*rotation_vol;// + 3*rot_nodes_instance->h_num_refinements[0];
	uint init_vol = translation_stride*translation_volume;
//	__global__
//	void initZero(float * d_target, size_t vol){
	initValue<<<gridDim,blockDim>>>(cam_score_instance->d_working_mem,init_vol,0.0f);
	cudaDeviceSynchronize();

	float * h_u_ave = (float *) malloc(sizeof(*h_u_ave));
	h_u_ave[0] = 0.0f;
	float * d_u_ave;
	CudaSafeCall(cudaMalloc((void **)&d_u_ave,sizeof(*d_u_ave)));
	CudaSafeCall(cudaMemcpy(d_u_ave,h_u_ave,sizeof(*d_u_ave),cudaMemcpyHostToDevice));

	dim3 cheby_dim = scan_nodes_instance->h_node_dim[0];
	dim3 block_dim = {8,8,8};
	dim3 grid_dim = {cheby_dim.x/block_dim.x,cheby_dim.y/block_dim.y,cheby_dim.z/block_dim.z}; //number of blocks
	float3 * h_scale = (float3 *) malloc(sizeof(*h_scale));
	float3 * d_scale;
	CudaSafeCall(cudaMalloc((void **)&d_scale,sizeof(*d_scale)));
	//the normalized coordinates of texture memory refer to the pixel dims, therefore the relative size of
	//two volumes is characterized by the ratio of pixel dims.
	assert(density_instance_query->h_pixel_size[0] == density_instance_target->h_pixel_size[0]);
	h_scale[0] = {((float) density_instance_query->h_pixel_dim[0].x)/((float) density_instance_target->h_pixel_dim[0].x),
	              ((float) density_instance_query->h_pixel_dim[0].y)/((float) density_instance_target->h_pixel_dim[0].y),
	              ((float) density_instance_query->h_pixel_dim[0].z)/((float) density_instance_target->h_pixel_dim[0].z)};
	CudaSafeCall(cudaMemcpy(d_scale,h_scale,sizeof(*d_scale),cudaMemcpyHostToDevice));
	if (density_instance_target->textureObj == nullptr || density_instance_query->textureObj == nullptr ){
		printf("Density needs to be transported to texture memory for this feature to work.\n");
		std::exit(0);
	}

	//independent of sweeps, need for cam score calculation. do beforehand.
//	u_ave_scan<squaref,512><<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,
//	d_u_ave);
	CudaCheckError();
	cudaDeviceSynchronize();

	printf("Scoring for translations %i -- %i \n",translation_offset,translation_offset+translation_volume);

	v_ave_filenodes<512><<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],d_scale,
			scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
			cam_score_instance->d_rot_nodes,cam_score_instance->h_rot_vol[0],translation_offset,translation_volume,translation_stride,
			cam_score_instance->d_v_ave);
	CudaCheckError();
	cudaDeviceSynchronize();

	u_t_v_filenodes<512><<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],d_scale,
			scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
			cam_score_instance->d_rot_nodes,cam_score_instance->h_rot_vol[0],translation_offset,translation_volume,translation_stride,
			cam_score_instance->d_v_ave,cam_score_instance->d_v_m_v_ave,cam_score_instance->d_u_t_v);
	CudaCheckError();
	cudaDeviceSynchronize();

//		__global__
//		void cam_score(	int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
//				size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v, float * d_u_ave)

	//TODO determine max number of blocks/threads
	cam_score_filenodes<<<128,256>>>(cam_score_instance->d_rot_nodes,cam_score_instance->h_rot_vol[0],translation_offset,translation_volume,translation_stride,
			cam_score_instance->d_v_ave,cam_score_instance->d_v_m_v_ave,cam_score_instance->d_u_t_v,d_u_ave);
	CudaCheckError();
	cudaDeviceSynchronize();

	float * h_out = (float *) malloc(cam_score_instance->h_rot_vol[0]*sizeof(h_out));
	CudaSafeCall(cudaMemcpy(h_out,cam_score_instance->d_v_ave,cam_score_instance->h_rot_vol[0]*sizeof(float),cudaMemcpyDeviceToHost));
	cudaDeviceSynchronize();
	std::fstream x_FileHandle;
	x_FileHandle.open(out_file, std::fstream::out);
	x_FileHandle.seekg(0, std::ios::beg);
	for (int r = 0;r<cam_score_instance->h_rot_vol[0];++r) x_FileHandle << h_out[r] << ",";
	x_FileHandle.flush();
	x_FileHandle.close();

}

//defunct, copy functionality of scoreAndMin
void CamScore::score(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes,
		const int& translation_offset, const int& translation_volume, const int& gridDim, const int& blockDim, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	CamScore_Register * cam_score_instance = &_instance_registry[gpu_index];
	Density_Register * density_instance_target = &target->_instance_registry[gpu_index];
	Density_Register * density_instance_query = &query->_instance_registry[gpu_index];
	ChebychevNodes_Register * scan_nodes_instance = &scan_nodes->_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = cam_score_instance->trans_node;
	McEwenNodes_Register * rot_nodes_instance = cam_score_instance->rot_node;
	uint rotation_vol = rot_nodes_instance->h_max_nodes[0];
	size_t translation_stride = 4*rotation_vol + 3*rot_nodes_instance->h_num_refinements[0];
	size_t sweep_size = 4*rotation_vol;
	uint init_vol = translation_stride*translation_volume;
	//	__global__
	//	void initZero(float * d_target, size_t vol){
	initValue<<<gridDim,blockDim>>>(cam_score_instance->d_working_mem,init_vol,0.0f);
	cudaDeviceSynchronize();
	float * h_u_ave = (float *) malloc(sizeof(*h_u_ave));
	h_u_ave[0] = 0.0f;
	float * d_u_ave;
	CudaSafeCall(cudaMalloc((void **)&d_u_ave,sizeof(*d_u_ave)));
	CudaSafeCall(cudaMemcpy(d_u_ave,h_u_ave,sizeof(*d_u_ave),cudaMemcpyHostToDevice));
	dim3 cheby_dim = scan_nodes_instance->h_node_dim[0];
	dim3 block_dim = {8,8,8};
	dim3 grid_dim = {cheby_dim.x/block_dim.x,cheby_dim.y/block_dim.y,cheby_dim.z/block_dim.z}; //number of blocks
	float3 * h_scale = (float3 *) malloc(sizeof(*h_scale));
	float3 * d_scale;
	CudaSafeCall(cudaMalloc((void **)&d_scale,sizeof(*d_scale)));
	//the normalized coordinates of texture memory refer to the pixel dims, therefore the relative size of
	//two volumes is characterized by the ratio of pixel dims.
	assert(density_instance_query->h_pixel_size[0] == density_instance_target->h_pixel_size[0]);
	h_scale[0] = {((float) density_instance_query->h_pixel_dim[0].x)/((float) density_instance_target->h_pixel_dim[0].x),
	              ((float) density_instance_query->h_pixel_dim[0].y)/((float) density_instance_target->h_pixel_dim[0].y),
	              ((float) density_instance_query->h_pixel_dim[0].z)/((float) density_instance_target->h_pixel_dim[0].z)};
	CudaSafeCall(cudaMemcpy(d_scale,h_scale,sizeof(*d_scale),cudaMemcpyHostToDevice));
	if (density_instance_target->textureObj == nullptr || density_instance_query->textureObj == nullptr ){
		printf("Density needs to be transported to texture memory for this feature to work.\n");
		std::exit(0);
	}

	//independent of sweeps, need for cam score calculation. do beforehand.
//	u_ave_scan<squaref,512><<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,
//	d_u_ave);
	CudaCheckError();
	cudaDeviceSynchronize();

	size_t sweep_num = 3;//rot_nodes_instance->h_num_refinements[0];
	//SWEEP LOOP
	CudaCheckError();
	printf("Scoring for translations %i -- %i \n",translation_offset,translation_offset+translation_volume);
	size_t rot_weight_offset = 0;
	for (int s = 0; s < sweep_num; ++s){
		printf("Scoring sweep %i - refinements: %u %u %u ...\n",s,rot_nodes_instance->h_refinements[3*s],rot_nodes_instance->h_refinements[3*s+1],rot_nodes_instance->h_refinements[3*s+2]);
		//1. v_ave
		//2. <v-v_ave | v-v_ave> <v-v_ave | u-u_ave>
		//3. <u-u_ave | u-u_ave>
		//4. cam_score
		//5. distributify
		//6. calc entropy

//		check_mapping<<<128,256>>>(s,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,
//				translation_volume,translation_stride,cam_score_instance->d_v_ave);
//		CudaCheckError();
//		cudaDeviceSynchronize();
//		continue;

		//1.
//		template <uint BLOCKSIZE>
//		__global__
//		void v_ave_sweep(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, float3 * d_scale,
//				float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
//				int s, uint * d_refinements, size_t d_num_refinements, int translation_offset, int translation_volume, size_t translation_stride,
//				float * d_v_ave)
		v_ave_sweep<<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],d_scale,
				scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
				s,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_volume,translation_stride,
				cam_score_instance->d_v_ave);
		CudaCheckError();
		cudaDeviceSynchronize();

		//2.
//		void u_t_v_sweep(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, float3 * d_scale,
//				float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
//				int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
//				size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v)
		u_t_v_sweep<<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],d_scale,
				scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
				s,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_volume,translation_stride,
				cam_score_instance->d_v_ave,cam_score_instance->d_v_m_v_ave,cam_score_instance->d_u_t_v,d_u_ave);
		CudaCheckError();
		cudaDeviceSynchronize();

//		__global__
//		void cam_score(	int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
//				size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v, float * d_u_ave)

		//TODO determine max number of blocks/threads
		cam_score<<<128,256>>>(s,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_volume,translation_stride,
				cam_score_instance->d_v_ave,cam_score_instance->d_v_m_v_ave,cam_score_instance->d_u_t_v,d_u_ave);
		CudaCheckError();
		cudaDeviceSynchronize();

		//distritbutify!
		//1. find min
		//2. shift by min
		//3. quadrature
		//4. divide to normalize

//		template <uint BLOCKSIZE>
//		__global__
//		void min_sweep(int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,size_t translation_stride,
//				size_t sweep_size, float * d_scores)
		min_sweep<1024><<<translation_volume,1024>>>(s,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],
				translation_stride,sweep_size,cam_score_instance->d_v_ave);
		CudaCheckError();
		cudaDeviceSynchronize();

//		__global__
//		void shift_by_min(int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
//				size_t translation_stride, size_t rot_vol, float * d_scores, float * d_dist)
		shift_by_min<<<translation_volume,1024>>>(s,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_stride,
				rotation_vol,cam_score_instance->d_v_ave,cam_score_instance->d_dist);
		CudaCheckError();
		cudaDeviceSynchronize();

//		template <uint BLOCKSIZE>
//		__global__
//		void quad_sweep(int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,size_t translation_stride,
//				float * d_weights, float * d_dist)
		quad_sweep<idf,1024><<<translation_volume,1024>>>(s,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],
				translation_offset,translation_stride,rot_nodes_instance->d_weights+rot_weight_offset,cam_score_instance->d_dist,1);
		CudaCheckError();
		cudaDeviceSynchronize();


//		__global__
//		void div_by_ave(int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset, size_t translation_stride,
//				float * d_dist){
		div_by_ave<<<translation_volume,1024>>>(s,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_stride,
				cam_score_instance->d_dist);

		quad_sweep<entf,1024><<<translation_volume,1024>>>(s,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],
				translation_offset,translation_stride,rot_nodes_instance->d_weights+rot_weight_offset,cam_score_instance->d_dist,2);

		rot_weight_offset += rot_nodes_instance->h_refinements[3*s + 1];

		continue;
	}

//	for (int i=0;i<sweep_num;i++)
//		printf("Ref %i counts %i\n",i,thrust::count(thrust::device_pointer_cast(&cam_score_instance->d_v_ave[0]),
//				thrust::device_pointer_cast(&cam_score_instance->d_v_ave[rotation_vol]),(float)(i+1)));

	int t = 0;
	int s = 1;
	int rot_num = rot_nodes_instance->h_refinements[3*s]*rot_nodes_instance->h_refinements[3*s+1]*rot_nodes_instance->h_refinements[3*s+2];
	float * h_out = (float *) malloc(rot_num*sizeof(float));
	float * d_get_score = cam_score_instance->d_v_ave + t*translation_stride;
	uint M_c = rot_nodes_instance->h_refinements[3*s];
	uint L_c = rot_nodes_instance->h_refinements[3*s+1];
	uint N_c = rot_nodes_instance->h_refinements[3*s+2];
	uint M_m = rot_nodes_instance->h_refinements[3*(rot_nodes_instance->h_num_refinements[0]-1)];
	uint L_m = rot_nodes_instance->h_refinements[3*(rot_nodes_instance->h_num_refinements[0]-1)+1];
	uint N_m = rot_nodes_instance->h_refinements[3*(rot_nodes_instance->h_num_refinements[0]-1)+2];
	uint a,b,g;
//			printf("%u %u %u | %i --> %u %u %u\n",M_c,L_c,N_c,r,a,b,g);
	//if s!=0, the rotation might have been handled in a preceding sweep. check for this.
	//(a,b,g)_local -- > (a,b,g)_global = (a_local*(M_m/M_c),b_local*((2*L_m-1)/(2*M_c-1)),g_local*(N_m/N_c))
	//(a,g,g)_global --> linear((a,b,g)_global) = b*M_m*N_m + g*M_m + a {designed to make b-indices to be
	//positioned next to each other}
	uint rot_linear_index;
	for (int r = 0; r< rot_num;++r){
		a = r/(L_c*N_c);
		b = (r - a*L_c*N_c)/N_c;
		g = r-b*N_c - a*L_c*N_c;
		rot_linear_index = (2*b+1)*((2*L_m-1)/(2*L_c-1))*M_m*N_m + g*(N_m/N_c)*M_m + a*(M_m/M_c);
		CudaSafeCall(cudaMemcpy(&h_out[r],&d_get_score[rot_linear_index],sizeof(float),cudaMemcpyDeviceToHost));
	}
	cudaDeviceSynchronize();
	std::fstream x_FileHandle;
	x_FileHandle.open("test_file2.txt", std::fstream::out);
	x_FileHandle.seekg(0, std::ios::beg);
	for (int r = 0;r<rot_num;++r) x_FileHandle << h_out[r] << ",";
	x_FileHandle.close();

	free(h_u_ave);
	CudaSafeCall(cudaFree(d_u_ave));
	CudaSafeCall(cudaFree(d_scale));
}

void CamScore::divAndEnt(ChebychevNodes * const& trans_nodes,const int& translation_offset,
		const int& translation_volume, float * const& h_quad, float * const& h_result, const int& gridDim,
		const int& blockDim, const int & gpu_index){
	//translation_volume means the total volume allocated on the device.
	CudaSafeCall(cudaSetDevice(gpu_index));
	h_result[0] = 0.0f;
	float * d_result;
	CudaSafeCall(cudaMalloc((void **)&d_result,sizeof(*d_result)));
	CudaSafeCall(cudaMemcpy(d_result,h_result,sizeof(*d_result),cudaMemcpyHostToDevice));
	//assume isotropy
	uint trans_node_dim = trans_nodes->_instance_registry[gpu_index].h_node_dim[0].x;
	float * d_trans_weights = trans_nodes->_instance_registry[gpu_index].d_weights;

	CamScore_Register * cam_score_instance = &_instance_registry[gpu_index];
	McEwenNodes_Register * rot_nodes_instance = cam_score_instance->rot_node;
	//init concat fields
	uint rotation_vol = rot_nodes_instance->h_max_nodes[0];
	size_t translation_stride = 4*rotation_vol + 3*rot_nodes_instance->h_num_refinements[0];
//	__global__
//	void div_by_quad_simple(const int translation_volume, const size_t translation_stride, const size_t rot_vol,
//			float * d_dist, float h_quad)
	div_by_quad_simple<<<gridDim,blockDim>>>(translation_volume,translation_stride,rotation_vol,cam_score_instance->d_dist,
			h_quad[0]);
	CudaCheckError();
	cudaDeviceSynchronize();

//	template <float (*F)(float &),uint BLOCKSIZE>
//	__global__
//	void quad_simple(uint * d_refinements, size_t translation_offset, size_t translation_stride, uint trans_node_dim,
//			float * d_trans_weights, double * d_weights, float * d_dist, float * d_result)
	quad_simple<entf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, translation_offset,translation_stride,
			trans_node_dim,d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);

	CudaCheckError();
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(h_result,d_result,sizeof(*d_result),cudaMemcpyDeviceToHost));
	cudaDeviceSynchronize();
}

void CamScore::shiftAndQuad(ChebychevNodes * const& trans_nodes,const int& translation_offset,
		const int& translation_volume, float * const& h_min, float * const& h_result, const int& gridDim,
		const int& blockDim, const int & gpu_index){
	//translation_volume means the total volume allocated on the device.
	CudaSafeCall(cudaSetDevice(gpu_index));
	float * d_result;
	CudaSafeCall(cudaMalloc((void **)&d_result,sizeof(*d_result)));
	float zero = 0.0f;
	CudaSafeCall(cudaMemcpy(d_result,&zero,sizeof(*d_result),cudaMemcpyHostToDevice));
	//assume isotropy
	uint trans_node_dim = trans_nodes->_instance_registry[gpu_index].h_node_dim[0].x;
	float * d_trans_weights = trans_nodes->_instance_registry[gpu_index].d_weights;

	CamScore_Register * cam_score_instance = &_instance_registry[gpu_index];
	McEwenNodes_Register * rot_nodes_instance = cam_score_instance->rot_node;
	//init concat fields
	uint rotation_vol = rot_nodes_instance->h_max_nodes[0];
	size_t translation_stride = 4*rotation_vol + 3*rot_nodes_instance->h_num_refinements[0];
//	__global__
//	void shift_by_min_simple(const int translation_offset, const int translation_volume, const size_t translation_stride, const size_t rot_vol,
//			float * d_scores, float * d_dist, float h_min)
	shift_by_min_simple<<<gridDim,blockDim>>>(translation_offset,translation_volume,translation_stride,rotation_vol,cam_score_instance->d_v_ave,
			cam_score_instance->d_dist,h_min[0]);
	CudaCheckError();
	cudaDeviceSynchronize();

//	template <float (*F)(float &),uint BLOCKSIZE>
//	__global__
//	void quad_simple(uint * d_refinements, size_t translation_offset, size_t translation_stride, uint trans_node_dim,
//			float * d_trans_weights, double * d_weights, float * d_dist, float * d_result)
	quad_simple<idf,1024><<<translation_volume,1024>>>(rot_nodes_instance->d_refinements, translation_offset,translation_stride, trans_node_dim,
			d_trans_weights, rot_nodes_instance->d_weights, cam_score_instance->d_dist,d_result);

	CudaCheckError();
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(h_result,d_result,sizeof(*d_result),cudaMemcpyDeviceToHost));

}

void CamScore::scoreBench(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes,
		const int& translation_offset, const int& translation_volume, float * const& h_translation_vol,
		const int& gridDim, const int& blockDim, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	CamScore_Register * cam_score_instance = &_instance_registry[gpu_index];
	Density_Register * density_instance_target = &target->_instance_registry[gpu_index];
	Density_Register * density_instance_query = &query->_instance_registry[gpu_index];
	ChebychevNodes_Register * scan_nodes_instance = &scan_nodes->_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = cam_score_instance->trans_node;
	McEwenNodes_Register * rot_nodes_instance = cam_score_instance->rot_node;

	h_translation_vol[0] = (float) vol((density_instance_target->h_pixel_dim[0] - density_instance_query->h_pixel_dim[0]));
	uint rotation_vol = rot_nodes_instance->h_max_nodes[0];
	size_t translation_stride = 4*rotation_vol + 3*rot_nodes_instance->h_num_refinements[0];
	size_t sweep_size = 4*rotation_vol;
	uint init_vol = translation_stride*translation_volume;
	//	__global__
	//	void initZero(float * d_target, size_t vol){
	initValue<<<gridDim,blockDim>>>(cam_score_instance->d_working_mem,init_vol,0.0f);
	cudaDeviceSynchronize();
	float * h_u_ave = (float *) malloc(sizeof(*h_u_ave));
	h_u_ave[0] = 0.0f;
	float * d_u_ave;
	CudaSafeCall(cudaMalloc((void **)&d_u_ave,sizeof(*d_u_ave)));
	CudaSafeCall(cudaMemcpy(d_u_ave,h_u_ave,sizeof(*d_u_ave),cudaMemcpyHostToDevice));
	dim3 cheby_dim = scan_nodes_instance->h_node_dim[0];
	dim3 block_dim = {8,8,8};
	dim3 grid_dim = {cheby_dim.x/block_dim.x,cheby_dim.y/block_dim.y,cheby_dim.z/block_dim.z}; //number of blocks
	float3 * h_scale = (float3 *) malloc(sizeof(*h_scale));
	float3 * d_scale;
	CudaSafeCall(cudaMalloc((void **)&d_scale,sizeof(*d_scale)));
	//the normalized coordinates of texture memory refer to the pixel dims, therefore the relative size of
	//two volumes is characterized by the ratio of pixel dims.
	assert(density_instance_query->h_pixel_size[0] == density_instance_target->h_pixel_size[0]);
	h_scale[0] = {((float) density_instance_query->h_pixel_dim[0].x)/((float) density_instance_target->h_pixel_dim[0].x),
	              ((float) density_instance_query->h_pixel_dim[0].y)/((float) density_instance_target->h_pixel_dim[0].y),
	              ((float) density_instance_query->h_pixel_dim[0].z)/((float) density_instance_target->h_pixel_dim[0].z)};
	CudaSafeCall(cudaMemcpy(d_scale,h_scale,sizeof(*d_scale),cudaMemcpyHostToDevice));
	if (density_instance_target->textureObj == nullptr || density_instance_query->textureObj == nullptr ){
		printf("Density needs to be transported to texture memory for this feature to work.\n");
		std::exit(0);
	}

	//independent of sweeps, need for cam score calculation. do beforehand.
	u_ave_scan<<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,
	d_u_ave);
	CudaCheckError();
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(h_u_ave,d_u_ave,sizeof(*d_u_ave),cudaMemcpyDeviceToHost));
	printf("Average for query function: %.10e\n",h_u_ave[0]);
	float * d_u_norm;
	float * h_u_norm = (float *) malloc(sizeof(*h_u_norm));
	h_u_norm[0] = 0.0f;
	CudaSafeCall(cudaMalloc((void**)&d_u_norm,sizeof(*d_u_norm)));
	CudaSafeCall(cudaMemcpy(d_u_norm,h_u_norm,sizeof(*d_u_norm),cudaMemcpyHostToDevice));

	//	void u_norm_scan(cudaTextureObject_t d_density_query, float * d_cheby_nodes, float * d_cheby_weights,
//			float * d_u_ave, float * d_u_norm)
	u_norm_scan<<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,
			d_u_ave,d_u_norm);
	CudaCheckError();
	cudaDeviceSynchronize();

	printf("Scoring for translations %i -- %i \n",translation_offset,translation_offset+translation_volume);
//		template <uint BLOCKSIZE>
//		__global__
//		void v_ave_sweep(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, float3 * d_scale,
//				float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
//				int s, uint * d_refinements, size_t d_num_refinements, int translation_offset, int translation_volume, size_t translation_stride,
//				float * d_v_ave)
	v_ave_sweep<<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],d_scale,
			scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
			0,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_volume,translation_stride,
			cam_score_instance->d_v_ave);
	CudaCheckError();
	cudaDeviceSynchronize();

//		void u_t_v_sweep(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, float3 * d_scale,
//				float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
//				int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
//				size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v)
	u_t_v_sweep<<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],d_scale,
			scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
			0,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_volume,translation_stride,
			cam_score_instance->d_v_ave,cam_score_instance->d_v_m_v_ave,cam_score_instance->d_u_t_v,d_u_ave);
	CudaCheckError();
	cudaDeviceSynchronize();

//		__global__
//		void cam_score(	int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
//				size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v, float * d_u_ave)

	cam_score<<<128,256>>>(0,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_volume,translation_stride,
			cam_score_instance->d_v_ave,cam_score_instance->d_v_m_v_ave,cam_score_instance->d_u_t_v,d_u_norm);
	CudaCheckError();
	cudaDeviceSynchronize();

//		template <uint BLOCKSIZE>
//		__global__
//		void min_sweep(int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,size_t translation_stride,
//				size_t sweep_size, float * d_scores)

	min_sweep<1024><<<translation_volume,1024>>>(0,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],
			translation_stride,sweep_size,cam_score_instance->d_v_ave);
	cudaDeviceSynchronize();
	CudaCheckError();

	float h_device_min = FLT_MAX;
	float * d_device_min;
	CudaSafeCall(cudaMalloc((void **)&d_device_min,sizeof(*d_device_min)));
	CudaSafeCall(cudaMemcpy(d_device_min,&h_device_min,sizeof(*d_device_min),cudaMemcpyHostToDevice));
//		template <uint BLOCKSIZE>
//		__global__
//		void min_sweep_strided(size_t translation_vol, size_t translation_stride, size_t sweep_size,
//				float * d_scores, float * d_device_min)
	int min_sweep_strided_threads = 512;
	int min_sweep_strided_blocks = translation_volume <= min_sweep_strided_threads ? 1 : translation_volume/min_sweep_strided_threads + 1;
	min_sweep_strided<1024><<<min_sweep_strided_blocks,min_sweep_strided_threads>>>
	(translation_volume, translation_stride, sweep_size, cam_score_instance->d_v_ave, d_device_min);
	cudaDeviceSynchronize();
	CudaCheckError();

	free(h_u_ave);
	CudaSafeCall(cudaFree(d_u_ave));
	CudaSafeCall(cudaFree(d_scale));
}

void CamScore::scoreSimple(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes,
		const int& translation_offset, const int& translation_volume, float * const& h_translation_vol,
		const int& gridDim, const int& blockDim, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	CamScore_Register * cam_score_instance = &_instance_registry[gpu_index];
	Density_Register * density_instance_target = &target->_instance_registry[gpu_index];
	Density_Register * density_instance_query = &query->_instance_registry[gpu_index];
	ChebychevNodes_Register * scan_nodes_instance = &scan_nodes->_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = cam_score_instance->trans_node;
	McEwenNodes_Register * rot_nodes_instance = cam_score_instance->rot_node;

	h_translation_vol[0] = (float) vol((density_instance_target->h_pixel_dim[0] - density_instance_query->h_pixel_dim[0]));
	uint rotation_vol = rot_nodes_instance->h_max_nodes[0];
	size_t translation_stride = 4*rotation_vol + 3*rot_nodes_instance->h_num_refinements[0];
	uint init_vol = translation_stride*translation_volume;
	//	__global__
	//	void initZero(float * d_target, size_t vol){
	initValue<<<gridDim,blockDim>>>(cam_score_instance->d_working_mem,init_vol,0.0f);
	cudaDeviceSynchronize();
	float * h_u_ave = (float *) malloc(sizeof(*h_u_ave));
	h_u_ave[0] = 0.0f;
	float * d_u_ave;
	CudaSafeCall(cudaMalloc((void **)&d_u_ave,sizeof(*d_u_ave)));
	CudaSafeCall(cudaMemcpy(d_u_ave,h_u_ave,sizeof(*d_u_ave),cudaMemcpyHostToDevice));
	dim3 cheby_dim = scan_nodes_instance->h_node_dim[0];
	dim3 block_dim = {8,8,8};
	dim3 grid_dim = {cheby_dim.x/block_dim.x,cheby_dim.y/block_dim.y,cheby_dim.z/block_dim.z}; //number of blocks
	float3 * h_scale = (float3 *) malloc(sizeof(*h_scale));
	float3 * d_scale;
	CudaSafeCall(cudaMalloc((void **)&d_scale,sizeof(*d_scale)));
	//the normalized coordinates of texture memory refer to the pixel dims, therefore the relative size of
	//two volumes is characterized by the ratio of pixel dims.
	assert(density_instance_query->h_pixel_size[0] == density_instance_target->h_pixel_size[0]);
	h_scale[0] = {((float) density_instance_query->h_pixel_dim[0].x)/((float) density_instance_target->h_pixel_dim[0].x),
	              ((float) density_instance_query->h_pixel_dim[0].y)/((float) density_instance_target->h_pixel_dim[0].y),
	              ((float) density_instance_query->h_pixel_dim[0].z)/((float) density_instance_target->h_pixel_dim[0].z)};
	CudaSafeCall(cudaMemcpy(d_scale,h_scale,sizeof(*d_scale),cudaMemcpyHostToDevice));
	if (density_instance_target->textureObj == nullptr || density_instance_query->textureObj == nullptr ){
		printf("Density needs to be transported to texture memory for this feature to work.\n");
		std::exit(0);
	}

	//independent of sweeps, need for cam score calculation. do beforehand.
	u_ave_scan<<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,
	d_u_ave);
	CudaCheckError();
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(h_u_ave,d_u_ave,sizeof(*d_u_ave),cudaMemcpyDeviceToHost));
	printf("Average for query function: %.10e\n",h_u_ave[0]);
	float * d_u_norm;
	float * h_u_norm = (float *) malloc(sizeof(*h_u_norm));
	h_u_norm[0] = 0.0f;
	CudaSafeCall(cudaMalloc((void**)&d_u_norm,sizeof(*d_u_norm)));
	CudaSafeCall(cudaMemcpy(d_u_norm,h_u_norm,sizeof(*d_u_norm),cudaMemcpyHostToDevice));

	//	void u_norm_scan(cudaTextureObject_t d_density_query, float * d_cheby_nodes, float * d_cheby_weights,
//			float * d_u_ave, float * d_u_norm)
	u_norm_scan<<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,
			d_u_ave,d_u_norm);
	CudaCheckError();
	cudaDeviceSynchronize();

	printf("Scoring for translations %i -- %i \n",translation_offset,translation_offset+translation_volume);
//		template <uint BLOCKSIZE>
//		__global__
//		void v_ave_sweep(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, float3 * d_scale,
//				float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
//				int s, uint * d_refinements, size_t d_num_refinements, int translation_offset, int translation_volume, size_t translation_stride,
//				float * d_v_ave)
	v_ave_sweep<<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],d_scale,
			scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
			0,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_volume,translation_stride,
			cam_score_instance->d_v_ave);
	CudaCheckError();
	cudaDeviceSynchronize();

//		void u_t_v_sweep(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, float3 * d_scale,
//				float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
//				int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
//				size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v)
	u_t_v_sweep<<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],d_scale,
			scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
			0,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_volume,translation_stride,
			cam_score_instance->d_v_ave,cam_score_instance->d_v_m_v_ave,cam_score_instance->d_u_t_v,d_u_ave);
	CudaCheckError();
	cudaDeviceSynchronize();

//		__global__
//		void cam_score(	int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
//				size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v, float * d_u_ave)

	cam_score<<<128,256>>>(0,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_volume,translation_stride,
			cam_score_instance->d_v_ave,cam_score_instance->d_v_m_v_ave,cam_score_instance->d_u_t_v,d_u_norm);
	CudaCheckError();
	cudaDeviceSynchronize();

	free(h_u_ave);
	CudaSafeCall(cudaFree(d_u_ave));
	CudaSafeCall(cudaFree(d_scale));
}

void CamScore::scoreAndMin(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes,
		const int& translation_offset, const int& translation_volume, float * const& h_result, float * const& h_translation_vol,
		const int& gridDim, const int& blockDim, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	CamScore_Register * cam_score_instance = &_instance_registry[gpu_index];
	Density_Register * density_instance_target = &target->_instance_registry[gpu_index];
	Density_Register * density_instance_query = &query->_instance_registry[gpu_index];
	ChebychevNodes_Register * scan_nodes_instance = &scan_nodes->_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = cam_score_instance->trans_node;
	McEwenNodes_Register * rot_nodes_instance = cam_score_instance->rot_node;

	h_translation_vol[0] = (float) vol((density_instance_target->h_pixel_dim[0] - density_instance_query->h_pixel_dim[0]));
	uint rotation_vol = rot_nodes_instance->h_max_nodes[0];
	size_t translation_stride = 4*rotation_vol + 3*rot_nodes_instance->h_num_refinements[0];
	size_t sweep_size = 4*rotation_vol;
	uint init_vol = translation_stride*translation_volume;
	//	__global__
	//	void initZero(float * d_target, size_t vol){
	initValue<<<gridDim,blockDim>>>(cam_score_instance->d_working_mem,init_vol,0.0f);
	cudaDeviceSynchronize();
	float * h_u_ave = (float *) malloc(sizeof(*h_u_ave));
	h_u_ave[0] = 0.0f;
	float * d_u_ave;
	CudaSafeCall(cudaMalloc((void **)&d_u_ave,sizeof(*d_u_ave)));
	CudaSafeCall(cudaMemcpy(d_u_ave,h_u_ave,sizeof(*d_u_ave),cudaMemcpyHostToDevice));
	dim3 cheby_dim = scan_nodes_instance->h_node_dim[0];
	dim3 block_dim = {8,8,8};
	dim3 grid_dim = {cheby_dim.x/block_dim.x,cheby_dim.y/block_dim.y,cheby_dim.z/block_dim.z}; //number of blocks
	float3 * h_scale = (float3 *) malloc(sizeof(*h_scale));
	float3 * d_scale;
	CudaSafeCall(cudaMalloc((void **)&d_scale,sizeof(*d_scale)));
	//the normalized coordinates of texture memory refer to the pixel dims, therefore the relative size of
	//two volumes is characterized by the ratio of pixel dims.
	assert(density_instance_query->h_pixel_size[0] == density_instance_target->h_pixel_size[0]);
	h_scale[0] = {((float) density_instance_query->h_pixel_dim[0].x)/((float) density_instance_target->h_pixel_dim[0].x),
	              ((float) density_instance_query->h_pixel_dim[0].y)/((float) density_instance_target->h_pixel_dim[0].y),
	              ((float) density_instance_query->h_pixel_dim[0].z)/((float) density_instance_target->h_pixel_dim[0].z)};
	CudaSafeCall(cudaMemcpy(d_scale,h_scale,sizeof(*d_scale),cudaMemcpyHostToDevice));
	if (density_instance_target->textureObj == nullptr || density_instance_query->textureObj == nullptr ){
		printf("Density needs to be transported to texture memory for this feature to work.\n");
		std::exit(0);
	}

	//independent of sweeps, need for cam score calculation. do beforehand.
	u_ave_scan<<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,
	d_u_ave);
	CudaCheckError();
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(h_u_ave,d_u_ave,sizeof(*d_u_ave),cudaMemcpyDeviceToHost));
	printf("Average for query function: %.10e\n",h_u_ave[0]);
	float * d_u_norm;
	float * h_u_norm = (float *) malloc(sizeof(*h_u_norm));
	h_u_norm[0] = 0.0f;
	CudaSafeCall(cudaMalloc((void**)&d_u_norm,sizeof(*d_u_norm)));
	CudaSafeCall(cudaMemcpy(d_u_norm,h_u_norm,sizeof(*d_u_norm),cudaMemcpyHostToDevice));

	//	void u_norm_scan(cudaTextureObject_t d_density_query, float * d_cheby_nodes, float * d_cheby_weights,
//			float * d_u_ave, float * d_u_norm)
	u_norm_scan<<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,
			d_u_ave,d_u_norm);
	CudaCheckError();
	cudaDeviceSynchronize();

	printf("Scoring for translations %i -- %i \n",translation_offset,translation_offset+translation_volume);
//		template <uint BLOCKSIZE>
//		__global__
//		void v_ave_sweep(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, float3 * d_scale,
//				float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
//				int s, uint * d_refinements, size_t d_num_refinements, int translation_offset, int translation_volume, size_t translation_stride,
//				float * d_v_ave)
	v_ave_sweep<<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],d_scale,
			scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
			0,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_volume,translation_stride,
			cam_score_instance->d_v_ave);
	CudaCheckError();
	cudaDeviceSynchronize();

//		void u_t_v_sweep(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, float3 * d_scale,
//				float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
//				int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
//				size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v)
	u_t_v_sweep<<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],d_scale,
			scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
			0,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_volume,translation_stride,
			cam_score_instance->d_v_ave,cam_score_instance->d_v_m_v_ave,cam_score_instance->d_u_t_v,d_u_ave);
	CudaCheckError();
	cudaDeviceSynchronize();

//		__global__
//		void cam_score(	int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,const int translation_volume,
//				size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v, float * d_u_ave)

	cam_score<<<128,256>>>(0,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],translation_offset,translation_volume,translation_stride,
			cam_score_instance->d_v_ave,cam_score_instance->d_v_m_v_ave,cam_score_instance->d_u_t_v,d_u_norm);
	CudaCheckError();
	cudaDeviceSynchronize();

//		template <uint BLOCKSIZE>
//		__global__
//		void min_sweep(int s, uint * d_refinements, size_t h_num_refinements, const int translation_offset,size_t translation_stride,
//				size_t sweep_size, float * d_scores)

	min_sweep<1024><<<translation_volume,1024>>>(0,rot_nodes_instance->d_refinements,rot_nodes_instance->h_num_refinements[0],
			translation_stride,sweep_size,cam_score_instance->d_v_ave);
	cudaDeviceSynchronize();
	CudaCheckError();

	float h_device_min = FLT_MAX;
	float * d_device_min;
	CudaSafeCall(cudaMalloc((void **)&d_device_min,sizeof(*d_device_min)));
	CudaSafeCall(cudaMemcpy(d_device_min,&h_device_min,sizeof(*d_device_min),cudaMemcpyHostToDevice));
//		template <uint BLOCKSIZE>
//		__global__
//		void min_sweep_strided(size_t translation_vol, size_t translation_stride, size_t sweep_size,
//				float * d_scores, float * d_device_min)
	int min_sweep_strided_threads = 512;
	int min_sweep_strided_blocks = translation_volume <= min_sweep_strided_threads ? 1 : translation_volume/min_sweep_strided_threads + 1;
	min_sweep_strided<1024><<<min_sweep_strided_blocks,min_sweep_strided_threads>>>
	(translation_volume, translation_stride, sweep_size, cam_score_instance->d_v_ave, d_device_min);
	cudaDeviceSynchronize();
	CudaCheckError();
	CudaSafeCall(cudaMemcpy(h_result,d_device_min,sizeof(*h_result),cudaMemcpyDeviceToHost));

	free(h_u_ave);
	CudaSafeCall(cudaFree(d_u_ave));
	CudaSafeCall(cudaFree(d_scale));
}

