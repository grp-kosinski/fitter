/*
 * score.h
 *
 *  Created on: Aug 18, 2021
 *      Author: kkarius
 */

#ifndef SCORE_HELPER_H_
#define SCORE_HELPER_H_

#include<Density.h>
#include<McEwenNodes.h>
#include<ChebychevNodes.h>

__device__
float squaref(float & f);
__device__
float idf(float & f);
__device__
float entf(float & f);
__host__ __device__
void eulers_to_memory(float3 * const& eulers,float * const& coeff);
__host__ __device__
void eulers_to_memory_inverse(float3 * const& eulers,float * const& coeff);
__device__
void rotation_to_memory_inverse(float * const& coeff, const uint3& abg,uint *const& refinements);
__device__
void rotation_to_memory(float * const& coeff, const uint3& abg,uint *const& refinements);
__device__
void translation_to_memory(float *const& transformation, float *const& d_trans_nodes,
		uint3 *const& t_node_dim, int trans_id);
__device__
void translation_to_memory_inverse(float *const& transformation, float *const& d_trans_nodes,
		uint3 *const& t_node_dim, int trans_id);
__device__
void translate(float3 & node_rt,const float3 & node_r,float3 *const& scale, float *const& translation);
__device__
void translate(float3 & node_rt,const float3 & node_r, float *const& translation);
__device__
void rotate(float3 & node_r,const float3 & node, float *const& rotation);
__host__ __device__
void rot_index(uint *const& refinements, const size_t& num_refinements, uint3 & abg, uint *const& rot_linear_index,
		const int& r, const int &s);
__host__ __device__
void rot_index(uint *const& refinements, uint3 & abg, uint *const& rot_linear_index, const int& r);
__host__ __device__
void rot_index(uint *const& refinements, uint3 & abg, const uint& r);
void index_to_transformation(Density *const& target, ChebychevNodes *const& trans_nodes, McEwenNodes *const& rot_nodes,
		int s, const size_t& index, float3 * const& translation, float3 * const& eulers, const int& gpu_index);

template <typename T>
__global__
void initValue(T * d_target, size_t vol, T value){
        size_t grid_size = blockDim.x*gridDim.x;
        int tid = blockDim.x*blockIdx.x + threadIdx.x;
        while (tid < vol ){
                d_target[tid] = value;
                tid += grid_size;
        }
}

//template<typename T>
//__global__
//void initValue(T * d_target, size_t vol, T value);
#endif /* SCORE_HELPER_H_ */
