/*
 * DensityGenerator.cu
 *
 *  Created on: Mar 3, 2021
 *      Author: kkarius
 */

#include<DensityGenerator.h>
#include<util.h>
#include<Particles.h>

void DensityGenerator::createInstanceFromParticles(Particles * const& particles, const int &gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		DensityGenerator_Register instance;
		Particles_Register * particles_instance = &particles->_instance_registry[gpu_index];
		instance.h_offset_shift = (float * ) malloc(3*sizeof(*instance.h_offset_shift));
		CudaSafeCall(cudaMalloc((void **)&instance.d_offset_shift,3*sizeof(instance.d_offset_shift)));
		if (particles_instance->h_external_memory[0] > 0){
			CudaSafeCall(cudaMalloc((void **)&instance.d_offsets,particles_instance->h_external_memory[0]*sizeof(instance.d_offset_shift)));
		} else {
			CudaSafeCall(cudaMalloc((void **)&instance.d_offsets,particles_instance->h_particle_count[0]*sizeof(instance.d_offset_shift)));
		}
		_instance_registry[gpu_index] = instance;
	}
}

__global__
void potatoe_stamp(float4 * d_particles, float * d_density, uint * d_offsets, int * d_stamp_offsets, float * d_stamp, size_t stamp_vol, size_t chunk_vol, uint chunk_count, size_t particle_num, size_t pixel_num){
      //load stamp and stamp offsets into shared memory
      extern __shared__ float s_stamp[];
      int * s_stamp_offsets = (int *) &s_stamp[chunk_vol];
      uint address;
      //for all chunks
      int c = 0;
      int load;
      while (c < chunk_count){
		  //load stamp chunk to shared
		  int t = threadIdx.x;
		  load = (c == chunk_count - 1) ? stamp_vol - (chunk_count-1)*chunk_vol : chunk_vol;
		  while (t < load){
			  s_stamp_offsets[t] = d_stamp_offsets[c*chunk_vol + t];
			  s_stamp[t] = d_stamp[c*chunk_vol + t];
			  t+= blockDim.x;
		  }
		  __syncthreads();
		  int w = blockIdx.x;
		  while (w < particle_num){
			  t = threadIdx.x;
			  while (t < load){
				  address = d_offsets[w] + s_stamp_offsets[t];
				  atomicAdd(&d_density[address], d_particles[w].w*s_stamp[t]);
		  	   	  __syncthreads();
				  t += blockDim.x;
			  }
			  w += gridDim.x;
		  }
		  c++;
      }
}

void _potatoe_stamp(float4 * d_particles, float * d_density, uint * d_offsets, int * d_stamp_offsets, float * d_stamp, size_t stamp_vol, size_t chunk_vol, uint chunk_count, size_t particle_num, size_t pixel_num,
		unsigned int gridDim, unsigned int blockDim, unsigned int shared_mem){
	switch (blockDim)
	{
	case 1024:
		potatoe_stamp<<<gridDim,1024,shared_mem>>>(d_particles,d_density,d_offsets,d_stamp_offsets,d_stamp,stamp_vol,chunk_vol,chunk_count,particle_num,pixel_num);
		break;
	case 512:
		potatoe_stamp<<<gridDim,512,shared_mem>>>(d_particles,d_density,d_offsets,d_stamp_offsets,d_stamp,stamp_vol,chunk_vol,chunk_count,particle_num,pixel_num);
		break;
	case 256:
		potatoe_stamp<<<gridDim,256,shared_mem>>>(d_particles,d_density,d_offsets,d_stamp_offsets,d_stamp,stamp_vol,chunk_vol,chunk_count,particle_num,pixel_num);
		break;
	case 128:
		potatoe_stamp<<<gridDim,128,shared_mem>>>(d_particles,d_density,d_offsets,d_stamp_offsets,d_stamp,stamp_vol,chunk_vol,chunk_count,particle_num,pixel_num);
		break;
	case 64:
		potatoe_stamp<<<gridDim,64,shared_mem>>>(d_particles,d_density,d_offsets,d_stamp_offsets,d_stamp,stamp_vol,chunk_vol,chunk_count,particle_num,pixel_num);
		break;
	case 32:
		potatoe_stamp<<<gridDim,32,shared_mem>>>(d_particles,d_density,d_offsets,d_stamp_offsets,d_stamp,stamp_vol,chunk_vol,chunk_count,particle_num,pixel_num);
		break;
	}
}

void DensityGenerator::freeInstance(const int& gpu_index){
	cudaSetDevice(gpu_index);
	DensityGenerator_Register * instance = &_instance_registry[gpu_index];
	cudaFree(instance->d_offset_shift);
	cudaFree(instance->d_offsets);
	free(instance->h_offset_shift);
}

void DensityGenerator::freeAllInstances(){
	for (auto const &instance: _instance_registry){
		cudaFree(instance.second.d_offset_shift);
		cudaFree(instance.second.d_offsets);
		free(instance.second.h_offset_shift);
	}
	_instance_registry.clear();
}
__global__
void coords_to_offsets(float4 * d_data, uint * d_offsets, float * d_pixel_size, uint3 * d_pixel_dim,float * d_shift,size_t particle_num){
	int t = blockDim.x * blockIdx.x + threadIdx.x;
	while (t < particle_num){
    	d_offsets[t] = __float2uint_rd((d_data[t].x + d_shift[0])/d_pixel_size[0]) +
		    		   __float2uint_rd((d_data[t].y + d_shift[1])/d_pixel_size[0])*d_pixel_dim->x +
					   __float2uint_rd((d_data[t].z + d_shift[2])/d_pixel_size[0])*d_pixel_dim->x*d_pixel_dim->y;
        t += blockDim.x*gridDim.x;
	}
}

void _coords_to_offsets(float4 * d_data, uint * d_offsets, float * d_pixel_size, uint3 * d_pixel_dim, float * d_shift, size_t particle_num,
		unsigned int gridDim, unsigned int blockDim){
	switch (blockDim)
	{
	case 1024:
		coords_to_offsets<<<gridDim,1024>>>(d_data,d_offsets,d_pixel_size, d_pixel_dim,d_shift,particle_num);
		break;
	case 512:
		coords_to_offsets<<<gridDim,512>>>(d_data,d_offsets,d_pixel_size, d_pixel_dim,d_shift,particle_num);
		break;
	case 256:
		coords_to_offsets<<<gridDim,256>>>(d_data,d_offsets,d_pixel_size, d_pixel_dim,d_shift,particle_num);
		break;
	case 128:
		coords_to_offsets<<<gridDim,128>>>(d_data,d_offsets,d_pixel_size, d_pixel_dim,d_shift,particle_num);
		break;
	case 64:
		coords_to_offsets<<<gridDim,64>>>(d_data,d_offsets,d_pixel_size, d_pixel_dim,d_shift,particle_num);
		break;
	case 32:
		coords_to_offsets<<<gridDim,32>>>(d_data,d_offsets,d_pixel_size, d_pixel_dim,d_shift,particle_num);
		break;
	}
}

void DensityGenerator::generateMolecularDensity(Density *const& density,Particles *const& particles, Stamp * const& stamp, int gridDim, int blockDim, int gpu_index){
	cudaSetDevice(gpu_index);
	size_t global_mem;
	size_t shared_mem;
	getMemoryInfo(gpu_index,global_mem,shared_mem);
	Density_Register * density_instance = &density->_instance_registry[gpu_index];
	Particles_Register * particles_instance = &particles->_instance_registry[gpu_index];
	DensityGenerator_Register * generator_instance = &_instance_registry[gpu_index];
	Stamp_Register * stamp_instance = &stamp->_instance_registry[gpu_index];

	//initiate with padding
	// "/2" since padding_dim is always odd and the "middle pixel" always part of the actual pixel dim
	uint3 h_padding_pixel_dim = {(uint) (density_instance->h_pixel_dim[0].x + 2*(stamp_instance->h_stamp_pixel_dim[0]/2)),
								 (uint) (density_instance->h_pixel_dim[0].y + 2*(stamp_instance->h_stamp_pixel_dim[1]/2)),
								 (uint) (density_instance->h_pixel_dim[0].z + 2*(stamp_instance->h_stamp_pixel_dim[2]/2))};
	float3 h_padding_coord_dim = {h_padding_pixel_dim.x*density_instance->h_pixel_size[0],
								  h_padding_pixel_dim.y*density_instance->h_pixel_size[0],
								  h_padding_pixel_dim.z*density_instance->h_pixel_size[0]};
	density_instance->h_coord_dim[0] = h_padding_coord_dim;
	density_instance->h_pixel_dim[0] = h_padding_pixel_dim;
	density_instance->h_molmap_padding[0] = {(stamp_instance->h_stamp_pixel_dim[0]/2)*density_instance->h_pixel_size[0],
											 (stamp_instance->h_stamp_pixel_dim[0]/2)*density_instance->h_pixel_size[0],
											 (stamp_instance->h_stamp_pixel_dim[0]/2)*density_instance->h_pixel_size[0]};
	CudaSafeCall(cudaMemcpy(density_instance->d_coord_dim,&h_padding_coord_dim,sizeof(h_padding_coord_dim),cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMemcpy(density_instance->d_pixel_dim,&h_padding_pixel_dim,sizeof(h_padding_pixel_dim),cudaMemcpyHostToDevice));

	size_t padding_pixel_vol = h_padding_pixel_dim.x*h_padding_pixel_dim.y*h_padding_pixel_dim.z;

	//create padding, only in case memory isn't managed externally
	if (density_instance->h_external_memory[0] == 0){
		CudaSafeCall(cudaFree(density_instance->d_data));
		CudaSafeCall(cudaMalloc((void **) &density_instance->d_data,padding_pixel_vol*sizeof(*density_instance->d_data)));
	}
	free(density_instance->h_data);
	density_instance->h_data = (TDensity *) malloc(padding_pixel_vol*sizeof(*density_instance->h_data));

	for (int i=0;i<3;i++) generator_instance->h_offset_shift[i] = - particles_instance->h_bounding_box[i] + density_instance->h_pixel_size[0]*(stamp_instance->h_stamp_pixel_dim[i]/2);
	CudaSafeCall(cudaMalloc((void **)&generator_instance->d_offset_shift,3*sizeof(generator_instance->d_offset_shift)));
	CudaSafeCall(cudaMemcpy(generator_instance->d_offset_shift,generator_instance->h_offset_shift,3*sizeof(generator_instance->d_offset_shift),cudaMemcpyHostToDevice));

	stamp->populateOffsets(density,gpu_index);
	size_t stamp_chunk_vol = shared_mem/(sizeof(float) + sizeof(int));
	uint stamp_chunk_count = stamp_instance->h_stamp_vol[0]/stamp_chunk_vol + 1;

	size_t particle_count = particles->particleCount(gpu_index);

//	coords_to_offsets<<<particle_count/512 + 1,512>>>
	_coords_to_offsets(particles_instance->d_data,generator_instance->d_offsets, density_instance->d_pixel_size,density_instance->d_pixel_dim,generator_instance->d_offset_shift,particle_count,gridDim,blockDim);
	CudaCheckError();
	cudaDeviceSynchronize();
	thrust::fill(thrust::device_pointer_cast(&density_instance->d_data[0]),thrust::device_pointer_cast(&density_instance->d_data[padding_pixel_vol]),0.0);
//	potatoe_stamp<<<24,128,shared_mem>>>
	_potatoe_stamp
	(particles_instance->d_data, density_instance->d_data, generator_instance->d_offsets, stamp_instance->d_stamp_offsets,
			stamp_instance->d_stamp, stamp_instance->h_stamp_vol[0], stamp_chunk_vol, stamp_chunk_count, particle_count,padding_pixel_vol,gridDim,blockDim,shared_mem);
	CudaCheckError();
	cudaDeviceSynchronize();
	density_instance->h_coord_offset[0] = {particles_instance->h_bounding_box[0],particles_instance->h_bounding_box[1],particles_instance->h_bounding_box[2]};
}
