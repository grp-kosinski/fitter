/*
 * CRotationGrid.cpp
 *
 *  Created on: Sep 17, 2019
 *      Author: kkarius
 */

#include <TransformationGrid.h>

__device__
float identity(float value)
{
   return value;
}

__device__
float entropy(float value)
{
   return -value*logf(value);
}

__device__ pointFloatToFloat_t p_identity = identity;
__device__ pointFloatToFloat_t p_entropy = entropy;

void threeaxisrot(double r11, double r12, double r21, double r31, double r32, float3* const& res){
  res->z = (float) atan2( r31, r32 ) + M_PI;
  res->y = (float) asin ( r21 ) + M_PI_2;
  res->x = (float) atan2( r11, r12 ) + M_PI;
}
__host__
void TransformationGrid::h_quat_to_euler(const float4 & q,float3* const& e){
	//body 3-2-1 convention
	threeaxisrot(2*(q.w*q.x + q.y*q.z),1-2*(q.x*q.x + q.y*q.y),
	             2*(q.w*q.y - q.x*q.z),
	             2*(q.w*q.z + q.x*q.y),1-2*(q.y*q.y + q.z*q.z),
                 e);
}

__host__
void TransformationGrid::h_matrix_to_quat(float * const& m,float4 & q){
	//https://en.wikipedia.org/wiki/Rotation_matrix#Quaternion
	float tr = m[0] + m[4] + m[8];
	float S;
	if (tr>0) {
		S = sqrtf(tr + 1)*2;
		q.w = 0.25*S;
		q.x = (m[5] - m[7])/S;
		q.y = (m[6] - m[2])/S;
		q.z = (m[1] - m[3])/S;
	} else if ((m[0] > m[4]) && (m[0] > m[8])){
		S = sqrtf(1 + m[0] - m[4] - m[8])*2;
		q.w = (m[5] - m[7])/S;
		q.x = 0.25*S;
		q.y = (m[3] + m[1])/S;
		q.z = (m[6] + m[2])/S;
	} else if (m[4] > m[8]){
		S = sqrtf(1 + m[4] - m[0] - m[8])*2;
		q.w = (m[6] - m[2])/S;
		q.x = (m[3] + m[1])/S;
		q.y = 0.25*S;
		q.z = (m[7] + m[5])/S;
	} else {
		S = sqrtf(1 + m[8] - m[0] - m[4])*2;
		q.w = (m[1] - m[3])/S;
		q.x = (m[6] + m[2])/S;
		q.y = (m[7] + m[5])/S;
		q.z = 0.25*S;
	}
}

__host__
void TransformationGrid::read_matrix_SO3_sampling(const char * file_name,float4 *& h_rotations, float3 *& h_rotations_euler, uint *& h_num_rotations){
	std::ifstream fh(file_name);
	std::string line;
	//skip first line
	std::getline(fh,line);
	std::getline(fh,line);
	std::istringstream iss(line);
	float num_rot;
	iss >> num_rot;
	*h_num_rotations = num_rot;
	h_rotations = (float4 *) malloc(h_num_rotations[0]*sizeof(*h_rotations));
	float * curr_matrix = (float *) malloc(9*sizeof(*curr_matrix));
	float4 curr_q;
	int c=0;
	while (std::getline(fh, line))
	{
	    std::istringstream iss(line);
	    for (int i = 0;i<9;i++) iss >> curr_matrix[i];
	    TransformationGrid::h_matrix_to_quat(curr_matrix,curr_q);
	    h_rotations[c] = curr_q;
	    c++;
	}

	std::ofstream eulers_rot("test/eulers.txt");
	h_rotations_euler = (float3 *) malloc(num_rot*sizeof(*h_rotations_euler));
	for (int i=0;i<c;i++){
		h_quat_to_euler(h_rotations[i],&h_rotations_euler[i]);
		eulers_rot << h_rotations_euler[i].x << "," << h_rotations_euler[i].y << "," << h_rotations_euler[i].z << "\n";
	}
	eulers_rot.close();
}

__device__
void TransformationGrid::d_euler_to_quat(const float3 &euler,float4 &quat){
	//body 3-2-1 convention
	quat.x = cos(euler.x/2)*cos(euler.y/2)*cos(euler.z/2) + sin(euler.x/2)*sin(euler.y/2)*sin(euler.z/2);
	quat.y = sin(euler.x/2)*cos(euler.y/2)*cos(euler.z/2) - cos(euler.x/2)*sin(euler.y/2)*sin(euler.z/2);
	quat.z = cos(euler.x/2)*sin(euler.y/2)*cos(euler.z/2) + sin(euler.x/2)*cos(euler.y/2)*sin(euler.z/2);
	quat.w = cos(euler.x/2)*cos(euler.y/2)*sin(euler.z/2) - sin(euler.x/2)*sin(euler.y/2)*cos(euler.z/2);
}

__host__
void TransformationGrid::h_euler_to_quat(const float3 &euler,float4 &quat){
	//body 3-2-1 convention
	quat.x = cos(euler.x/2)*cos(euler.y/2)*cos(euler.z/2) + sin(euler.x/2)*sin(euler.y/2)*sin(euler.z/2);
	quat.y = sin(euler.x/2)*cos(euler.y/2)*cos(euler.z/2) - cos(euler.x/2)*sin(euler.y/2)*sin(euler.z/2);
	quat.z = cos(euler.x/2)*sin(euler.y/2)*cos(euler.z/2) + sin(euler.x/2)*cos(euler.y/2)*sin(euler.z/2);
	quat.w = cos(euler.x/2)*cos(euler.y/2)*sin(euler.z/2) - sin(euler.x/2)*sin(euler.y/2)*cos(euler.z/2);
}


struct normalize_chamfer: public thrust::unary_function<float,float>
{
//	thrust::pair<thrust::device_vector<float>::iterator,thrust::device_vector<float>::iterator> d_minmax;
    float min;
    float max;
	__device__
	//norm = lambda u: 1/(_max - _min)*u - _min/(_max - _min)
	//since the chamfer score maps bad solutions to high scores and good solutions to low scores
	//exchange min am max
	float operator()(const float& u) const {
    	return u/(min - max) - max/(min - max);
	}
};

struct normalize_envelope: public thrust::unary_function<float,float>
{
//	thrust::pair<thrust::device_vector<float>::iterator,thrust::device_vector<float>::iterator> d_minmax;
    float * d_min;
    float * d_max;
	__device__
	//norm = lambda u: 1/(_max - _min)*u - _min/(_max - _min)
	//since the chamfer score maps bad solutions to high scores and good solutions to low scores
	//exchange min am max
	float operator()(const float& u) const {
    	return u/(d_max[0] - d_min[0]) - d_min[0]/(d_max[0] - d_min[0]);
	}
};


struct entropy: public thrust::unary_function<float,float>
{
	__device__
	void operator()(float& u) {
		if (u != 0)
			u = u*__logf(u);
	}
};

__host__
void TransformationGrid::refine_translation_grid(uint & i, float * h_translation){
	uint * h_translation_refinement_dim_new = (uint *) malloc(3*sizeof(*h_translation_refinement_dim_new));
    for (int i = 0;i<3;i++)h_translation_refinement_dim_new[i] = 2*h_translation_refinement_dim[i] - 1;
    int double_layer_volume = 2*h_translation_refinement_dim_new[0]*h_translation_refinement_dim_new[1] - h_translation_refinement_dim[0]*h_translation_refinement_dim[1];
    int double_bar_volume_A = 2*h_translation_refinement_dim_new[0] - h_translation_refinement_dim[0];
    int double_bar_volume_B = 2*h_translation_refinement_dim_new[0];
    int first_bar_volume_A = h_translation_refinement_dim[0] - 1;
    int first_bar_volume_B = h_translation_refinement_dim_new[0];
    int type_A_layer_vol = h_translation_refinement_dim_new[0]*h_translation_refinement_dim_new[1] - h_translation_refinement_dim[0]*h_translation_refinement_dim[1];
    int d  = i/double_layer_volume;
    int i1 = i%double_layer_volume;
    int i2 = 0;
    int z = 2*d+int(i1 >= type_A_layer_vol);
    int b = 0;
    int y = 0;
    int x = 0;
    if (i1 < type_A_layer_vol){
    	b = i1/double_bar_volume_A;
        i2 = i1%double_bar_volume_A;
        y = 2*b + int(i2 >= first_bar_volume_A);
        if (i2 < first_bar_volume_A){
        	x = 2*i2 + 1;
        } else {
        	x = i2 - first_bar_volume_A;
        }
    } else {
    	i1 = i1%type_A_layer_vol;
        b = i1/double_bar_volume_B;
        i2 = i1%double_bar_volume_B;
            y = 2*b + int(i2 >= first_bar_volume_B);
            if (i2 < first_bar_volume_B){
                x = i2;
            } else {
                x = i2 - first_bar_volume_B;
            }
        }
//	per_dim_new = [t/(n-1) for n,t in zip(h_translation_refinement_dim_new,total_dim)]
//    for i in indices:
//        positions.append(tuple([i[j]*per_dim_new[j] for j in range(3)]))
//        #print(positions[-1])
//    return positions
}

__host__
void TransformationGrid::normalize_chamfer_score(){
	//currently, fast enough. host <--> device transfer is unneccessary
//	thrust::device_ptr begin()
//	thrust::pair<thrust::device_vector<float>::iterator,thrust::device_vector<float>::iterator> d_minmax = thrust::minmax_element(thrust::device,thrust::device_ptr(d_scores[0]),thrust::device_ptr(d_scores[h_num_transformations[0]-1]));
//	thrust::pair<thrust::device_vector<float>::iterator,thrust::device_vector<float>::iterator> h_minmax;
//	h_minmax = d_minmax;
//	normalize_chamfer unary_normalize_function;
//	unary_normalize_function.min = h_minmax.first[0];
//	unary_normalize_function.max = h_minmax.second[0];
//	thrust::transform(td_scores.begin(),td_scores.end(),td_scores.begin(),unary_normalize_function);
//	d_scores = thrust::raw_pointer_cast(&td_scores[0]);
}

template <size_t block_size>
__global__
void float_min_max(float * d_data,size_t num_data, float * d_min, float * d_max){
	__shared__ float reduce[2*block_size];
	float * _min = &reduce[0];
	float * _max = &reduce[block_size];
	int num_blocks = num_data/blockDim.x + 1;

	//initiate
	if (blockIdx.x == 0){
		_min[threadIdx.x] = FLT_MAX;
		_max[threadIdx.x] = -FLT_MAX;
	}
	__syncthreads();

	//gather
	int b = blockIdx.x;
	while (b < num_blocks){
		int t = b*blockDim.x + threadIdx.x;
		while (t < num_data){
			_min[threadIdx.x] = fminf(_min[threadIdx.x],d_data[t]);
			_max[threadIdx.x] = fmaxf(_max[threadIdx.x],d_data[t]);
			t += blockDim.x;
		}
		b += gridDim.x;
	}
	__syncthreads();
	//reduce - max blocksize 128 hardcoded
	if (threadIdx.x < 64){
		_min[threadIdx.x] = fminf(_min[threadIdx.x],_min[threadIdx.x + 64]);
		_max[threadIdx.x] = fmaxf(_max[threadIdx.x],_max[threadIdx.x + 64]);
	}
	__syncthreads();

	if (threadIdx.x < 32){
		_min[threadIdx.x] = fminf(_min[threadIdx.x],_min[threadIdx.x + 32]);
		_max[threadIdx.x] = fmaxf(_max[threadIdx.x],_max[threadIdx.x + 32]);
	}
	__syncthreads();

	if (threadIdx.x < 16){
		_min[threadIdx.x] = fminf(_min[threadIdx.x],_min[threadIdx.x + 16]);
		_max[threadIdx.x] = fmaxf(_max[threadIdx.x],_max[threadIdx.x + 16]);
	}
	__syncthreads();

	if (threadIdx.x < 8){
		_min[threadIdx.x] = fminf(_min[threadIdx.x],_min[threadIdx.x + 8]);
		_max[threadIdx.x] = fmaxf(_max[threadIdx.x],_max[threadIdx.x + 8]);
	}
	__syncthreads();

	if (threadIdx.x < 4){
//		printf("%i %f %f %f\n",threadIdx.x,_min[threadIdx.x],_min[threadIdx.x + 4],fminf(_min[threadIdx.x],_min[threadIdx.x + 4]));
		_min[threadIdx.x] = fminf(_min[threadIdx.x],_min[threadIdx.x + 4]);
		_max[threadIdx.x] = fmaxf(_max[threadIdx.x],_max[threadIdx.x + 4]);
	}
	__syncthreads();

	if (threadIdx.x < 2){
//		printf("%i %f %f %f\n",threadIdx.x,_min[threadIdx.x],_min[threadIdx.x + 2],fminf(_min[threadIdx.x],_min[threadIdx.x + 2]));
		_min[threadIdx.x] = fminf(_min[threadIdx.x],_min[threadIdx.x + 2]);
		_max[threadIdx.x] = fmaxf(_max[threadIdx.x],_max[threadIdx.x + 2]);
	}
	__syncthreads();

	if (blockIdx.x == 0 && threadIdx.x ==0){
		d_min[0] = fminf(_min[0],_min[1]);
		d_max[0] = fmaxf(_max[0],_max[1]);
	}
}

__host__
void TransformationGrid::shift_and_scale_chamfer_score(float * d_score, float * d_normalized_score,float *d_min, float *d_max, size_t num_scores){
//	shift_and_scale_envelope_score_kernel<128><<<32,128>>>(d_score,d_normalized_score,d_min,d_max,num_scores);
	float_min_max<128><<<32,128>>>(d_score,num_scores,d_min,d_max);
	normalize_envelope unary_function;
	unary_function.d_min = d_min;
	unary_function.d_max = d_max;
	thrust::transform(thrust::device_pointer_cast(&d_score[0]),thrust::device_pointer_cast(&d_score[num_scores-1]),thrust::device_pointer_cast(&d_normalized_score[0]),unary_function);
	float * h_sum = (float *) malloc(sizeof(*h_sum));
//	h_sum[0] = thrust::reduce(thrust::device_pointer_cast(&d_normalized_score[0]),thrust::device_pointer_cast(&d_normalized_score[num_scores-1]));
	float sum = thrust::reduce(thrust::device_pointer_cast(&d_normalized_score[0]),thrust::device_pointer_cast(&d_normalized_score[num_scores-1]));
	float * d_sum;
	cudaMalloc((void **)&d_sum,sizeof(*d_sum));
	cudaMemcpy(d_sum,h_sum,sizeof(*h_sum),cudaMemcpyHostToDevice);
	CudaCheckError();
//	thrust::for_each(thrust::device_pointer_cast(&d_normalized_score[0]),thrust::device_pointer_cast(&d_normalized_score[num_scores-1]), thrust::placeholders::_1 /= sum);
//	thrust::for_each(thrust::device_pointer_cast(&d_normalized_score[0]),thrust::device_pointer_cast(&d_normalized_score[num_scores-1]), entropy());
//	float entr = -1.0*thrust::reduce(thrust::device_pointer_cast(&d_normalized_score[0]),thrust::device_pointer_cast(&d_normalized_score[num_scores-1]));
//	printf("Entropy for envelope score: %f\n",entr);
}


__host__
void TransformationGrid::shift_and_scale_envelope_score(float * d_score, float * d_normalized_score,float *d_min, float *d_max, size_t num_scores){
//	shift_and_scale_envelope_score_kernel<128><<<32,128>>>(d_score,d_normalized_score,d_min,d_max,num_scores);
	float_min_max<128><<<32,128>>>(d_score,num_scores,d_min,d_max);
	normalize_envelope unary_function;
	unary_function.d_min = d_min;
	unary_function.d_max = d_max;
	thrust::transform(thrust::device_pointer_cast(&d_score[0]),thrust::device_pointer_cast(&d_score[num_scores-1]),thrust::device_pointer_cast(&d_normalized_score[0]),unary_function);
	float * h_sum = (float *) malloc(sizeof(*h_sum));
//	h_sum[0] = thrust::reduce(thrust::device_pointer_cast(&d_normalized_score[0]),thrust::device_pointer_cast(&d_normalized_score[num_scores-1]));
	float sum = thrust::reduce(thrust::device_pointer_cast(&d_normalized_score[0]),thrust::device_pointer_cast(&d_normalized_score[num_scores-1]));
	float * d_sum;
	cudaMalloc((void **)&d_sum,sizeof(*d_sum));
	cudaMemcpy(d_sum,h_sum,sizeof(*h_sum),cudaMemcpyHostToDevice);
	CudaCheckError();
//	thrust::for_each(thrust::device_pointer_cast(&d_normalized_score[0]),thrust::device_pointer_cast(&d_normalized_score[num_scores-1]), thrust::placeholders::_1 /= sum);
//	thrust::for_each(thrust::device_pointer_cast(&d_normalized_score[0]),thrust::device_pointer_cast(&d_normalized_score[num_scores-1]), entropy());
//	float entr = -1.0*thrust::reduce(thrust::device_pointer_cast(&d_normalized_score[0]),thrust::device_pointer_cast(&d_normalized_score[num_scores-1]));
//	printf("Entropy for envelope score: %f\n",entr);
}

TransformationGrid::~TransformationGrid() {
        // TODO Auto-generated destructor stub
}
//
//int main (int argc, char *argv[]) {
//	TransformationGrid::evaluate(argv[1]);
//}
