/*
 * CParticles.h
 *
 *  Created on: Feb 25, 2019
 *      Author: kkarius
 */

#ifndef PARTICLES_H_
#define PARTICLES_H_

#include <string>
#include <vector>
#include <cmath>
#include <cstdio>
#include <cctype>
#include <sstream>
#include <regex>
#include <iostream>
#include <util.h>
#include <assert.h>
#include <map>

#include <cuda_util_include/cutil_math.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <cuda_device_runtime_api.h>

#include <thrust/pair.h>
#include <thrust/copy.h>
#include <thrust/device_malloc.h>
#include <thrust/device_ptr.h>
#include <thrust/extrema.h>
#include <thrust/execution_policy.h>
#include <thrust/reduce.h>
#include <thrust/functional.h>
#include <thrust/transform.h>

#include <PdbReader.h>


struct Particles_Register {
	float4 * h_data = nullptr;
 	float4 * d_data = nullptr;
	float * h_bounding_box= nullptr;
	float * d_bounding_box= nullptr;
	size_t * h_particle_count = nullptr;
	size_t * d_particle_count = nullptr;
	size_t * h_external_memory = nullptr;
};

class Particles{ //: public GPUObject {
public:
	Particles(){};
	virtual ~Particles(){};
	int fromPdb(const char * file_path);
	float3 volume(int gpu_index);
	size_t particleCount(int gpu_index);
	void boundingBoxDimensions(float3 *& dimensions, int gpu_index);
	void find_bounds(int gpu_index);
	void getBounds(float *const& bounds, int gpu_index);
	void getParticleCount(size_t *const& count, int gpu_index);
	void readFromPdb(PdbReader * const & reader, int gpu_index);
	void writeTransformedPdb(const char * fname, const float3& rotation, const float3& translation, const int & gpu_index);
	void createInstanceFromReader(int gpu_index,int reader_index, const size_t & external_memory = 0);
	void createInstance(int gpu_index,const size_t & external_memory);
	void freeInstance(const int& gpu_index);	
	void freeAllInstances(void);
	std::vector<PdbReader> pdb_readers;
	__host__ std::pair<float3,float3> rotationally_safe_bounding_box(void);
	__host__ void center_on_density(float * const& h_coord_dim);
	__host__ void origin_to_center(void);
//private:
	friend class Density;
	friend class DensityGenerator;
	std::map<int,Particles_Register> _instance_registry;
};
#endif /* PARTICLES_H_ */


