/*
 * CamScoreImproved.cu
 *
 *  Created on: Sep 14, 2021
 *      Author: kkarius
 */

#include <CamScoreImproved.h>
#include <score_helper.h>
#include <thrust/distance.h>

__global__
void v_ave(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, uint3 * d_query_pdim,uint3 * d_cheby_node_dim,
           float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
	   uint * d_refinements, const int translation_offset,const int translation_end, size_t translation_stride, float * d_v_ave){
	__shared__ float concat[512];
	__shared__ float translation[3];
	__shared__ float rotation[9];
	__shared__ uint rot_linear_index;
	float3 half_query_dim = {(float) d_query_pdim->x,(float) d_query_pdim->y,(float) d_query_pdim->z};
	half_query_dim *= 0.5f;
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;
	float3 node_coords_target = {d_cheby_nodes[blockDim.x*blockIdx.x + threadIdx.x],
				     d_cheby_nodes[blockDim.y*blockIdx.y + threadIdx.y + d_cheby_node_dim->x],
				     d_cheby_nodes[blockDim.z*blockIdx.z + threadIdx.z + d_cheby_node_dim->x + d_cheby_node_dim->y]};
	float3 node_coords_target_r;
	float3 node_coords_target_rt;
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
			    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     			    *d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];

	uint total_rotations = d_refinements[0]*d_refinements[1]*d_refinements[2];

	int t; // translations
	int r = 0; // rotations
	uint3 abg;
	while (r < total_rotations){
		//write rotation to memory
		if (tid == 0){
//			uint3 abg;
			rot_index(d_refinements, abg, &rot_linear_index, r);
			rotation_to_memory(&rotation[0],abg,d_refinements);
#ifdef DEBUG_BASE_SCORE_TEST
	rotation[0] = 1.0; rotation[1] = 0.0; rotation[2] = 0.0; rotation[3] = 0.0; rotation[4] = 1.0; rotation[5] = 0.0; rotation[6] = 0.0; rotation[7] = 0.0;rotation[8] = 1.0;
#endif
		}
		__syncthreads();
		//given that both query(q) and target(t) scan nodes are at [0,1], the scaling(s) and translating (tr) formula is
		//t = s*(q-0.5f)+tr
		//apply rotate around coordinate origin, then shift from [-0.5,-0.5,-0.5] - [0.5,0.5,0.5] to [0,0,0] - [1,1,1]
		rotate(node_coords_target_r,node_coords_target,&rotation[0]);
		t = 0;
		while(t < translation_end - translation_offset){
			if (tid == 0){
				translation_to_memory(&translation[0], d_trans_cheby_nodes, d_trans_cheby_node_dim, translation_offset+t);
				translation[0] = translation[0] + half_query_dim.x;
				translation[1] = translation[1] + half_query_dim.y;
				translation[2] = translation[2] + half_query_dim.z;
			}
			__syncthreads();
			translate(node_coords_target_rt,node_coords_target_r, &translation[0]);
			concat[tid] = node_weight*tex3D<float>(d_density_target, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z);
			__syncthreads();
			if (tid < 256) { concat[tid] += concat[tid + 256];} __syncthreads();
			if (tid < 128) { concat[tid] += concat[tid + 128];} __syncthreads();
			if (tid <  64) { concat[tid] += concat[tid +  64];} __syncthreads();
			if (tid <  32) { concat[tid] += concat[tid +  32];} __syncthreads();
			if (tid <  16) { concat[tid] += concat[tid +  16];} __syncthreads();
			if (tid <   8) { concat[tid] += concat[tid +   8];} __syncthreads();
			if (tid <   4) { concat[tid] += concat[tid +   4];} __syncthreads();
			if (tid <   2) { concat[tid] += concat[tid +   2];} __syncthreads();
			if (tid <   1) { concat[tid] += concat[tid +   1];} __syncthreads();
			if (tid==0){
				atomicAdd(&d_v_ave[t*translation_stride+rot_linear_index], concat[0]);
			}
			t += 1;
		}
		r += 1;
	}
}

__global__
void u_t_v(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, uint3 * d_query_pdim,uint3 * d_cheby_node_dim,
	  float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
          uint * d_refinements, const int translation_offset,const int translation_end, size_t translation_stride, 
	  float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v,float * d_u_ave){
	__shared__ float concat1[512]; //v_m_v_ave
	__shared__ float concat2[512];// u_t_v
	__shared__ float translation[3];
	__shared__ float rotation[9];
	__shared__ uint rot_linear_index;
	__shared__ float v_ave;
	float u_ave = d_u_ave[0];
	float3 half_query_dim = {(float) d_query_pdim->x,(float) d_query_pdim->y,(float) d_query_pdim->z};
	half_query_dim *= 0.5f;
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;
	float3 node_coords_target = {d_cheby_nodes[blockDim.x*blockIdx.x + threadIdx.x],
				     d_cheby_nodes[blockDim.y*blockIdx.y + threadIdx.y + d_cheby_node_dim->x],
				     d_cheby_nodes[blockDim.z*blockIdx.z + threadIdx.z + d_cheby_node_dim->x + d_cheby_node_dim->y]};
	float3 node_coords_query = node_coords_target + half_query_dim;
	float3 node_coords_target_r;
	float3 node_coords_target_rt;
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
			    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     			    *d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];
	float v_m_v_ave;

	uint total_rotations = d_refinements[0]*d_refinements[1]*d_refinements[2];

	int t; // translations
	int r = 0; // rotations
	while (r < total_rotations){
		//write rotation to memory
		if (tid == 0){
			uint3 abg;
			rot_index(d_refinements, abg, &rot_linear_index, r);
			rotation_to_memory(&rotation[0],abg,d_refinements);
#ifdef DEBUG_BASE_SCORE_TEST
			rotation[0] = 1.0; rotation[1] = 0.0; rotation[2] = 0.0; rotation[3] = 0.0; rotation[4] = 1.0; rotation[5] = 0.0; rotation[6] = 0.0; rotation[7] = 0.0;rotation[8] = 1.0;
#endif
		}
		__syncthreads();
		//apply rotate around coordinate origin
		rotate(node_coords_target_r,node_coords_target,&rotation[0]);
		t = 0;
		while(t < translation_end - translation_offset){
			if (tid == 0){
				translation_to_memory(&translation[0], d_trans_cheby_nodes, d_trans_cheby_node_dim, translation_offset+t);
				translation[0] = translation[0] + half_query_dim.x;
				translation[1] = translation[1] + half_query_dim.y;
				translation[2] = translation[2] + half_query_dim.z;
				v_ave = d_v_ave[t*translation_stride+rot_linear_index];
			}
			__syncthreads();
			translate(node_coords_target_rt,node_coords_target_r,&translation[0]);
			v_m_v_ave = tex3D<float>(d_density_target, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z)-v_ave;
			concat1[tid] = node_weight*v_m_v_ave*v_m_v_ave;
			concat2[tid] = node_weight*v_m_v_ave*(tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z) - u_ave);
			//if (translation_offset + t == 42 && r == 162)
                        //        printf("%i %i %f %f\n",r,tid,tex3D<float>(d_density_target, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z)-v_ave,tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z) - u_ave);
			//if (r == 47 && t == 0){

			//float tex = tex3D<float>(d_density_target, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z);
			////if (tex > 0.0f && tid < 2)
			////if (tid < 8)
               		//     //printf("S: %i | %f,%f,%f | %1.7e\n",tid,node_coords_target_rt.x,node_coords_target_rt.y,node_coords_target_rt.z,tex);
			////if (tex > 0.0f)
			//	printf("%03d,%.10e,%.10e,%.10e,%.10e\n",tid,tex3D<float>(d_density_target, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z),v_ave,tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z),u_ave);
			//	//printf("%03d|%f,%f,%f|%f,%f,%f\n",tid, node_coords_target_rt.x, node_coords_target_rt.y,node_coords_target_rt.z, node_coords_query.x, node_coords_query.y,node_coords_query.z);
			//}
			__syncthreads();
			if (tid < 256)  { concat1[tid] += concat1[tid + 256];  concat2[tid] += concat2[tid + 256];} __syncthreads();
			if (tid < 128)  { concat1[tid] += concat1[tid + 128];  concat2[tid] += concat2[tid + 128];} __syncthreads();
			if (tid < 64)   { concat1[tid] += concat1[tid + 64];   concat2[tid] += concat2[tid + 64];} __syncthreads();
			if (tid < 32)   { concat1[tid] += concat1[tid + 32];   concat2[tid] += concat2[tid + 32];} __syncthreads();
			if (tid < 16)   { concat1[tid] += concat1[tid + 16];   concat2[tid] += concat2[tid + 16];} __syncthreads();
			if (tid < 8)    { concat1[tid] += concat1[tid + 8];    concat2[tid] += concat2[tid + 8];} __syncthreads();
			if (tid < 4)    { concat1[tid] += concat1[tid + 4];    concat2[tid] += concat2[tid + 4];} __syncthreads();
			if (tid < 2)    { concat1[tid] += concat1[tid + 2];    concat2[tid] += concat2[tid + 2];} __syncthreads();
			if (tid < 1)    { concat1[tid] += concat1[tid + 1];    concat2[tid] += concat2[tid + 1];} __syncthreads();
			if (tid==0){
//				if (t==0 )
//					printf("VNORMF: %.10e\n",concat1[0]);
				atomicAdd(&d_v_m_v_ave[t*translation_stride+rot_linear_index], concat1[0]);
//				printf("%07d | %.10e\n",t*translation_stride+rot_linear_index,concat1[0]);
				atomicAdd(&d_u_t_v[t*translation_stride+rot_linear_index], concat2[0]);
			}
			t += 1;
		}
		r += 1;
	}
}

__global__
void cam_score(	uint * d_refinements, const int translation_offset,const int translation_end,
		size_t translation_stride, float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v, float * d_u_norm){
	uint rot_linear_index;
	float sqrt_u_norm = sqrtf(d_u_norm[0]);
	//get it into the register for fast access - worth it? I don't know
	uint total_rotations = d_refinements[0]*d_refinements[1]*d_refinements[2];
	int t; // translations
	int r = blockIdx.x; // rotations
	uint3 abg;
	while (r < total_rotations){
		rot_index(d_refinements, abg,&rot_linear_index, r);
		t= threadIdx.x;
		size_t tr_off;
		while(t < translation_end - translation_offset){
			tr_off = t*translation_stride + rot_linear_index;
			//d_v_ave doubles as memory space for the cam score, since it's not needed anymore
			if (d_v_m_v_ave[tr_off] != 0.0f){
				d_v_ave[tr_off] = d_u_t_v[tr_off]/(sqrt_u_norm*sqrtf(d_v_m_v_ave[tr_off]));
			} else {
				d_v_ave[tr_off] = 0.0f;
			}
			t += blockDim.x;
		}
		r += gridDim.x;
	}
}


__global__
void u_ave_scan(cudaTextureObject_t d_density_query, uint3 * d_query_pdim, uint3 * d_cheby_node_dim, float * d_cheby_nodes, float * d_cheby_weights,
		float * d_u_ave){
	__shared__ float concat[512]; //u_ave
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;
	float3 node_coords_query = {d_cheby_nodes[blockDim.x*blockIdx.x + threadIdx.x] + 0.5f*((float) d_query_pdim->x),
				    d_cheby_nodes[blockDim.y*blockIdx.y + threadIdx.y + d_cheby_node_dim->x] + 0.5f*((float) d_query_pdim->y),
				    d_cheby_nodes[blockDim.z*blockIdx.z + threadIdx.z + d_cheby_node_dim->x + d_cheby_node_dim->y] + 0.5f*((float) d_query_pdim->z)};
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
			    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     			    *d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];
	float u = tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z);
        //printf("%i,%i,%i %03d %.10e %.10e %.10e | %.10e | %.10e\n",blockDim.x*blockIdx.x + threadIdx.x,blockDim.y*blockIdx.y + threadIdx.y + d_cheby_node_dim->x,blockDim.z*blockIdx.z + threadIdx.z + d_cheby_node_dim->x + d_cheby_node_dim->y,tid,node_coords_query.x,node_coords_query.y,node_coords_query.z,node_weight,u);
	concat[tid] = node_weight*tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z);
	__syncthreads();
//	if (concat[tid] > 0.0f)
//		printf("%i:%e.10 %.10e,\n",tid,node_weight,concat[tid]);
	if (tid < 256) {concat[tid] += concat[tid + 256];} __syncthreads();
	if (tid < 128) {concat[tid] += concat[tid + 128];} __syncthreads();
	if (tid <  64) {concat[tid] += concat[tid +  64]; } __syncthreads();
	if (tid <  32) {concat[tid] += concat[tid +  32]; } __syncthreads();
	if (tid <  16) {concat[tid] += concat[tid +  16]; } __syncthreads();
	if (tid <   8) {concat[tid] += concat[tid +  8];  } __syncthreads();
	if (tid <   4) {concat[tid] += concat[tid +  4];  } __syncthreads();
	if (tid <   2) {concat[tid] += concat[tid +  2];  } __syncthreads();
	if (tid <   1) {concat[tid] += concat[tid +  1];  } __syncthreads();
	if (tid==0){
		 atomicAdd(&d_u_ave[0], concat[0]);
	}
}

__global__
void u_norm_scan(cudaTextureObject_t d_density_query, uint3 * d_query_pdim, float * d_cheby_nodes, float * d_cheby_weights,
		uint3 * d_cheby_node_dim, float * d_u_ave, float * d_u_norm){
	__shared__ float concat[512]; //u_ave
	__shared__ float u_ave;
	int tid = threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y;
	if (tid == 0)
		u_ave = d_u_ave[0];
	__syncthreads();
	float3 node_coords_query = {d_cheby_nodes[blockDim.x*blockIdx.x + threadIdx.x] + 0.5f*((float) d_query_pdim->x),
				    d_cheby_nodes[blockDim.y*blockIdx.y + threadIdx.y + d_cheby_node_dim->x] + 0.5f*((float) d_query_pdim->y),
				    d_cheby_nodes[blockDim.z*blockIdx.z + threadIdx.z + d_cheby_node_dim->x + d_cheby_node_dim->y] + 0.5f*((float) d_query_pdim->z)};
	float node_weight =  d_cheby_weights[blockDim.x*blockIdx.x + threadIdx.x]
			    *d_cheby_weights[blockDim.y*blockIdx.y + threadIdx.y]
     			    *d_cheby_weights[blockDim.z*blockIdx.z + threadIdx.z];
	float u = tex3D<float>(d_density_query, node_coords_query.x, node_coords_query.y,node_coords_query.z);
	concat[tid] = node_weight*(u-u_ave)*(u-u_ave);
	__syncthreads();
	if (tid < 256) {concat[tid] += concat[tid + 256];} __syncthreads();
	if (tid < 128) {concat[tid] += concat[tid + 128];} __syncthreads();
	if (tid <  64) {concat[tid] += concat[tid + 64]; } __syncthreads();
	if (tid <  32) {concat[tid] += concat[tid + 32]; } __syncthreads();
	if (tid <  16) {concat[tid] += concat[tid + 16]; } __syncthreads();
	if (tid <   8) {concat[tid] += concat[tid + 8];  } __syncthreads();
	if (tid <   4) {concat[tid] += concat[tid + 4];  } __syncthreads();
	if (tid <   2) {concat[tid] += concat[tid + 2];  } __syncthreads();
	if (tid <   1) {concat[tid] += concat[tid + 1];  } __syncthreads();
	if (tid==0){
		atomicAdd(&d_u_norm[0], concat[0]);
	}
}

__global__
void local_rot_max(uint * d_refinements, size_t translation_stride, float * d_scores){
	uint total_rotations = d_refinements[0]*d_refinements[1]*d_refinements[2];
	float * score_offset = d_scores + blockIdx.x*translation_stride;
	float rot_max = d_scores[(blockIdx.x+1)*translation_stride - 3];
	uint * ent_offset = reinterpret_cast<uint*>(d_scores) + (blockIdx.x+1)*translation_stride - 1;
	uint r = threadIdx.x;
//	if (threadIdx.x == 0)
//		printf("trans: %i value:%f\n",blockIdx.x,rot_max);
	while (r < total_rotations){
		if (score_offset[r] == rot_max){
			ent_offset[0] = r;
			if (blockIdx.x == 0)
				printf("trans: %i value:%f rot:%u\n",blockIdx.x,rot_max,r);
		}
		r += blockDim.x;
	}
}

template <uint BLOCKSIZE>
__global__
void min_max_per_trans(uint * d_refinements, size_t translation_stride, float * d_scores){
	//blockIdx.x == translation index
	__shared__ float concat_min[2*BLOCKSIZE];
//	__shared__ float concat_max[2*BLOCKSIZE];
	uint rot_linear_index;
	uint total_rotations = d_refinements[0]*d_refinements[1]*d_refinements[2];
	//score offset for translations
	float * score_offset = d_scores + blockIdx.x*translation_stride;
	float * min_offset = d_scores + (blockIdx.x+1)*translation_stride - 4;
//	float * max_offset = d_scores + (blockIdx.x+1)*translation_stride - 3;
	int tid = threadIdx.x;
	int r = tid;
	while (r < 2*BLOCKSIZE){
		concat_min[r] = FLT_MAX;
//		concat_max[r] = FLT_MIN;
		r += blockDim.x;
	}
	r = tid;
	uint3 abg;
	while (r < total_rotations){
		rot_index(d_refinements, abg,&rot_linear_index, r);
		concat_min[threadIdx.x] = min(concat_min[threadIdx.x],score_offset[rot_linear_index]);
//		concat_max[threadIdx.x] = max(concat_max[threadIdx.x],score_offset[rot_linear_index]);
		r += blockDim.x;
	}
	__syncthreads();
	if (BLOCKSIZE >= 1024){if (tid < 512) { concat_min[tid] = min(concat_min[tid],concat_min[tid + 512]);} __syncthreads();}
	if (BLOCKSIZE >= 512) {if (tid < 256) { concat_min[tid] = min(concat_min[tid],concat_min[tid + 256]);} __syncthreads();}
	if (BLOCKSIZE >= 256) {if (tid < 128) { concat_min[tid] = min(concat_min[tid],concat_min[tid + 128]);} __syncthreads();}
	if (BLOCKSIZE >= 128) {if (tid <  64) { concat_min[tid] = min(concat_min[tid],concat_min[tid +  64]);} __syncthreads();}
	if (BLOCKSIZE >= 64)  {if (tid <  32) { concat_min[tid] = min(concat_min[tid],concat_min[tid +  32]);} __syncthreads();}
	if (BLOCKSIZE >= 32)  {if (tid <  16) { concat_min[tid] = min(concat_min[tid],concat_min[tid +  16]);} __syncthreads();}
	if (BLOCKSIZE >= 16)  {if (tid <   8) { concat_min[tid] = min(concat_min[tid],concat_min[tid +   8]);} __syncthreads();}
	if (BLOCKSIZE >= 8)   {if (tid <   4) { concat_min[tid] = min(concat_min[tid],concat_min[tid +   4]);} __syncthreads();}
	if (BLOCKSIZE >= 4)   {if (tid <   2) { concat_min[tid] = min(concat_min[tid],concat_min[tid +   2]);} __syncthreads();}
	if (BLOCKSIZE >= 2)   {if (tid <   1) { concat_min[tid] = min(concat_min[tid],concat_min[tid +   1]);} __syncthreads();}
	if (tid==0){
		min_offset[0] = concat_min[0];
	}
}

template <uint BLOCKSIZE>
__global__
void min_strided(size_t translation_offset, size_t translation_end, size_t translation_stride, uint * d_refinements,
		float * d_scores, float * d_device_min)
{
	__shared__ float concat[2*BLOCKSIZE];
	//uint total_rotations = d_refinements[0]*d_refinements[1]*d_refinements[2];
	//score offset for translations
	float * min_offset = d_scores + (blockIdx.x+1)*translation_stride - 4;
	int tid = threadIdx.x;
	int grid_size = blockDim.x*gridDim.x;
	int t = blockIdx.x*blockDim.x + threadIdx.x;
	while (t < 2*BLOCKSIZE){
		concat[t] = FLT_MAX;
		t += blockDim.x;
	}
	t = blockIdx.x*blockDim.x +threadIdx.x + translation_offset;
	while (t < translation_end - translation_offset){
		concat[threadIdx.x] = min(concat[threadIdx.x],min_offset[t*translation_stride]);
		t += grid_size;
	}
	__syncthreads();
	if (BLOCKSIZE >= 1024){if (tid < 512) { concat[tid] = min(concat[tid],concat[tid + 512]);} __syncthreads();}
	if (BLOCKSIZE >=  512){if (tid < 256) { concat[tid] = min(concat[tid],concat[tid + 256]);} __syncthreads();}
	if (BLOCKSIZE >=  256){if (tid < 128) { concat[tid] = min(concat[tid],concat[tid + 128]);} __syncthreads();}
	if (BLOCKSIZE >=  128){if (tid <  64) { concat[tid] = min(concat[tid],concat[tid +  64]);} __syncthreads();}
	if (BLOCKSIZE >=   64){if (tid <  32) { concat[tid] = min(concat[tid],concat[tid +  32]);} __syncthreads();}
	if (BLOCKSIZE >=   32){if (tid <  16) { concat[tid] = min(concat[tid],concat[tid +  16]);} __syncthreads();}
	if (BLOCKSIZE >=   16){if (tid <   8) { concat[tid] = min(concat[tid],concat[tid +   8]);} __syncthreads();}
	if (BLOCKSIZE >=    8){if (tid <   4) { concat[tid] = min(concat[tid],concat[tid +   4]);} __syncthreads();}
	if (BLOCKSIZE >=    4){if (tid <   2) { concat[tid] = min(concat[tid],concat[tid +   2]);} __syncthreads();}
	if (BLOCKSIZE >=    2){if (tid <   1) { concat[tid] = min(concat[tid],concat[tid +   1]);} __syncthreads();}
//	if (tid < 32) min_warp_reduce<float,BLOCKSIZE>(concat,tid); __syncthreads();
	if (tid==0){
		atomicMin(&d_device_min[0],concat[0]);
	}
}

__global__
void shift_by_min_simple(const int translation_offset,const int translation_volume, const size_t translation_stride, const size_t rot_vol,
                float * d_scores, float * d_dist, float h_min){
        //blocks - translations
        //threads - rotations
        float * rot_offset_scores;
        float * rot_offset_dist;
        int bid = blockIdx.x;
        int tid;
        while (bid < translation_volume){
                rot_offset_scores = d_scores + translation_stride*bid;
                rot_offset_dist = d_dist + translation_stride*bid;
                tid = threadIdx.x;
                while (tid < rot_vol){
//                      if (tid == 0 && bid <10 )
//                              printf("%i: %f - %f = %f\n",bid,rot_offset_scores[tid],min,rot_offset_scores[tid] - min);
                        rot_offset_dist[tid] = rot_offset_scores[tid] - h_min;
                        tid += blockDim.x;
                }
                bid += gridDim.x;
        }
}


template <float (*F)(float &),uint BLOCKSIZE>
__global__
void quad_simple(uint * d_refinements, int translation_offset, size_t translation_stride, uint trans_node_dim,
                float * d_trans_weights, double * d_weights, float * d_dist, float * d_result){
        //translation offset must be real == connected to the geometric structure
        __shared__ float concat[2*BLOCKSIZE];
        uint rot_linear_index;
        //get it into the register for fast access - worth it? I don't know
        uint total_rotations = d_refinements[0]*d_refinements[1]*d_refinements[2];
        //score offset for translations
        uint3 trans_index;
        int t = translation_offset + blockIdx.x;
        float * dist_offset = d_dist + blockIdx.x*translation_stride;
        int tid = threadIdx.x;
        int r = tid;
        while (r < 2*BLOCKSIZE){
                concat[r] = 0.0f;
                r += blockDim.x;
        }
        r = tid;
        uint3 abg;
        while (r < total_rotations){
                rot_index(d_refinements, 1, abg,&rot_linear_index, r,0);
                concat[threadIdx.x] += d_weights[abg.y]*F(dist_offset[rot_linear_index]);
                r += blockDim.x;
        }
        __syncthreads();
        if (BLOCKSIZE >= 1024){if (tid < 512) { concat[tid] += concat[tid + 512];} __syncthreads();}
        if (BLOCKSIZE >= 512){if (tid < 256) { concat[tid] += concat[tid + 256];} __syncthreads();}
        if (BLOCKSIZE >= 256){if (tid < 128) { concat[tid] += concat[tid + 128];} __syncthreads();}
        if (BLOCKSIZE >= 128){if (tid < 64) { concat[tid] += concat[tid + 64];} __syncthreads();}
        if (BLOCKSIZE >= 64){if (tid < 32) { concat[tid] += concat[tid + 32];} __syncthreads();}
        if (BLOCKSIZE >= 32){if (tid < 16) { concat[tid] += concat[tid + 16];} __syncthreads();}
        if (BLOCKSIZE >= 16){if (tid < 8) { concat[tid] += concat[tid + 8];} __syncthreads();}
        if (BLOCKSIZE >= 8){if (tid <  4) { concat[tid] += concat[tid + 4];} __syncthreads();}
        if (BLOCKSIZE >= 4){if (tid <  2) { concat[tid] += concat[tid + 2];} __syncthreads();}
        if (BLOCKSIZE >= 2){if (tid <  1) { concat[tid] += concat[tid + 1];} __syncthreads();}
//      if (tid < 32) warp_reduce<float,BLOCKSIZE>(concat,tid); __syncthreads();
        if (tid==0){
//              if (concat[0] > 0.0f)
//                      printf("%i: %.10e\n",t,concat[0]);
                trans_index.z = t/(trans_node_dim*trans_node_dim);
                trans_index.y = (t - trans_index.z*trans_node_dim*trans_node_dim)/trans_node_dim;
                trans_index.x = t - trans_index.y*trans_node_dim - trans_index.z*trans_node_dim*trans_node_dim;
                atomicAdd(&d_result[0],d_trans_weights[trans_index.x]*d_trans_weights[trans_index.y]*d_trans_weights[trans_index.z]*concat[0]);
        }
}

void CamScoreImproved::scoreAndMinChunks(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes, float *const& h_scores, float *const& h_min,
                            float *const& h_max, uint *const& h_max_index, size_t * max_translation_volumes_per_gpu,const size_t& translation_offset,const int& translation_end, const int & gpu_num, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	CamScoreImproved_Register * cam_score_improved_instance = &_instance_registry[gpu_index];
	Density_Register * density_instance_target = &target->_instance_registry[gpu_index];
	Density_Register * density_instance_query = &query->_instance_registry[gpu_index];
	ChebychevNodes_Register * scan_nodes_instance = &scan_nodes->_instance_registry[gpu_index];

	//prepare kernel parameters, calculate once-per-search parameters
	cam_score_improved_instance->h_u_ave[0] = 0.0f;
	CudaSafeCall(cudaMemcpy(cam_score_improved_instance->d_u_ave,cam_score_improved_instance->h_u_ave,sizeof(*cam_score_improved_instance->d_u_ave),cudaMemcpyHostToDevice));
	dim3 cheby_dim = scan_nodes_instance->h_node_dim[0];
	const dim3 block_dim = {8,8,8};
	dim3 grid_dim = {cheby_dim.x/block_dim.x,cheby_dim.y/block_dim.y,cheby_dim.z/block_dim.z}; //number of blocks
	
	//the normalized coordinates of texture memory refer to the pixel dims, therefore the relative size of
	//two volumes is characterized by the ratio of pixel dims.
	assert(abs(density_instance_query->h_pixel_size[0] - density_instance_target->h_pixel_size[0]) < 0.0001f);
	float h_translation_vol = (float) vol((density_instance_target->h_pixel_dim[0] - density_instance_query->h_pixel_dim[0]));
	//	__global__
	//	void initZero(float * d_target, size_t vol){

	if (density_instance_target->textureObj == nullptr)
		target->createTexture(gpu_index);
	if (density_instance_query->textureObj == nullptr )
		query->createTexture(gpu_index);

	//independent of sweeps, need for cam score calculation. do beforehand.
	u_ave_scan<<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],density_instance_query->d_pixel_dim,scan_nodes_instance->d_node_dim,scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,
	cam_score_improved_instance->d_u_ave);
	CudaCheckError();
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(cam_score_improved_instance->h_u_ave,cam_score_improved_instance->d_u_ave,sizeof(*cam_score_improved_instance->d_u_ave),cudaMemcpyDeviceToHost));
	cam_score_improved_instance->h_u_norm[0] = 0.0f;
	CudaSafeCall(cudaMalloc((void**)&cam_score_improved_instance->d_u_norm,sizeof(*cam_score_improved_instance->d_u_norm)));
	CudaSafeCall(cudaMemcpy(cam_score_improved_instance->d_u_norm,cam_score_improved_instance->h_u_norm,sizeof(*cam_score_improved_instance->d_u_norm),cudaMemcpyHostToDevice));

	//__global__
	//void u_norm_scan(cudaTextureObject_t d_density_query, uint3 * d_query_pdim, float * d_cheby_nodes, float * d_cheby_weights,
        //uint3 * d_cheby_node_dim, float * d_u_ave, float * d_u_norm)
	u_norm_scan<<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],density_instance_query->d_pixel_dim,scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,scan_nodes_instance->d_node_dim,
			cam_score_improved_instance->d_u_ave,cam_score_improved_instance->d_u_norm);
	CudaCheckError();
	cudaDeviceSynchronize();

	cam_score_improved_instance->h_device_min[0] = FLT_MAX;
	CudaSafeCall(cudaMemcpy(cam_score_improved_instance->d_device_min,cam_score_improved_instance->h_device_min,sizeof(*cam_score_improved_instance->d_device_min),cudaMemcpyHostToDevice));
	//init concat fields
	
	printf("-------------------------------------------\n");
	printf("CamScoreImproved calculated at gpu: \t%i\n",gpu_index);
	printf("Average for query function: \t\t%.10e\n",cam_score_improved_instance->h_u_ave[0]);
	printf("-------------------------------------------\n");

	//while (current_offset < translation_volume_total){
	if (translation_end <= translation_offset){
		printf("Translation offset %u and end %u make no sense ... abort.",translation_offset,translation_end);
		return;
	}	
	size_t translation_volume = translation_end - translation_offset;
	std::vector<size_t> translation_volumes_per_gpu(gpu_num);
	for (int g=0; g < gpu_num; g++)translation_volumes_per_gpu[g] = std::min<size_t>(max_translation_volumes_per_gpu[gpu_index],translation_volume/gpu_num + 1);	
	size_t global_stride = 0;
	size_t initial_offset = 0;
	size_t local_translation_volume = translation_volumes_per_gpu[gpu_index];
	for (int g = 0; g < gpu_num; g++) global_stride += translation_volumes_per_gpu[g];
        for (int s = 0; s < gpu_index; s++) initial_offset += translation_volumes_per_gpu[s];
                
	size_t current_offset = translation_offset + initial_offset;
	size_t current_end;
	bool copy_yes = translation_volume > global_stride;
	while (current_offset < translation_end){
		current_end = current_offset + local_translation_volume < translation_end ?
				 current_offset + local_translation_volume : translation_end;
		//void scoreAndMin(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes, float *const& h_scores, float *const& h_min, float *const& h_max, uint *const& h_max_index, const size_t& translation_offset,const size_t& translation_volume, size_t * translation_offset, const size_t translation_stride, const int & gpu_index);	
		scoreAndMin(target, query, scan_nodes, h_scores, h_min, h_max, h_max_index, current_offset, current_end,gpu_index);	
		current_offset += global_stride;
		//CudaSafeCall(cudaMemcpy();
	}	
}

void CamScoreImproved::scoreAndMin(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes, float *const& h_scores, float *const& h_min, float *const& h_max, uint *const& h_max_index, const size_t& current_offset, const size_t& current_end, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	CamScoreImproved_Register * cam_score_improved_instance = &_instance_registry[gpu_index];
	Density_Register * density_instance_target = &target->_instance_registry[gpu_index];
	Density_Register * density_instance_query = &query->_instance_registry[gpu_index];
	ChebychevNodes_Register * scan_nodes_instance = &scan_nodes->_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = cam_score_improved_instance->trans_node;
	McEwenNodes_Register * rot_nodes_register = cam_score_improved_instance->rot_node;
	size_t rotation_vol = rot_nodes_register->h_max_nodes[0];
	size_t translation_stride = 3*rotation_vol + 4;
	dim3 cheby_dim = scan_nodes_instance->h_node_dim[0];
	const dim3 block_dim = {8,8,8};
	dim3 grid_dim = {cheby_dim.x/block_dim.x,cheby_dim.y/block_dim.y,cheby_dim.z/block_dim.z}; //number of blocks

	initValue<<<128,512>>>(cam_score_improved_instance->d_working_mem,translation_stride*(current_end-current_offset),0.0f);
	CudaCheckError();
	cudaDeviceSynchronize();
//	void v_ave(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, uint3 * d_query_pdim,uint3 * d_cheby_node_dim,
//           float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
//	   uint * d_refinements, const int translation_offset,const int translation_volume, size_t translation_stride, float * d_v_ave){
	v_ave<<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],density_instance_query->d_pixel_dim,scan_nodes_instance->d_node_dim,
			scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
			rot_nodes_register->d_refinements,current_offset,current_end,translation_stride,
			cam_score_improved_instance->d_v_ave);
	CudaCheckError();
	cudaDeviceSynchronize();
//	__global__
//	void u_t_v(cudaTextureObject_t d_density_target, cudaTextureObject_t d_density_query, uint3 * d_query_pdim,uint3 * d_cheby_node_dim,
//	float * d_cheby_nodes, float * d_cheby_weights, float * d_trans_cheby_nodes, uint3 * d_trans_cheby_node_dim,
//        uint * d_refinements, const int translation_offset,const int translation_volume, size_t translation_stride, 
//	float * d_v_ave, float * d_v_m_v_ave, float * d_u_t_v,float * d_u_ave){
	u_t_v<<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],density_instance_query->d_pixel_dim,scan_nodes_instance->d_node_dim,
			scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
			rot_nodes_register->d_refinements,current_offset,current_end,translation_stride,
			cam_score_improved_instance->d_v_ave,cam_score_improved_instance->d_v_m_v_ave,cam_score_improved_instance->d_u_t_v,cam_score_improved_instance->d_u_ave);
	CudaCheckError();
	cudaDeviceSynchronize();
	cam_score<<<grid_dim,block_dim>>>(rot_nodes_register->d_refinements,current_offset,current_end,translation_stride,
			cam_score_improved_instance->d_v_ave,cam_score_improved_instance->d_v_m_v_ave,cam_score_improved_instance->d_u_t_v,cam_score_improved_instance->d_u_norm);
	CudaCheckError();
	cudaDeviceSynchronize();
	//min max
//	template <uint BLOCKSIZE>
//	__global__
//	void min_max_per_trans(uint * d_refinements, size_t translation_stride, float * d_scores)
	int grid_dim_min_max = current_end-current_offset;
	min_max_per_trans<1024><<<grid_dim_min_max,1024>>>(rot_nodes_register->d_refinements,translation_stride,cam_score_improved_instance->d_cam);
	CudaCheckError();
	cudaDeviceSynchronize();
	//copy to host

	//determine the index of each maximum for later reference. write it to the spot reserved for entropy, since both will not be
	//needed at the same time.
//	__global__
//	void local_rot_max(uint * d_refinements, size_t translation_stride, float * d_scores)
//	local_rot_max<<<translation_vol,1024>>>(rot_nodes_register->d_refinements,translation_stride,cam_score_improved_instance->d_cam);
//	CudaCheckError();
//	cudaDeviceSynchronize();
	thrust::device_ptr<float> start_ptr;
	thrust::device_ptr<float> max_ptr;
	for (int t = 0;t < current_end-current_offset;++t){
	    start_ptr = thrust::device_pointer_cast(&cam_score_improved_instance->d_cam[t*translation_stride]);
	    max_ptr = thrust::max_element(start_ptr, start_ptr + rotation_vol);
	    h_max[current_offset+t] = max_ptr[0];
	    h_max_index[current_offset+t] = &max_ptr[0] - &start_ptr[0];
	    //if (rot_nodes_register->h_max_nodes[0]>1) printf("T:%i max rot value (R,score):(%i,%f)\n",current_offset+t,h_max_index[current_offset+t],h_max[current_offset+t]);
	}
	//std::vector<float> rot_maxima(h_max,h_max + current_volume);
	//std::vector<float>::iterator max_trans = std::max_element(rot_maxima.begin(),rot_maxima.end());
	//printf("Max translation:%i - %f\n",*max_trans,std::distance(rot_maxima.begin(),max_trans));
//	template <uint BLOCKSIZE>
//	__global__
//	void min_sweep_strided(size_t translation_vol, size_t translation_stride, uint * d_refinements,
//			float * d_scores, float * d_device_min)
	min_strided<1024><<<128,256>>>(current_offset,current_end,translation_stride,rot_nodes_register->d_refinements,cam_score_improved_instance->d_cam,cam_score_improved_instance->d_device_min);
	CudaCheckError();
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(cam_score_improved_instance->h_device_min,cam_score_improved_instance->d_device_min,sizeof(*cam_score_improved_instance->d_device_min),cudaMemcpyDeviceToHost));
	h_min[0] = cam_score_improved_instance->h_device_min[0];

	//remember memory layout, need stride to catch the right scores
	for (int t = 0; t < current_end - current_offset; ++t){
		CudaSafeCall(cudaMemcpy(h_scores + (current_offset + t)*rotation_vol,cam_score_improved_instance->d_cam + t*translation_stride,rotation_vol*sizeof(*h_scores),cudaMemcpyDeviceToHost));
	}
	cudaDeviceSynchronize();
}	

void CamScoreImproved::score(Density *const& target, Density *const& query, ChebychevNodes *const& scan_nodes,
		float *const& h_scores, float *const& h_min, float *const& h_max, uint *const& h_max_index,
		const int& translation_volume_per_gpu,  const int& translation_volume_total, const int& gpu_num,
		const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	CamScoreImproved_Register * cam_score_improved_instance = &_instance_registry[gpu_index];
	Density_Register * density_instance_target = &target->_instance_registry[gpu_index];
	Density_Register * density_instance_query = &query->_instance_registry[gpu_index];
	ChebychevNodes_Register * scan_nodes_instance = &scan_nodes->_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = cam_score_improved_instance->trans_node;
	McEwenNodes_Register * rot_nodes_register = cam_score_improved_instance->rot_node;
	size_t rotation_vol = rot_nodes_register->h_max_nodes[0];
	size_t translation_vol = trans_nodes_instance->h_node_dim[0].x*trans_nodes_instance->h_node_dim[0].y*trans_nodes_instance->h_node_dim[0].z;

	//prepare kernel parameters, calculate once-per-search parameters
	float * h_u_ave = (float *) malloc(sizeof(*h_u_ave));
	h_u_ave[0] = 0.0f;
	float * d_u_ave;
	CudaSafeCall(cudaMalloc((void **)&d_u_ave,sizeof(*d_u_ave)));
	CudaSafeCall(cudaMemcpy(d_u_ave,h_u_ave,sizeof(*d_u_ave),cudaMemcpyHostToDevice));
	dim3 cheby_dim = scan_nodes_instance->h_node_dim[0];
	const dim3 block_dim = {8,8,8};
	dim3 grid_dim = {cheby_dim.x/block_dim.x,cheby_dim.y/block_dim.y,cheby_dim.z/block_dim.z}; //number of blocks
	assert(abs(density_instance_query->h_pixel_size[0] - density_instance_target->h_pixel_size[0]) < 0.0001f);
	if (density_instance_target->textureObj == nullptr || density_instance_query->textureObj == nullptr ){
		printf("Density needs to be transported to texture memory for this feature to work.\n");
		std::exit(0);
	}

	float h_translation_vol = (float) vol((density_instance_target->h_pixel_dim[0] - density_instance_query->h_pixel_dim[0]));
	size_t translation_stride = 3*rotation_vol + 4;
	//	__global__
	//	void initZero(float * d_target, size_t vol){
	//independent of sweeps, need for cam score calculation. do beforehand.
	u_ave_scan<<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],density_instance_query->d_pixel_dim,scan_nodes_instance->d_node_dim,scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,
	d_u_ave);
	CudaCheckError();
	cudaDeviceSynchronize();
	CudaSafeCall(cudaMemcpy(h_u_ave,d_u_ave,sizeof(*d_u_ave),cudaMemcpyDeviceToHost));
	float * d_u_norm;
	float * h_u_norm = (float *) malloc(sizeof(*h_u_norm));
	h_u_norm[0] = 0.0f;
	CudaSafeCall(cudaMalloc((void**)&d_u_norm,sizeof(*d_u_norm)));
	CudaSafeCall(cudaMemcpy(d_u_norm,h_u_norm,sizeof(*d_u_norm),cudaMemcpyHostToDevice));

	//__global__
	//void u_norm_scan(cudaTextureObject_t d_density_query, uint3 * d_query_pdim, float * d_cheby_nodes, float * d_cheby_weights,
	//	uint3 * d_cheby_node_dim, float * d_u_ave, float * d_u_norm){
	u_norm_scan<<<grid_dim,block_dim>>>(density_instance_query->textureObj[0],density_instance_query->d_pixel_dim,scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,scan_nodes_instance->d_node_dim,
			d_u_ave,d_u_norm);
	CudaCheckError();
	cudaDeviceSynchronize();

	float * d_device_min;
	float * h_device_min = (float *) malloc(sizeof(*h_device_min));
	CudaSafeCall(cudaMalloc((void **)&d_device_min,sizeof(*d_device_min)));
	h_device_min[0] = FLT_MAX;
	CudaSafeCall(cudaMemcpy(d_device_min,h_device_min,sizeof(*d_device_min),cudaMemcpyHostToDevice));
	//init concat fields
	int g = 0;
	uint c = gpu_index + g*gpu_num;
	size_t current_offset = (gpu_index + g*gpu_num)*translation_volume_per_gpu;
	size_t current_volume;
	uint chunks = translation_vol/(translation_volume_per_gpu*gpu_num) + 1;
//	float * h_current_min;
//	float * h_current_max;
//	thrust::device_pothrust::device_pointer_cast(&cam_score_improved_instance->d_cam[0]);
	
	printf("-------------------------------------------\n");
	printf("CamScoreImproved calculated at gpu: \t%i\n",gpu_index);
	printf("Average for query function: \t\t%.10e\n",h_u_ave[0]);
	printf("-------------------------------------------\n");
	int ** local_mins = (int **)malloc(chunks * sizeof(int *));
	for (int i=0; i<chunks; i++)
		local_mins[i] = (int *)malloc(translation_volume_per_gpu * sizeof(int));

	while (current_offset < translation_volume_total){
		current_volume = std::min<size_t>(translation_volume_per_gpu,translation_volume_total-current_offset);

		initValue<<<128,512>>>(cam_score_improved_instance->d_working_mem,translation_stride*current_volume,0.0f);
		cudaDeviceSynchronize();
		v_ave<<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],density_instance_query->d_pixel_dim,scan_nodes_instance->d_node_dim,
				scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
				rot_nodes_register->d_refinements,current_offset,current_volume,translation_stride,
				cam_score_improved_instance->d_v_ave);
		CudaCheckError();
		cudaDeviceSynchronize();
		u_t_v<<<grid_dim,block_dim>>>(density_instance_target->textureObj[0],density_instance_query->textureObj[0],density_instance_query->d_pixel_dim,scan_nodes_instance->d_node_dim,
				scan_nodes_instance->d_node_list,scan_nodes_instance->d_weights,trans_nodes_instance->d_node_list,trans_nodes_instance->d_node_dim,
				rot_nodes_register->d_refinements,current_offset,current_volume,translation_stride,
				cam_score_improved_instance->d_v_ave,cam_score_improved_instance->d_v_m_v_ave,cam_score_improved_instance->d_u_t_v,d_u_ave);
		CudaCheckError();
		cudaDeviceSynchronize();
		cam_score<<<128,256>>>(rot_nodes_register->d_refinements,current_offset,current_volume,translation_stride,
				cam_score_improved_instance->d_v_ave,cam_score_improved_instance->d_v_m_v_ave,cam_score_improved_instance->d_u_t_v,d_u_norm);
		CudaCheckError();
		cudaDeviceSynchronize();
		//min max
//		template <uint BLOCKSIZE>
//		__global__
//		void min_max_per_trans(uint * d_refinements, size_t translation_stride, float * d_scores)
		min_max_per_trans<1024><<<translation_vol,1024>>>(rot_nodes_register->d_refinements,translation_stride,cam_score_improved_instance->d_cam);
		CudaCheckError();
		cudaDeviceSynchronize();
		//copy to host

		//determine the index of each maximum for later reference. write it to the spot reserved for entropy, since both will not be
		//needed at the same time.
//		__global__
//		void local_rot_max(uint * d_refinements, size_t translation_stride, float * d_scores)
//		local_rot_max<<<translation_vol,1024>>>(rot_nodes_register->d_refinements,translation_stride,cam_score_improved_instance->d_cam);
//		CudaCheckError();
//		cudaDeviceSynchronize();
		//thrust::device_ptr<float> start_ptr;
		//thrust::device_ptr<float> max_ptr;
		//for (int t = 0;t < current_volume;++t){
		//    start_ptr = thrust::device_pointer_cast(&cam_score_improved_instance->d_cam[t*translation_stride]);
		//    max_ptr = thrust::max_element(start_ptr, start_ptr + rotation_vol);
		//    h_max[c*translation_volume_per_gpu+t] = max_ptr[0];
		//    h_max_index[c*translation_volume_per_gpu+t] = &max_ptr[0] - &start_ptr[0];
	        //    if (rot_nodes_register->h_max_nodes[0]>1)
		//    	printf("T:%i max rot value (R,score):(%i,%f)\n",c*translation_volume_per_gpu+t,h_max_index[c*translation_volume_per_gpu+t],h_max[c*translation_volume_per_gpu+t]);
		//}
		//std::vector<float> rot_maxima(h_max,h_max + current_volume);
		//std::vector<float>::iterator max_trans = std::max_element(rot_maxima.begin(),rot_maxima.end());
//		template <uint BLOCKSIZE>
//		__global__
//		void min_sweep_strided(size_t translation_vol, size_t translation_stride, uint * d_refinements,
//				float * d_scores, float * d_device_min)
		min_strided<1024><<<128,256>>>(current_offset,current_volume,translation_stride,rot_nodes_register->d_refinements,cam_score_improved_instance->d_cam,d_device_min);
		CudaCheckError();
		cudaDeviceSynchronize();
		CudaSafeCall(cudaMemcpy(h_device_min,d_device_min,sizeof(*d_device_min),cudaMemcpyDeviceToHost));
		h_min[c] = h_device_min[0];

		printf("GPU:%i g-chunk:%i chunk:%i - min:%f\n",gpu_index,g,c,h_device_min[0]);

		//remember memory layout, need stride to catch the right scores
		for (int t = 0; t < current_volume; ++t){
			CudaSafeCall(cudaMemcpy(h_scores + (c*translation_volume_per_gpu + t)*rotation_vol,cam_score_improved_instance->d_cam + t*translation_stride,rotation_vol*sizeof(*h_scores),cudaMemcpyDeviceToHost));
//			CudaSafeCall(cudaMemcpy(h_max+current_offset+t,cam_score_improved_instance->d_cam + (t+1)*translation_stride-3,sizeof(*h_max),cudaMemcpyDeviceToHost));
		}
		cudaDeviceSynchronize();
//		float * h_out = (float *) malloc(cam_score_improved_instance->h_rot_vol[0]*sizeof(h_out));
//		CudaSafeCall(cudaMemcpy(h_out,cam_score_improved_instance->d_v_ave,cam_score_improved_instance->h_rot_vol[0]*sizeof(float),cudaMemcpyDeviceToHost));
//		cudaDeviceSynchronize();
//		std::fstream x_FileHandle;
//		x_FileHandle.open(out_file, std::fstream::out);
//		x_FileHandle.seekg(0, std::ios::beg);
//		for (int r = 0;r<cam_score_improved_instance->h_rot_vol[0];++r) x_FileHandle << h_out[r] << ",";
//		x_FileHandle.flush();
//		x_FileHandle.close();
		g++;
		printf("Scored for translations %i -- %i on gpu %i\n",current_offset,current_offset+current_volume,gpu_index);
		c = gpu_index + g*gpu_num;
		current_offset = c*translation_volume_per_gpu;
	}
	free(h_u_ave);
	free(h_u_norm);
	for (int i=0; i<chunks; i++)
		free(local_mins[i]);
	free(local_mins);

	CudaSafeCall(cudaFree(d_u_ave));
	CudaSafeCall(cudaFree(d_u_norm));
	CudaSafeCall(cudaFree(d_device_min));

}


void CamScoreImproved::createInstance(ChebychevNodes *const& trans_nodes,McEwenNodes *const& rot_nodes, size_t * const& translation_vol, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	if (_instance_registry.find(gpu_index) == _instance_registry.end()){
		CamScoreImproved_Register instance;
		instance.trans_node = &trans_nodes->_instance_registry[gpu_index];
		instance.rot_node = &rot_nodes->_instance_registry[gpu_index];
		size_t free_mem;
		size_t total_mem;
		uint rotation_vol = instance.rot_node->h_max_nodes[0];
		cudaMemGetInfo( &free_mem, &total_mem );
		float  float_vol = (0.9f*free_mem)/((3.0f*rotation_vol+4)*sizeof(float));
		translation_vol[gpu_index] = (size_t) float_vol;
		uint transformation_vol = rotation_vol*translation_vol[gpu_index];
		instance.h_cam = (float *) malloc(sizeof(*instance.h_cam)*transformation_vol);
//		instance.h_translation_vol = (uint *) malloc(sizeof(*instance.h_translation_vol));
		printf("Maximal translation volume per GPU: %u in gpu %i...\n",translation_vol[gpu_index],gpu_index);
//		CudaSafeCall(cudaMalloc((void **) &instance.d_translation_vol,sizeof(*instance.d_translation_vol)));
		size_t byte_volume = (3*rotation_vol+4)*translation_vol[gpu_index];
		printf("Trying to allocate %u bytes as working memory ...\n",byte_volume*sizeof(float));
		CudaSafeCall(cudaMalloc((void **) &instance.d_working_mem,byte_volume*sizeof(float)));
		printf("Done ...\n");
		instance.d_v_ave = instance.d_working_mem;
		instance.d_cam = instance.d_working_mem;
		instance.d_v_m_v_ave = instance.d_v_ave + rotation_vol;
		instance.d_u_t_v = instance.d_v_m_v_ave + rotation_vol;
		CudaSafeCall(cudaMalloc((void **) &instance.d_u_ave,sizeof(*instance.d_u_ave)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_u_norm,sizeof(*instance.d_u_norm)));
		CudaSafeCall(cudaMalloc((void **) &instance.d_device_min,sizeof(*instance.d_device_min)));
		instance.h_u_ave = (float *) malloc(sizeof(*instance.h_u_ave));
		instance.h_u_norm = (float *) malloc(sizeof(*instance.h_u_norm));
		instance.h_device_min = (float *) malloc(sizeof(*instance.h_device_min));
//		instance.h_translation_vol[0] = translation_vol[gpu_index];
//		CudaSafeCall(cudaMemcpy(instance.d_translation_vol,instance.h_translation_vol,sizeof(*instance.h_translation_vol),cudaMemcpyHostToDevice));
		_instance_registry[gpu_index] = instance;
	}
}
