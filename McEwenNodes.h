/*
 * McEwenNodes.h
 *
 *  Created on: Mar 26, 2021
 *      Author: kkarius
 */

#ifndef MCEWENNODES_H_
#define MCEWENNODES_H_


#include <string>
#include <vector>
#include <cmath>
#include <cstdio>
#include <cctype>
#include <iostream>
#include <util.h>
#include <map>
#include <vector>

#include <cuda_util_include/cutil_math.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <cuda_device_runtime_api.h>


struct McEwenNodes_Register {
	uint * h_refinements = nullptr;
	uint * d_refinements = nullptr;
	size_t * h_num_refinements = nullptr;
	size_t * d_num_refinements = nullptr;
	double * h_weights = nullptr;
	double * d_weights = nullptr;
	size_t * h_max_nodes = nullptr;
};


class McEwenNodes {
public:
	McEwenNodes(){};
	static std::vector<uint> refinement_from_level(const int& level);
	void createInstance(const int & refinement_level,int gpu_index);
	void createInstance(const uint3& MLN,int gpu_index);
	void buildWeights(double * const & h_weights, uint *const& h_refinements, size_t *const& h_num_refinements);
	void getRotationIndex(uint3 & abg,const uint& r,const int& gpu_index);
	void getRotationEulers(float3 & abg,const uint& r,const int& gpu_index);
	void getRotations(float4 * rot_qs, const uint& offset, const uint& vol, const int& gpu_index);
	void quadrature(float * h_values,float & result,const int& gpu_index);
	int getMaximumNodes(int gpu_index);
	virtual ~McEwenNodes(){};
//private:
	friend class Density;
	friend class CamScore;
	std::map<int,McEwenNodes_Register> _instance_registry;
};


#endif /* MCEWENNODES_H_ */
