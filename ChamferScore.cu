/*
 * ChamferScore.cu
 *
 *  Created on: Aug 14, 2021
 *      Author: kkarius
 */

#include <ChamferScore.h>
#include <score_helper.h>

__device__
void offset_to_coord(uint *const& u, float3 *const& coord, uint3 *const pixel_dim, float3 *const half_coord_dim, float *const& pixel_size){
	uint3 index;
	index.z = (*u)/(pixel_dim->x*pixel_dim->y);
	index.y = (*u - index.z*pixel_dim->x*pixel_dim->y)/pixel_dim->x;
	index.x = *u - index.z*pixel_dim->x*pixel_dim->y - index.y*pixel_dim->x;
	coord->x = (__uint2float_rn(index.x) + 0.5)*pixel_size[0] - half_coord_dim->x;
	coord->y = (__uint2float_rn(index.y) + 0.5)*pixel_size[0] - half_coord_dim->y;
	coord->z = (__uint2float_rn(index.z) + 0.5)*pixel_size[0] - half_coord_dim->z;
}

__device__
void coord_to_offset(float3 * const& coord, uint * const offset, uint3 * const& pixel_dim, float* const& pixel_size){
	offset[0] =  __float2uint_rd(coord->z/pixel_size[0])*pixel_dim->x*pixel_dim->y +
		   __float2uint_rd(coord->y/pixel_size[0])*pixel_dim->x +
		   __float2uint_rd(coord->x/pixel_size[0]);
}

__device__
bool coord_in_volume(float3 const& coord, float3 const coord_dim){
	bool ret = true;
	ret &= (0 < coord.x) && (coord.x < coord_dim.x);
	ret &= (0 < coord.y) && (coord.y < coord_dim.y);
	ret &= (0 < coord.z) && (coord.z < coord_dim.z);
	return ret;
}

template <uint BLOCKSIZE>
__global__
void chamfer_sweep(uint * d_query_surface_pixels, uint total_surface_pixels, float * d_trans_cheby_nodes,
		uint3 * d_trans_cheby_node_dim, int s, uint * d_refinements, size_t h_num_refinements,
		const int translation_offset,const int translation_volume, size_t translation_stride,
		float * d_chamfer_score, uint3 diffmap_pixel_dim, uint3 query_pixel_dim, float3 * d_query_coord_dim,
		float3 diffmap_coord_dim, float diffmap_pixel_size,float * d_diffmap, float3 target_coord_dim){
	__shared__ float diff_concat[BLOCKSIZE];
	__shared__ float translation[3];
	__shared__ float rotation[9];
	__shared__ uint rot_linear_index;
	float3 half_query_coord_dim = {0.5f*d_query_coord_dim->x,0.5f*d_query_coord_dim->y,0.5f*d_query_coord_dim->z};
	float3 padding_offset = 0.5*(diffmap_coord_dim - target_coord_dim);
	float3 pixel_coord_query;
	float3 pixel_coord_query_r;
	float3 pixel_coord_query_rt;
	uint diff_pixel_index;
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	uint total_rotations = d_refinements[3*s]*d_refinements[3*s+1]*d_refinements[3*s+2];
	int t; // translations
	int r = 0; // rotations
	//init shared memory
	diff_concat[threadIdx.x] = 0.0f;
	if (tid < total_surface_pixels){
		offset_to_coord(&d_query_surface_pixels[tid],&pixel_coord_query,
				&query_pixel_dim,&half_query_coord_dim,&diffmap_pixel_size);

		while (r < total_rotations){
//		while (r < 1){
			//write rotation to memory
			if (threadIdx.x == 0){
				uint3 abg;
				rot_index(d_refinements, h_num_refinements, abg, &rot_linear_index, r, s);
				if (s>0){
					if (abg.x%2 == 0 && (2*abg.y+1)%3 == 0 && abg.z%2 == 0){
						r+=1;
						continue;
					}
				}
				rotation_to_memory(&rotation[0],abg,d_refinements,s);
			}
//			rotation[0] = 1.0; rotation[1] = 0.0;rotation[2] = 0.0;rotation[3] = 0.0; rotation[4] = 1.0;rotation[5] = 0.0;rotation[6] = 0.0; rotation[7] = 0.0;rotation[8] = 1.0;
			__syncthreads();
			rotate(pixel_coord_query_r,pixel_coord_query,&rotation[0]);
			pixel_coord_query_r += padding_offset;
			t= 0;
			while(t < translation_volume){
				if (threadIdx.x == 0){
					translation_to_memory(&translation[0], d_trans_cheby_nodes, d_trans_cheby_node_dim, translation_offset+t);
					translation[0] *= target_coord_dim.x;
					translation[1] *= target_coord_dim.y;
					translation[2] *= target_coord_dim.z;
				}
				__syncthreads();
				translate(pixel_coord_query_rt,pixel_coord_query_r, &translation[0]);
				diff_pixel_index = __float2uint_rd(pixel_coord_query_rt.z/diffmap_pixel_size)*diffmap_pixel_dim.x*diffmap_pixel_dim.y +
						   	   	   __float2uint_rd(pixel_coord_query_rt.y/diffmap_pixel_size)*diffmap_pixel_dim.x +
							   	  __float2uint_rd(pixel_coord_query_rt.x/diffmap_pixel_size);
				diff_concat[threadIdx.x] = d_diffmap[diff_pixel_index]; __syncthreads();
//				if (r==0 && t==0){
//					printf("%f %f %f: %f\n",pixel_coord_query_rt.x,pixel_coord_query_rt.y,pixel_coord_query_rt.z,diff_concat[threadIdx.x]);
//				}

				if(BLOCKSIZE >= 512){if (tid < 256) { diff_concat[tid] += diff_concat[tid + 256];} __syncthreads();}
				if(BLOCKSIZE >= 256){if (tid < 128) { diff_concat[tid] += diff_concat[tid + 128];} __syncthreads();}
				if(BLOCKSIZE >= 128){if (tid <  64) { diff_concat[tid] += diff_concat[tid + 64];} __syncthreads();}
				if (tid <  32) { diff_concat[tid] += diff_concat[tid + 32];} __syncthreads();
				if (tid <  16) { diff_concat[tid] += diff_concat[tid + 16];} __syncthreads();
				if (tid <   8) { diff_concat[tid] += diff_concat[tid + 8];} __syncthreads();
				if (tid <   4) { diff_concat[tid] += diff_concat[tid + 4];} __syncthreads();
				if (tid <   2) { diff_concat[tid] += diff_concat[tid + 2];} __syncthreads();
				if (tid <   1) { diff_concat[tid] += diff_concat[tid + 1];} __syncthreads();
				if (tid==0){
					atomicAdd(&d_chamfer_score[t*translation_stride+rot_linear_index], diff_concat[0]);
				}
				t += 1;
			}
			r += 1;
		}

	}
}

void ChamferScore::score(Density *const& target, Density *const& query, const int& translation_offset,
		const int& translation_volume,const int& gpu_index){
	cudaSetDevice(gpu_index);
	Density_Register * target_density_instance = &target->_instance_registry[gpu_index];
	Density_Register * query_density_instance = &query->_instance_registry[gpu_index];
	ChamferScore_Register * score_instance = &_instance_registry[gpu_index];
	ChebychevNodes_Register * trans_nodes_instance = score_instance->trans_node;
	McEwenNodes_Register * rot_nodes_instance = score_instance->rot_node;
	size_t total_rotations = rot_nodes_instance->h_max_nodes[0];
	size_t total_translations = vol(trans_nodes_instance->h_node_dim[0]);
	size_t translation_stride = score_instance->h_translation_stride[0];
	uint query_surface_pixels = query_density_instance->h_on_surface_vol[0];
	for (int t = 0; t < translation_volume; t++){
		thrust::fill(thrust::device_pointer_cast(&score_instance->d_score[t*translation_stride]),
					 thrust::device_pointer_cast(&score_instance->d_score[t*translation_stride+total_rotations]),0.0f);
	}
	CudaCheckError();
	const int blockDim = 256;
	int gridDim = query_surface_pixels/blockDim + 1;

//	printf("DIFFMAPSUM:%f\n",thrust::reduce(thrust::device_pointer_cast(&score_instance->d_diffmap[0]),thrust::device_pointer_cast(&score_instance->d_diffmap[vol(score_instance->h_padding_pixel_dim[0])])));

//	template <uint BLOCKSIZE>
//	__global__
//	void chamfer_sweep(uint * d_query_surface_pixels, uint total_surface_pixels, float * d_trans_cheby_nodes,
//			uint3 * d_trans_cheby_node_dim, int s, uint * d_refinements, size_t h_num_refinements,
//			const int translation_offset,const int translation_volume, size_t translation_stride,
//			float * d_chamfer_score, uint3 diffmap_pixel_dim, uint3 query_pixel_dim, float3 * d_query_coord_dim,
//			float3 diffmap_coord_dim, float diffmap_pixel_size,float * d_diffmap, float3 target_coord_dim)
	chamfer_sweep<blockDim><<<gridDim,blockDim>>>(query_density_instance->d_on_surface,query_surface_pixels,trans_nodes_instance->d_node_list,
			trans_nodes_instance->d_node_dim, 0, rot_nodes_instance->d_refinements, rot_nodes_instance->h_num_refinements[0],
			translation_offset, translation_volume, score_instance->h_translation_stride[0],
			score_instance->d_score, score_instance->h_padding_pixel_dim[0],query_density_instance->h_pixel_dim[0],query_density_instance->d_coord_dim,
			score_instance->h_padding_coord_dim[0], target_density_instance->h_pixel_size[0],score_instance->d_diffmap,target_density_instance->h_coord_dim[0]);
	cudaDeviceSynchronize();
	CudaCheckError();
}

void ChamferScore::getPaddingVolume(uint & padding_vol, const int & gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	ChamferScore_Register * instance = &_instance_registry[gpu_index];
	padding_vol = vol(instance->h_padding_pixel_dim[0]);
}


void ChamferScore::createInstance(Density *const& target, Density *const& query, ChebychevNodes *const& trans_nodes,McEwenNodes *const& rot_nodes,
		const size_t & translation_vol, const int & gpu_index){
		CudaSafeCall(cudaSetDevice(gpu_index));
		if (_instance_registry.find(gpu_index) == _instance_registry.end()){
			ChamferScore_Register instance;
			Density_Register * target_density_instance = &target->_instance_registry[gpu_index];
			McEwenNodes_Register * rot_nodes_instance = &rot_nodes->_instance_registry[gpu_index];
			uint3 target_pixel_dim;
			target->getPixelDim(target_pixel_dim,gpu_index);
			float pixel_size;
			target->getPixelSize(pixel_size,gpu_index);
			float3 target_coord_dim;
			target->getCoordDim(target_coord_dim,gpu_index);
			uint rotation_vol = rot_nodes_instance->h_max_nodes[0];
			size_t sweep_space_vol = 2*rotation_vol + 3*rot_nodes_instance->h_num_refinements[0];
			uint max_pixel_dim_query;
			query->getMaxPixelDim(max_pixel_dim_query,gpu_index);
			uint padding = max_pixel_dim_query/2+1;
			uint3 padding_pixel_dim = {target_pixel_dim.x+2*padding,target_pixel_dim.y+2*padding,target_pixel_dim.z+2*padding};
			float3 padding_coord_dim = {pixel_size*padding_pixel_dim.x,pixel_size*padding_pixel_dim.y,pixel_size*padding_pixel_dim.z};

			instance.h_diffmap = (float *) malloc(sizeof(*instance.h_diffmap)*vol(padding_pixel_dim));
			for (int i=0;i<vol(padding_pixel_dim); ++i) instance.h_diffmap[i] = 0.0f;
			instance.h_translation_vol = (size_t *) malloc(sizeof(*instance.h_translation_vol));
			instance.h_translation_stride = (size_t *) malloc(sizeof(*instance.h_translation_stride));
			instance.h_target_padding = (uint *) malloc(sizeof(*instance.h_target_padding));
			instance.h_padding_pixel_dim = (uint3 *) malloc(sizeof(*instance.h_padding_pixel_dim));
			instance.h_padding_coord_dim = (float3 *) malloc(sizeof(*instance.h_padding_coord_dim));

			CudaSafeCall(cudaMalloc((void **) &instance.d_translation_vol,sizeof(*instance.d_translation_vol)));
			CudaSafeCall(cudaMalloc((void **) &instance.d_diffmap,sizeof(*instance.d_diffmap)*vol(padding_pixel_dim)));
			CudaSafeCall(cudaMalloc((void **) &instance.d_working_mem,sweep_space_vol*translation_vol*sizeof(float)));

			instance.trans_node = &trans_nodes->_instance_registry[gpu_index];
			instance.rot_node = &rot_nodes->_instance_registry[gpu_index];

			instance.h_target_padding[0] = padding;
			instance.h_padding_pixel_dim[0] = padding_pixel_dim;
			instance.h_padding_coord_dim[0] = padding_coord_dim;
			instance.h_translation_stride[0] = sweep_space_vol;
			instance.d_score = instance.d_working_mem;
			instance.d_dist = instance.d_score + rotation_vol;
			instance.d_entropies = instance.d_dist + rotation_vol;
			instance.h_translation_vol[0] = translation_vol;
			instance.d_entropies = instance.d_dist + rotation_vol;
			instance.h_translation_vol[0] = translation_vol;
			instance.h_target_padding[0] = padding;
			CudaSafeCall(cudaMemcpy(instance.d_translation_vol,instance.h_translation_vol,sizeof(*instance.h_translation_vol),cudaMemcpyHostToDevice));
			_instance_registry[gpu_index] = instance;
		}
}

void ChamferScore::defineSurfaces(Density *const& target, const float& threshold0_t, const float& threshold1_t,
		Density *const& query, const float& threshold0_q, const float& threshold1_q,const int& gpu_index){
	CudaSafeCall(cudaSetDevice(gpu_index));
	target->defineSurface(threshold0_t,threshold1_t,gpu_index);
	query->defineSurface(threshold0_q,threshold1_q,gpu_index);
}
