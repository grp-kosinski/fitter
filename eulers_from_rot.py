# coding: utf-8
import sympy
from sympy import *
phi, theta, psi = symbols("phi theta psi")
import numpy as np
Rm = np.ndarray(shape=(3,3),dtype=object)
x, y, z = symbols("x y z")
alpha = symbols("alpha")
C = 1-cos(alpha)
c = cos(alpha)
s = sin(alpha)
Rm[0,0]
Rm[0,0] = x*x*C + c
Rm[0,1] = x*y*C - z*s
Rm[0,2] = x*z*C + y*s
Rm[1,0] = y*x*C + z*s
Rm[1,1] = y*y*C + c
Rm[1,2] = y*z*C - x*s
Rm[2,0] = z*y*C + x*s
Rm[2,0] = z*x*C - y*s
Rm[2,1] = z*y*C + x*s
Rm[2,2] = z*z*C + c
one = np.copy(Rm)
for i in range(3):
    for j in range(3):
        one[i,j] = one[i,j].subs({x:0,y:0,z:1,alpha:phi})
two = np.copy(Rm)
for i in range(3):
    for j in range(3):
        two[i,j] = two[i,j].subs({x:-sin(phi),y:cos(phi),z:0,alpha:theta})
three = np.copy(Rm)
for i in range(3):
    for j in range(3):
        three[i,j] = three[i,j].subs({x:cos(phi)*sin(theta),y:sin(phi)*sin(theta),z:cos(theta),alpha:psi})
R=three@two@one
x0 = np.ndarray(shape=(3,1))
x0[0,0] = 1
x0[1,0] = 0
x0[2,0] = 0
R@x0
x0[0,0]
x1 = R@x0
x2 = np.ndarray(shape=(3,1),dtype=object)
for i in range(3):
    x2[i,0] = x1[i,0].simplify()
y0 = np.ndarray(shape=(3,1))
y0[0,0] = 0
y0[1,0] = 1
y0[2,0] = 0
y1 = R@y0
y2 = np.ndarray(shape=(3,1),dtype=object)
for i in range(3):
    y2[i,0] = y1[i,0].simplify()
z0 = np.ndarray(shape=(3,1))
z0[0,0] = 0 
z0[1,0] = 0 
z0[2,0] = 1
z1 = R@z0
z2 = np.ndarray(shape=(3,1),dtype=object)
z1 = R@z0
for i in range(3):
    z2[i,0] = z1[i,0].simplify()
for i in range(3):
    for j in range(3):
        R[i,j] = R[i,j].simplify()
        print(i,j,R[i,j])
