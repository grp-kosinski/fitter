/*
 * CLabeler.cpp
 *
 *  Created on: Feb 25, 2019
 *      Author: kkarius
 */
#include <Labeler.h>

__device__
int neighbour_num(int index, int i,int l){
	switch (i){
		case 0:
			return index - l*l - l - 1;
		case 1:
			return index - l*l - l;
		case 2:
			return index - l*l - l + 1;
		case 3:
			return index - l*l     - 1;
		case 4:
			return index - l*l ;
		case 5:
			return index - l*l     + 1;
		case 6:
			return index - l*l + l - 1;
		case 7:
			return index - l*l + l;
		case 8:
			return index - l*l + l + 1;
		case 9:
			return index      - l - 1;
		case 10:
			return index      - l;
		case 11:
			return index      - l + 1;    
		case 12:
			return index          - 1;
		case 13:
			return index          + 1;
		case 14:
			return index      + l - 1;
		case 15:
			return index      + l;
		case 16:
			return index      + l + 1;
		case 17:
			return index + l*l - l - 1;
		case 18:
			return index + l*l - l;
		case 19:
			return index + l*l - l + 1;
		case 20:
			return index + l*l     - 1;
		case 21:
			return index + l*l;
		case 22:
			return index + l*l     + 1;
		case 23:
			return index + l*l + l - 1;
		case 24:
			return index + l*l + l;
		case 25:
			return index + l*l + l + 1;
	}
	return index;
}


__device__
uint findRoot(uint * sLabels, uint label){
//PSEUDO CODE: FUNCTIONS FINDROOT and UNION - {Stava, Benes 2011} modifications by Karius 20
//FindRoot(equivalenceArray, elementAddress)
//while(equivalenceArray[elementAddress] != elementAddress)
//elementAddress <− equivalenceArray[elementAddress];
//return elementAddress;
	while(sLabels[label] != label){
		label = sLabels[label];
	}
	return label;
}

__device__
void _union(int * dSegData, uint * dLabels, int address0, int address1,int sChanged[1],int n){
//	PSEUDOCODE
//	root0 <− FindRoot(equivalenceArray, elementAddress0);
//	root1 <− FindRoot(equivalenceArray, elementAddress1);
//	//connect an equivalence tree with a higher label to the tree with a lower label
//	if(root0 < root1) equivalenceArray[root1] <− root0;
//	if(root1 < root0) equivalenceArray[root0] <− root1;
	if (address0<n && address1 <n){
		if (dSegData[address0] == dSegData[address1]){
			int root0 = findRoot(dLabels,address0);
			int root1 = findRoot(dLabels,address1);
			if(root0 < root1) atomicMin(dLabels + root1,root0); //dLabels[root1] = root0;
			if(root1 < root0) atomicMin(dLabels + root0,root1); //dLabels[root0] = root1;
			if(root0 != root1) sChanged[0] = 1;
//			if (threadIdx.x == 8 && blockIdx.x + blockIdx.y + blockIdx.z == 0 && address0 == 264 && address1==263){
//				printf("%u %u\n",address0,address1);
//			}
		}
	}
}

__device__ 
uint map_label_to_global(uint local, uint pixel_dim){
	uint z = local/64;
	uint y = (local - 64*z)/8;
	uint x = local - 64*z - 8*y;
	z += blockIdx.z*blockDim.z;
	y += blockIdx.y*blockDim.y;
	x += blockIdx.x*blockDim.x;
	return pixel_dim*pixel_dim*z + pixel_dim*y + x;
}

__global__
void ccl_kernel1(int * dSegData, uint pixel_dim, uint * dLabelsData){
	//PSEUDO CODE OF KERNEL 1 - {Stava, Benes 2011} modifications by Karius 20
	//INPUT:
	//dSegData
	//OUTPUT:
	//dLabelsData
	////2D array of segmented input data - 3D for our case
	////2D array of labels (equivalence array) - 3D for our case
	//shared sSegs[];
	////shared memory used to store the segments
	//shared sLabels[];
	////shared memory that is used to compute the local solution
	//localIndex <− localAddress(threadIdx);
	////local address of the element in the shared memory
	//sSegs[localIndex] <− loadSegDataToSharedMem(dSegData, threadIdx.x, threadIdx.y);
	//sSegs[borders] <− setBordersAroundSubBlockToZero();
	//shared sChanged[1];
	//syncThreads();
	//label <− localIndex;
	//while(1) {
	////Pass 1 of the CCL algorithm
	//sLabels[localIndex] <− label;
	//if(threadIdx == (0,0)) sChanged[0] <− 0;
	//syncThreads();
	//newLabel <− label;
	////find the minimal label from the neighboring elements
	//for(allNeighbors)
	//ifl(sSegs[localIndex] == sSegs[neighIndex]) newLabel <− min(newLabel, sLabels[neighIndex]);
	//syncThreads();
	////If the new label is smaller than the old one merge the equivalence trees
	//if(newLabel < labal) {
	//atomicMin(sLabels+label, newLabel);
	//sChanged[0] <− 1
	//}
	//syncThreads();
	//if(sChanged[0] == 1) break;
	////the local solution has been found, exit the loop
	////Pass 2 of the CCL algorithm
	//label <− findRoot(sLabels, label);
	//syncThreads();
	//}
	////store the result to the device memory
	//globalIndex <− globalAddress(blockIdx, threadIdx)
	//dLabalsData[globalIndex] <− transferAddressToGlobalSpace(label);
	__shared__ int sSegs[1000]; //10 x 10 x 10 for sSegs due to 0 padding on edges
	__shared__ uint sLabels[512]; // 8 x 8 x 8 for 512 in total
	__shared__ int sChanged[1];
	//offset for unqiue labeling
//	size_t n = pixel_dim*pixel_dim*pixel_dim;
	int label;
	int newLabel;
	int labelIndex = threadIdx.z*64 + threadIdx.y*8 + threadIdx.x;
	int localIndex = (threadIdx.z + 1)*100 + (threadIdx.y + 1)*10 + threadIdx.x + 1;
	uint3 globalVectorIndex = {blockIdx.x*blockDim.x + threadIdx.x, blockIdx.y*blockDim.y + threadIdx.y,blockIdx.z*blockDim.z + threadIdx.z};
	int globalIndex = globalVectorIndex.z*pixel_dim*pixel_dim + globalVectorIndex.y*pixel_dim + globalVectorIndex.x;
	//set all to background, more readable, slight abuse of labelIndex, since it spans 0..511
	if (labelIndex < 500){
		sSegs[2*labelIndex] = -1;
		sSegs[2*labelIndex + 1] = -1;
	}
	__syncthreads();
	if (globalVectorIndex.x < pixel_dim && globalVectorIndex.y < pixel_dim && globalVectorIndex.z < pixel_dim){
		sSegs[localIndex] = dSegData[globalIndex];
	} else {
		sSegs[localIndex] = -1;
	}
	__syncthreads();
	label = labelIndex;
	while(1) {
	//Pass 1 of the CCL algorithm
		sLabels[labelIndex] = label;
		if(threadIdx.x == 0 && threadIdx.y == 0 && threadIdx.z == 0) sChanged[0] = 0;
		__syncthreads();
		newLabel = label;
	//find the minimal label from the neighboring elements
 		for (int i = 0; i < 26;i++)
		{
			if (sSegs[localIndex] != -1.0){	
				if (sSegs[localIndex] == sSegs[neighbour_num(localIndex,i,10)]){
					//if (neighbour_num(labelIndex,i,8)>511 || neighbour_num(labelIndex,i,8) <0){
					newLabel = min(newLabel, sLabels[neighbour_num(labelIndex,i,8)]);}
				//}
			}	
		}  
		__syncthreads();
		//If the new label is smaller than the old one merge the equivalence trees
		if(newLabel < label) {
			atomicMin(sLabels + label, newLabel);
			sChanged[0] = 1;
		}
		__syncthreads();
		//next line is different in the pseudocode
		if( sChanged[0] == 0) break; //the local solution has been found, exit the loop
		//Pass 2 of the CCL algorithm
		label = findRoot(sLabels, label);
		__syncthreads();
	}
	//store the result to the device memory
	__syncthreads();
	if (globalVectorIndex.x < pixel_dim && globalVectorIndex.y < pixel_dim && globalVectorIndex.z < pixel_dim){
		dLabelsData[globalIndex] = map_label_to_global(sLabels[labelIndex],pixel_dim);
	}
	__syncthreads();
}

__global__
void ccl_kernel2(int * dSegData, uint * dLabels, uint pixel_dim, int d){
	//PSEUDO CODE OF KERNEL 2 - {Stava, Benes 2011} modifications by Karius 20
	//The code is heavily modified but the basic principle should be the same
	//INPUT:
	//dSegData
	//dLabelsData
	//dSubBlockDIm
	//OUTPUT:
	//dLabelsData
	////2D array of segmented input data
	////2D array of labels
	////dimensions of the merged tiles
	////2D array of labels
	//subBlockId <− threadIdx + blockIdx ∗ blockDim; //id (x,y) of the merged tile
	//repetitions <− subBlockDIm / blockDIm/z;
	////how many times are the thread reused for the given subblock?
	//shared sChanged[1];
	////shared memory used to check whether the solution is final or not
	//while(1) {
	//if(threadIdx == (0,0,0)) sChanged[0] <− 0;
	//syncThreads();
	////process the bottomhorizontal border
	//for(i in 0:repetitions) {
	//x <− subBlockId.x ∗ subBlockDim + threadIdx.z + i∗ blockDim.z;
	//y <− (subBlockId.y+1) ∗ subBlockDim − 1;
	//if(!leftBorder) Union(dLabelsData, dSegData, globalAddr(x,y), globalAddr(x−1, y+1), sChanged)
	//Union(dLabelsData, dSegData, globalAddr(x,y), globalAddr(x, y+1), sChanged)
	//if(!rightBorder) Union(dLabelsData, dSegData, globalAddr(x,y), globalAddr(x+1, y+1), sChanged)
	//}
	////process the right vertical border
	//for(i in 0:repetitions) {
	//x <− (subBlockId.x+1) ∗ subBlockDim − 1;
	//y <− subBlockId.y ∗ subBlockDim + threadIdx.z + i∗ blockDim.z;
	//if(!topBorder) Union(dLabelsData, dSegData, globalAddr(x,y), globalAddr(x+1, y−1), sChanged)
	//Union(dLabelsData, dSegData, globalAddr(x,y), globalAddr(x+1, y), sChanged)
	//if(!bottomBorder) Union(dLabelsData, dSegData, globalAddr(x,y), globalAddr(x+1, y+1), sChanged)
	//}
	//syncThreads();
	//if(sChanged[0] == 0) break; //no changes −> the tiles are merged
	//syncThreads();
	__shared__ int sChanged[1];
	uint metric[3] = {1,pixel_dim,pixel_dim*pixel_dim};
	//linearize block offset - block is 16*d in length, otherwise standard linearisation
	int global_offset = 16*d*(blockIdx.x*metric[0]+blockIdx.y*metric[1]+blockIdx.z*metric[2]);
	int tid = threadIdx.x;
	int l = 8*d;
	int n = pixel_dim*pixel_dim*pixel_dim;
	//kernel runs on 12 faces, 64 threads each
	while(true){
		if (tid==0){
			sChanged[0] = 0;
		}
		__syncthreads();
		//face 0
		if ((0 <= tid) && (tid <= 63)){
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					_union(dSegData,dLabels,
				   	//global offset + face offset + tile offset
					global_offset + l*metric[0] + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2],
					global_offset + l*metric[0] + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2] - 1*metric[0],
					sChanged,n);
				}
			}

		}
		//face 1
		if ((64 <= tid) && (tid <= 127)){
			tid -= 64;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					_union(dSegData,dLabels,
					//global offset + face offset + tile offset
					global_offset + l*metric[1] + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2],
					global_offset + l*metric[1] + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2] - 1*metric[1],
					sChanged,n);
				}
			}
		}
		//face 2
		if ((128 <= tid) && (tid <= 191)){
			tid -= 128;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					_union(dSegData,dLabels,
				   	//global offset + face offset + tile offset
					global_offset + l*metric[2] + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1],
					global_offset + l*metric[2] + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1] - 1*metric[2],
					sChanged,n);
				}
			}
		}
		//face 3
		if ((192 <= tid) && (tid <= 255)){
			tid -= 192;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					_union(dSegData,dLabels,
				   	//global offset + face offset + tile offset
					global_offset + l*(metric[0] + metric[1]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2],
					global_offset + l*(metric[0] + metric[1]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2] - 1*metric[1],
					sChanged,n);
				}
			}
		}
		//face 4
		if ((256 <= tid) && (tid <= 319)){
			tid -= 256;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					_union(dSegData,dLabels,
						   //global offset + face offset + tile offset
						   global_offset + l*(metric[0] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1],
						   global_offset + l*(metric[0] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1] - 1*metric[2],
						   sChanged,n);
				}
			}
		}
		//face 5
		if ((320 <= tid) && (tid <= 383)){
			tid -= 320;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					_union(dSegData,dLabels,
					//global offset + face offset + tile offset
					global_offset + l*(metric[0] + metric[1]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2],
					global_offset + l*(metric[0] + metric[1]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2] - 1*metric[0],
					sChanged,n);
				}
			}
		}
		//face 6
		if ((384 <= tid) && (tid <= 447)){
			tid -= 384;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					_union(dSegData,dLabels,
					//global offset + face offset + tile offset
					global_offset + l*(metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1],
					global_offset + l*(metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1] - 1*metric[2],
					sChanged,n);
				}
			}
		}
		//face 7
		if ((448 <= tid) && (tid <= 511)){
			tid -= 448;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					_union(dSegData,dLabels,
					//global offset + face offset + tile offset
					global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1],
					global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1] - 1*metric[2],
					sChanged,n);
				}
			}
		}
		//face 8
		if ((512 <= tid) && (tid <= 575)){
			tid -= 512;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					_union(dSegData,dLabels,
					//global offset + face offset + tile offset
					global_offset + l*(metric[0] + metric[2]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2],
					global_offset + l*(metric[0] + metric[2]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2] - 1*metric[0],
					sChanged,n);
				}
			}
		}
		//face 9
		if ((576 <= tid) && (tid <= 639)){
			tid -= 576;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					_union(dSegData,dLabels,
					//global offset + face offset + tile offset
					global_offset + l*(metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2],
					global_offset + l*(metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2] - 1*metric[1],
					sChanged,n);
				}
			}
		}
		//face 10
		if ((640 <= tid) && (tid <= 703)){
			tid -= 640;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					_union(dSegData,dLabels,
					//global offset + face offset + tile offset
					global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2],
					global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2] - 1*metric[1],
					sChanged,n);
				}
			}
		}
		//face 11
		if ((704 <= tid) && (tid <= 768)){
			tid -= 704;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					_union(dSegData,dLabels,
					//global offset + face offset + tile offset
					global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2],
					global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2] - 1*metric[0],
					sChanged,n);
				}
			}
		}
		__syncthreads();
		if(sChanged[0] == 0) break;
	}
}

__global__
void ccl_kernel3(int * dSegData, uint * dLabels, uint pixel_dim, int d){
	//this kernel is supposed to flatten the equivalence trees in the border regions
	//using findRoot
	uint metric[3] = {1,pixel_dim,pixel_dim*pixel_dim};
	//linearize block offset - block is 16*d in length, otherwise standard linearisation
	int global_offset = 16*d*(blockIdx.x*metric[0]+blockIdx.y*metric[1]+blockIdx.z*metric[2]);
	int tid = threadIdx.x;
	int l = 8*d;
	//kernel runs on 12 faces, 64 threads each
		//face 0
		if ((0 <= tid) && (tid <= 63)){
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					dLabels[global_offset + l*metric[0] + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2]] = 				 findRoot(dLabels,global_offset + l*metric[0] + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2]);
					dLabels[global_offset + l*metric[0] + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2] - 1*metric[0]] = findRoot(dLabels,global_offset + l*metric[0] + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2] - 1*metric[0]);
				}
			}
		}
		//face 1
		if ((64 <= tid) && (tid <= 127)){
			tid -= 64;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					dLabels[global_offset + l*metric[1] + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2]] = 				 findRoot(dLabels, global_offset + l*metric[1] + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2]);
					dLabels[global_offset + l*metric[1] + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2] - 1*metric[1]] = findRoot(dLabels, global_offset + l*metric[1] + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2] - 1*metric[1]);
				}
			}
		}	
		//face 2
		if ((128 <= tid) && (tid <= 191)){
			tid -= 128;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					dLabels[global_offset + l*metric[2] + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1]] = 				 findRoot(dLabels,global_offset + l*metric[2] + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1]);
					dLabels[global_offset + l*metric[2] + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1] - 1*metric[2]] = findRoot(dLabels,global_offset + l*metric[2] + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1] - 1*metric[2]);
				}
			}
		}
		//face 3
		if ((192 <= tid) && (tid <= 255)){
			tid -= 192;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					dLabels[global_offset + l*(metric[0] + metric[1]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2]] = 			   findRoot(dLabels,global_offset + l*(metric[0] + metric[1]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2]);
					dLabels[global_offset + l*(metric[0] + metric[1]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2] - 1*metric[1]] = findRoot(dLabels,global_offset + l*(metric[0] + metric[1]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2] - 1*metric[1]);
				}
			}
		}
		//face 4
		if ((256 <= tid) && (tid <= 319)){
			tid -= 256;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					dLabels[global_offset + l*(metric[0] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1]] = 			   findRoot(dLabels,global_offset + l*(metric[0] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1]);
					dLabels[global_offset + l*(metric[0] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1] - 1*metric[2]] = findRoot(dLabels,global_offset + l*(metric[0] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1] - 1*metric[2]);
				}
			}
		}
		//face 5
		if ((320 <= tid) && (tid <= 383)){
			tid -= 320;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					dLabels[global_offset + l*(metric[0] + metric[1]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2]] = 			   findRoot(dLabels,global_offset + l*(metric[0] + metric[1]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2]);
					dLabels[global_offset + l*(metric[0] + metric[1]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2] - 1*metric[0]] = findRoot(dLabels,global_offset + l*(metric[0] + metric[1]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2] - 1*metric[0]);
				}
			}
		}
		//face 6
		if ((384 <= tid) && (tid <= 447)){
			tid -= 384;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					dLabels[global_offset + l*(metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1]] = 			   findRoot(dLabels,global_offset + l*(metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1]);
					dLabels[global_offset + l*(metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1] - 1*metric[2]] = findRoot(dLabels,global_offset + l*(metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1] - 1*metric[2]);
				}
			}
		}
		//face 7
		if ((448 <= tid) && (tid <= 511)){
			tid -= 448;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					dLabels[global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1]] = 			   findRoot(dLabels, global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1]);
					dLabels[global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1] - 1*metric[2]] = findRoot(dLabels, global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[1] - 1*metric[2]);
				}
			}
		}
		//face 8
		if ((512 <= tid) && (tid <= 575)){
			tid -= 512;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					dLabels[global_offset + l*(metric[0] + metric[2]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2]] = 			   findRoot(dLabels,global_offset + l*(metric[0] + metric[2]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2]);
					dLabels[global_offset + l*(metric[0] + metric[2]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2] - 1*metric[0]] = findRoot(dLabels,global_offset + l*(metric[0] + metric[2]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2] - 1*metric[0]);
				}
			}
		}
		//face 9
		if ((576 <= tid) && (tid <= 639)){
			tid -= 576;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					dLabels[global_offset + l*(metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2]] = 			   findRoot(dLabels,global_offset + l*(metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2]);
					dLabels[global_offset + l*(metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2] - 1*metric[1]] = findRoot(dLabels,global_offset + l*(metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2] - 1*metric[1]);
				}
			}
		}
		//face 10first
		if ((640 <= tid) && (tid <= 703)){
			tid -= 640;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					dLabels[global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2]] = 			   findRoot(dLabels,global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2]);
					dLabels[global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2] - 1*metric[1]] = findRoot(dLabels,global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[0] + ((tid%8)*d + j)*metric[2] - 1*metric[1]);
				}
			}
		}
		//face 11
		if ((704 <= tid) && (tid <= 768)){
			tid -= 704;
			for (int i=0;i<d;i++){
				for (int j=0;j<d;j++){
					dLabels[global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2]] = 			   findRoot(dLabels, global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2]);
					dLabels[global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2] - 1*metric[0]] = findRoot(dLabels, global_offset + l*(metric[0] + metric[1] + metric[2]) + ((tid/8)*d + i)*metric[1] + ((tid%8)*d + j)*metric[2] - 1*metric[0]);
				}
			}
		}
}

__global__
void ccl_kernel4(int * dSegData, uint * dLabels, uint n){
	//simply call findroot for every element
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	if (tid>=n) return;
	//TODO: maybe assign label 0 to background
	dLabels[tid] = findRoot(dLabels,dLabels[tid]);
}

__global__
void ccl_kernel(int * seg_data, uint * labels,uint log_2_dim){
	//general procedure adapted by - {Stava, Benes 2011} modifications by Karius 20
	dim3 ccl_kernel1_grid_dim = {log_2_dim/8,log_2_dim/8,log_2_dim/8};
	dim3 ccl_kernel1_block_dim = {8,8,8}; //hard coded

	//8 cubes of 8*8*8
	uint merge_iterations = (uint)(logf((float) (log_2_dim/16))/logf(2) + 1);
//	printf("Merge iterations: %u\n",merge_iterations);
	//ccl_kernel1(float * dSegData, int * dLabelsData) - solve ccl problem locally
	if (threadIdx.x ==0 ){
		ccl_kernel1<<<ccl_kernel1_grid_dim,ccl_kernel1_block_dim>>>(seg_data,log_2_dim,labels);
		cudaDeviceSynchronize();
		int d = 1;
		dim3 merge_block_dim = {16,16,16};
		dim3 ccl_kernel2_3_grid_dim;
		for (int i=0;i<merge_iterations;i++){
			ccl_kernel2_3_grid_dim = {log_2_dim/merge_block_dim.x,
									  log_2_dim/merge_block_dim.y,
									  log_2_dim/merge_block_dim.z};
//			printf("Merge block dim: %u %u %u\n",ccl_kernel2_3_grid_dim.x,ccl_kernel2_3_grid_dim.y,ccl_kernel2_3_grid_dim.z);
			//786 is hard coded
			//ccl_kernel2(float * dSegData, int * dLabels, uint * d_pixel_dim, int d)
			ccl_kernel2<<<ccl_kernel2_3_grid_dim,768>>>(seg_data,labels,log_2_dim,d);
			cudaDeviceSynchronize();
			//ccl_kernel3(int * dSegData, int * dLabels, uint * d_pixel_dim, int d)
			ccl_kernel3<<<ccl_kernel2_3_grid_dim,768>>>(seg_data,labels,log_2_dim,d);
			cudaDeviceSynchronize();
			d *= 2;
			merge_block_dim = {merge_block_dim.x*2,merge_block_dim.y*2,merge_block_dim.z*2};
		}
		//ccl_kernel4(float * dSegData, int * dLabels, uint n)
		ccl_kernel4<<<(log_2_dim*log_2_dim*log_2_dim)/1024 + 1,1024>>>(seg_data,labels,log_2_dim*log_2_dim*log_2_dim);
	}
}

__device__
uint3 linear_to_index_space(const uint &linear_index, uint * const &pixel_dim){
	uint3 ret;
	ret.z = linear_index/(pixel_dim[0]*pixel_dim[1]);
	ret.y = (linear_index - ret.z*pixel_dim[0]*pixel_dim[1])/pixel_dim[0];
	ret.x = linear_index - ret.y*pixel_dim[0] - ret.z*pixel_dim[0]*pixel_dim[1];
	return ret;
}


struct out_of_vol : public thrust::unary_function<uint,bool>
{
	uint * d_pixel_dim;
	uint * d_pixel_dim_padding;
    __device__
	bool operator()(const uint& u) const {
    	uint3 index_address = linear_to_index_space(u,d_pixel_dim_padding);
    	bool ret = !((index_address.x<d_pixel_dim[0]) && (index_address.y<d_pixel_dim[1]) && (index_address.z<d_pixel_dim[2]));
//    	printf("%u %u %u < %u %u %u | %s\n",index_address.x,index_address.y,index_address.z,d_pixel_dim[0],d_pixel_dim[1],d_pixel_dim[2],ret ? "true":"false");
    	return ret;
	}
};

std::map<uint,uint> Labeler::label_occurrence(Density *const& density,uint *const& d_labels,const uint & log_2_dim,
		const char * prefix, const bool& to_mrc,const int& gpu_index){
	//replace excessive labels with shit label
	//labels has padding vol
	CudaSafeCall(cudaSetDevice(gpu_index));
	thrust::counting_iterator<uint> linear_addresses(0);

	uint3 pixel_dim_padding = {log_2_dim,log_2_dim,log_2_dim};
	uint3 pixel_dim;
	density->getPixelDim(pixel_dim,gpu_index);
	uint shit_label = vol(pixel_dim_padding);
	uint * d_pixel_dim;
	uint * d_pixel_dim_padding;
	CudaSafeCall(cudaMalloc((void **)&d_pixel_dim,3*sizeof(*d_pixel_dim)));
	CudaSafeCall(cudaMalloc((void **)&d_pixel_dim_padding,3*sizeof(*d_pixel_dim_padding)));
	CudaSafeCall(cudaMemcpy(d_pixel_dim,&pixel_dim,3*sizeof(*d_pixel_dim),cudaMemcpyHostToDevice));
	CudaSafeCall(cudaMemcpy(d_pixel_dim_padding,&pixel_dim_padding,3*sizeof(*d_pixel_dim_padding),cudaMemcpyHostToDevice));

	thrust::device_ptr<uint> d_labels_begin =thrust::device_pointer_cast(&d_labels[0]);
	thrust::device_ptr<uint> d_labels_end =thrust::device_pointer_cast(&d_labels[vol(pixel_dim_padding)]);

	thrust::device_vector<uint> td_labels_copy(vol(pixel_dim_padding));
	thrust::copy(d_labels_begin,d_labels_end,td_labels_copy.begin());
	out_of_vol unary_bound_check_function;
	unary_bound_check_function.d_pixel_dim = d_pixel_dim;
	unary_bound_check_function.d_pixel_dim_padding = d_pixel_dim_padding;
	thrust::replace_if(d_labels_begin,d_labels_end,linear_addresses,unary_bound_check_function,shit_label);

	std::map<uint,uint> occurences;
	thrust::sort(d_labels_begin,d_labels_end);
	int num_bins = vol(pixel_dim_padding);
	thrust::device_vector<uint> td_labels_unique(num_bins);
	thrust::device_vector<uint>::iterator unique_end = thrust::unique_copy(d_labels_begin, d_labels_end, td_labels_unique.begin());
	thrust::host_vector<uint> unique_labels(td_labels_unique.begin(),unique_end);
	int d = thrust::distance(td_labels_unique.begin(), unique_end);
	uint * d_labels_unique = thrust::raw_pointer_cast(&td_labels_unique[0]);
	thrust::device_vector<uint> td_counting(num_bins);
	thrust::upper_bound(d_labels_begin, d_labels_end, td_labels_unique.begin(), td_labels_unique.end(), td_counting.begin());
	td_counting.resize(d);
	uint * d_counting = thrust::raw_pointer_cast(&td_counting[0]);
	uint * h_counting = (uint *) malloc(d*sizeof(*h_counting));
	cudaMemcpy(h_counting,d_counting,d*sizeof(uint),cudaMemcpyDeviceToHost);
	CudaCheckError();
	occurences[unique_labels[0]] = h_counting[0];
	for (int i=1;i<d;i++){
		occurences[unique_labels[i]] = h_counting[i] - h_counting[i-1];
	}

	for (auto const& x : occurences)
	{
	    std::cout << x.first << ':' << x.second << std::endl;
	}
	if (to_mrc){
		for (auto const& x : occurences)
		{
			//0 is background
			if (x.first != 0 && x.first != shit_label){
				density->writeLabelToMrc(prefix,thrust::raw_pointer_cast(&td_labels_copy[0]),x.first,log_2_dim,gpu_index);
			}
		    std::cout << x.first << ':' << x.second << std::endl;
		}
	}

	return occurences;
}

//void Labeler::unique_labels_to_mrc(Density density,uint *const& d_labels,uint log_2_dim,const int& gpu_index){
//	//TODO: uses pointlessly much memory, especially the occurrences, maybe make this less dumb
//	std::map<uint,uint> ret;
//	size_t label_size = log_2_dim*log_2_dim*log_2_dim;
//	thrust::device_ptr<uint> d_labels_begin =thrust::device_pointer_cast(&d_labels[0]);
//	thrust::device_ptr<uint> d_labels_end =thrust::device_pointer_cast(&d_labels[label_size]);
//
//	thrust::device_vector<uint> td_labels_copy(label_size);
//	thrust::copy(d_labels_begin,d_labels_end,td_labels_copy.begin());
//	thrust::sort(d_labels_begin,d_labels_end);
////	cudaDeviceSynchronize();
//	int num_bins = td_labels.back() + 1;
//	thrust::device_vector<uint> td_labels_unique(num_bins);
//	thrust::device_vector<uint>::iterator unique_end = thrust::unique_copy(d_labels_begin, d_labels_end, td_labels_unique.begin());
//	thrust::host_vector<uint> unique_labels(td_labels_unique.begin(),unique_end);
//	int d = thrust::distance(td_labels_unique.begin(), unique_end);
//	uint * d_labels_unique = thrust::raw_pointer_cast(&td_labels_unique[0]);
//	uint * d_labels = thrust::raw_pointer_cast(&td_labels[0]);
//	thrust::device_vector<uint> td_counting(num_bins);
//	thrust::upper_bound(d_labels_begin, d_labels_end, td_labels_unique.begin(), td_labels_unique.end(), td_counting.begin());
//	td_counting.resize(d);
//	uint * d_counting = thrust::raw_pointer_cast(&td_counting[0]);
//	uint * h_counting = (uint *) malloc(d*sizeof(*h_counting));
//	cudaMemcpy(h_counting,d_counting,d*sizeof(uint),cudaMemcpyDeviceToHost);
//	CudaCheckError();
//	//optionally I could replace the map with something thrust internal to enable further processing
//	ret[unique_labels[0]] = h_counting[0];
//
//	for (int i=1;i<d;i++){
//		ret[unique_labels[i]] = h_counting[i] - h_counting[i-1];
//	}
//	uint max_label = 0;//prob background
//	uint max_label_amount =0;
//	for (uint i=0;i<d;i++){
//		if (ret[unique_labels[i]] > max_label_amount){
//			max_label_amount = ret[unique_labels[i]];
//			max_label = unique_labels[i];
//		}
//	}
//
//	uint label;
//	std::string fname_s("test/part_");
//	std::string fname;
//	for (uint i=0;i<d;i++){
//			label = unique_labels[i];
//			if (label != max_label){
//				fname = fname_s + std::to_string(label) + std::string(".mrc");
//				density.to_mrc_write_only(td_labels_copy,label,fname.c_str(),log_2_dim);
//			}
//	}
//}
//
void Labeler::ccl(int * d_seg_data, uint * d_labels,uint log_2_dim,const int & gpu_index){
	cudaSetDevice(gpu_index);
	ccl_kernel<<<1,1>>>(d_seg_data,d_labels,log_2_dim);
	cudaDeviceSynchronize();
	CudaCheckError();
}

#include <cuda_util_include/cutil_math.h>

__global__
void copy_kernel(int * source,int * dest, uint3 source_dim, uint3 dest_dim){
	uint3 src_pxl_idx = {blockDim.x*blockIdx.x + threadIdx.x,
						blockDim.y*blockIdx.y + threadIdx.y,
						blockDim.z*blockIdx.z + threadIdx.z};
	if (src_pxl_idx.x < source_dim.x && src_pxl_idx.y < source_dim.y && src_pxl_idx.z < source_dim.z){
		dest[src_pxl_idx.z*source_dim.x*source_dim.y + src_pxl_idx.y*source_dim.x + src_pxl_idx.x] =
				source[src_pxl_idx.z*dest_dim.x*dest_dim.y + src_pxl_idx.y*dest_dim.x + src_pxl_idx.x];
	}
}

//#ifdef CCL
#include <stdio.h>

int main(int argc, char ** argv)
{

    Labeler labeler;
    Density density;
    int * d_segmentation;
	uint3 pixel_dim;
    uint max_dim;

//#ifndef CLUSTER
    if (argc == 3)
    {
    	std::string mrc_file(argv[1]);
    	TDensity threshold = (TDensity) std::stof(argv[2]);
    	if (file_exists(mrc_file)){
    		density.createInstanceFromMrc(mrc_file.c_str(),0.0f,0);
    		density.getPixelDim(pixel_dim,0);
    		CudaSafeCall(cudaMalloc((void **)&d_segmentation,vol(pixel_dim)*(sizeof(*d_segmentation))));
    		density.createDensityMask(threshold,d_segmentation,0);

   			//determine padding
   			max_dim = std::max(pixel_dim.x,pixel_dim.y);
   			max_dim = std::max(max_dim,pixel_dim.z);
   			uint log_2_dim = ipow(2,(int)(std::log((float) max_dim)/std::log(2))+1);
//    		uint * h_pixel_dim_padding = (uint *) malloc(3*sizeof(*h_pixel_dim_padding));
//    		uint * d_pixel_dim_padding;
//    		CudaSafeCall(cudaMalloc((void **)&d_pixel_dim_padding,sizeof(*d_pixel_dim_padding)*3));
//    		for (int i=0;i<3;i++) h_pixel_dim_padding[i] = log_2_dim;
//    		cudaMemcpy(d_pixel_dim_padding,h_pixel_dim_padding,sizeof(*d_pixel_dim_padding)*3,cudaMemcpyHostToDevice);
    		uint padding_vol = log_2_dim*log_2_dim*log_2_dim;

    		int * d_segmentation_padding;
    		CudaSafeCall(cudaMalloc((void **)&d_segmentation_padding,padding_vol*sizeof(*d_segmentation_padding)));
//    		int * h_segs_padding = (int *) malloc(padding_vol*sizeof(*h_segs_padding));
    		thrust::fill(thrust::device_pointer_cast(&d_segmentation_padding[0]),thrust::device_pointer_cast(&d_segmentation_padding[padding_vol]),0);
    		CudaCheckError();

			uint layer = pixel_dim.x*pixel_dim.y;
			uint layer_padding = log_2_dim*log_2_dim;
			uint bar_padding = log_2_dim;
			uint bar = pixel_dim.x;
			for (uint z = 0; z < pixel_dim.z; z++){
				for (uint y = 0; y < pixel_dim.y; y++){
						cudaMemcpy(d_segmentation_padding + z*layer_padding + y*bar_padding,d_segmentation + z*layer + y*bar,bar*sizeof(*d_segmentation),cudaMemcpyDeviceToDevice);
				}
			}
    		uint * d_labels;
    		CudaSafeCall(cudaMalloc((void **)&d_labels,padding_vol*sizeof(*d_labels)));
			Labeler::ccl(d_segmentation_padding,d_labels,log_2_dim,0);
			Labeler::label_occurrence(&density,d_labels,log_2_dim,"test/label_test",true,0);

//    		Density d;
//    		d.createInstanceFromBounds({100.0f,100.0f,100.0f},1.0f,0,0);
//    		d.setPixelCube({10,10,10},{30,30,30},3.0f,0);
//    		d.setPixelCube({60,10,10},{30,30,30},3.0f,0);
//    		d.setPixelCube({10,60,10},{30,30,30},3.0f,0);
//    		d.setPixelCube({10,10,60},{30,30,30},3.0f,0);
//    		d.setPixelCube({60,60,10},{30,30,30},3.0f,0);
//    		d.setPixelCube({60,10,60},{30,30,30},3.0f,0);
//    		d.setPixelCube({10,60,60},{30,30,30},3.0f,0);
//    		d.setPixelCube({60,60,60},{30,30,30},3.0f,0);
//    		d.setPixelCube({40,20,20},{20,10,10},2.0f,0);
//    		d.setPixelCube({40,20,70},{20,10,10},2.0f,0);
//    		d.setPixelCube({40,70,20},{20,10,10},2.0f,0);
//    		d.setPixelCube({40,70,70},{20,10,10},2.0f,0);
//       	d.setPixelCube({20,40,20},{10,20,10},2.0f,0);
//       	d.setPixelCube({20,40,70},{10,20,10},2.0f,0);
//       	d.setPixelCube({70,40,20},{10,20,10},2.0f,0);
//       	d.setPixelCube({70,40,70},{10,20,10},2.0f,0);
//     		d.setPixelCube({20,20,40},{10,10,20},1.0f,0);
//     		d.setPixelCube({20,70,40},{10,10,20},1.0f,0);
//     		d.setPixelCube({70,20,40},{10,10,20},1.0f,0);
//     		d.setPixelCube({70,70,40},{10,10,20},1.0f,0);
//    		d.toMrc("./ccl_test.mrc",0,0);
    	} else {
    		printf("File %s doesn't exist ...\n",mrc_file.c_str());
    	}

    } else {
    	printf("Calling pattern:\n"
    			"ccl file.mrc threshold");
    }

    return 0;
}

