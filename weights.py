from sympy.physics.quantum.spin import Rotation
from sympy import pi,re,im
import csv
import math
from itertools import product

def cap(a):
	if a>1:
		return 1
	elif a<-1:
		return -1
	else:
		return a

def threeaxisrot(r11,r12,r21,r31,r32):
	return (math.atan2(cap(r31),cap(r32)),math.asin(cap(r21)),math.atan2(cap(r11),cap(r12)))

def quat_to_euler(q):
	return threeaxisrot(2*(q[3]*q[1] + q[0]*q[2]),\
						   q[0]*q[0] + q[3]*q[3] - q[1]*q[1] - q[2]*q[2], \
						   -2*(q[3]*q[2] - q[0]*q[1]),\
						    2*(q[1]*q[2] + q[0]*q[3]),\
						     q[0]*q[0] - q[3]*q[3] - q[1]*q[1] + q[2]*q[2])


def read_weights(fn):
	fh = open(fn)
	content = [line.strip() for line in fh][4:]
	quats = [[float(e) for e in line.split(" ")[:-1] if len(e)>0] for line in content]
	return quats

def build_WignerD(N,eulers):
	ret = dict()
	for i,euler in enumerate(eulers):
		for l in range(0,N+1):
			for m,n in product(range(-l,l+1),range(-l,l+1)):
				ret[(i,l,m,n)] = Rotation.D(l,m,n,euler[0],euler[1],euler[2]).doit()
	return ret

def I1(l_max):
	triplets = []
	for l in range(l_max+1):
		for m,n in product(range(-l,l+1),range(-l,l+1)):
			triplets.append((l,m,n))
	ret = []
	for t in triplets:
		l,m,n = t
		if ((m<=0) and (0 <= n)) or ((m>0) and (0>n)):
			ret.append(t)
	return ret

def I2(l_max):
	triplets = []
	for l in range(l_max+1):
		for m,n in product(range(-l,l+1),range(-l,l+1)):
			triplets.append((l,m,n))
	ret = []
	for t in triplets:
		l,m,n = t
		if ((m <= 0) and (0 < -n)) or ((m > 0) and (0 >= -n)):
			ret.append(t)
	return ret

def A1(D,I1):
	ret = dict()
	for k,v in D.items():
		if (k[1],k[2],k[3]) in I1:
			ret[k] = re(v)
	return ret

def A2(D,I2):
	ret = dict()
	for k,v in D.items():
		if (k[1],k[2],k[3]) in I2:
			ret[k] = -im(v)
	return ret		

def A_w(A1,A2,I1,I2,w):
	v = [0]*(len(A1) + len(A2))
	for j in range(len(v)):
		for i in range(len(w)):
			if j<len(A1):
				v[j] += A1[(i,l,m,n)]

import scipy
# N=1
# A1
# (l,m,n)
# (0,0,0)
# (1,-1,0)
# (1,-1,1)
# (1,0,0)
# (1,0,1)
# (1,1,-1)

# A2 - no l=0 term due to restrictions of index set
# (l,m,n)
# (1,-1,-1)
# (1,0,-1)
# (1,1,0)
# (1,1,1)

N = 1
fn = "./cffk-orientation-dc8bb42/data/c48u1.quat"
quats = read_weights(fn)
eulers = [quat_to_euler(q) for q in quats]
eulers = [(0,0,0),(pi,0,0),(0,pi,0),(pi,pi,0)]
D = build_WignerD(N, eulers)
i1 = I1(N)
i2 = I2(N)
a1 = A1(D,i1)
a2 = A2(D,i2)

# print(i1)
# print(i2)
# print(len(a1),len(a2),len(D))
# print("===A1===")
# for k,v in a1.items():
# 	print(k, v)
# print("===A2===")
# for k,v in a2.items():
# 	print(k, v)
# print("================")
# for k,v in D.items():
# 	print(k, v)

from scipy.sparse import csc_matrix
from scipy.sparse.linalg import lsqr
import numpy as np

A = np.zeros((len(i1)+len(i2),len(eulers)))
for a,(k,v) in enumerate(a1.items()):
	#k is (i,l,m,n)
	A[a%len(i1),k[0]] = v
for a,(k,v) in enumerate(a2.items()):
	#k is (i,l,m,n)
	A[len(i1) + a%len(i2),k[0]] = v

b = np.zeros((A.shape[0],))
b[0] = 1
print(A)
print(b)
A = csc_matrix(A)

x, istop, itn, normr = lsqr(A, b)[:4]
print(x,normr,istop)